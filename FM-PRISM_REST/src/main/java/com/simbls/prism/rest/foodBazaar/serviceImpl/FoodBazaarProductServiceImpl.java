package com.simbls.prism.rest.foodBazaar.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.simbls.prism.rest.dao.foodBazaar.FoodBazaarProductDao;
import com.simbls.prism.rest.foodBazaar.service.FoodBazaarProductService;
import com.simbls.prism.rest.model.industry.Product;

@Service
public class FoodBazaarProductServiceImpl implements FoodBazaarProductService {
	private static final Logger logger = Logger.getLogger(FoodBazaarProductServiceImpl.class);
	@Autowired private FoodBazaarProductDao foodBazaarProductDAO;

	/**
	 * 
	 */
	public Product productRegister(Product productData) {
		logger.info("Initiating insertion into Product Table - Target DB " + productData.getFirmDBName() + " ::: REST ::: Product Service Impl");
		Product insertingProduct = new Product();
		try{
			foodBazaarProductDAO.productRegisterDAO(productData);
			logger.info("Product Successfully Populated ::: REST ::: Product Service Impl");
			insertingProduct = productData;
		} catch (MySQLIntegrityConstraintViolationException  e){
			logger.info("Product - " + insertingProduct.getProductName() + " was not Populated. ::: REST ::: Product Service Impl");
			logger.error("Constraint violation", e);
		} catch (Exception e){
			logger.info("Product - " + insertingProduct.getProductName() + " was not Populated. ::: REST ::: Product Service Impl");
			logger.error("Constraint violation", e);
	    }
		logger.info("Returning Product with ProductName - " + insertingProduct.getProductName() + " ::: REST ::: Product Service Impl");
		return insertingProduct;
	}
	
	/**
	 * 
	 */
	public Product productUpdate(Product productData) {
		logger.info("Initiating updation of Product Table - Target DB " + productData.getFirmDBName() + " ::: REST ::: Product Service Impl");
		Product updatingProduct = new Product();
		try{
			foodBazaarProductDAO.productUpdateDAO(productData);
			logger.info("Product Successfully Updated ::: REST ::: Product Service Impl");
			updatingProduct = productData;
		} catch (MySQLIntegrityConstraintViolationException  e){
			logger.info("Product - " + updatingProduct.getProductName() + " was not Updated. ::: REST ::: Product Service Impl");
			logger.error("Constraint violation", e);
		} catch (Exception e){
			logger.info("Product - " + updatingProduct.getProductName() + " was not Updated. ::: REST ::: Product Service Impl");
			logger.error("Constraint violation", e);
	    }
		logger.info("Returning Product with ProductName - " + updatingProduct.getProductName() + " ::: REST ::: Product Service Impl");
		return updatingProduct;
	}
	
	public List<Product> retrieveProductByParameter(Product productData) {
		logger.info("Initiating DB search to retrieve Product List. Target DB - " + productData.getFirmDBName() + "::: REST ::: FoodBazaarProductService");
		List<Product> retrievedProduct = foodBazaarProductDAO.retrieveProductByParameterDAO(productData);
		logger.info("Successfully obtained - " + retrievedProduct.size() + " registered Products");
		logger.info("Returning to Product Controller ::: REST ::: Product Service IMPL");
		return retrievedProduct;
	}
	
	public Product retrieveProductPurchaseSaleDetails(Product productData){
		logger.info("Initiating DB search to retrieve Product Purchase and Sales Details. Target DB - " + productData.getFirmDBName() + "::: REST ::: FoodBazaarProductService");
		Product retrievedProduct = foodBazaarProductDAO.retrieveProductPurchaseSaleDetailsDAO(productData);
		logger.info("Successfully obtained registered Product Purchase and Sales Details");
		logger.info("Returning to Product Controller ::: REST ::: Product Service IMPL");
		return retrievedProduct;
	}

	
}
