package com.simbls.prism.rest.clothing.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.simbls.prism.rest.clothing.service.ClothingDealerService;
import com.simbls.prism.rest.dao.clothing.ClothingDealerDao;
import com.simbls.prism.rest.model.industry.Dealer;

@Service
public class ClothingDealerServiceImpl implements ClothingDealerService {
	private static final Logger logger = Logger.getLogger(ClothingDealerServiceImpl.class);
	@Autowired private ClothingDealerDao clothingDealerDAO;

	/**
	 * 
	 */
	public Dealer dealerRegister(Dealer dealerData) {
		logger.info("Initiating insertion into Dealer Table - Target DB " + dealerData.getFirmDBName() + " ::: REST ::: Dealer Service Impl");
		Dealer insertingDealer = new Dealer();
		try{
			clothingDealerDAO.dealerRegisterDAO(dealerData);
			logger.info("Dealer with GST - " + dealerData.getDealerGST() + " Successfully Populated ::: REST ::: Dealer Service Impl");
			insertingDealer =  dealerData;
		} catch (MySQLIntegrityConstraintViolationException  e){
			logger.info("Dealer with GST  - " + insertingDealer.getDealerGST() + " was not Populated. ::: REST ::: Dealer Service Impl");
			logger.error("Constraint violation", e);
		} catch (Exception e){
			logger.info("Dealer with GST  - " + insertingDealer.getDealerGST() + " was not Populated. ::: REST ::: Dealer Service Impl");
			logger.error("Constraint violation", e);
	    }
		logger.info("Returning Dealer with Dealer GST - " + insertingDealer.getDealerGST() + " ::: REST ::: Dealer Service Impl");
		return insertingDealer;
	}
	
	/**
	 * 
	 */
	public Dealer dealerUpdate(Dealer dealerData) {
		logger.info("Initiating updation into Dealer Table - Target DB " + dealerData.getFirmDBName() + " ::: REST ::: Dealer Service Impl");
		Dealer updatingDealer = new Dealer();
		try{
			clothingDealerDAO.dealerUpdateDAO(dealerData);
			logger.info("Dealer with GST - " + dealerData.getDealerGST() + " Successfully Updated ::: REST ::: Dealer Service Impl");
			updatingDealer =  dealerData;
		} catch (MySQLIntegrityConstraintViolationException  e){
			logger.info("Dealer with GST  - " + updatingDealer.getDealerGST() + " was not Updated. ::: REST ::: Dealer Service Impl");
			logger.error("Constraint violation", e);
		} catch (Exception e){
			logger.info("Dealer with GST  - " + updatingDealer.getDealerGST() + " was not Updated. ::: REST ::: Dealer Service Impl");
			logger.error("Constraint violation", e);
	    }
		logger.info("Returning Dealer with Dealer GST - " + updatingDealer.getDealerGST() + " ::: REST ::: Dealer Service Impl");
		return updatingDealer;
		
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Dealer> retrieveDealerList(Dealer dealerData) {
		logger.info("Initiating DB search to retrieve Dealer List. Target DB - " + dealerData.getFirmDBName() + "::: REST ::: CeramicsDealerService");
		List<Dealer> registeredDealerList = clothingDealerDAO.retrieveDealerListDAO(dealerData);
		logger.info("Successfully obtained - " + registeredDealerList.size() + " registered Dealers");
		logger.info("Returning to Dealer Controller ::: REST ::: Dealer Service IMPL");
		return registeredDealerList;
	}
	
	/**
	 * 
	 * @return
	 */
	public Dealer retrieveDealerDetails(Dealer dealerData) {
		logger.info("Initiating DB search to retrieve Dealer List. Target DB - " + dealerData.getFirmDBName() + "::: REST ::: CeramicsDealerService");
//		Dealer retrieveDealerDetails = clothingDealerDAO.retrieveDealerDetailsDAO(dealerData);
		logger.info("Successfully obtained Dealer Details");
		logger.info("Returning to Dealer Controller ::: REST ::: Dealer Service IMPL");
		return null;
	}

}
