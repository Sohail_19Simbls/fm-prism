package com.simbls.prism.rest.foodBazaar.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.dao.foodBazaar.FoodBazaarAccountsDao;
import com.simbls.prism.rest.foodBazaar.service.FoodBazaarAccountsService;
import com.simbls.prism.rest.model.industry.Accounts;

@Service
public class FoodBazaarAccountsServiceImpl implements FoodBazaarAccountsService {
	private static final Logger logger = Logger.getLogger(FoodBazaarAccountsServiceImpl.class);
	@Autowired private FoodBazaarAccountsDao foodBazaarAccountsDAO;

	/**
	 * 
	 */
	public List<Accounts> retrieveConsolidatedPurchaseDetails(Accounts accountsData) {
		logger.info("Initiating DB search to retrieve Consolidated Purchase Details. Target DB - " + accountsData.getFirmDBName() + "::: REST ::: FoodBazaarAccountsServiceImpl");
		List<Accounts> retrieveConsolidatedPurchaseDetails = foodBazaarAccountsDAO.retrieveConsolidatedPurchaseDetailsDAO(accountsData);
		logger.info("Successfully obtained - " + retrieveConsolidatedPurchaseDetails.size() + " consolidated Purchase Details");
		logger.info("Returning to Accounts Controller ::: REST ::: FoodBazaarAccountsServiceImpl");
		return retrieveConsolidatedPurchaseDetails;
	}
	
	/**
	 * 
	 */
	public List<Accounts> retrieveConsolidatedSalesDetails(Accounts accountsData) {
		logger.info("Initiating DB search to retrieve Consolidated Sales Details. Target DB - " + accountsData.getFirmDBName() + "::: REST ::: FoodBazaarAccountsServiceImpl");
		List<Accounts> retrieveConsolidatedSalesDetails = foodBazaarAccountsDAO.retrieveConsolidatedSalesDetailsDAO(accountsData);
		logger.info("Successfully obtained - " + retrieveConsolidatedSalesDetails.size() + " consolidated Sales Details");
		logger.info("Returning to Accounts Controller ::: REST ::: FoodBazaarAccountsServiceImpl");
		return retrieveConsolidatedSalesDetails;
	}
	
	

}
