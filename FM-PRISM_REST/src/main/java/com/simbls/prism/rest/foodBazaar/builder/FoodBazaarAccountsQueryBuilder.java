package com.simbls.prism.rest.foodBazaar.builder;

import java.util.Map;

import org.apache.log4j.Logger;

import com.simbls.prism.rest.model.industry.Accounts;

public class FoodBazaarAccountsQueryBuilder {

	private static final Logger logger = Logger.getLogger(FoodBazaarAccountsQueryBuilder.class);
	
	/**
	 * Building a query to retrieve Accounts.
	 * If the Accounts Name is null 
	 * @param parameters
	 * @return
	 * @author Sohail Razvi
	 */

	public String retrieveConsolidatedPurchaseDetailsQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Accounts operations. Captured Object - " + parameters.toString() + ". ::: REST ::: FoodBazaarAccountsQueryBuilder");
		Accounts accountsData = (Accounts) parameters.get("accountsData");
		logger.info("Retrieving Consolidated Purchase Accounts from target DBName - " + accountsData.getFirmDBName().toString());
		String firmDBName = accountsData.getFirmDBName();
		StringBuilder retrievedConsolidatedPurchaseQuery = new StringBuilder();
		retrievedConsolidatedPurchaseQuery.append("SELECT '"
				+ firmDBName + "' firm_DB_name, "
				+ " purchase_consolidated.frm_purchase_bill_consolidated_id purchase_consolidated_id, "
				+ " purchase_consolidated.frm_purchase_dealer_id dealer_id, "
				+ " purchase_consolidated.frm_purchase_invoice_id invoice_id, "
				+ " purchase_consolidated.frm_purchase_date purchase_date, "
				+ " purchase_consolidated.frm_purchase_0 purchase_0, "
				+ " purchase_consolidated.frm_purchase_5 purchase_5, "
				+ " purchase_consolidated.frm_purchase_12 purchase_12, "
				+ " purchase_consolidated.frm_purchase_18 purchase_18, "
				+ " purchase_consolidated.frm_purchase_28 purchase_28, "
				+ " purchase_consolidated.frm_purchase_cgst purchase_CGST, "
				+ " purchase_consolidated.frm_purchase_sgst purchase_SGST, "
				+ " purchase_consolidated.frm_purchase_cess purchase_CESS, "
				+ " purchase_consolidated.frm_purchase_entered_date purchase_entered_date "
				+ " FROM " + accountsData.getFirmDBName() +".frm_purchase_bill_consolidated purchase_consolidated ");
		
		if(accountsData.getPurchaseCurrentMonth() != null) {
			retrievedConsolidatedPurchaseQuery.append(" WHERE frm_purchase_date LIKE '%" + accountsData.getPurchaseCurrentMonth() + "'");
		} else if (accountsData.getPurchaseCurrentMonth() == "" || accountsData.getPurchaseCurrentMonth() == null) {
			retrievedConsolidatedPurchaseQuery.append(" WHERE frm_purchase_date BETWEEN '" + accountsData.getPurchaseStartDate() + "' AND '" + accountsData.getPurchaseEndDate() + "'");
		}

		logger.info("Generated Query - " + retrievedConsolidatedPurchaseQuery.toString() + " ::: REST ::: FoodBazaarAccountsQueryBuilder");
		return retrievedConsolidatedPurchaseQuery.toString();
	}
	
	/**
	 * 
	 * @param parameters
	 * @return
	 */
	public String retrieveConsolidatedSalesDetailsQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Accounts operations. Captured Object - " + parameters.toString() + ". ::: REST ::: FoodBazaarAccountsQueryBuilder");
		Accounts accountsData = (Accounts) parameters.get("accountsData");
		logger.info("Retrieving Consolidated Sales Accounts from target DBName - " + accountsData.getFirmDBName().toString());
		StringBuilder retrievedConsolidatedSalesQuery = new StringBuilder();
		retrievedConsolidatedSalesQuery.append("SELECT "
				+ " sales_consolidated.frm_sales_bill_consolidated_id sales_consolidated_id, "
				+ " sales_consolidated.frm_sales_dealer_category_id sales_dealer_category_id, "
				+ " sales_consolidated.frm_sales_date sales_date, "
				+ " sales_consolidated.frm_sales_bill_end_number sales_bill_end_number, "
				+ " sales_consolidated.frm_sales_0 sales_0, "
				+ " sales_consolidated.frm_sales_5 sales_5, "
				+ " sales_consolidated.frm_sales_12 sales_12, "
				+ " sales_consolidated.frm_sales_18 sales_18, "
				+ " sales_consolidated.frm_sales_28 sales_28, "
				+ " sales_consolidated.frm_sales_cgst sales_CGST, "
				+ " sales_consolidated.frm_sales_sgst sales_SGST, "
				+ " sales_consolidated.frm_sales_cess sales_CESS "
				+ " FROM " + accountsData.getFirmDBName() + ".frm_sales_bill_consolidated sales_consolidated ");
		
		if(accountsData.getSalesCurrentMonth() != null) {
			retrievedConsolidatedSalesQuery.append(" WHERE sales_consolidated.frm_sales_date like '%" + accountsData.getSalesCurrentMonth() + "'");
		} else if (accountsData.getSalesCurrentMonth() == "" || accountsData.getSalesCurrentMonth() == null) {
			retrievedConsolidatedSalesQuery.append(" WHERE sales_consolidated.frm_sales_date BETWEEN '" + accountsData.getSalesStartDate() + "' AND '" + accountsData.getSalesEndDate() + "'");
		}
		
		logger.info("Generated Query - " + retrievedConsolidatedSalesQuery.toString() + " ::: REST ::: FoodBazaarAccountsQueryBuilder");
		return retrievedConsolidatedSalesQuery.toString();
	}
	
	public String retrieveDealerQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Accounts operations. Captured Object - " + parameters.toString() + ". ::: REST ::: FoodBazaarAccountsQueryBuilder");
		long purchaseDealerID =  (Long) parameters.get("purchaseDealerID");
		String firmDBName = (String) parameters.get("firmDBName");
		logger.info("Retrieving Dealer Name Details from target DBName - " + firmDBName.toString());
		StringBuilder selectDealerQuery = new StringBuilder();
		
		selectDealerQuery.append("SELECT * "
				+ " FROM " + firmDBName + ".frm_dealer  "
				+ " WHERE frm_dealer_id = " + purchaseDealerID);
		
		logger.info("Generated Query - " + selectDealerQuery.toString() + " ::: REST ::: FoodBazaarProductQueryBuilder");
		return selectDealerQuery.toString();
	}
	
	public String retrieveInvoiceQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Accounts operations. Captured Object - " + parameters.toString() + ". ::: REST ::: FoodBazaarAccountsQueryBuilder");
		long purchaseInvoiceID =  (Long) parameters.get("purchaseInvoiceID");
		String firmDBName = (String) parameters.get("firmDBName");
		logger.info("Retrieving Invoice Details from target DBName - " + firmDBName.toString());
		StringBuilder selectInvoiceQuery = new StringBuilder();
		
		selectInvoiceQuery.append("SELECT * "
				+ " FROM " + firmDBName + ".frm_invoice "
				+ " WHERE frm_invoice_id = " + purchaseInvoiceID);
		
		logger.info("Generated Query - " + selectInvoiceQuery.toString() + " ::: REST ::: FoodBazaarProductQueryBuilder");
		return selectInvoiceQuery.toString();
	}

}
