package com.simbls.prism.rest.model.industry;

public class Accounts {
	/* ---------------------------------------- */
	/*	Parameters related to Purchase Details	*/
	/* ---------------------------------------- */
	private long consolidatedPurchaseID;
	private long purchaseDealerID;
	private long purchaseInvoiceID;
	private String purchaseDate;
	private String purchaseDealerName;
	private String purchaseDealerGST;
	private String purchase_0_slab;
	private String purchase_5_slab;
	private String purchase_12_slab;
	private String purchase_18_slab;
	private String purchase_28_slab;
	private String purchase_CGST;
	private String purchase_SGST;
	private String purchase_IGST;
	private String purchase_CESS;
	private String purchaseEnteredDate;
	private String purchaseCurrentMonth;
	private String purchaseStartDate;
	private String purchaseEndDate;
	
	/* ---------------------------------------- */
	/*	Parameters related to Sales Details	*/
	/* ---------------------------------------- */
	private long consolidatedSalesID;
	private int salesDealerCategoryID;
	private String salesDate;
	private String salesBillStartNumber;
	private String salesBillEndNumber;
	private String sales_0_slab;
	private String sales_5_slab;
	private String sales_12_slab;
	private String sales_18_slab;
	private String sales_28_slab;
	private String sales_CGST;
	private String sales_SGST;
	private String sales_IGST;
	private String sales_CESS;
	private String salesCurrentMonth;
	private String salesStartDate;
	private String salesEndDate;
	
	private Dealer dealerObject;
	private Invoice invoiceObject;
	
	
	/* ---------------------------------------- */
	/*	Parameters related to specific Industry and Firm	*/
	/* ---------------------------------------- */
	private String industryCategory;
	private String firmDBName;
	private String firmDBUserName;
	private String firmDBPassword;
	
	public long getConsolidatedPurchaseID() {
		return consolidatedPurchaseID;
	}
	public void setConsolidatedPurchaseID(long consolidatedPurchaseID) {
		this.consolidatedPurchaseID = consolidatedPurchaseID;
	}
	public long getPurchaseDealerID() {
		return purchaseDealerID;
	}
	public void setPurchaseDealerID(long purchaseDealerID) {
		this.purchaseDealerID = purchaseDealerID;
	}
	public long getPurchaseInvoiceID() {
		return purchaseInvoiceID;
	}
	public void setPurchaseInvoiceID(long purchaseInvoiceID) {
		this.purchaseInvoiceID = purchaseInvoiceID;
	}
	public String getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public String getPurchaseDealerName() {
		return purchaseDealerName;
	}
	public void setPurchaseDealerName(String purchaseDealerName) {
		this.purchaseDealerName = purchaseDealerName;
	}
	public String getPurchaseDealerGST() {
		return purchaseDealerGST;
	}
	public void setPurchaseDealerGST(String purchaseDealerGST) {
		this.purchaseDealerGST = purchaseDealerGST;
	}
	public String getPurchase_0_slab() {
		return purchase_0_slab;
	}
	public void setPurchase_0_slab(String purchase_0_slab) {
		this.purchase_0_slab = purchase_0_slab;
	}
	public String getPurchase_5_slab() {
		return purchase_5_slab;
	}
	public void setPurchase_5_slab(String purchase_5_slab) {
		this.purchase_5_slab = purchase_5_slab;
	}
	public String getPurchase_12_slab() {
		return purchase_12_slab;
	}
	public void setPurchase_12_slab(String purchase_12_slab) {
		this.purchase_12_slab = purchase_12_slab;
	}
	public String getPurchase_18_slab() {
		return purchase_18_slab;
	}
	public void setPurchase_18_slab(String purchase_18_slab) {
		this.purchase_18_slab = purchase_18_slab;
	}
	public String getPurchase_28_slab() {
		return purchase_28_slab;
	}
	public void setPurchase_28_slab(String purchase_28_slab) {
		this.purchase_28_slab = purchase_28_slab;
	}
	public String getPurchase_CGST() {
		return purchase_CGST;
	}
	public void setPurchase_CGST(String purchase_CGST) {
		this.purchase_CGST = purchase_CGST;
	}
	public String getPurchase_SGST() {
		return purchase_SGST;
	}
	public void setPurchase_SGST(String purchase_SGST) {
		this.purchase_SGST = purchase_SGST;
	}
	public String getPurchase_IGST() {
		return purchase_IGST;
	}
	public void setPurchase_IGST(String purchase_IGST) {
		this.purchase_IGST = purchase_IGST;
	}
	public String getPurchase_CESS() {
		return purchase_CESS;
	}
	public void setPurchase_CESS(String purchase_CESS) {
		this.purchase_CESS = purchase_CESS;
	}
	public String getPurchaseEnteredDate() {
		return purchaseEnteredDate;
	}
	public void setPurchaseEnteredDate(String purchaseEnteredDate) {
		this.purchaseEnteredDate = purchaseEnteredDate;
	}
	public String getPurchaseCurrentMonth() {
		return purchaseCurrentMonth;
	}
	public void setPurchaseCurrentMonth(String purchaseCurrentMonth) {
		this.purchaseCurrentMonth = purchaseCurrentMonth;
	}
	public String getPurchaseStartDate() {
		return purchaseStartDate;
	}
	public void setPurchaseStartDate(String purchaseStartDate) {
		this.purchaseStartDate = purchaseStartDate;
	}
	public String getPurchaseEndDate() {
		return purchaseEndDate;
	}
	public void setPurchaseEndDate(String purchaseEndDate) {
		this.purchaseEndDate = purchaseEndDate;
	}
	public long getConsolidatedSalesID() {
		return consolidatedSalesID;
	}
	public void setConsolidatedSalesID(long consolidatedSalesID) {
		this.consolidatedSalesID = consolidatedSalesID;
	}
	public int getSalesDealerCategoryID() {
		return salesDealerCategoryID;
	}
	public void setSalesDealerCategoryID(int salesDealerCategoryID) {
		this.salesDealerCategoryID = salesDealerCategoryID;
	}
	public String getSalesDate() {
		return salesDate;
	}
	public void setSalesDate(String salesDate) {
		this.salesDate = salesDate;
	}
	public String getSalesBillStartNumber() {
		return salesBillStartNumber;
	}
	public void setSalesBillStartNumber(String salesBillStartNumber) {
		this.salesBillStartNumber = salesBillStartNumber;
	}
	public String getSalesBillEndNumber() {
		return salesBillEndNumber;
	}
	public void setSalesBillEndNumber(String salesBillEndNumber) {
		this.salesBillEndNumber = salesBillEndNumber;
	}
	public String getSales_0_slab() {
		return sales_0_slab;
	}
	public void setSales_0_slab(String sales_0_slab) {
		this.sales_0_slab = sales_0_slab;
	}
	public String getSales_5_slab() {
		return sales_5_slab;
	}
	public void setSales_5_slab(String sales_5_slab) {
		this.sales_5_slab = sales_5_slab;
	}
	public String getSales_12_slab() {
		return sales_12_slab;
	}
	public void setSales_12_slab(String sales_12_slab) {
		this.sales_12_slab = sales_12_slab;
	}
	public String getSales_18_slab() {
		return sales_18_slab;
	}
	public void setSales_18_slab(String sales_18_slab) {
		this.sales_18_slab = sales_18_slab;
	}
	public String getSales_28_slab() {
		return sales_28_slab;
	}
	public void setSales_28_slab(String sales_28_slab) {
		this.sales_28_slab = sales_28_slab;
	}
	public String getSales_CGST() {
		return sales_CGST;
	}
	public void setSales_CGST(String sales_CGST) {
		this.sales_CGST = sales_CGST;
	}
	public String getSales_SGST() {
		return sales_SGST;
	}
	public void setSales_SGST(String sales_SGST) {
		this.sales_SGST = sales_SGST;
	}
	public String getSales_IGST() {
		return sales_IGST;
	}
	public void setSales_IGST(String sales_IGST) {
		this.sales_IGST = sales_IGST;
	}
	public String getSales_CESS() {
		return sales_CESS;
	}
	public void setSales_CESS(String sales_CESS) {
		this.sales_CESS = sales_CESS;
	}
	public String getSalesCurrentMonth() {
		return salesCurrentMonth;
	}
	public void setSalesCurrentMonth(String salesCurrentMonth) {
		this.salesCurrentMonth = salesCurrentMonth;
	}
	public String getSalesStartDate() {
		return salesStartDate;
	}
	public void setSalesStartDate(String salesStartDate) {
		this.salesStartDate = salesStartDate;
	}
	public String getSalesEndDate() {
		return salesEndDate;
	}
	public void setSalesEndDate(String salesEndDate) {
		this.salesEndDate = salesEndDate;
	}
	public Dealer getDealerObject() {
		return dealerObject;
	}
	public void setDealerObject(Dealer dealerObject) {
		this.dealerObject = dealerObject;
	}
	public Invoice getInvoiceObject() {
		return invoiceObject;
	}
	public void setInvoiceObject(Invoice invoiceObject) {
		this.invoiceObject = invoiceObject;
	}
	public String getIndustryCategory() {
		return industryCategory;
	}
	public void setIndustryCategory(String industryCategory) {
		this.industryCategory = industryCategory;
	}
	public String getFirmDBName() {
		return firmDBName;
	}
	public void setFirmDBName(String firmDBName) {
		this.firmDBName = firmDBName;
	}
	public String getFirmDBUserName() {
		return firmDBUserName;
	}
	public void setFirmDBUserName(String firmDBUserName) {
		this.firmDBUserName = firmDBUserName;
	}
	public String getFirmDBPassword() {
		return firmDBPassword;
	}
	public void setFirmDBPassword(String firmDBPassword) {
		this.firmDBPassword = firmDBPassword;
	}
			
}
