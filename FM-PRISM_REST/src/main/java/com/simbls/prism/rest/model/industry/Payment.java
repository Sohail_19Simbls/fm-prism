package com.simbls.prism.rest.model.industry;

import java.util.ArrayList;
import java.util.List;

public class Payment {
	private long paymentID;
	private long invoiceID;
	private long dealerID;
	private String paymentMode;
	private String paymentTransactionNumber;
	private String paymentIssuedDate;
	private String paymentEncashDate;
	private String paymentAmount;
	private String paymentBank;
	private String paymentEnteredBy;
	private String paymentEnteredDate;
	private String paymentUpdatedBy;
	private String paymentUpdatedDate;
	
	/* ------------------------------------- */
	/* Following refer to Payment Details */
	/* ------------------------------------- */
	private String paymentStatus;
	private String dealerName;
	private String invoiceNumber;
	private String invoiceBillingDate;
	private String invoiceDeliveryDate;
	private String invoiceAmount;
	private String invoiceMarketFees;
	private String invoiceTransportationUnloadingFees;
	private String invoicePaymentDueDate;
	private String todaysDate;
	private String billingStartingDate;
	private String billingEndingDate;
	private String paymentDueDate;
	private Payment paymentObject;
	
	private List<Payment> todaysIssuedPaymentDetails = new ArrayList<Payment>();
	private List<Payment> todaysEnCashedPaymentDetails = new ArrayList<Payment>();
	
	private List<Payment> conditionalIssuedPaymentDetails = new ArrayList<Payment>();
	private List<Payment> conditionalEnCashedPaymentDetails = new ArrayList<Payment>();
	
	private String firmDBName;
	private String firmDBUserName;
	private String firmDBPassword;
	
	public long getPaymentID() {
		return paymentID;
	}
	public void setPaymentID(long paymentID) {
		this.paymentID = paymentID;
	}
	public long getInvoiceID() {
		return invoiceID;
	}
	public void setInvoiceID(long invoiceID) {
		this.invoiceID = invoiceID;
	}
	public long getDealerID() {
		return dealerID;
	}
	public void setDealerID(long dealerID) {
		this.dealerID = dealerID;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getPaymentTransactionNumber() {
		return paymentTransactionNumber;
	}
	public void setPaymentTransactionNumber(String paymentTransactionNumber) {
		this.paymentTransactionNumber = paymentTransactionNumber;
	}
	public String getPaymentIssuedDate() {
		return paymentIssuedDate;
	}
	public void setPaymentIssuedDate(String paymentIssuedDate) {
		this.paymentIssuedDate = paymentIssuedDate;
	}
	public String getPaymentEncashDate() {
		return paymentEncashDate;
	}
	public void setPaymentEncashDate(String paymentEncashDate) {
		this.paymentEncashDate = paymentEncashDate;
	}
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getPaymentBank() {
		return paymentBank;
	}
	public void setPaymentBank(String paymentBank) {
		this.paymentBank = paymentBank;
	}
	public String getPaymentEnteredBy() {
		return paymentEnteredBy;
	}
	public void setPaymentEnteredBy(String paymentEnteredBy) {
		this.paymentEnteredBy = paymentEnteredBy;
	}
	public String getPaymentEnteredDate() {
		return paymentEnteredDate;
	}
	public void setPaymentEnteredDate(String paymentEnteredDate) {
		this.paymentEnteredDate = paymentEnteredDate;
	}
	public String getPaymentUpdatedBy() {
		return paymentUpdatedBy;
	}
	public void setPaymentUpdatedBy(String paymentUpdatedBy) {
		this.paymentUpdatedBy = paymentUpdatedBy;
	}
	public String getPaymentUpdatedDate() {
		return paymentUpdatedDate;
	}
	public void setPaymentUpdatedDate(String paymentUpdatedDate) {
		this.paymentUpdatedDate = paymentUpdatedDate;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getInvoiceBillingDate() {
		return invoiceBillingDate;
	}
	public void setInvoiceBillingDate(String invoiceBillingDate) {
		this.invoiceBillingDate = invoiceBillingDate;
	}
	public String getInvoiceDeliveryDate() {
		return invoiceDeliveryDate;
	}
	public void setInvoiceDeliveryDate(String invoiceDeliveryDate) {
		this.invoiceDeliveryDate = invoiceDeliveryDate;
	}
	public String getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(String invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public String getInvoiceMarketFees() {
		return invoiceMarketFees;
	}
	public void setInvoiceMarketFees(String invoiceMarketFees) {
		this.invoiceMarketFees = invoiceMarketFees;
	}
	public String getInvoiceTransportationUnloadingFees() {
		return invoiceTransportationUnloadingFees;
	}
	public void setInvoiceTransportationUnloadingFees(String invoiceTransportationUnloadingFees) {
		this.invoiceTransportationUnloadingFees = invoiceTransportationUnloadingFees;
	}
	public String getInvoicePaymentDueDate() {
		return invoicePaymentDueDate;
	}
	public void setInvoicePaymentDueDate(String invoicePaymentDueDate) {
		this.invoicePaymentDueDate = invoicePaymentDueDate;
	}
	public String getTodaysDate() {
		return todaysDate;
	}
	public void setTodaysDate(String todaysDate) {
		this.todaysDate = todaysDate;
	}
	public String getBillingStartingDate() {
		return billingStartingDate;
	}
	public void setBillingStartingDate(String billingStartingDate) {
		this.billingStartingDate = billingStartingDate;
	}
	public String getBillingEndingDate() {
		return billingEndingDate;
	}
	public void setBillingEndingDate(String billingEndingDate) {
		this.billingEndingDate = billingEndingDate;
	}
	public String getPaymentDueDate() {
		return paymentDueDate;
	}
	public void setPaymentDueDate(String paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	public Payment getPaymentObject() {
		return paymentObject;
	}
	public void setPaymentObject(Payment paymentObject) {
		this.paymentObject = paymentObject;
	}
	public List<Payment> getTodaysIssuedPaymentDetails() {
		return todaysIssuedPaymentDetails;
	}
	public void setTodaysIssuedPaymentDetails(List<Payment> todaysIssuedPaymentDetails) {
		this.todaysIssuedPaymentDetails = todaysIssuedPaymentDetails;
	}
	public List<Payment> getTodaysEnCashedPaymentDetails() {
		return todaysEnCashedPaymentDetails;
	}
	public void setTodaysEnCashedPaymentDetails(List<Payment> todaysEnCashedPaymentDetails) {
		this.todaysEnCashedPaymentDetails = todaysEnCashedPaymentDetails;
	}
	public List<Payment> getConditionalIssuedPaymentDetails() {
		return conditionalIssuedPaymentDetails;
	}
	public void setConditionalIssuedPaymentDetails(List<Payment> conditionalIssuedPaymentDetails) {
		this.conditionalIssuedPaymentDetails = conditionalIssuedPaymentDetails;
	}
	public List<Payment> getConditionalEnCashedPaymentDetails() {
		return conditionalEnCashedPaymentDetails;
	}
	public void setConditionalEnCashedPaymentDetails(List<Payment> conditionalEnCashedPaymentDetails) {
		this.conditionalEnCashedPaymentDetails = conditionalEnCashedPaymentDetails;
	}
	public String getFirmDBName() {
		return firmDBName;
	}
	public void setFirmDBName(String firmDBName) {
		this.firmDBName = firmDBName;
	}
	public String getFirmDBUserName() {
		return firmDBUserName;
	}
	public void setFirmDBUserName(String firmDBUserName) {
		this.firmDBUserName = firmDBUserName;
	}
	public String getFirmDBPassword() {
		return firmDBPassword;
	}
	public void setFirmDBPassword(String firmDBPassword) {
		this.firmDBPassword = firmDBPassword;
	}		
}
