package com.simbls.prism.rest.foodBazaar.service;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.industry.Payment;

@Component
public interface FoodBazaarPaymentService {
	/**
	 * Register Payment details 
	 * @param payment
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public void paymentRegister(Payment payment) throws Exception;
	
	
	/**
	 * Retrieve List of Payments For Today
	 * @return
	 * @author Sohail Razvi
	 */
	public Payment retrieveTodaysPaymentDetails(Payment paymentData);
	/**
	 * Retrieve List of Registered Payments
	 * @return
	 * @author Sohail Razvi
	 */
	public Payment retrieveConditionalPaymentDetails(Payment paymentData);
}
