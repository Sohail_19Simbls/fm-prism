package com.simbls.prism.rest.foodBazaar.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.dao.foodBazaar.FoodBazaarProductDao;
import com.simbls.prism.rest.foodBazaar.service.FoodBazaarStockService;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Service
public class FoodBazaarStockServiceImpl implements FoodBazaarStockService {
	private static final Logger logger = Logger.getLogger(FoodBazaarStockServiceImpl.class);
	@Autowired private FoodBazaarProductDao foodBazaarProductDAO;

	
	
	public List<SalesBasket> retrieveDetailProductList(SalesBasket salesBasketData) {
		logger.info("Initiating DB search to retrieve Product List. Target DB - " + salesBasketData.getFirmDBName() + "::: REST ::: FoodBazaarProductService");
//		List<SalesBasket> retrievedDetailProductList = foodBazaarProductDAO.retrieveDetailProductListDAO(salesBasketData);
//		logger.info("Successfully obtained - " + retrievedDetailProductList.size() + " registered Products");
		logger.info("Returning to Product Controller ::: REST ::: Product Service IMPL");
//		return retrievedDetailProductList;
		return null;
	}
}
