package com.simbls.prism.rest.ceramics.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.simbls.prism.rest.ceramics.service.CeramicsDealerService;
import com.simbls.prism.rest.dao.ceramics.CeramicsDealerDao;
import com.simbls.prism.rest.model.industry.Dealer;

@Service
public class CeramicsDealerServiceImpl implements CeramicsDealerService {
	private static final Logger logger = Logger.getLogger(CeramicsDealerServiceImpl.class);
	@Autowired private CeramicsDealerDao ceramicsDealerDAO;

	/**
	 * 
	 */
	public Dealer dealerRegister(Dealer dealerData) {
		logger.info("Initiating insertion into Dealer Table - Target DB " + dealerData.getFirmDBName() + " ::: REST ::: CeramicsDealerServiceImpl");
		Dealer insertingDealer = new Dealer();
		try{
			ceramicsDealerDAO.dealerRegisterDAO(dealerData);
			logger.info("Dealer with GST - " + dealerData.getDealerGST() + " Successfully Populated ::: REST ::: CeramicsDealerServiceImpl");
			insertingDealer =  dealerData;
		} catch (MySQLIntegrityConstraintViolationException  e){
			logger.info("Dealer with GST  - " + insertingDealer.getDealerGST() + " was not Populated. ::: REST ::: CeramicsDealerServiceImpl");
			logger.error("Constraint violation", e);
		} catch (Exception e){
			logger.info("Dealer with GST  - " + insertingDealer.getDealerGST() + " was not Populated. ::: REST ::: CeramicsDealerServiceImpl");
			logger.error("Constraint violation", e);
	    }
		logger.info("Returning Dealer with Dealer GST - " + insertingDealer.getDealerGST() + " ::: REST ::: CeramicsDealerServiceImpl");
		return insertingDealer;
	}
	
	/**
	 * 
	 */
	public Dealer dealerUpdate(Dealer dealerData) {
		logger.info("Initiating updation into Dealer Table - Target DB " + dealerData.getFirmDBName() + " ::: REST ::: CeramicsDealerServiceImpl");
		Dealer updatingDealer = new Dealer();
		try{
			ceramicsDealerDAO.dealerUpdateDAO(dealerData);
			logger.info("Dealer with GST - " + dealerData.getDealerGST() + " Successfully Updated ::: REST ::: CeramicsDealerServiceImpl");
			updatingDealer =  dealerData;
		} catch (MySQLIntegrityConstraintViolationException  e){
			logger.info("Dealer with GST  - " + updatingDealer.getDealerGST() + " was not Updated. ::: REST ::: CeramicsDealerServiceImpl");
			logger.error("Constraint violation", e);
		} catch (Exception e){
			logger.info("Dealer with GST  - " + updatingDealer.getDealerGST() + " was not Updated. ::: REST ::: CeramicsDealerServiceImpl");
			logger.error("Constraint violation", e);
	    }
		logger.info("Returning Dealer with Dealer GST - " + updatingDealer.getDealerGST() + " ::: REST ::: CeramicsDealerServiceImpl");
		return updatingDealer;
		
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Dealer> retrieveDealerList(Dealer dealerData) {
		logger.info("Initiating DB search to retrieve Dealer List. Target DB - " + dealerData.getFirmDBName() + "::: REST ::: CeramicsDealerServiceImpl");
		List<Dealer> registeredDealerList = ceramicsDealerDAO.retrieveDealerListDAO(dealerData);
		logger.info("Successfully obtained - " + registeredDealerList.size() + " registered Dealers");
		logger.info("Returning to Dealer Controller ::: REST ::: CeramicsDealerServiceImpl");
		return registeredDealerList;
	}
	
	/**
	 * 
	 * @return
	 */
	public Dealer retrieveDealerDetails(Dealer dealerData) {
		logger.info("Initiating DB search to retrieve Dealer List. Target DB - " + dealerData.getFirmDBName() + "::: REST ::: CeramicsDealerServiceImpl");
//		Dealer retrieveDealerDetails = ceramicsDealerDAO.retrieveDealerDetailsDAO(dealerData);
		logger.info("Successfully obtained Dealer Details");
		logger.info("Returning to Dealer Controller ::: REST ::: CeramicsDealerServiceImpl");
		return null;
	}

}
