package com.simbls.prism.rest.foodBazaar.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.industry.ProductPurchase;

@Component
public interface FoodBazaarProductPurchaseService {
	/**
	 * Register Product Purchase Details
	 * @param productPurchaseData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public ProductPurchase productPurchaseDetailsRegister(ProductPurchase productPurchaseData) throws Exception;

	/**
	 * Update Product Purchase Details
	 * @param productPurchaseData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public ProductPurchase productPurchaseDetailsUpdate(ProductPurchase productPurchaseData) throws Exception;
	
	/**
	 * Update Product Purchase Details
	 * @param productPurchaseData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public void productPurchaseDetailsDelete(ProductPurchase productPurchaseData) throws Exception;
	
	/**
	 * Retrieve all registered Dealers
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<ProductPurchase> retrieveProductPurchaseDetails(ProductPurchase productPurchaseData) throws Exception;
	
	/**
	 * Retrieve all registered Dealers
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public ProductPurchase checkInvoiceStatus(ProductPurchase productPurchaseData) throws Exception;
	
}
