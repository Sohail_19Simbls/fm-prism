package com.simbls.prism.rest.foodBazaar.builder;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.simbls.prism.rest.model.industry.ProductPurchase;
import com.simbls.prism.rest.model.industry.SalesBasket;

public class FoodBazaarProductSalesQueryBuilder {
	private static final Logger logger = Logger.getLogger(FoodBazaarProductSalesQueryBuilder.class);

	/**
	 * 
	 * @param parameters
	 * @return
	 */
	public String productSalesBasketQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Product Sales Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		SalesBasket salesBasketData = (SalesBasket) parameters.get("salesBasketData");
		logger.info("Retrieving ProductID, DealerID and InvoiceID From Purchase Table - Target DBName - " + salesBasketData.getFirmDBName().toString());
		StringBuilder retrieveSaleProductQuery = new StringBuilder();
		retrieveSaleProductQuery.append("SELECT "
				+ " purchase.frm_purchase_product_id product_id,"
				+ " purchase.frm_purchase_dealer_id AS dealer_id,"
				+ " purchase.frm_purchase_invoice_id AS invoice_id,"
				/** ----------------------------------------------------- **/
				+ " product.frm_product_company product_company, "
				+ " product.frm_product_name product_name, "
				+ " product.frm_product_barcode product_barcode, "
				+ " product.frm_product_category_id product_category, "
				+ " product.frm_product_sub_category_id product_sub_category, "
				/** ----------------------------------------------------- **/
				+ " purchase.frm_purchase_mrp product_mrp,"
				+ " purchase.frm_purchase_tax product_purchase_tax,"
				+ " purchase.frm_purchase_cess product_purchase_cess,"
				+ " purchase.frm_purchase_packed_quantity product_packed_quantity, "
				+ " purchase.frm_purchase_unit product_unit, "
				+ " purchase.frm_purchase_unit_cost product_purchase_price, "
				+ " purchase.frm_purchase_paid_qty + frm_purchase_free_qty AS total_stock,"
				+ " purchase.frm_product_threshold_qty product_threshold,"
				+ " purchase.frm_purchase_primary_discount product_primary_discount,"
				+ " purchase.frm_purchase_secondary_discount product_secondary_discount,"
				+ " purchase.frm_purchase_expiry_date product_expiry_date, "
				/** ----------------------------------------------------- **/
				+ " sales.frm_sales_discount product_sales_discount, "
				+ " sales.frm_sales_price product_sales_price, "
				+ " sales.frm_sales_tax product_sales_tax, "
				+ " sales.frm_sales_cess product_sales_cess, "
				+ " sales.frm_sales_sold_quantity product_sold_quantity, "
				/** ----------------------------------------------------- **/ 
				+ " dealer.frm_dealer_category_id dealer_category_id "
				/** ----------------------------------------------------- **/ 
				+ " FROM "
				+ salesBasketData.getFirmDBName() + ".frm_product_purchase purchase "
				+ " JOIN "
				+ salesBasketData.getFirmDBName() + ".frm_product_sales sales "
				+ " ON purchase.frm_purchase_product_id = sales.frm_sales_product_id "
				+ " AND purchase.frm_purchase_dealer_id = sales.frm_sales_dealer_id "
				+ " AND purchase.frm_purchase_invoice_id = sales.frm_sales_invoice_id"
				+ " JOIN "
				+ salesBasketData.getFirmDBName() + ".frm_dealer dealer "
				+ " JOIN "
				+ salesBasketData.getFirmDBName() + ".frm_product product "
				+ " ON "
				+ " product.frm_product_id = sales.frm_sales_product_id "
				+ " AND product.frm_product_id = purchase.frm_purchase_product_id "
				+ " AND  dealer.frm_dealer_id = purchase.frm_purchase_dealer_id "
				+ " AND  sales.frm_sales_rowstate = 0"
				+ " AND  purchase.frm_purchase_rowstate = 0"
				+ " AND (purchase.frm_purchase_paid_qty + purchase.frm_purchase_free_qty) != sales.frm_sales_sold_quantity ");
		if(salesBasketData.getProductBarCode() != null && !salesBasketData.getProductBarCode().isEmpty()){
			retrieveSaleProductQuery.append(" AND product.frm_product_barcode='" + salesBasketData.getProductBarCode() + "'");
		} else if(salesBasketData.getProductName() !=null && !salesBasketData.getProductName().isEmpty()){
			retrieveSaleProductQuery.append(" AND product.frm_product_name like '" + salesBasketData.getProductName() + "%'");
		}
		logger.info("Generated Query - " + retrieveSaleProductQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return retrieveSaleProductQuery.toString();
	}
	

	/**
	 * 
	 * @param parameters
	 * @return
	 */
	public String generateSalesBasketBillQuery(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Product Sales Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		@SuppressWarnings("unchecked")
		List<SalesBasket> salesBillData = (List<SalesBasket>) parameters.get("salesBasketData");
		logger.info("Retrieving Latest Sales Bill - Target DBName - " + salesBillData.get(0).getFirmDBName().toString());
		StringBuilder generateSalesBasketBillQuery = new StringBuilder();
		generateSalesBasketBillQuery.append("INSERT INTO " + salesBillData.get(0).getFirmDBName() + ".frm_sales_bill "
								+ " (frm_sales_bill_sold_by,"
								+ " frm_sales_bill_date,"
								+ " frm_sales_bill_number, "
								+ " frm_sales_dealer_id, "
								+ " frm_sales_invoice_id, "
								+ " frm_sales_product_id, "
								+ " frm_sales_product_quantity, "
								+ " frm_sales_bill_customer_name, "
								+ " frm_sales_bill_customer_contact) "
						+ " VALUES ");
		for(SalesBasket listOfObjects : salesBillData){
			generateSalesBasketBillQuery.append(""
				+ " ("		+ "'" + listOfObjects.getSalesManName() + "', "
							+ "'" + listOfObjects.getSaleBillIssuedDate() + "', "
							+ listOfObjects.getSaleBillNumber() + ", "
							+ listOfObjects.getDealerID() + ", "
							+ listOfObjects.getInvoiceID() + ", "
							+ listOfObjects.getProductID() + ", "
							+ listOfObjects.getProductSoldQuantity() + ", "
							+ "'" + listOfObjects.getCustomerName() + "', "
							+ "'" + listOfObjects.getCustomerContactNumber() + "'), ");
		}
		generateSalesBasketBillQuery = generateSalesBasketBillQuery.deleteCharAt(generateSalesBasketBillQuery.length() - 2);
		logger.info("Generated Query - " + generateSalesBasketBillQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return generateSalesBasketBillQuery.toString();
	}
	
	/**
	 * 
	 * @param parameters
	 * @return
	 */
	public String productSalesConsolidated_UPSERT_Query(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Insertion of Consolidated Product Purchase Details ::: REST ::: FoodBazaarProductPurchaseQueryBuilder");
		@SuppressWarnings("unchecked")
		List<SalesBasket> salesBasketData = (List<SalesBasket>) parameters.get("salesBasketData");
		logger.info("Retrieving Product Purchase Details - Target DBName - " + salesBasketData.get(0).getFirmDBName().toString());
		StringBuilder productSalesConsolidated_UPSERT_Query = new StringBuilder();

		productSalesConsolidated_UPSERT_Query.append("INSERT INTO " + salesBasketData.get(0).getFirmDBName() + ".frm_sales_bill_consolidated"
				+ " (frm_sales_date,"
				+ " frm_sales_dealer_category_id,"
				+ " frm_sales_bill_start_number,"
				+ " frm_sales_bill_end_number,"
				+ " frm_sales_0,"
				+ " frm_sales_5,"
				+ " frm_sales_12,"
				+ " frm_sales_18,"
				+ " frm_sales_28,"
				+ " frm_sales_cgst,"
				+ " frm_sales_sgst,"
				+ " frm_sales_cess)"
		+ " VALUES ");
		
		for(SalesBasket individualSoldObject : salesBasketData){
			if(individualSoldObject.getProductSales_For_0_Tax_Amount() == null){
				individualSoldObject.setProductSales_For_0_Tax_Amount("0"); 
			};
			if(individualSoldObject.getProductSales_For_5_Tax_Amount() == null){
				individualSoldObject.setProductSales_For_5_Tax_Amount("0");
			};
			if(individualSoldObject.getProductSales_For_12_Tax_Amount() == null){
				individualSoldObject.setProductSales_For_12_Tax_Amount("0");
			};
			if(individualSoldObject.getProductSales_For_18_Tax_Amount() == null){
				individualSoldObject.setProductSales_For_18_Tax_Amount("0");
			};
			if(individualSoldObject.getProductSales_For_28_Tax_Amount() == null){
				individualSoldObject.setProductSales_For_28_Tax_Amount("0");
			};
			if(individualSoldObject.getProductSales_Effective_CGST() == null){
				individualSoldObject.setProductSales_Effective_CGST("0");
			};
			if(individualSoldObject.getProductSales_Effective_SGST() == null){
				individualSoldObject.setProductSales_Effective_SGST("0");
			};
			if(individualSoldObject.getProductSales_Effective_CESS() == null){
				individualSoldObject.setProductSales_Effective_CESS("0");
			};
			productSalesConsolidated_UPSERT_Query.append(
					 "('" + individualSoldObject.getSaleBillCompactDate() + "',"
					+ ""  + individualSoldObject.getDealerCategoryID() + ","
					+ "'',"
					+ "'',"
					+ "'" + individualSoldObject.getProductSales_For_0_Tax_Amount() + "',"
					+ "'" + individualSoldObject.getProductSales_For_5_Tax_Amount() + "',"
					+ "'" + individualSoldObject.getProductSales_For_12_Tax_Amount() + "',"
					+ "'" + individualSoldObject.getProductSales_For_18_Tax_Amount() + "',"
					+ "'" + individualSoldObject.getProductSales_For_28_Tax_Amount() + "',"
					+ "'" + individualSoldObject.getProductSales_Effective_CGST() + "',"
					+ "'" + individualSoldObject.getProductSales_Effective_SGST() + "',"
					+ "'" + individualSoldObject.getProductSales_Effective_CESS() + "'), ");
		}
		
		productSalesConsolidated_UPSERT_Query = productSalesConsolidated_UPSERT_Query.deleteCharAt(productSalesConsolidated_UPSERT_Query.length() - 2);
		productSalesConsolidated_UPSERT_Query.append(" ON DUPLICATE KEY UPDATE "
								+ " frm_sales_bill_end_number = frm_sales_bill_end_number + 1, "
								+ " frm_sales_0 = frm_sales_0 + VALUES(frm_sales_0), "
								+ " frm_sales_5 = frm_sales_5 + VALUES(frm_sales_5), "
								+ " frm_sales_12 = frm_sales_12 + VALUES(frm_sales_12), "
								+ " frm_sales_18 = frm_sales_18 + VALUES(frm_sales_18), "
								+ " frm_sales_28 = frm_sales_28 + VALUES(frm_sales_28), "
								+ " frm_sales_cgst = frm_sales_cgst + VALUES(frm_sales_cgst), "
								+ " frm_sales_sgst = frm_sales_sgst + VALUES(frm_sales_sgst), "
								+ " frm_sales_cess = frm_sales_cess + VALUES(frm_sales_cess)");

		logger.info("Generated Query - " + productSalesConsolidated_UPSERT_Query.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return productSalesConsolidated_UPSERT_Query.toString();
	}
	
	/**
	 * 
	 * @param parameters
	 * @return
	 */
	public String updateSoldQuantityQuery(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Updation of Sold Quantity in Product Sales Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		SalesBasket soldObjectData = (SalesBasket) parameters.get("salesBasketData");
		logger.info("Retrieving Latest Sales Bill - Target DBName - " + soldObjectData.getFirmDBName().toString());
		StringBuilder updateSoldQuantityQuery = new StringBuilder();
		updateSoldQuantityQuery.append("UPDATE " + soldObjectData.getFirmDBName() + ".frm_product_sales sales "
				+ " SET sales.frm_sales_sold_quantity =  IF (sales.frm_sales_sold_quantity < (SELECT (purchase.frm_purchase_paid_qty + purchase.frm_purchase_free_qty) AS total_for_invoice "
				+ " FROM " + soldObjectData.getFirmDBName() + ".frm_product_purchase purchase "
				+ " WHERE purchase.frm_purchase_product_id = " + soldObjectData.getProductID()
				+ " AND purchase.frm_purchase_unit_cost = " + soldObjectData.getProductPurchasePrice() 
				+ " ORDER BY purchase.frm_purchase_id LIMIT 1), "
				
				+ " sales.frm_sales_sold_quantity + 0.5 , sales.frm_sales_sold_quantity), "
				
				+ " sales.frm_sales_rowstate =  IF (sales.frm_sales_sold_quantity = (SELECT (purchase.frm_purchase_paid_qty + purchase.frm_purchase_free_qty) AS total_for_invoice "
				+ " FROM " + soldObjectData.getFirmDBName() + ".frm_product_purchase purchase "
				+ " WHERE purchase.frm_purchase_product_id = " + soldObjectData.getProductID()
				+ " AND purchase.frm_purchase_unit_cost = " + soldObjectData.getProductPurchasePrice() 
				+ " ORDER BY purchase.frm_purchase_id LIMIT 1), "
				
				+ " 1 , sales.frm_sales_rowstate) "
				
				+ " WHERE 	sales.frm_sales_product_id = " + soldObjectData.getProductID()
				+ " AND sales.frm_sales_price = " + soldObjectData.getProductSalesPrice() 
				+ " AND sales.frm_sales_rowstate = 0 LIMIT 1");
		logger.info("Generated Query - " + updateSoldQuantityQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return updateSoldQuantityQuery.toString();
	}
	
	/**
	 * 
	 * @param parameters
	 * @return
	 */
	
	public String updatePurchaseRowStateQuery(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Updation of Row State in Product Purchase Details Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		SalesBasket soldObjectData = (SalesBasket) parameters.get("salesBasketData");
		logger.info("Retrieving Latest Sales Bill - Target DBName - " + soldObjectData.getFirmDBName().toString());
		StringBuilder updatePurchaseRowStateQuery = new StringBuilder();
		updatePurchaseRowStateQuery.append(" UPDATE " + soldObjectData.getFirmDBName() + ".frm_product_purchase purchase "
				+ " SET purchase.frm_purchase_rowstate = "
				+ " IF (" 
						+ " (purchase.frm_purchase_paid_qty + purchase.frm_purchase_free_qty) "
							+ " = "
						+ " (SELECT frm_sales_sold_quantity FROM "
												+ soldObjectData.getFirmDBName() + ".frm_product_sales sales "
														+ "WHERE sales.frm_sales_product_id = " + soldObjectData.getProductID()
														+ " AND sales.frm_sales_price = " + soldObjectData.getProductSalesPrice()
														+ " AND sales.frm_sales_rowstate = 1 LIMIT 1) ,"
						+ " 1 , purchase.frm_purchase_rowstate)"
				+ " WHERE purchase.frm_purchase_product_id = " + soldObjectData.getProductID()
				+ " AND purchase.frm_purchase_unit_cost = " + soldObjectData.getProductPurchasePrice()
				+ " AND purchase.frm_purchase_rowstate = 0 LIMIT 1");
		logger.info("Generated Query - " + updatePurchaseRowStateQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return updatePurchaseRowStateQuery.toString();
	}
	
	/**
	 * 
	 * @param parameters
	 * @return
	 */
	
	public String updateSalesRowStateQuery(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Updation of Row State in Product Sales Details Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		SalesBasket soldObjectData = (SalesBasket) parameters.get("salesBasketData");
		logger.info("Retrieving Latest Sales Bill - Target DBName - " + soldObjectData.getFirmDBName().toString());
		StringBuilder updateSalesRowStateQuery = new StringBuilder();
		updateSalesRowStateQuery.append("UPDATE " + soldObjectData.getFirmDBName() + ".frm_product_sales sales "
				+ " SET sales.frm_sales_rowstate =  sales.frm_sales_rowstate + "
				+ " CASE WHEN "
				+ " sales.frm_sales_sold_quantity = "
				+ " (SELECT SUM(purchase.frm_purchase_paid_qty + purchase.frm_purchase_free_qty) AS total_for_invoice "
				+ " FROM " + soldObjectData.getFirmDBName() + ".frm_product_purchase purchase "
				+ " WHERE purchase.frm_purchase_product_id = " + soldObjectData.getProductID()
				+ " AND purchase.frm_purchase_invoice_id = " + soldObjectData.getInvoiceIDArray().get(0) + ") "
				+ " THEN  1 "
				+ " ELSE 0 "
				+ " END "
				+ " WHERE 	sales.frm_sales_product_id = " + soldObjectData.getProductID()
				+ " AND sales.frm_sales_invoice_id = " + soldObjectData.getInvoiceIDArray().get(0));
		logger.info("Generated Query - " + updateSalesRowStateQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return updateSalesRowStateQuery.toString();
	}
	
	/**
	 * 
	 * @param parameters
	 * @return
	 */
	public String getLatestSalesBillQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Product Sales Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		SalesBasket salesBillData = (SalesBasket) parameters.get("salesBasketData");
		logger.info("Retrieving Latest Sales Bill - Target DBName - " + salesBillData.getFirmDBName().toString());
		StringBuilder getLatestSalesBillQuery = new StringBuilder();
		getLatestSalesBillQuery.append("SELECT * FROM "+ salesBillData.getFirmDBName() + ".frm_sales_bill ORDER BY frm_sales_bill_id DESC LIMIT 1");
		logger.info("Generated Query - " + getLatestSalesBillQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return getLatestSalesBillQuery.toString();
	}

	/**
	 * 
	 * @param parameters
	 * @return
	 */
	public String getLatestEntryInSalesDetailsQuery(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Product Sales Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		ProductPurchase productPurchaseData = (ProductPurchase) parameters.get("productPurchaseData");
		logger.info("Retrieving Latest Sales Bill - Target DBName - " + productPurchaseData.getFirmDBName().toString());
		StringBuilder getLatestEntryInSalesDetailsQuery = new StringBuilder();
		getLatestEntryInSalesDetailsQuery.append("SELECT * FROM " + productPurchaseData.getFirmDBName() + ".frm_product_sales WHERE "
								+ "frm_sales_product_id = " + productPurchaseData.getProductID() 
								+ " AND frm_sales_dealer_id = " + productPurchaseData.getDealerID()
								+ " ORDER BY frm_sales_id DESC LIMIT 1");
		logger.info("Generated Query - " + getLatestEntryInSalesDetailsQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return getLatestEntryInSalesDetailsQuery.toString();
	}
	
	public String retrieveCustomisedSalesDetailsQuery(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Todays Product Sales ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		SalesBasket salesBasketData = (SalesBasket) parameters.get("salesBasketData");
		logger.info("Retrieving Todays Sales - Target DBName - " + salesBasketData.getFirmDBName().toString());
		StringBuilder getLatestEntryInSalesDetailsQuery = new StringBuilder();
		getLatestEntryInSalesDetailsQuery.append("SELECT "
										/* --------------------------------------- */
										+ " saleBill.frm_sales_bill_number bill_number, "
										+ " saleBill.frm_sales_bill_sold_by sales_man, "
										+ " saleBill.frm_sales_bill_customer_name customer_name, "
										+ " saleBill.frm_sales_bill_customer_contact customer_number, "
										+ " product.frm_product_company product_company, "
										+ " product.frm_product_name product_name, "
										+ " product.frm_product_packed_quantity packed_quantity, "
										+ " product.frm_product_packed_unit packed_unit, "
										+ " saleBill.frm_sales_product_quantity sold_quantity, "
										+ " sales_details.frm_sales_price sales_price, "
										+ " sales_details.frm_sales_discount sales_discount "
										/* --------------------------------------- */
										+ " FROM " + salesBasketData.getFirmDBName() + ".frm_sales_bill saleBill "
										+ " JOIN " + salesBasketData.getFirmDBName() + ".frm_product product "
										+ " JOIN " + salesBasketData.getFirmDBName() + ".frm_product_purchase purchase_details "
										+ " JOIN " + salesBasketData.getFirmDBName() + ".frm_product_sales sales_details "
										+ " ON saleBill.frm_sales_product_id = product.frm_product_id "
										+ " AND product.frm_product_id = purchase_details.frm_purchase_product_id "
										+ " AND product.frm_product_id = sales_details.frm_sales_product_id "
										+ " AND purchase_details.frm_purchase_product_id = sales_details.frm_sales_product_id ");
		
		if(salesBasketData.getTodaysDate() != null) {
			getLatestEntryInSalesDetailsQuery.append(" WHERE frm_sales_bill_date like '" + salesBasketData.getTodaysDate() + "%' "
										+ " GROUP BY saleBill.frm_sales_bill_id");
		} else if (salesBasketData.getTodaysDate() == "" || salesBasketData.getTodaysDate() == null) {
			getLatestEntryInSalesDetailsQuery.append(" WHERE frm_sales_bill_date BETWEEN "
								+ "'" + salesBasketData.getSalesStartDate() + "' AND "
								+ "'" + salesBasketData.getSalesEndDate() + "' GROUP BY saleBill.frm_sales_bill_id");
		}
										
		logger.info("Generated Query - " + getLatestEntryInSalesDetailsQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return getLatestEntryInSalesDetailsQuery.toString();
	}
	

}
