package com.simbls.prism.rest.clothing.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.dao.clothing.ClothingPaymentDao;
import com.simbls.prism.rest.clothing.service.ClothingPaymentService;
import com.simbls.prism.rest.model.industry.Invoice;
import com.simbls.prism.rest.model.industry.Payment;

@Service
public class ClothingPaymentServiceImpl implements ClothingPaymentService {
	private static final Logger logger = Logger.getLogger(ClothingPaymentServiceImpl.class);
	@Autowired private ClothingPaymentDao clothingPaymentDAO;

	/**
	 * 
	 */
	public void paymentRegister(Payment paymentData) {
		logger.info("Initiating insertion into Payment Table - Target DB " + paymentData.getFirmDBName() + " ::: REST ::: Payment Service Impl");
		clothingPaymentDAO.paymentRegisterDAO(paymentData);
		logger.info("Payment Successfully Populated ::: REST ::: Payment Service Impl");
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	public Payment retrieveTodaysPaymentDetails(Payment paymentData) {
		logger.info("Initiating DB search to retrieve Payment List. Target DB - " + paymentData.getFirmDBName() + "::: REST ::: CeramicsPaymentService");
		Payment returningPaymentObject = new Payment();
		List<Payment> todaysIssuedPaymentDetails = clothingPaymentDAO.retrieveTodaysIssuedPaymentDetailsDAO(paymentData);
		logger.info("Successfully obtained - " + todaysIssuedPaymentDetails.size() + " todays Issued Payments");
		List<Payment> todaysEnCashedPaymentDetails  = clothingPaymentDAO.retrieveTodaysEnCashedPaymentDetailsDAO(paymentData);
		logger.info("Successfully obtained - " + todaysEnCashedPaymentDetails.size() + " todays Encashed Payments");
		returningPaymentObject.setTodaysIssuedPaymentDetails(todaysIssuedPaymentDetails);
		returningPaymentObject.setTodaysEnCashedPaymentDetails(todaysEnCashedPaymentDetails);
		logger.info("Returning to Payment Controller ::: REST ::: Payment Service IMPL");
		return returningPaymentObject;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Invoice> retrievePaymentDetails(Invoice invoiceData) {
		logger.info("Initiating DB search to retrieve Payment List. Target DB - " + invoiceData.getFirmDBName() + "::: REST ::: CeramicsPaymentService");
		/*List<Invoice> registeredPaymentDetailsList = clothingPaymentDAO.retrieveInvoicePaymentDetailsDAO(invoiceData);*/
		/*logger.info("Successfully obtained - " + registeredPaymentDetailsList.size() + " registered Payments");*/
		logger.info("Returning to Payment Controller ::: REST ::: Payment Service IMPL");
		return null;
	}

}
