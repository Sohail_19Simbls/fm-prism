package com.simbls.prism.rest.helper;

import java.sql.Driver;

import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

@Configuration
@MapperScan("com.simbls.prism.rest.dao") 
public class ConnectionInitiator {
	
	public static final String CENTRAL_DB_NAME = "fmprism_central";
	public static final String CENTRAL_USER_NAME = "root";
	public static final String CENTRAL_PASSWORD = "root";
	
	@Value("${firmDBName}")
	private String firmDBName;
	
	@Value("${firmDBUserName}")
	private String firmDBUserName;
	
	@Value("${firmDBPassword}")
	private String firmDBPassword;

	@SuppressWarnings("unchecked")
	@Primary
	@Bean
	public DataSource dataSourcePrimary() throws ClassNotFoundException {
		SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
		dataSource.setDriverClass((Class<? extends Driver>) Class.forName("com.mysql.jdbc.Driver"));
		dataSource.setUsername(CENTRAL_USER_NAME);
		dataSource.setPassword(CENTRAL_PASSWORD);
		dataSource.setUrl("jdbc:mysql://localhost:3306/" + CENTRAL_DB_NAME);
	    return dataSource;
	}
	
//	@Primary
	@Bean(name = "transactionManagerPrimary")
	public DataSourceTransactionManager transactionManager() throws ClassNotFoundException {
	    return new DataSourceTransactionManager(dataSourcePrimary());
	}
	
	@Primary
	@Bean
	public SqlSessionFactoryBean sqlSessionFactory() throws ClassNotFoundException {
		SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
	    sessionFactory.setDataSource(dataSourcePrimary());
	    sessionFactory.setTypeAliasesPackage("com.simbls.prism.rest.model");
	    return sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	@Bean
	public DataSource dataSourceSecondary() throws ClassNotFoundException {
		SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
		dataSource.setDriverClass((Class<? extends Driver>) Class.forName("com.mysql.jdbc.Driver"));
		dataSource.setUsername(firmDBUserName);
		dataSource.setPassword(firmDBPassword);
		dataSource.setUrl("jdbc:mysql://localhost:3306/" + firmDBName);
	    return dataSource;
	}
	
	@Bean(name = "transactionManagerSecondary")
	public DataSourceTransactionManager transactionManagerSecondary() throws ClassNotFoundException {
	    return new DataSourceTransactionManager(dataSourceSecondary());
	}
	
	@Bean
	public SqlSessionFactoryBean sqlSessionFactorySecondary() throws ClassNotFoundException {
		SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
	    sessionFactory.setDataSource(dataSourceSecondary());
	    sessionFactory.setTypeAliasesPackage("com.simbls.prism.rest.model");
	    return sessionFactory;
	}
	
}