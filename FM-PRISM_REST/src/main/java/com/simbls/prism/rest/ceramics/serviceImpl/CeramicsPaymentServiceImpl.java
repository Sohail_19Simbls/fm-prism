package com.simbls.prism.rest.ceramics.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.dao.ceramics.CeramicsPaymentDao;
import com.simbls.prism.rest.ceramics.service.CeramicsPaymentService;
import com.simbls.prism.rest.model.industry.Invoice;
import com.simbls.prism.rest.model.industry.Payment;

@Service
public class CeramicsPaymentServiceImpl implements CeramicsPaymentService {
	private static final Logger logger = Logger.getLogger(CeramicsPaymentServiceImpl.class);
	@Autowired private CeramicsPaymentDao ceramicsPaymentDAO;

	/**
	 * 
	 */
	public void paymentRegister(Payment paymentData) {
		logger.info("Initiating insertion into Payment Table - Target DB " + paymentData.getFirmDBName() + " ::: REST ::: CeramicsPaymentServiceImpl");
		ceramicsPaymentDAO.paymentRegisterDAO(paymentData);
		logger.info("Payment Successfully Populated ::: REST ::: CeramicsPaymentServiceImpl");
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	public Payment retrieveTodaysPaymentDetails(Payment paymentData) {
		logger.info("Initiating DB search to retrieve Payment List. Target DB - " + paymentData.getFirmDBName() + "::: REST ::: CeramicsPaymentService");
		Payment returningPaymentObject = new Payment();
		List<Payment> todaysIssuedPaymentDetails = ceramicsPaymentDAO.retrieveTodaysIssuedPaymentDetailsDAO(paymentData);
		logger.info("Successfully obtained - " + todaysIssuedPaymentDetails.size() + " todays Issued Payments");
		List<Payment> todaysEnCashedPaymentDetails  = ceramicsPaymentDAO.retrieveTodaysEnCashedPaymentDetailsDAO(paymentData);
		logger.info("Successfully obtained - " + todaysEnCashedPaymentDetails.size() + " todays Encashed Payments");
		returningPaymentObject.setTodaysIssuedPaymentDetails(todaysIssuedPaymentDetails);
		returningPaymentObject.setTodaysEnCashedPaymentDetails(todaysEnCashedPaymentDetails);
		logger.info("Returning to Payment Controller ::: REST ::: CeramicsPaymentServiceImpl");
		return returningPaymentObject;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Invoice> retrievePaymentDetails(Invoice invoiceData) {
		logger.info("Initiating DB search to retrieve Payment List. Target DB - " + invoiceData.getFirmDBName() + "::: REST ::: CeramicsPaymentService");
		/*List<Invoice> registeredPaymentDetailsList = ceramicsPaymentDAO.retrieveInvoicePaymentDetailsDAO(invoiceData);*/
		/*logger.info("Successfully obtained - " + registeredPaymentDetailsList.size() + " registered Payments");*/
		logger.info("Returning to Payment Controller ::: REST ::: CeramicsPaymentServiceImpl");
		return null;
	}

}
