package com.simbls.prism.rest.clothing.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.dao.clothing.ClothingInvoiceDao;
import com.simbls.prism.rest.clothing.service.ClothingInvoiceService;
import com.simbls.prism.rest.model.industry.Invoice;

@Service
public class ClothingInvoiceServiceImpl implements ClothingInvoiceService {
	private static final Logger logger = Logger.getLogger(ClothingInvoiceServiceImpl.class);
	@Autowired private ClothingInvoiceDao clothingInvoiceDAO;

	/**
	 * 
	 */
	public void invoiceRegister(Invoice invoiceData) {
		logger.info("Initiating insertion into Invoice Table - Target DB " + invoiceData.getFirmDBName() + " ::: REST ::: Invoice Service Impl");
		clothingInvoiceDAO.invoiceRegisterDAO(invoiceData);
		logger.info("Invoice Successfully Populated ::: REST ::: Invoice Service Impl");
	}
	
	/**
	 * 
	 */
	public void invoiceUpdate(Invoice invoiceData) {
		logger.info("Initiating updation into Invoice Table - Target DB " + invoiceData.getFirmDBName() + " ::: REST ::: Invoice Service Impl");
		clothingInvoiceDAO.invoiceUpdateDAO(invoiceData);
		logger.info("Invoice Successfully Updated ::: REST ::: Invoice Service Impl");
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Invoice> retrieveInvoiceList(Invoice invoiceData) {
		logger.info("Initiating DB search to retrieve Invoice List ::: REST ::: CeramicsInvoiceService");
		List<Invoice> registeredInvoiceList = clothingInvoiceDAO.retrieveInvoiceListDAO(invoiceData);
		logger.info("Successfully obtained - " + registeredInvoiceList.size() + " registered Invoices");
		logger.info("Returning to Invoice Controller ::: REST ::: Invoice Service IMPL");
		return registeredInvoiceList;
	}

	/**
	 * 
	 * @return
	 */
	public List<Invoice> retrieveInvoiceDetails(Invoice invoiceData) {
		logger.info("Initiating DB search to retrieve Products Purchased against Invoice ID ::: REST ::: CeramicsInvoiceService");
		List<Invoice> invoicePurchaseDetails = clothingInvoiceDAO.retrieveInvoiceDetailsDAO(invoiceData);
		logger.info("Successfully obtained - " + invoicePurchaseDetails.size() + " registered Invoices");
		logger.info("Returning to Invoice Controller ::: REST ::: Invoice Service IMPL");
		return invoicePurchaseDetails;
	}
}
