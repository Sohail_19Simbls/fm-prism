package com.simbls.prism.rest.ceramics.builder;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;

import com.simbls.prism.rest.model.industry.Product;
import com.simbls.prism.rest.model.industry.SalesBasket;

public class CeramicsProductQueryBuilder {

	private static final Logger logger = Logger.getLogger(CeramicsProductQueryBuilder.class);
	
	
	/**
	 * Building a query to retrieve Product.
	 * If the Product Name is null 
	 * @param parameters
	 * @return
	 * @author Sohail Razvi
	 */
	
	
	public String retrieveProductByParameterQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Product operations. Captured Object - " + parameters.toString() + ". ::: REST ::: FoodBazaarProductQueryBuilder");
		Product productData = (Product) parameters.get("productData");
		String firmDBName = productData.getFirmDBName();
		logger.info("Retrieving Product target DBName - " + productData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		/**
		 * Creating a condition-less SELECT query.
		 */
		SelectUserQuery.append("SELECT * FROM " + firmDBName + ".frm_product");
		/**
		 * Validation for productName name empty. If ProductName name is not empty, create Query.
		 */
		if(productData.getProductID() != 0){
			SelectUserQuery.append(" WHERE frm_product_id =" + productData.getProductID());
		}
		else if(productData.getProductName() != null && !productData.getProductName().isEmpty()){
			SelectUserQuery.append(" WHERE frm_product_name like '" + productData.getProductName() + "%'");
		} 
		/**
		 * Validation for barCode not empty. If BarCode is not empty, Create Query.
		 */
		else if(productData.getProductBarCode() != null && !productData.getProductBarCode().isEmpty()){
			SelectUserQuery.append(" WHERE frm_product_barcode = '" + productData.getProductBarCode() + "'");
		}
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarProductQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	
	public String retrieveDetailProductListQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Product operations. Captured Object - " + parameters.toString() + ". ::: REST ::: FoodBazaarProductQueryBuilder");
		SalesBasket salesBasketData = (SalesBasket) parameters.get("salesBasketData");
		String firmDBName = salesBasketData.getFirmDBName();
		logger.info("Retrieving Product target DBName - " + salesBasketData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
				
		SelectUserQuery.append("SELECT '"
				+ firmDBName + "' firm_DB_name, "
				+ " product.frm_product_id product_id, "
				+ " product.frm_product_company product_company, "
				+ " product.frm_product_name product_name, "
				+ " product.frm_product_packed_quantity packed_product_quantity, "
				+ " product.frm_product_packed_unit packed_product_unit, "
				+ " product.frm_product_category_id product_category, "
				+ " product.frm_product_sub_category_id product_sub_category, "
				+ " product.frm_product_barcode product_barcode "
				+ " FROM "
				+  firmDBName + ".frm_product product");
		
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarProductQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String retrieveProductPurchaseDetailsQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Product operations. Captured Object - " + parameters.toString() + ". ::: REST ::: FoodBazaarProductQueryBuilder");
		long productID =  (Long) parameters.get("productID");
		String firmDBName = (String) parameters.get("firmDBName");
		logger.info("Retrieving Product Purchase Details from target DBName - " + firmDBName.toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		
		SelectUserQuery.append("SELECT "
				+ " purchase.frm_purchase_mrp product_mrp, "
				+ " purchase.frm_purchase_tax product_tax, "
				+ " purchase.frm_purchase_cess product_cess, "
				+ " purchase.frm_purchase_unit_cost product_unit_cost, "
				+ " SUM(purchase.frm_purchase_paid_qty) product_purchased_quantity, "
				+ " SUM(purchase.frm_purchase_free_qty) free_quantity, "
				+ " AVG(purchase.frm_product_threshold_qty) threshold_quantity, "
				+ " purchase.frm_purchase_primary_discount primary_discount, "
				+ " purchase.frm_purchase_secondary_discount secondary_discount "
				+ " FROM " + firmDBName + ".frm_product_purchase purchase "
				+ " WHERE frm_purchase_product_id = " + productID);
		
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarProductQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String retrieveProductSalesDetailsQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Product operations. Captured Object - " + parameters.toString() + ". ::: REST ::: FoodBazaarProductQueryBuilder");
		long productID =  (Long) parameters.get("productID");
		String firmDBName = (String) parameters.get("firmDBName");
		logger.info("Retrieving Product Sales Details from target DBName - " + firmDBName.toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		
		SelectUserQuery.append("SELECT "
				+ " sales.frm_sales_price product_sales_price, "
				+ " sales.frm_sales_discount sales_discount, "
				+ " sales.frm_sales_tax sales_tax, "
				+ " sales.frm_sales_cess sales_cess, "
				+ " SUM(sales.frm_sales_sold_quantity) product_sold_quantity "
				+ " FROM " + firmDBName + ".frm_product_sales sales "
				+ " WHERE frm_sales_product_id = " + productID);
		
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarProductQueryBuilder");
		return SelectUserQuery.toString();
	}

}
