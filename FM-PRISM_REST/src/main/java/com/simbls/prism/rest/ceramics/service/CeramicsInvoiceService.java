package com.simbls.prism.rest.ceramics.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.industry.Invoice;

@Component
public interface CeramicsInvoiceService {
	/**
	 * Register Invoice details 
	 * @param invoice
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public void invoiceRegister(Invoice invoiceData) throws Exception;
	
	/**
	 * Update Invoice details 
	 * @param invoice
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public void invoiceUpdate(Invoice invoiceData) throws Exception;
	
	/**
	 * Retrieve List of Registered Invoices
	 * @return
	 * @author Sohail Razvi
	 */
	public List<Invoice> retrieveInvoiceList(Invoice invoiceData);
	
	
	/**
	 * 	Retrieve Invoice Details Along with Products Purchased Against a Specific to Invoice
	 * By Invoice ID from Firm DB
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<Invoice> retrieveInvoiceDetails(Invoice invoiceData);
}
