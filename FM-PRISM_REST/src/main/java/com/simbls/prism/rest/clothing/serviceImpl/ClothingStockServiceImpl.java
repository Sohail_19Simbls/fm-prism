package com.simbls.prism.rest.clothing.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.dao.clothing.ClothingProductDao;
import com.simbls.prism.rest.clothing.service.ClothingStockService;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Service
public class ClothingStockServiceImpl implements ClothingStockService {
	private static final Logger logger = Logger.getLogger(ClothingStockServiceImpl.class);
	@Autowired private ClothingProductDao clothingProductDAO;

	
	
	public List<SalesBasket> retrieveDetailProductList(SalesBasket salesBasketData) {
		logger.info("Initiating DB search to retrieve Product List. Target DB - " + salesBasketData.getFirmDBName() + "::: REST ::: CeramicsProductService");
		List<SalesBasket> retrievedDetailProductList = clothingProductDAO.retrieveDetailProductListDAO(salesBasketData);
		logger.info("Successfully obtained - " + retrievedDetailProductList.size() + " registered Products");
		logger.info("Returning to Product Controller ::: REST ::: Product Service IMPL");
		return retrievedDetailProductList;
	}
}
