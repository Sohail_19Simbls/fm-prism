package com.simbls.prism.rest.dao.central;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.simbls.prism.rest.central.builder.FirmQueryBuilder;
import com.simbls.prism.rest.model.central.Firm;
import com.simbls.prism.rest.model.central.Industry;

@Component("userDao")
public interface FirmDao {
	
	@Insert ("INSERT INTO fmprism_central.cnt_firm("
			+ "cnt_firm_name,"
			+ "cnt_firm_abbreviation,"
			+ "cnt_firm_approval_status,"
			+ "cnt_firm_gst_number,"
			+ "cnt_firm_pan_number,"
			+ "cnt_firm_tin_number,"
			+ "cnt_firm_proprietor_name,"
			+ "cnt_firm_category,"
			+ "cnt_firm_primary_contact,"
			+ "cnt_firm_secondary_contact,"
			+ "cnt_firm_shop_number,"
			+ "cnt_firm_address_one,"
			+ "cnt_firm_street,"
			+ "cnt_firm_area,"
			+ "cnt_firm_city,"
			+ "cnt_firm_pincode,"
			+ "cnt_firm_state,"
			+ "cnt_firm_bank_one,"
			+ "cnt_firm_acc_one,"
			+ "cnt_firm_bank_two,"
			+ "cnt_firm_acc_two,"
			+ "cnt_firm_bank_three,"
			+ "cnt_firm_acc_three,"
			+ "cnt_firm_added_by,"
			+ "cnt_firm_added_date,"
			+ "cnt_firm_expiry_date,"
			+ "cnt_firm_db_name,"
			+ "cnt_firm_db_username,"
			+ "cnt_firm_db_password,"
			+ "cnt_firm_rowstate)"
		+ "values("
			+ "#{firmName},"
			+ "#{firmAbbrivation},"
			+ "#{firmApprovalStatus},"
			+ "#{firmGSTNumber},"
			+ "#{firmPANNumber},"
			+ "#{firmTINNumber},"
			+ "#{firmPropriterName},"
			+ "#{firmCategory},"
			+ "#{firmPrimaryContact},"
			+ "#{firmSecondaryContact},"
			+ "#{firmShopNumber},"
			+ "#{firmAddress},"
			+ "#{firmStreet},"
			+ "#{firmArea},"
			+ "#{firmCity},"
			+ "#{firmPincode},"
			+ "#{firmState},"
			+ "#{firmBankOne},"
			+ "#{firmAccountOne},"
			+ "#{firmBankTwo},"
			+ "#{firmAccountTwo},"
			+ "#{firmBankThree},"
			+ "#{firmAccountThree},"
			+ "#{firmAddedBy},"
			+ "#{firmAddedDate},"
			+ "#{firmExpiryDate},"
			+ "#{firmDBName},"
			+ "#{firmDBUserName},"
			+ "#{firmDBPassword},"
			+ "#{firmRowState})")
	public void firmRegisterDAO(Firm firm);
	
	/**
	 * 
	 * @param firmData
	 */
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmDBQuery")
	public void createFirmDBDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmDealerTableQuery")
	public void createFirmDealerTableDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmProductMapTableQuery")
	public void createFirmProductMapTableDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmDiscountTableQuery")
	public void createFirmDiscountTableDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmDiscountMapperTableQuery")
	public void createFirmDiscountMapperTableDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmEmployeeTableQuery")
	public void createFirmEmployeeTableDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmExpenseTableQuery")
	public void createFirmExpenseTableDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmInvoiceTableQuery")
	public void createFirmInvoiceTableDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmPaymentTableQuery")
	public void createFirmPaymentTableDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmProductTableQuery")
	public void createFirmProductTableDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmProductMapperTableQuery")
	public void createFirmProductMapperTableDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmProductPurchaseTableQuery")
	public void createFirmProductPurchaseTableDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmProductSalesTableQuery")
	public void createFirmProductSalesTableDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmPurchaseBillConsolidatedTableQuery")
	public void createFirmPurchaseBillConsolidatedTableDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmSalesBillTableQuery")
	public void createFirmSalesBillTableDAO(@Param("firmData") Firm firmData);
	
	@InsertProvider(type = FirmQueryBuilder.class, method = "createFirmSalesBillConsolidatedTableQuery")
	public void createFirmSalesBillConsolidatedTableDAO(@Param("firmData") Firm firmData);
	
	
	/**
	 * @param firmId
	 * @return
	 */
	@Select("SELECT * FROM fmprism_central.cnt_firm WHERE cnt_firm_id = #{firmId}")
	@Results({ 
		@Result(property = "firmId",					column = "cnt_firm_id"),
		@Result(property = "firmName",					column = "cnt_firm_name"),
		@Result(property = "firmAbbrivation",			column = "cnt_firm_abbreviation"),
		@Result(property = "industryObject",			column = "cnt_industry_category",		one=@One(select="getIndustryCategoryDAO")),
		@Result(property = "firmApprovalStatus",		column = "cnt_firm_approval_status"),
		@Result(property = "firmGSTNumber",				column = "cnt_firm_gst_number"),
		@Result(property = "firmPANNumber",				column = "cnt_firm_pan_number"),
		@Result(property = "firmTINNumber",				column = "cnt_firm_tin_number"),
		@Result(property = "firmPropriterName",			column = "cnt_firm_proprietor_name"),
		@Result(property = "firmCategory",				column = "cnt_firm_category"),
		@Result(property = "firmPrimaryContact",		column = "cnt_firm_primary_contact"),
		@Result(property = "firmSecondaryContact",		column = "cnt_firm_secondary_contact"),
		@Result(property = "firmShopNumber",			column = "cnt_firm_shop_number"),
		@Result(property = "firmAddress",				column = "cnt_firm_address_one"),
		@Result(property = "firmStreet",				column = "cnt_firm_street"),
		@Result(property = "firmArea",					column = "cnt_firm_area"),
		@Result(property = "firmCity",					column = "cnt_firm_city"),
		@Result(property = "firmPincode",				column = "cnt_firm_pincode"),
		@Result(property = "firmState",					column = "cnt_firm_state"),
		@Result(property = "firmBankOne",				column = "cnt_firm_bank_one"),
		@Result(property = "firmAccountOne",			column = "cnt_firm_acc_one"),
		@Result(property = "firmBankTwo",				column = "cnt_firm_bank_two"),
		@Result(property = "firmAccountTwo",			column = "cnt_firm_acc_two"),
		@Result(property = "firmBankThree",				column = "cnt_firm_bank_three"),
		@Result(property = "firmAccountThree",			column = "cnt_firm_acc_three"),
		@Result(property = "firmAddedBy",				column = "cnt_firm_added_by"),
		@Result(property = "firmAddedDate",				column = "cnt_firm_added_date"),
		@Result(property = "firmUpdatedBy",				column = "cnt_firm_updated_by"),
		@Result(property = "firmUpdatedDate",			column = "cnt_firm_updated_date"),
		@Result(property = "firmApprovedBy",			column = "cnt_firm_approved_by"),
		@Result(property = "firmApprovedDate",			column = "cnt_firm_approved_date"),
		@Result(property = "firmExpiryDate",			column = "cnt_firm_expiry_date"),
		@Result(property = "firmDeletedDate",			column = "cnt_firm_deleted_date"),
		@Result(property = "firmDBName",				column = "cnt_firm_db_name"),
		@Result(property = "firmDBUserName",			column = "cnt_firm_db_username"),
		@Result(property = "firmDBPassword",			column = "cnt_firm_db_password"),
		@Result(property = "firmRowState",				column = "cnt_firm_rowstate")
	})
	public Firm getFirmDetailsByIdDAO(@Param("firmId") long firmId);
	
	/**
	 * 
	 * @param industryID
	 * @return
	 */
	@Select("SELECT * FROM fmprism_central.cnt_industry WHERE cnt_industry_id = #{industryID}")
	@Results({ 	
		@Result(property = "industryID",				column = "cnt_industry_id"),
		@Result(property = "industry",					column = "cnt_industry")
	})
	public Industry getIndustryCategoryDAO(int industryID);
	
}
