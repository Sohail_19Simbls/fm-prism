package com.simbls.prism.rest.helper;

import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

public class PasswordEncodeDecodeGen {
	
	private static final Logger logger = Logger.getLogger(PasswordEncodeDecodeGen.class);
	
	public static String getEncodedPassword(String password){
		logger.info("Obtained password - " + password + " ::: REST ::: Password EncodeDecodeGenerator");
		logger.info("Inserting crypting Salt ::: REST ::: Password EncodeDecodeGenerator");
		String encodedPassword = BCrypt.hashpw(password, BCrypt.gensalt(6));
		logger.info("Encoded Password - " + encodedPassword + " ::: REST ::: Password EncodeDecodeGenerator");
		return encodedPassword;
	}
	
	public static Boolean checkPW(String enteredPassword, String dataBasePassword){
		logger.info("Initiating password check ::: REST ::: Password EncodeDecodeGenerator");
		logger.info("Obtained Normal Password - " + enteredPassword + " ::: Obtained Encrypted Password - " + dataBasePassword);
		Boolean result = BCrypt.checkpw(enteredPassword, dataBasePassword);
		logger.info("Password check result - " + result + " ::: Returning from Password EncodeDecodeGenerator");
		return result;
	}

}
