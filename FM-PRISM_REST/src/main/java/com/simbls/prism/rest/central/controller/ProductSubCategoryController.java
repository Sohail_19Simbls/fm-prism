package com.simbls.prism.rest.central.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.simbls.prism.rest.central.service.ProductSubCategoryService;
import com.simbls.prism.rest.model.central.ProductSubCategory;

@Controller
@RequestMapping(value="/productSubCategory")
public class ProductSubCategoryController {
	private static final Logger logger = Logger.getLogger(ProductSubCategoryController.class);
	@Autowired private ProductSubCategoryService productSubCategoryService;
	
	@RequestMapping(value = "/retrieveProductSubCategories", method = RequestMethod.POST)
	public ResponseEntity<List<ProductSubCategory>> retrieveProductSubCategories(@RequestBody ProductSubCategory productSubCategory) throws Exception {
		logger.info("Continuing Retrieval of Product Sub Category from Central DB ::: REST ::: ProductSubCategoryController");
		List<ProductSubCategory> productSubCategoryList = productSubCategoryService.retrieveProductSubCategories(productSubCategory);
		logger.info("Redirecting Product Sub Categories to WEB ::: REST ::: ProductSubCategoryController");
		return ResponseEntity.accepted().body(productSubCategoryList);
	}

}
