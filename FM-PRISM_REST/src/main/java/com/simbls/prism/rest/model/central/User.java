package com.simbls.prism.rest.model.central;

import java.util.Date;

public class User {

	private long userId;
	private long firmId;
	private String userFirstName;
	private String userLastName;
	private String userName;
	private String userPassword;
	private int userFirstLogin;
	private String userContactOne;
	private String userContactTwo;
	private String userHouseNo;
	private String userHouseName;
	private String userStreet;
	private String userLocality;
	private String userCity;
	private String userPincode;
	private String userState;
	private int userCategory;
	private int userParentId;
	private String userAddedBy;
	private Date userAddedDate;
	private String userUpdatedBy;
	private Date userUpdatedDate;
	private int userApprovedState;
	private String userApprovedBy;
	private Date userApprovedDate;
	private Date userDeletedDate;
	private int userRowState;
	
	private User userObject;
	
	private Firm firmObject;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getUserFirmId() {
		return firmId;
	}

	public void setUserFirmId(long firmId) {
		this.firmId = firmId;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public int getUserFirstLogin() {
		return userFirstLogin;
	}

	public void setUserFirstLogin(int userFirstLogin) {
		this.userFirstLogin = userFirstLogin;
	}

	public String getUserContactOne() {
		return userContactOne;
	}

	public void setUserContactOne(String userContactOne) {
		this.userContactOne = userContactOne;
	}

	public String getUserContactTwo() {
		return userContactTwo;
	}

	public void setUserContactTwo(String userContactTwo) {
		this.userContactTwo = userContactTwo;
	}

	public String getUserHouseNo() {
		return userHouseNo;
	}

	public void setUserHouseNo(String userHouseNo) {
		this.userHouseNo = userHouseNo;
	}

	public String getUserHouseName() {
		return userHouseName;
	}

	public void setUserHouseName(String userHouseName) {
		this.userHouseName = userHouseName;
	}

	public String getUserStreet() {
		return userStreet;
	}

	public void setUserStreet(String userStreet) {
		this.userStreet = userStreet;
	}

	public String getUserLocality() {
		return userLocality;
	}

	public void setUserLocality(String userLocality) {
		this.userLocality = userLocality;
	}

	public String getUserCity() {
		return userCity;
	}

	public void setUserCity(String userCity) {
		this.userCity = userCity;
	}

	public String getUserPincode() {
		return userPincode;
	}

	public void setUserPincode(String userPincode) {
		this.userPincode = userPincode;
	}

	public String getUserState() {
		return userState;
	}

	public void setUserState(String userState) {
		this.userState = userState;
	}

	public int getUserCategory() {
		return userCategory;
	}

	public void setUserCategory(int userCategory) {
		this.userCategory = userCategory;
	}

	public int getUserParentId() {
		return userParentId;
	}

	public void setUserParentId(int userParentId) {
		this.userParentId = userParentId;
	}

	public String getUserAddedBy() {
		return userAddedBy;
	}

	public void setUserAddedBy(String userAddedBy) {
		this.userAddedBy = userAddedBy;
	}

	public Date getUserAddedDate() {
		return userAddedDate;
	}

	public void setUserAddedDate(Date userAddedDate) {
		this.userAddedDate = userAddedDate;
	}

	public String getUserUpdatedBy() {
		return userUpdatedBy;
	}

	public void setUserUpdatedBy(String userUpdatedBy) {
		this.userUpdatedBy = userUpdatedBy;
	}

	public Date getUserUpdatedDate() {
		return userUpdatedDate;
	}

	public void setUserUpdatedDate(Date userUpdatedDate) {
		this.userUpdatedDate = userUpdatedDate;
	}

	public int getUserApprovedState() {
		return userApprovedState;
	}

	public void setUserApprovedState(int userApprovedState) {
		this.userApprovedState = userApprovedState;
	}

	public String getUserApprovedBy() {
		return userApprovedBy;
	}

	public void setUserApprovedBy(String userApprovedBy) {
		this.userApprovedBy = userApprovedBy;
	}

	public Date getUserApprovedDate() {
		return userApprovedDate;
	}

	public void setUserApprovedDate(Date userApprovedDate) {
		this.userApprovedDate = userApprovedDate;
	}

	public Date getUserDeletedDate() {
		return userDeletedDate;
	}

	public void setUserDeletedDate(Date userDeletedDate) {
		this.userDeletedDate = userDeletedDate;
	}

	public int getUserRowState() {
		return userRowState;
	}

	public void setUserRowState(int userRowState) {
		this.userRowState = userRowState;
	}

	public User getUserObject() {
		return userObject;
	}

	public void setUserObject(User userObject) {
		this.userObject = userObject;
	}

	public Firm getFirmObject() {
		return firmObject;
	}

	public void setFirmObject(Firm firmObject) {
		this.firmObject = firmObject;
	}
	
}
