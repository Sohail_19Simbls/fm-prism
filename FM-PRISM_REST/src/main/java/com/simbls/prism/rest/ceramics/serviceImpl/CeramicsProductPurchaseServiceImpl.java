package com.simbls.prism.rest.ceramics.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.ceramics.service.CeramicsProductPurchaseService;
import com.simbls.prism.rest.dao.ceramics.CeramicsInvoiceDao;
import com.simbls.prism.rest.dao.ceramics.CeramicsProductPurchaseDao;
import com.simbls.prism.rest.dao.ceramics.CeramicsProductSalesDao;
import com.simbls.prism.rest.model.industry.Invoice;
import com.simbls.prism.rest.model.industry.ProductPurchase;

@Service
public class CeramicsProductPurchaseServiceImpl implements CeramicsProductPurchaseService {
	private static final Logger logger = Logger.getLogger(CeramicsProductPurchaseServiceImpl.class);
	@Autowired private CeramicsProductPurchaseDao ceramicsProductPurchaseDAO;
	@Autowired private CeramicsProductSalesDao ceramicsProductSalesDAO;
	@Autowired private CeramicsInvoiceDao ceramicsInvoiceDAO;
	
	/**
	 * Product Details Register Targets two tables.
	 * First Table	:	frm_product_purchase - Inserts a new Row for each and every entry.
	 * Second Table :	frm_purchase_bill_consolidated - Inserts a new Row for each and every Invoice and keeps updating it.
	 * 					The Second table is used to generate a purchase Tax Invoice for the Accountant Dashboard.
	 * @param productPurchaseData
	 * @author Sohail Razvi
	 * 
	 */
	public List<ProductPurchase> productPurchaseDetailsRegister(List<ProductPurchase> productPurchaseData) throws Exception {
		logger.info("Initiating insertion into Product Purchase Table - Target DB " + productPurchaseData.get(0).getFirmDBName() + " ::: REST ::: Product Purchase Service Impl");
		ceramicsProductPurchaseDAO.productPurchaseDetailsRegisterDAO(productPurchaseData);
		logger.info("Check if user has requested autoupdate of the Sales price ::: REST ::: Product Purchase Service Impl");
		//	Organizing Purchase Data based on Tax
		for(ProductPurchase individualProductPurchaseData : productPurchaseData){
			logger.info("Initiating insertion into Purchase Bill Consolidated Table. Target DB - " + individualProductPurchaseData.getFirmDBName() + " ::: REST ::: Product Purchase Service Impl");
			String tempPercentage =  individualProductPurchaseData.getProductPurchaseTax();
			logger.info("Initiating Check for obtained Percentage - " + tempPercentage + " ::: REST ::: Product Purchase Impl");
			if(tempPercentage.equals("0.00")){
				individualProductPurchaseData.setProductPurchase_For_0_Tax_Amount(individualProductPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 0% being added. Amount - " + individualProductPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
			}else if(tempPercentage.equals("0.05")){
				individualProductPurchaseData.setProductPurchase_For_5_Tax_Amount(individualProductPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 5% being added. Amount - " + individualProductPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
			}else if(tempPercentage.equals("0.12")){
				individualProductPurchaseData.setProductPurchase_For_12_Tax_Amount(individualProductPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 12% being added. Amount - " + individualProductPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
			}else if(tempPercentage.equals("0.18")){
				individualProductPurchaseData.setProductPurchase_For_18_Tax_Amount(individualProductPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 18% being added. Amount - " + individualProductPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
			}else if(tempPercentage.equals("0.28")){
				individualProductPurchaseData.setProductPurchase_For_28_Tax_Amount(individualProductPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 28% being added. Amount - " + individualProductPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
			}
			individualProductPurchaseData.setProductPurchase_Effective_CGST(String.valueOf(Float.parseFloat(individualProductPurchaseData.getProductPurchase_TaxAmount())/2));
			individualProductPurchaseData.setProductPurchase_Effective_SGST(String.valueOf(Float.parseFloat(individualProductPurchaseData.getProductPurchase_TaxAmount())/2));
			ceramicsProductPurchaseDAO.productPurchaseConsolidated_UPSERT_DAO(individualProductPurchaseData);
		}
		
		logger.info("Product Successfully Populated ::: REST ::: Product Purchase Service Impl");
		return productPurchaseData;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<ProductPurchase> retrieveProductPurchaseDetails(ProductPurchase productPurchaseData) {
		logger.info("Initiating DB search to retrieve ProductPurchase List. Target DB - " + productPurchaseData.getFirmDBName() + "::: REST ::: CeramicsProductPurchaseService");
		List<ProductPurchase> registeredProductPurchaseList = ceramicsProductPurchaseDAO.retrieveProductPurchaseDetailsDAO(productPurchaseData);
		logger.info("Successfully obtained - " + registeredProductPurchaseList.size() + " registered ProductPurchases");
		logger.info("Returning to ProductPurchase Controller ::: REST ::: ProductPurchase Service IMPL");
		return registeredProductPurchaseList;
	}
	
	/**
	 * 
	 * @return
	 */
	public ProductPurchase checkInvoiceStatus(ProductPurchase productPurchaseData) {
		logger.info("Initiating DB check to verify ProductPurchase Amount with Invoice Amount. Target DB - " + productPurchaseData.getFirmDBName() + "::: REST ::: CeramicsProductPurchaseService");
		List<ProductPurchase> productPurchaseBasketDetails = ceramicsProductPurchaseDAO.checkInvoiceStatusDAO(productPurchaseData);
		logger.info("Successfully obtained -  registered ProductPurchases");
		logger.info("Initiating calculations to check invoice Status ::: REST ::: CeramicsProductPurchaseService");
		logger.info("Creating a Return Object by name 'invoiceStatus' of Type ProductPurchase and initializing other Variables....");
		ProductPurchase invoiceStatus = new ProductPurchase();
		String invoiceAmount = null;
		String marketFees = null ;
		String unloadingAndTransportationCharges = null;
		double productUnitCost = 0.0;
		double productPurchaseQuantity = 0.0;
		double productFreeQuantity = 0.0;
		double productPurchaseTotalQuantity = 0.0;
		double productTotalCost = 0.0;
		double productPimaryDiscount = 0.0;
		double productSecondaryDiscount = 0.0;
		double productTotalDiscount = 0.0;
		double productTotalDiscountedCost = 0.0;
		double productTax = 0.0;
		double productCess = 0.0;
		double productTotalTax = 0.0; 
		double finalProductCost = 0.0;
		logger.info("Check if the obtained Object comprising of all the Invoice Details is Filled or Empty....");
		if(productPurchaseBasketDetails.isEmpty()){
			logger.info("Empty Object Detected. Initiating invoice details retrival Query...");
			logger.info("Creating invoiceData Object and populating it with DelearID and InvoiceID");
			Invoice invoiceData = new Invoice();
			invoiceData.setDealerID(productPurchaseData.getDealerID());
			invoiceData.setInvoiceID(productPurchaseData.getInvoiceID());
			invoiceData.setFirmDBName(productPurchaseData.getFirmDBName());
			logger.info("Performing search over Database to retrieve Invoice Details of Invoice Number -" + productPurchaseData.getInvoiceNumber() + " ::: REST ::: CeramicsProductPurchaseService");
			List<Invoice> retrivedInvoiceData = ceramicsInvoiceDAO.retrieveInvoiceListDAO(invoiceData);
			logger.info("Assigning the obtained values of InvoiceAmount, Market Fees and Transportation Charges ::: REST ::: CeramicsProductPurchaseService");
			invoiceAmount = retrivedInvoiceData.get(0).getInvoiceAmount();
			marketFees = retrivedInvoiceData.get(0).getInvoiceMarketFees();
			unloadingAndTransportationCharges = retrivedInvoiceData.get(0).getInvoiceTransportationUnloadingFees();
		} else {
			logger.info("Object of size - " + productPurchaseBasketDetails.size() + " detected ::: REST ::: CeramicsProductPurchaseService");
			logger.info("Initiating loop to calculate TotalCost and Final Cost of all the products ::: REST ::: CeramicsProductPurchaseService");
			for(int i=0; i<productPurchaseBasketDetails.size(); i++) {
				logger.info("--------------------------------------------------------------------------------------------------");
				productUnitCost = Double.parseDouble(productPurchaseBasketDetails.get(i).getProductPurchaseUnitCost());
				logger.info("Product Unit Cost - " + productUnitCost);
				productPurchaseQuantity = Double.parseDouble(productPurchaseBasketDetails.get(i).getProductPurchasePaidQuantity());
				logger.info("Product Purchased Quantity - " + productPurchaseQuantity);
				if(productPurchaseBasketDetails.get(i).getProductPurchaseFreeQuantity() != null){
					productFreeQuantity = Double.parseDouble(productPurchaseBasketDetails.get(i).getProductPurchaseFreeQuantity());
					logger.info("Product Free Quantity - " + productFreeQuantity);
				}
				productPurchaseTotalQuantity += (productPurchaseQuantity + productFreeQuantity);
				logger.info("Product Purchased Total Quantity - " + productPurchaseTotalQuantity);
				productTotalCost = productTotalCost + (productUnitCost * productPurchaseQuantity);
				logger.info("Product Total Cost - " + productTotalCost);
				productPimaryDiscount = Double.parseDouble(productPurchaseBasketDetails.get(i).getProductPurchasePrimaryDiscount());
				productSecondaryDiscount = Double.parseDouble(productPurchaseBasketDetails.get(i).getProductPurchaseSecondaryDiscount());
				productTotalDiscount = productPimaryDiscount + productSecondaryDiscount;
				logger.info("Product Total Discount - " + productTotalDiscount);
				logger.info("Initiating Check if the Primary and Secondary Discount are in Percentage or Rupees ::: REST ::: CeramicsProductPurchaseService");
				if(productPimaryDiscount < 1 && productSecondaryDiscount < 1) {
					logger.info("Primary and Secondary Discount are found to be in Percentage ::: REST ::: CeramicsProductPurchaseService");
					productTotalDiscountedCost = (productUnitCost * productPurchaseQuantity) * (1-productTotalDiscount);
					logger.info("Product Total Discounted Cost - " + productTotalDiscountedCost);
				} else {
					logger.info("Primary and Secondary Discount are found to be in Rupees ::: REST ::: CeramicsProductPurchaseService");
					productTotalDiscountedCost = (productUnitCost * productPurchaseQuantity) - productTotalDiscount;
					logger.info("Product Total Discounted Cost - " + productTotalDiscountedCost);
				}
				productTax = Double.parseDouble(productPurchaseBasketDetails.get(i).getProductPurchaseTax());
				productCess = Double.parseDouble(productPurchaseBasketDetails.get(i).getProductPurchaseCess());
				productTotalTax = productTax + productCess;
				logger.info("Total Product Tax - " + productTotalTax);
				finalProductCost = finalProductCost + (productTotalDiscountedCost * (1+ productTotalTax));
				logger.info("Final Product Cost - " + finalProductCost);
				invoiceAmount = productPurchaseBasketDetails.get(i).getInvoiceAmount();
				logger.info("Invoice Amount - " + invoiceAmount);
				marketFees = productPurchaseBasketDetails.get(i).getMarketFee();
				logger.info("Market Fees - " + marketFees);
				unloadingAndTransportationCharges = productPurchaseBasketDetails.get(i).getUnloadingAndTransportationCharges();
				logger.info("Unloading and Transportation Charges - " + unloadingAndTransportationCharges);
				logger.info("--------------------------------------------------------------------------------------------------");
			}
		}
	
		invoiceStatus.setProductTotalCost(productTotalCost);
		invoiceStatus.setProductPurchaseTotalQuantity(String.valueOf(productPurchaseTotalQuantity));
		invoiceStatus.setFinalProductCost(Math.ceil(finalProductCost));
		invoiceStatus.setInvoiceAmount(invoiceAmount);
		invoiceStatus.setMarketFee(marketFees);
		invoiceStatus.setUnloadingAndTransportationCharges(unloadingAndTransportationCharges);
		
		logger.info("Returning to ProductPurchase Controller ::: REST ::: ProductPurchase Service IMPL");
		return invoiceStatus;
	}



}
