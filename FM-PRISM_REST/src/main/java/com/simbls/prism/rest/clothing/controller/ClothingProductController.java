package com.simbls.prism.rest.clothing.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.clothing.service.ClothingProductService;
import com.simbls.prism.rest.model.industry.Product;

@Controller
@RequestMapping(value = "/clothing_product")
public class ClothingProductController {
	private static final Logger logger = Logger.getLogger(ClothingProductController.class);
	@Autowired private ClothingProductService clothingProductService;
	/**
	 * Product Registration
	 * @param productData
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@ResponseBody
	@RequestMapping(value = "/productRegister", method = RequestMethod.POST)
	public Product productRegister(@RequestBody Product productData) throws Exception{
		logger.info("... Continuing Product Registration ::: REST ::: CeramicsProductController");
		Product insertedProduct = clothingProductService.productRegister(productData);
		logger.info("Returning to WEB ::: REST ::: CeramicsProductController");
		return insertedProduct;
	}

	/**
	 * Product Updation
	 * @param productData
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@ResponseBody
	@RequestMapping(value = "/productUpdate", method = RequestMethod.POST)
	public Product productUpdate(@RequestBody Product productData) throws Exception{
		logger.info("... Continuing Product Updation ::: REST ::: CeramicsProductController");
		Product updatedProduct = clothingProductService.productUpdate(productData);
		logger.info("Returning to WEB ::: REST ::: CeramicsProductController");
		return updatedProduct;
	}
	
	/**
	 * Retrieves Single Product by Parameter.
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveProductByParameter", method=RequestMethod.POST)
	public ResponseEntity<List<Product>> retrieveProductByParameter(@RequestBody Product productData){
		logger.info("Continuing retrival of Product list ::: REST ::: CeramicsProductController");
		List<Product> retrievedProduct = clothingProductService.retrieveProductByParameter(productData);
		logger.info("Returning Obtained Product  " + retrievedProduct.size() + " to WEB ::: REST ::: CeramicsProductController");
		return ResponseEntity.accepted().body(retrievedProduct);
	}
	

}
