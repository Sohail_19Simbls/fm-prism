package com.simbls.prism.rest.foodBazaar.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.simbls.prism.rest.dao.foodBazaar.FoodBazaarProductDao;
import com.simbls.prism.rest.dao.foodBazaar.FoodBazaarProductSalesDao;
import com.simbls.prism.rest.foodBazaar.service.FoodBazaarProductSalesService;
import com.simbls.prism.rest.helper.GSTSalesReportGenerator;
import com.simbls.prism.rest.model.industry.Product;
import com.simbls.prism.rest.model.industry.ProductSales;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Service
public class FoodBazaarProductSalesServiceImpl implements FoodBazaarProductSalesService {
	private static final Logger logger = Logger.getLogger(FoodBazaarProductSalesServiceImpl.class);
	@Autowired private FoodBazaarProductSalesDao foodBazaarProductSalesDAO;
	@Autowired private FoodBazaarProductDao foodBazaarProductDAO;
	@Autowired private GSTSalesReportGenerator GSTSalesReportGenerator;
	
	/**
	 * 
	 */
	public List<ProductSales> productSalesRegister(List<ProductSales> productSalesData) throws Exception {
		logger.info("Initiating insertion into Product Sales Table - Target DB " + productSalesData.get(0).getFirmDBName() + " ::: REST ::: Product Sales Service Impl");
		List<ProductSales> returningSalesData = new ArrayList<ProductSales>();
		for(ProductSales individualSalesDetails : productSalesData){
			try{
				foodBazaarProductSalesDAO.productSalesRegisterDAO(individualSalesDetails);
				logger.info("Sale Details Successfully Populated ::: REST ::: Dealer Service Impl");
				returningSalesData.add(individualSalesDetails);
			} catch (MySQLIntegrityConstraintViolationException  e){
				logger.info("Sales Details were not Populated. ::: REST ::: Dealer Service Impl");
				logger.error("Constraint violation", e);
			} catch (Exception e){
				logger.info("Sales Details were not Populated. ::: REST ::: Dealer Service Impl");
				logger.error("Constraint violation", e);
		    }
		}
		logger.info("Product Successfully Populated ::: WEB ::: Product Sales Servie Impl");
		return returningSalesData;
	}

	
	/**
	 * 
	 */
	public ProductSales productSalesDetailsUpdate(ProductSales productSalesData) throws Exception {
		logger.info("Initiating update into Product Sales Table - Target DB " + productSalesData.getFirmDBName() + " ::: REST ::: Product Sales Service Impl");
		foodBazaarProductSalesDAO.productSalesDetailsUpdateDAO(productSalesData);
		logger.info("Product Successfully Populated ::: WEB ::: Product Sales Servie Impl");
		return productSalesData;
	}
	
	/**
	 * The following method works whenever a Product is scanned for selling. The logical explanation is as follows.
	 * 	Any Product that is to be sold at the counter is either searched with name or BarCode.
	 * 	The scanned product is searched from Product Table and product ID obtained.
	 * 	Retrieve latest list of entries from Purchase Details Table and Sales Details Table combined.
	 * 	An Instance variable of type List<SalesBasket> is created in Sales Basket POJO to append the obtained result.
	 * 	Any result obtained either 0, 1 or more than 1 are appended to the incoming object and returned.
	 * 	The query results in either 0 rows, 1 row or more than one.
	 * 	------------------------------------------------------------------------------------------------------------	
	 * 	If 0 rows are returned, then re-obtained the searched product and populate it in the incoming object and return.
	 * 	For 0 rows the Property "objectReturnedFromDB" of POJO SalesBasket remains empty. Check this at front-end.
	 * 	------------------------------------------------------------------------------------------------------------
	 * 	If 1 row is returned then populate the obtained result from DB in the incoming object and send.
	 * 	For 1 row the Property "ProductObject" of POJO SalesBasket remains empty. Check this at front end.
	 * 	------------------------------------------------------------------------------------------------------------
	 * 	If more than 1 row is returned then 
	 * 			Create an ArrayList of invoiceID's
	 * 			Create an ArrayList of M.R.P's
	 * 			Create an ArrayList of Expiry Dates
	 * 	Delete all the redundant elements and set the Arrays into ObjectReturnedFromDB  
	 * 
	 * 	Finally before the object is returned, Obtained the latest Bill Number and increment it by one, update the incoming SalesBasket object and return.
	 * @param salesBaskteData
	 * @return updated salesBsketData
	 * @author Sohail Razvi
	 */
	public SalesBasket productSalesBasket(SalesBasket salesBaskteData) throws Exception {
		logger.info("Initiating retrival of Product Sales Details for Sale Basket. Target DB -" + salesBaskteData.getFirmDBName() + " ::: REST ::: Product Sales Service Impl");
		List<SalesBasket> objectReturnedFromDB  = foodBazaarProductSalesDAO.productSalesBasketDAO(salesBaskteData);
		if(objectReturnedFromDB.isEmpty()){
			logger.info("No Object obtained from DB. Return the Object with Product Object. ::: REST ::: Product Sales Service Impl");
			Product productData = new Product();
			if(salesBaskteData.getProductName() != null && !salesBaskteData.getProductName().isEmpty()){
				productData.setProductName(salesBaskteData.getProductName());
			}else if(salesBaskteData.getProductBarCode() != null && !salesBaskteData.getProductBarCode().isEmpty()){
				productData.setProductBarCode(salesBaskteData.getProductBarCode());
			}
			productData.setFirmDBName(salesBaskteData.getFirmDBName());
			List<Product> retrievedProduct = foodBazaarProductDAO.retrieveProductByParameterDAO(productData);
			if(retrievedProduct.isEmpty()){
				logger.info("The Product searched does not exist. ::: REST::: Product Sales Service Impl");
			} else {
				logger.info("The Product searched is retrieved. ::: REST::: Product Sales Service Impl");
				salesBaskteData.setProductObject(retrievedProduct);
			}
			logger.info("Obtaining Latest Bill Number from sales Bill Table target DB - " + salesBaskteData.getFirmDBName() + " ::: REST ::: Product Sales Service Impl");
			SalesBasket billObject = foodBazaarProductSalesDAO.getLatestSalesBillQuery(salesBaskteData);
			if(billObject == null){
				salesBaskteData.getObjectReturnedFromDB().get(0).setSaleBillNumber("1");
			} else{
				logger.info("Inserting the Bill Number - " + billObject.getSaleBillNumber() + "into returning Object ::: REST ::: Product Sales Service Impl");
				long billNumber = Integer.parseInt(billObject.getSaleBillNumber()) + 1;
				salesBaskteData.setSaleBillNumber(String.valueOf(billNumber));
			}
		} else if(objectReturnedFromDB.size() == 1){
			logger.info("Only one Object obtained from DB ::: REST ::: Product Sales Service Impl");
			salesBaskteData.setObjectReturnedFromDB(objectReturnedFromDB);
			
			ArrayList<Integer> invoiceIDArray = new ArrayList<Integer>();
			ArrayList<String> productMRPArray = new ArrayList<String>();
			ArrayList<String> productExpiryDateArray = new ArrayList<String>();
			for(int i = 0; i < objectReturnedFromDB.size(); i++){
				if(!invoiceIDArray.contains(objectReturnedFromDB.get(i).getInvoiceID())){
					invoiceIDArray.add((int) objectReturnedFromDB.get(i).getInvoiceID());
				}
				if(!productMRPArray.contains(objectReturnedFromDB.get(i).getProductMRP())){
					productMRPArray.add(objectReturnedFromDB.get(i).getProductMRP());
				}
				if(!productExpiryDateArray.contains(objectReturnedFromDB.get(i).getProductExpiryDate())){
					productExpiryDateArray.add(objectReturnedFromDB.get(i).getProductExpiryDate());
				}
			}
			
			for(int i = 0; i < objectReturnedFromDB.size(); i++){
				objectReturnedFromDB.get(i).setInvoiceIDArray(invoiceIDArray);
				objectReturnedFromDB.get(i).setProductMRPArray(productMRPArray);
				objectReturnedFromDB.get(i).setProductExpiryDateArray(productExpiryDateArray);
			}
			
			logger.info("Obtaining Latest Bill Number from sales Bill Table target DB - " + salesBaskteData.getFirmDBName() + " ::: REST ::: Product Sales Service Impl");
			SalesBasket billObject = foodBazaarProductSalesDAO.getLatestSalesBillQuery(salesBaskteData);
			if(billObject == null){
				salesBaskteData.getObjectReturnedFromDB().get(0).setSaleBillNumber("1");
			} else{
				logger.info("Inserting the Bill Number - " + billObject.getSaleBillNumber() + "into returning Object ::: REST ::: Product Sales Service Impl");
				long billNumber = Integer.parseInt(billObject.getSaleBillNumber()) + 1;
				salesBaskteData.getObjectReturnedFromDB().get(0).setSaleBillNumber(String.valueOf(billNumber));
			}
		}else if (objectReturnedFromDB.size() > 1){
			logger.info( objectReturnedFromDB.size() + " Objects obtained from DB ::: REST ::: Product Sales Service Impl");
			logger.info("Condensing the Array into as few Rows as possible based on M.R.P.");
			ArrayList<Integer> invoiceIDArray = new ArrayList<Integer>();
			ArrayList<String> productMRPArray = new ArrayList<String>();
			ArrayList<String> productExpiryDateArray = new ArrayList<String>();
			for(int i = 0; i < objectReturnedFromDB.size(); i++){
				if(!invoiceIDArray.contains(objectReturnedFromDB.get(i).getInvoiceID())){
					invoiceIDArray.add((int) objectReturnedFromDB.get(i).getInvoiceID());
				}
				if(!productMRPArray.contains(objectReturnedFromDB.get(i).getProductMRP())){
					productMRPArray.add(objectReturnedFromDB.get(i).getProductMRP());
				}
				if(!productExpiryDateArray.contains(objectReturnedFromDB.get(i).getProductExpiryDate())){
					productExpiryDateArray.add(objectReturnedFromDB.get(i).getProductExpiryDate());
				}
			}
			
			for(int i = 0; i < objectReturnedFromDB.size(); i++){
				int productQuantityInStock = 0;
				for(int k = objectReturnedFromDB.size() - 1; k > 0; k--){
					if((i != k) && (objectReturnedFromDB.get(i).getProductMRP().equals(objectReturnedFromDB.get(k).getProductMRP()))){
						productQuantityInStock = Integer.parseInt(objectReturnedFromDB.get(i).getProductInStock()) + Integer.parseInt(objectReturnedFromDB.get(k).getProductInStock());
						objectReturnedFromDB.get(i).setProductInStock(String.valueOf(productQuantityInStock));
						objectReturnedFromDB.remove(k);
					}
				}
			}
			for(int i = 0; i < objectReturnedFromDB.size(); i++){
				objectReturnedFromDB.get(i).setInvoiceIDArray(invoiceIDArray);
				objectReturnedFromDB.get(i).setProductMRPArray(productMRPArray);
				objectReturnedFromDB.get(i).setProductExpiryDateArray(productExpiryDateArray);
			}
			
			logger.info("Condenced the Object into last - " + objectReturnedFromDB.size() + " Rows. Returning to FrontEnd ::: REST ::: Product Sales Service Impl");
			logger.info("Obtaining Latest Bill Number from sales Bill Table target DB - " + salesBaskteData.getFirmDBName() + " ::: REST ::: Product Sales Service Impl");
			SalesBasket billObject = foodBazaarProductSalesDAO.getLatestSalesBillQuery(salesBaskteData);
			if(billObject == null){
				salesBaskteData.getObjectReturnedFromDB().get(0).setSaleBillNumber("1");
			} else{
				logger.info("Inserting the Bill Number - " + billObject.getSaleBillNumber() + "into returning Object ::: REST ::: Product Sales Service Impl");
				long billNumber = Integer.parseInt(billObject.getSaleBillNumber()) + 1;
				for(SalesBasket individualObjectReturned : objectReturnedFromDB){
					individualObjectReturned.setSaleBillNumber(String.valueOf(billNumber));
				}
			}
			salesBaskteData.setObjectReturnedFromDB(objectReturnedFromDB);
		}		
		logger.info("Obtained Data from DB. Returning back. ::: WEB ::: Product Sales Servie Impl");
		return salesBaskteData;
	}		

	
	/**
	 * The following method performs 3 major operations
	 * 		Populates the Sales Bill table (frm_sales_bill), with the necessary details.
	 * 		Updates the Product Sales Details Table (frm_product_sales) with the sold quantity.
	 * 		Finally populates the Consolidated Sales Bill Table (frm_sale_bill_consolidated) with the purchase details.
	 * @param salesBasketData
	 * @return
	 */
	public List<SalesBasket> generateSalesBasketBill(List<SalesBasket> salesBasketData) throws Exception {
		logger.info("Initiating insertion into Product Sales Table - Target DB " + salesBasketData.get(0).getFirmDBName() + " ::: REST ::: Product Sales Service Impl");
		foodBazaarProductSalesDAO.generateSalesBasketBillDAO(salesBasketData);
		logger.info("Initiating updation of Sold quantity in Purchase Details Table ::: REST ::: Product Sales Service Impl");
		
		for(SalesBasket soldItemObject : salesBasketData){
			float tempSoldQty = Float.parseFloat(soldItemObject.getProductSoldQuantity()) * 2; 
			for (int i = 0; i < tempSoldQty; i++){
				foodBazaarProductSalesDAO.updateSoldQuantityDAO(soldItemObject);
			}
//			foodBazaarProductSalesDAO.updatePurchaseRowStateDAO(soldItemObject);
//			foodBazaarProductSalesDAO.updateSalesRowStateDAO(soldItemObject);
		}
		logger.info("Initiating insertion into Sales Bill Consolidated Table. Target DB - " + salesBasketData.get(0).getFirmDBName() + " ::: REST ::: Product Purchase Service Impl");
		GSTSalesReportGenerator.salesDetailsRegister(salesBasketData);
		logger.info("Product Successfully Populated ::: REST ::: Product Sales Servie Impl");
		return salesBasketData;
	}
	
	/**
	 * 
	 * @return
	 */
	public SalesBasket retrieveTodaysSalesDetails(SalesBasket salesBasketData) {
		logger.info("Initiating DB search to retrieve todays Sales. Target DB - " + salesBasketData.getFirmDBName() + "::: REST ::: Product Purchase Service Impl");
		SalesBasket returningSalesDetailsObject = new SalesBasket();
		List<SalesBasket> todaysSalesDetails = foodBazaarProductSalesDAO.retrieveCustomisedSalesDetailsDAO(salesBasketData);
		logger.info("Successfully obtained - " + todaysSalesDetails.size() + " todays Issued Payments");
		returningSalesDetailsObject.setTodaysSales(todaysSalesDetails);
		logger.info("Returning to Sales Controller ::: REST ::: Product Purchase Service Impl");
		return returningSalesDetailsObject;
	}
	
	public SalesBasket retrieveCustomisedSalesDetails(SalesBasket salesBasketData) {
		logger.info("Initiating DB search to retrieve Customised Sales. Target DB - " + salesBasketData.getFirmDBName() + "::: REST ::: Product Purchase Service Impl");
		SalesBasket returningSalesDetailsObject = new SalesBasket();
		List<SalesBasket> customisedSalesDetails = foodBazaarProductSalesDAO.retrieveCustomisedSalesDetailsDAO(salesBasketData);
		logger.info("Successfully obtained - " + customisedSalesDetails.size() + " todays Issued Payments");
		returningSalesDetailsObject.setCustomisedSales(customisedSalesDetails);
		logger.info("Returning to Sales Controller ::: REST ::: Product Purchase Service Impl");
		return returningSalesDetailsObject;
	}
}

