package com.simbls.prism.rest.ceramics.builder;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.simbls.prism.rest.model.industry.ProductPurchase;
import com.simbls.prism.rest.model.industry.SalesBasket;

public class CeramicsProductSalesQueryBuilder {
	private static final Logger logger = Logger.getLogger(CeramicsProductSalesQueryBuilder.class);

	
	public String productSalesBasketQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Product Sales Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		SalesBasket salesBasketData = (SalesBasket) parameters.get("salesBasketData");
		logger.info("Retrieving ProductID, DealerID and InvoiceID From Purchase Table - Target DBName - " + salesBasketData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("SELECT "
				+ " purchase.frm_purchase_product_id product_id,"
				+ " purchase.frm_purchase_dealer_id AS dealer_id,"
				+ " purchase.frm_purchase_invoice_id AS invoice_id,"
				/** ----------------------------------------------- **/
				+ " product.frm_product_company product_company, "
				+ " product.frm_product_name product_name, "
				+ " product.frm_product_barcode product_barcode, "
				+ " product.frm_product_category_id product_category, "
				+ " product.frm_product_sub_category_id product_sub_category, "
				/** ----------------------------------------------- **/
				+ " purchase.frm_purchase_mrp product_mrp,"
				+ " purchase.frm_purchase_tax product_purchase_tax,"
				+ " purchase.frm_purchase_cess product_purchase_cess,"
				+ " purchase.frm_purchase_packed_quantity product_packed_quantity, "
				+ " purchase.frm_purchase_unit product_unit, "
				+ " purchase.frm_purchase_unit_cost product_purchase_price, "
				+ " purchase.frm_purchase_paid_qty + frm_purchase_free_qty AS total_stock,"
				+ " purchase.frm_product_threshold_qty product_threshold,"
				+ " purchase.frm_purchase_primary_discount product_primary_discount,"
				+ " purchase.frm_purchase_secondary_discount product_secondary_discount,"
				+ " purchase.frm_purchase_expiry_date product_expiry_date, "
				/** ----------------------------------------------- **/
				+ " sales.frm_sales_discount product_sales_discount, "
				+ " sales.frm_sales_price product_sales_price, "
				+ " sales.frm_sales_tax product_sales_tax, "
				+ " sales.frm_sales_cess product_sales_cess, "
				+ " sales.frm_sales_sold_quantity product_sold_quantity, "
				/** ----------------------------------------------- **/
				+ " dealer.frm_dealer_category_id dealer_category_id "
				/** ----------------------------------------------- **/
				+ " FROM "
				+ salesBasketData.getFirmDBName() + ".frm_product_purchase purchase "
				+ " JOIN "
				+ salesBasketData.getFirmDBName() + ".frm_product_sales sales "
				+ " ON purchase.frm_purchase_product_id = sales.frm_sales_product_id "
				+ " AND purchase.frm_purchase_dealer_id = sales.frm_sales_dealer_id "
				+ " AND purchase.frm_purchase_invoice_id = sales.frm_sales_invoice_id"
				+ " JOIN "
				+ salesBasketData.getFirmDBName() + ".frm_dealer dealer "
				+ " JOIN "
				+ salesBasketData.getFirmDBName() + ".frm_product product "
				+ " ON "
				+ " product.frm_product_id = sales.frm_sales_product_id "
				+ " AND product.frm_product_id = purchase.frm_purchase_product_id "
				+ " AND  dealer.frm_dealer_id = purchase.frm_purchase_dealer_id "
				+ " AND  sales.frm_sales_rowstate = 0"
				+ " AND  purchase.frm_purchase_rowstate = 0"
				+ " AND (purchase.frm_purchase_paid_qty + purchase.frm_purchase_free_qty) != sales.frm_sales_sold_quantity ");
		if(salesBasketData.getProductBarCode() != null && !salesBasketData.getProductBarCode().isEmpty()){
			SelectUserQuery.append(" AND product.frm_product_barcode='" + salesBasketData.getProductBarCode() + "'");
		} else if(salesBasketData.getProductName() !=null && !salesBasketData.getProductName().isEmpty()){
			SelectUserQuery.append(" AND product.frm_product_name like '" + salesBasketData.getProductName() + "%'");
		}
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return SelectUserQuery.toString();
	}
	

	
	public String generateSalesBasketBillQuery(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Product Sales Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		@SuppressWarnings("unchecked")
		List<SalesBasket> salesBillData = (List<SalesBasket>) parameters.get("salesBasketData");
		logger.info("Retrieving Latest Sales Bill - Target DBName - " + salesBillData.get(0).getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("INSERT INTO " + salesBillData.get(0).getFirmDBName() + ".frm_sales_bill "
								+ " (frm_sales_bill_sold_by,"
								+ " frm_sales_bill_date,"
								+ " frm_sales_bill_number, "
								+ " frm_sales_dealer_id, "
								+ " frm_sales_invoice_id, "
								+ " frm_sales_product_id, "
								+ " frm_sales_product_quantity, "
								+ " frm_sales_bill_customer_name, "
								+ " frm_sales_bill_customer_contact) "
						+ " VALUES ");
		for(SalesBasket listOfObjects : salesBillData){
			SelectUserQuery.append(""
				+ " ("		+ "'" + listOfObjects.getSalesManName() + "', "
							+ "'" + listOfObjects.getSaleBillIssuedDate() + "', "
							+ listOfObjects.getSaleBillNumber() + ", "
							+ listOfObjects.getDealerID() + ", "
							+ listOfObjects.getInvoiceID() + ", "
							+ listOfObjects.getProductID() + ", "
							+ listOfObjects.getProductSoldQuantity() + ", "
							+ "'" + listOfObjects.getCustomerName() + "', "
							+ "'" + listOfObjects.getCustomerContactNumber() + "'), ");
		}
		SelectUserQuery = SelectUserQuery.deleteCharAt(SelectUserQuery.length() - 2);
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	
	public String productSalesConsolidated_UPSERT_Query(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Insertion of Consolidated Product Purchase Details ::: REST ::: FoodBazaarProductPurchaseQueryBuilder");
		@SuppressWarnings("unchecked")
		List<SalesBasket> salesBasketData = (List<SalesBasket>) parameters.get("salesBasketData");
		logger.info("Retrieving Product Purchase Details - Target DBName - " + salesBasketData.get(0).getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();

		SelectUserQuery.append("INSERT INTO " + salesBasketData.get(0).getFirmDBName() + ".frm_sales_bill_consolidated"
				+ " (frm_sales_date,"
				+ " frm_sales_dealer_category_id,"
				+ " frm_sales_bill_start_number,"
				+ " frm_sales_bill_end_number,"
				+ " frm_sales_0,"
				+ " frm_sales_5,"
				+ " frm_sales_12,"
				+ " frm_sales_18,"
				+ " frm_sales_28,"
				+ " frm_sales_cgst,"
				+ " frm_sales_sgst,"
				+ " frm_sales_cess)"
		+ " VALUES ");
		
		for(SalesBasket individualSoldObject : salesBasketData){
			if(individualSoldObject.getProductSales_For_0_Tax_Amount() == null){
				individualSoldObject.setProductSales_For_0_Tax_Amount("0"); 
			};
			if(individualSoldObject.getProductSales_For_5_Tax_Amount() == null){
				individualSoldObject.setProductSales_For_5_Tax_Amount("0");
			};
			if(individualSoldObject.getProductSales_For_12_Tax_Amount() == null){
				individualSoldObject.setProductSales_For_12_Tax_Amount("0");
			};
			if(individualSoldObject.getProductSales_For_18_Tax_Amount() == null){
				individualSoldObject.setProductSales_For_18_Tax_Amount("0");
			};
			if(individualSoldObject.getProductSales_For_28_Tax_Amount() == null){
				individualSoldObject.setProductSales_For_28_Tax_Amount("0");
			};
			if(individualSoldObject.getProductSales_Effective_CGST() == null){
				individualSoldObject.setProductSales_Effective_CGST("0");
			};
			if(individualSoldObject.getProductSales_Effective_SGST() == null){
				individualSoldObject.setProductSales_Effective_SGST("0");
			};
			if(individualSoldObject.getProductSales_Effective_CESS() == null){
				individualSoldObject.setProductSales_Effective_CESS("0");
			};
			SelectUserQuery.append(
					 "('" + individualSoldObject.getSaleBillCompactDate() + "',"
					+ ""  + individualSoldObject.getDealerCategoryID() + ","
					+ "'',"
					+ "'',"
					+ "'" + individualSoldObject.getProductSales_For_0_Tax_Amount() + "',"
					+ "'" + individualSoldObject.getProductSales_For_5_Tax_Amount() + "',"
					+ "'" + individualSoldObject.getProductSales_For_12_Tax_Amount() + "',"
					+ "'" + individualSoldObject.getProductSales_For_18_Tax_Amount() + "',"
					+ "'" + individualSoldObject.getProductSales_For_28_Tax_Amount() + "',"
					+ "'" + individualSoldObject.getProductSales_Effective_CGST() + "',"
					+ "'" + individualSoldObject.getProductSales_Effective_SGST() + "',"
					+ "'" + individualSoldObject.getProductSales_Effective_CESS() + "'), ");
		}
		
		SelectUserQuery = SelectUserQuery.deleteCharAt(SelectUserQuery.length() - 2);
		SelectUserQuery.append(" ON DUPLICATE KEY UPDATE "
								+ " frm_sales_bill_end_number = frm_sales_bill_end_number + 1, "
								+ " frm_sales_0 = frm_sales_0 + VALUES(frm_sales_0), "
								+ " frm_sales_5 = frm_sales_5 + VALUES(frm_sales_5), "
								+ " frm_sales_12 = frm_sales_12 + VALUES(frm_sales_12), "
								+ " frm_sales_18 = frm_sales_18 + VALUES(frm_sales_18), "
								+ " frm_sales_28 = frm_sales_28 + VALUES(frm_sales_28), "
								+ " frm_sales_cgst = frm_sales_cgst + VALUES(frm_sales_cgst), "
								+ " frm_sales_sgst = frm_sales_sgst + VALUES(frm_sales_sgst), "
								+ " frm_sales_cess = frm_sales_cess + VALUES(frm_sales_cess)");

		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String updateSoldQuantityQuery(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Updation of Sold Quantity in Product Sales Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		SalesBasket soldObjectData = (SalesBasket) parameters.get("salesBasketData");
		logger.info("Retrieving Latest Sales Bill - Target DBName - " + soldObjectData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append(" UPDATE " + soldObjectData.getFirmDBName() + ".frm_product_sales sales "
						+ " SET sales.frm_sales_sold_quantity =  CASE WHEN "
						+ " sales.frm_sales_sold_quantity < (SELECT SUM(purchase.frm_purchase_paid_qty + purchase.frm_purchase_free_qty) AS total_for_invoice "
						+ " FROM " + soldObjectData.getFirmDBName() + ".frm_product_purchase purchase"
						+ " WHERE purchase.frm_purchase_product_id =  " + soldObjectData.getProductID()
						+ " AND purchase.frm_purchase_invoice_id = " + soldObjectData.getInvoiceIDArray().get(0) + ") "
						+ " THEN sales.frm_sales_sold_quantity + " + soldObjectData.getProductSoldQuantity()
						+ " ELSE sales.frm_sales_sold_quantity END "
						+ " WHERE sales.frm_sales_product_id = " + soldObjectData.getProductID()
						+ " AND sales.frm_sales_invoice_id = " + soldObjectData.getInvoiceIDArray().get(0));
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	/**
	 * 
	 * @param parameters
	 * @return
	 */
	
	public String updatePurchaseRowStateQuery(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Updation of Row State in Product Purchase Details Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		SalesBasket soldObjectData = (SalesBasket) parameters.get("salesBasketData");
		logger.info("Retrieving Latest Sales Bill - Target DBName - " + soldObjectData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append(" UPDATE " + soldObjectData.getFirmDBName() + ".frm_product_purchase purchase "
				+ " SET purchase.frm_purchase_rowstate =  purchase.frm_purchase_rowstate+ "
				+ " CASE WHEN "
				+ " purchase.frm_purchase_paid_qty + purchase.frm_purchase_free_qty  = "
				+ " (SELECT sales.frm_sales_sold_quantity FROM " + soldObjectData.getFirmDBName() + ".frm_product_sales sales "
				+ " WHERE sales.frm_sales_product_id = " + soldObjectData.getProductID()
				+ " AND sales.frm_sales_invoice_id = " + soldObjectData.getInvoiceIDArray().get(0) + ") "
				+ " THEN  1 "
				+ " ELSE 0 "
				+ " END "
				+ " WHERE 	purchase.frm_purchase_product_id = " + soldObjectData.getProductID()
				+ " AND purchase.frm_purchase_invoice_id = " + soldObjectData.getInvoiceIDArray().get(0));
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	/**
	 * 
	 * @param parameters
	 * @return
	 */
	
	public String updateSalesRowStateQuery(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Updation of Row State in Product Sales Details Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		SalesBasket soldObjectData = (SalesBasket) parameters.get("salesBasketData");
		logger.info("Retrieving Latest Sales Bill - Target DBName - " + soldObjectData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("UPDATE " + soldObjectData.getFirmDBName() + ".frm_product_sales sales "
				+ " SET sales.frm_sales_rowstate =  sales.frm_sales_rowstate + "
				+ " CASE WHEN "
				+ " sales.frm_sales_sold_quantity = "
				+ " (SELECT SUM(purchase.frm_purchase_paid_qty + purchase.frm_purchase_free_qty) AS total_for_invoice "
				+ " FROM " + soldObjectData.getFirmDBName() + ".frm_product_purchase purchase "
				+ " WHERE purchase.frm_purchase_product_id = " + soldObjectData.getProductID()
				+ " AND purchase.frm_purchase_invoice_id = " + soldObjectData.getInvoiceIDArray().get(0) + ") "
				+ " THEN  1 "
				+ " ELSE 0 "
				+ " END "
				+ " WHERE 	sales.frm_sales_product_id = " + soldObjectData.getProductID()
				+ " AND sales.frm_sales_invoice_id = " + soldObjectData.getInvoiceIDArray().get(0));
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	
	public String getLatestSalesBillQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Product Sales Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		SalesBasket salesBillData = (SalesBasket) parameters.get("salesBasketData");
		logger.info("Retrieving Latest Sales Bill - Target DBName - " + salesBillData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("SELECT * FROM "+ salesBillData.getFirmDBName() + ".frm_sales_bill ORDER BY frm_sales_bill_id DESC LIMIT 1");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return SelectUserQuery.toString();
	}

	public String getLatestEntryInSalesDetailsQuery(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Product Sales Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		ProductPurchase productPurchaseData = (ProductPurchase) parameters.get("productPurchaseData");
		logger.info("Retrieving Latest Sales Bill - Target DBName - " + productPurchaseData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("SELECT * FROM " + productPurchaseData.getFirmDBName() + ".frm_product_sales WHERE "
								+ "frm_sales_product_id = " + productPurchaseData.getProductID() 
								+ " AND frm_sales_dealer_id = " + productPurchaseData.getDealerID()
								+ " ORDER BY frm_sales_id DESC LIMIT 1");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return SelectUserQuery.toString();
	
	}

}
