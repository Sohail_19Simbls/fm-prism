package com.simbls.prism.rest.model.industry;

public class FirmDBCredentials {

	private String firmDBName;
	private String firmDBUserName;
	private String firmDBPassword;
	
	private FirmDBCredentials firmDBCredentialsObject;
	
	public String getfirmDBName() {
		return firmDBName;
	}
	public void setfirmDBName(String firmDBName) {
		this.firmDBName = firmDBName;
	}
	public String getfirmDBUserName() {
		return firmDBUserName;
	}
	public void setfirmDBUserName(String firmDBUserName) {
		this.firmDBUserName = firmDBUserName;
	}
	public String getfirmDBPassword() {
		return firmDBPassword;
	}
	public void setfirmDBPassword(String firmDBPassword) {
		this.firmDBPassword = firmDBPassword;
	}
	public FirmDBCredentials getFirmDBCredentialsObject() {
		return firmDBCredentialsObject;
	}
	public void setFirmDBCredentialsObject(FirmDBCredentials firmDBCredentialsObject) {
		this.firmDBCredentialsObject = firmDBCredentialsObject;
	}
	
	
}
