package com.simbls.prism.rest.dao.foodBazaar;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.simbls.prism.rest.foodBazaar.builder.FoodBazaarProductPurchaseQueryBuilder;
import com.simbls.prism.rest.model.industry.Product;
import com.simbls.prism.rest.model.industry.ProductPurchase;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Component("foodBazaarProductPurchaseDAO")
public interface FoodBazaarProductPurchaseDao {

	@Insert("INSERT into ${firmDBName}.frm_product_purchase("
			+ "frm_purchase_product_id,"
			+ "frm_purchase_dealer_id,"
			+ "frm_purchase_invoice_id,"
			+ "frm_purchase_mrp,"
			+ "frm_purchase_tax,"
			+ "frm_purchase_cess,"
			+ "frm_purchase_packed_quantity,"
			+ "frm_purchase_unit,"
			+ "frm_purchase_unit_cost,"
			+ "frm_purchase_paid_qty,"
			+ "frm_purchase_free_qty,"
			+ "frm_product_threshold_qty,"
			+ "frm_purchase_primary_discount,"
			+ "frm_purchase_secondary_discount,"
			+ "frm_purchase_arrival_date,"
			+ "frm_purchase_manufacture_date,"
			+ "frm_purchase_expiry_date,"
			+ "frm_purchase_entered_by,"
			+ "frm_purchase_entered_date,"
			+ "frm_purchase_rowstate)"
+ "values("
			+ " #{productID},"
			+ " #{dealerID},"
			+ " #{invoiceID},"
			+ " #{productPurchaseMRP},"
			+ " #{productPurchaseTax},"
			+ " #{productPurchaseCess},"
			+ " #{productPurchasePackedQuantity},"
			+ " #{productPurchaseUnit},"
			+ " #{productPurchaseUnitCost},"
			+ " #{productPurchasePaidQuantity},"
			+ " #{productPurchaseFreeQuantity},"
			+ " #{productPurchaseThresholdQuantity},"
			+ " #{productPurchasePrimaryDiscount},"
			+ " #{productPurchaseSecondaryDiscount},"
			+ " #{productPurchaseArrivalDate},"
			+ " #{productPurchaseManufactureDate},"
			+ " #{productPurchaseExpiryDate},"
			+ " #{productPurchaseEnteredBy},"
			+ " #{productPurchaseEnteredDate},"
			+ " #{productPurchaseRowState})")
	
	public void productPurchaseDetailsRegisterDAO(ProductPurchase productPurchaseData)   throws MySQLIntegrityConstraintViolationException;
	
	/**
	 * 
	 * @param productPurchaseData
	 */
	@Update("UPDATE ${firmDBName}.frm_product_purchase "
			+ " SET "
			+ " frm_purchase_mrp = #{productPurchaseMRP}, "
			+ " frm_purchase_tax = #{productPurchaseTax}, "
			+ " frm_purchase_packed_quantity = #{productPurchasePackedQuantity}, "
			+ " frm_purchase_unit = #{productPurchaseUnit}, "
			+ " frm_purchase_unit_cost = #{productPurchaseUnitCost}, "
			+ " frm_purchase_paid_qty = #{productPurchasePaidQuantity}, "
			+ " frm_purchase_free_qty = #{productPurchaseFreeQuantity}, "
			+ " frm_product_threshold_qty = #{productPurchaseThresholdQuantity}, "
			+ " frm_purchase_primary_discount = #{productPurchaseSecondaryDiscount}, "
			+ " frm_purchase_secondary_discount = #{productPurchaseSecondaryDiscount}, "
			+ " frm_purchase_updated_by = #{productPurchaseUpdatedBy}, "
			+ " frm_purchase_updated_date = #{productPurchaseUpdatedDate} "
			+ " WHERE frm_purchase_id = #{productPurchaseDetailsID}"
			+ " AND frm_purchase_product_id = #{productID}"
			+ " AND frm_purchase_dealer_id = #{dealerID}"
			+ " AND frm_purchase_invoice_id = #{invoiceID}")
	public void productPurchaseDetailsUpdateDAO(ProductPurchase productPurchaseData);
	
	/**
	 * 
	 * @param productPurchaseData
	 */
	@Delete("DELETE FROM ${firmDBName}.frm_product_purchase WHERE frm_purchase_id = #{productPurchaseDetailsID}")
	public void productPurchaseDetailsDeleteDAO(ProductPurchase productPurchaseData);

	@InsertProvider(type = FoodBazaarProductPurchaseQueryBuilder.class, method = "productPurchaseConsolidated_UPSERT_Query")
	public void productPurchaseConsolidated_UPSERT_DAO(@Param("productPurchaseData") ProductPurchase productPurchaseData);
	
	@Update("UPDATE ${firmDBName}.frm_product_sales "
			+ "	SET "
			+ " frm_purchase_rowstate = #{productPurchaseRowState} "
			+ " WHERE (frm_purchase_product_id = #{productID} "
			+ " AND frm_purchase_dealer_id = #{dealerID} "
			+ " AND frm_purchase_invoice_id = #{invoiceID}")
	public void updatePurchaseRowStateDAO(SalesBasket salesBasketData);
	
	
	@SelectProvider(type = FoodBazaarProductPurchaseQueryBuilder.class, method = "retrieveProductPurchaseDetailsQuery")
	@Results({
		@Result(property="productPurchaseDetailsID",			column="purchase_id"),
		@Result(property="productID",							column="product_id"),
		@Result(property="productObject",						column="{productID=product_id, firmDBName=firm_DB_name}",		one=@One(select = "retrieveProductDetailsDAO")),
		@Result(property="dealerID",							column="dealer_id"),
		@Result(property="invoiceID",							column="invoice_id"),
		@Result(property="productPurchaseMRP",					column="purchase_mrp"),
		@Result(property="productPurchaseTax",					column="purchase_tax"),
		@Result(property="productPurchaseCess",					column="purchase_cess"),
		@Result(property="productPurchasePackedQuantity",		column="packed_quantity"),
		@Result(property="productPurchaseUnit",					column="packed_unit"),
		@Result(property="productPurchaseUnitCost",				column="purchase_unit_cost"),
		@Result(property="productPurchasePaidQuantity",			column="purchase_paid_qty"),
		@Result(property="productPurchaseFreeQuantity",			column="purchase_free_qty"),
		@Result(property="productPurchaseThresholdQuantity",	column="threshold_qty"),
		@Result(property="productPurchasePrimaryDiscount",		column="primary_discount"),
		@Result(property="productPurchaseSecondaryDiscount",	column="secondary_discount")
	})
	public List<ProductPurchase> retrieveProductPurchaseDetailsDAO(@Param("productPurchaseData") ProductPurchase productPurchaseData);
	
	@SelectProvider(type = FoodBazaarProductPurchaseQueryBuilder.class, method = "retrieveProductDetailsQuery")
	@Results({
		@Result(property="productID",				column="frm_product_id"),
		@Result(property="productCompany",			column="frm_product_company"),
		@Result(property="productName",				column="frm_product_name"),
		@Result(property="productPackedQuantity",	column="frm_product_packed_quantity"),
		@Result(property="productUnit",				column="frm_product_packed_unit"),
		@Result(property="productBarCode",			column="frm_product_barcode")
	})
	public Product retrieveProductDetailsDAO(@Param("productID") long productID, @Param("firmDBName") String firmDBName);
	
	
	@SelectProvider(type = FoodBazaarProductPurchaseQueryBuilder.class,  method = "checkInvoiceStatusQuery")
	@Results({
		@Result(property="invoiceID",								column="invoice_id"),
		@Result(property="dealerID",								column="dealer_id"),
		@Result(property="invoiceNumber",							column="invoice_number"),
		@Result(property="invoiceAmount",							column="invoice_amount"),
		@Result(property="marketFee",								column="market_fee"),
		@Result(property="unloadingAndTransportationCharges",		column="extra_charges"),
		@Result(property="productID",								column="product_id"),
		@Result(property="productPurchaseTax",						column="product_tax"),
		@Result(property="productPurchaseCess",						column="product_cess"),
		@Result(property="productPurchasePrimaryDiscount",			column="primary_discount"),
		@Result(property="productPurchaseSecondaryDiscount",		column="secondary_discount"),
		@Result(property="productPurchaseUnitCost",					column="unit_cost"),
		@Result(property="productPurchasePaidQuantity",				column="paid_quantity")
	})
	public List<ProductPurchase> checkInvoiceStatusDAO(@Param("productPurchaseData") ProductPurchase productPurchaseData);
	

}
