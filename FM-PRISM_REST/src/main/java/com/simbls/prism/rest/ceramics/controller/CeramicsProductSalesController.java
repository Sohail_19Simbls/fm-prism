package com.simbls.prism.rest.ceramics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.ceramics.service.CeramicsProductSalesService;
import com.simbls.prism.rest.model.industry.ProductSales;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Controller
@RequestMapping(value = "/ceramics_productSales")
public class CeramicsProductSalesController {
	private static final Logger logger = Logger.getLogger(CeramicsProductSalesController.class);
	@Autowired private CeramicsProductSalesService ceramicsProductSalesService;
	
	/**
	 * 
	 * @param productSalesData
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/productSalesRegister", method = RequestMethod.POST)
	public ProductSales productSalesRegister(@RequestBody ProductSales productSalesData) throws Exception{
		logger.info("... Continuing Product Sales Registration ::: REST ::: CeramicsProductSalesController");
		ProductSales productSalesObject = ceramicsProductSalesService.productSalesRegister(productSalesData);
		logger.info("Returning to WEB ::: REST ::: CeramicsProductSalesController");
		return productSalesObject;
	}
	
	/**
	 * 
	 * @param salesBasketData
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/productSalesBasket", method = RequestMethod.POST)
	public ResponseEntity<SalesBasket> productSalesBasket(@RequestBody SalesBasket salesBasketData) throws Exception{
		logger.info("... Continuing Product Sales into Sales Basket ::: REST ::: CeramicsProductSalesController");
		SalesBasket salesBasketObject = ceramicsProductSalesService.productSalesBasket(salesBasketData);
		logger.info("Returning to WEB ::: REST ::: CeramicsProductSalesController");
		return ResponseEntity.accepted().body(salesBasketObject);
	}
	
	/**
	 * Initiate population of Sale Bill and Sale Bill Consolidated Table 
	 * @param salesBasketData
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/generateSalesBasketBill", method = RequestMethod.POST)
	public ResponseEntity<List<SalesBasket>> generateSalesBasketBill(@RequestBody List<SalesBasket> salesBasketData) throws Exception{
		logger.info("... Continuing Product Sales into Sales Basket ::: REST ::: CeramicsProductSalesController");
		List<SalesBasket> salesBasketObject = ceramicsProductSalesService.generateSalesBasketBill(salesBasketData);
		logger.info("Returning to WEB ::: REST ::: CeramicsProductSalesController");
		return ResponseEntity.accepted().body(salesBasketObject);
	}

}
