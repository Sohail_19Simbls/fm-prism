package com.simbls.prism.rest.central.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.central.service.ProductCategoryService;
import com.simbls.prism.rest.dao.central.ProductCategoryDao;
import com.simbls.prism.rest.model.central.ProductCategory;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService{
	private static final Logger logger = Logger.getLogger(ProductCategoryServiceImpl.class);
	@Autowired private ProductCategoryDao productCategoryDao;
	
	/**
	 * Populating Product Categories from Central DB
	 * @return productCategoryList
	 * @author Sohail Razvi  
	 */
	public List<ProductCategory> retrieveProductCategories(ProductCategory productCategoryData){
		logger.info("Retrieving Product Categories from Central DB ::: REST ::: ProductCategoryServiceImpl");
		List<ProductCategory> productCategoryList = productCategoryDao.retrieveProductCategoriesDAO(productCategoryData);
		logger.info("Successfully obtained Product Categories size - " + productCategoryList.size() + " ::: REST ::: ProductCategoryServiceImpl");
		return productCategoryList;
	}

}
