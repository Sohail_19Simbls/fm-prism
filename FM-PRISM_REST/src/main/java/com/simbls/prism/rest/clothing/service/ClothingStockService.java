package com.simbls.prism.rest.clothing.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.industry.SalesBasket;

@Component
public interface ClothingStockService {
		
	
	/**
	 * Retrieve all Product by BarCode.
	 * @return
	 */
	public List<SalesBasket> retrieveDetailProductList(SalesBasket salesBasketData);
	
}
