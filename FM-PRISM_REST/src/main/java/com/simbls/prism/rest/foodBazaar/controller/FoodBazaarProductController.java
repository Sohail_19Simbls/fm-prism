package com.simbls.prism.rest.foodBazaar.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.foodBazaar.service.FoodBazaarProductService;
import com.simbls.prism.rest.model.industry.Product;

@Controller
@RequestMapping(value = "/foodBazaar_product")
public class FoodBazaarProductController {
	private static final Logger logger = Logger.getLogger(FoodBazaarProductController.class);
	@Autowired private FoodBazaarProductService foodBazaarProductService;
	/**
	 * Product Registration
	 * @param productData
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@ResponseBody
	@RequestMapping(value = "/productRegister", method = RequestMethod.POST)
	public Product productRegister(@RequestBody Product productData) throws Exception{
		logger.info("... Continuing Product Registration ::: REST ::: FoodBazaarProductController");
		Product insertedProduct = foodBazaarProductService.productRegister(productData);
		logger.info("Returning to WEB ::: REST ::: FoodBazaarProductController");
		return insertedProduct;
	}

	/**
	 * Product Updation
	 * @param productData
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@ResponseBody
	@RequestMapping(value = "/productUpdate", method = RequestMethod.POST)
	public Product productUpdate(@RequestBody Product productData) throws Exception{
		logger.info("... Continuing Product Updation ::: REST ::: FoodBazaarProductController");
		Product updatedProduct = foodBazaarProductService.productUpdate(productData);
		logger.info("Returning to WEB ::: REST ::: FoodBazaarProductController");
		return updatedProduct;
	}
	
	/**
	 * Retrieves Single Product by Parameter.
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveProductByParameter", method=RequestMethod.POST)
	public ResponseEntity<List<Product>> retrieveProductByParameter(@RequestBody Product productData){
		logger.info("Continuing retrival of Product list ::: REST ::: FoodBazaarProductController");
		List<Product> retrievedProduct = foodBazaarProductService.retrieveProductByParameter(productData);
		logger.info("Returning Obtained Product  " + retrievedProduct.size() + " to WEB ::: REST ::: FoodBazaarProductController");
		return ResponseEntity.accepted().body(retrievedProduct);
	}
	
	/**
	 * Retrieves Product Purchase and Sales Details.
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveProductPurchaseSaleDetails", method=RequestMethod.POST)
	public ResponseEntity<Product> retrieveProductPurchaseSaleDetails(@RequestBody Product productData){
		logger.info("Continuing retrival of Product Purchase and Sales Details ::: REST ::: FoodBazaarProductController");
		Product retrievedProductPurchaseSalesDetails = foodBazaarProductService.retrieveProductPurchaseSaleDetails(productData);
		logger.info("Returning Obtained Product to WEB ::: REST ::: FoodBazaarProductController");
		return ResponseEntity.accepted().body(retrievedProductPurchaseSalesDetails);
	}

}
