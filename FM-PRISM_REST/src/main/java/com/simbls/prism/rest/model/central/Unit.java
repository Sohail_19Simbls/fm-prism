package com.simbls.prism.rest.model.central;

public class Unit {

	private int unitID;
	private String unit;
	
	private Unit unitObject;

	public int getUnitID() {
		return unitID;
	}

	public void setUnitID(int unitID) {
		this.unitID = unitID;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Unit getUnitObject() {
		return unitObject;
	}

	public void setUnitObject(Unit unitObject) {
		this.unitObject = unitObject;
	}
	
	
}
