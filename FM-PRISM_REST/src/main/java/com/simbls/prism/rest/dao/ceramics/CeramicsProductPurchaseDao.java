package com.simbls.prism.rest.dao.ceramics;

import java.util.List;

import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import com.simbls.prism.rest.clothing.builder.ClothingProductPurchaseQueryBuilder;
import com.simbls.prism.rest.model.industry.ProductPurchase;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Component("ceramicsProductPurchaseDAO")
public interface CeramicsProductPurchaseDao {

	@InsertProvider(type = ClothingProductPurchaseQueryBuilder.class, method = "productPurchaseDetailsRegisterQuery")
	public void productPurchaseDetailsRegisterDAO(@Param("productPurchaseData") List<ProductPurchase> productPurchaseData);
	

	@InsertProvider(type = ClothingProductPurchaseQueryBuilder.class, method = "productPurchaseConsolidated_UPSERT_Query")
	public void productPurchaseConsolidated_UPSERT_DAO(@Param("productPurchaseData") ProductPurchase productPurchaseData);
	
	@Update("UPDATE ${firmDBName}.frm_product_sales "
			+ "	SET "
			+ " frm_purchase_rowstate = #{productPurchaseRowState} "
			+ " WHERE (frm_purchase_product_id = #{productID} "
			+ " AND frm_purchase_dealer_id = #{dealerID} "
			+ " AND frm_purchase_invoice_id = #{invoiceID}")
	public void updatePurchaseRowStateDAO(SalesBasket salesBasketData);
	
	
	@SelectProvider(type = ClothingProductPurchaseQueryBuilder.class, method = "retrieveProductPurchaseDetailsQuery")
	@Results({
		@Result(property="productPurchaseDetailsID",			column="frm_purchase_id"),
		@Result(property="productID",							column="frm_purchase_product_id"),
		@Result(property="dealerID",							column="frm_purchase_dealer_id"),
		@Result(property="invoiceID",							column="frm_purchase_invoice_id"),
		@Result(property="productPurchaseMRP",					column="frm_purchase_mrp"),
		@Result(property="productPurchaseTax",					column="frm_purchase_tax"),
		@Result(property="productPurchaseCess",					column="frm_purchase_cess"),
		@Result(property="productPurchasePackedQuantity",		column="frm_purchase_packed_quantity"),
		@Result(property="productPurchaseUnit",					column="frm_purchase_unit"),
		@Result(property="productPurchaseUnitCost",				column="frm_purchase_unit_cost"),
		@Result(property="productPurchasePaidQuantity",			column="frm_purchase_paid_qty"),
		@Result(property="productPurchaseFreeQuantity",			column="frm_purchase_free_qty"),
		@Result(property="productPurchaseThresholdQuantity",	column="frm_product_threshold_qty"),
		@Result(property="productPurchasePrimaryDiscount",		column="frm_purchase_primary_discount"),
		@Result(property="productPurchaseSecondaryDiscount",	column="frm_purchase_secondary_discount"),
		@Result(property="productPurchaseArrivalDate",			column="frm_purchase_arrival_date"),
		@Result(property="productPurchaseManufactureDate",		column="frm_purchase_manufacture_date"),
		@Result(property="productPurchaseExpiryDate",			column="frm_purchase_expiry_date"),
		@Result(property="productPurchaseEnteredBy",			column="frm_purchase_entered_by"),
		@Result(property="productPurchaseEnteredDate",			column="frm_purchase_entered_date"),
		@Result(property="productPurchaseUpdatedBy",			column="frm_purchase_updated_by"),
		@Result(property="productPurchaseUpdatedDate",			column="frm_purchase_updated_date"),
		@Result(property="productPurchaseRowState",				column="frm_purchase_rowstate")
	})
	public List<ProductPurchase> retrieveProductPurchaseDetailsDAO(@Param("productPurchaseData") ProductPurchase productPurchaseData);
	
	
	
	@SelectProvider(type = ClothingProductPurchaseQueryBuilder.class,  method = "checkInvoiceStatusQuery")
	@Results({
		@Result(property="invoiceID",								column="invoice_id"),
		@Result(property="dealerID",								column="dealer_id"),
		@Result(property="invoiceNumber",							column="invoice_number"),
		@Result(property="invoiceAmount",							column="invoice_amount"),
		@Result(property="marketFee",								column="market_fee"),
		@Result(property="unloadingAndTransportationCharges",		column="extra_charges"),
		@Result(property="productID",								column="product_id"),
		@Result(property="productPurchaseTax",						column="product_tax"),
		@Result(property="productPurchaseCess",						column="product_cess"),
		@Result(property="productPurchasePrimaryDiscount",			column="primary_discount"),
		@Result(property="productPurchaseSecondaryDiscount",		column="secondary_discount"),
		@Result(property="productPurchaseUnitCost",					column="unit_cost"),
		@Result(property="productPurchasePaidQuantity",				column="paid_quantity")
	})
	public List<ProductPurchase> checkInvoiceStatusDAO(@Param("productPurchaseData") ProductPurchase productPurchaseData);
	

}
