package com.simbls.prism.rest.dao.ceramics;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import com.simbls.prism.rest.foodBazaar.builder.FoodBazaarInvoiceQueryBuilder;
import com.simbls.prism.rest.model.industry.Invoice;

@Component("ceramicsInvoiceDAO")
public interface CeramicsInvoiceDao {

	@Insert("INSERT into ${firmDBName}.frm_invoice("
			+ "frm_invoice_dealer_id,"
			+ "frm_invoice_number,"
			+ "frm_invoice_bill_date,"
			+ "frm_invoice_delivery_date,"
			+ "frm_invoice_amount,"
			+ "frm_invoice_market_fee,"
			+ "frm_invoice_transportation_unloading_fee,"
			+ "frm_invoice_payment_due_date,"
			+ "frm_invoice_entered_by,"
			+ "frm_invoice_entered_date)"
	+ "values("
			+ " #{dealerID},"
			+ " #{invoiceNumber},"
			+ " #{invoiceBillingDate},"
			+ " #{invoiceDeliveryDate},"
			+ " #{invoiceAmount},"
			+ " #{invoiceMarketFees},"
			+ " #{invoiceTransportationUnloadingFees},"
			+ " #{invoicePaymentDueDate},"
			+ " #{invoiceEnteredBy},"
			+ " #{invoiceEnteredDate})")
	
	public void invoiceRegisterDAO(Invoice invoice);
	
	@Update("UPDATE ${firmDBName}.frm_invoice "
			+ " SET "
			+ " frm_invoice_dealer_id = #{dealerID}, "
			+ " frm_invoice_number = #{invoiceNumber}, "
			+ " frm_invoice_bill_date = #{invoiceBillingDate}, "
			+ " frm_invoice_delivery_date = #{invoiceDeliveryDate}, "
			+ " frm_invoice_amount = #{invoiceAmount}, "
			+ " frm_invoice_market_fee = #{invoiceMarketFees}, "
			+ " frm_invoice_transportation_unloading_fee = #{invoiceTransportationUnloadingFees}, "
			+ " frm_invoice_payment_due_date = #{invoicePaymentDueDate}, "
			+ " frm_invoice_updated_by = #{invoiceUpdatedBy}, "
			+ " frm_invoice_updated_date = #{invoiceUpdatedDate} "
			+ " WHERE frm_invoice_id = #{invoiceID}")
	public void invoiceUpdateDAO(Invoice invoiceData);
	
	/**
	 * 
	 * @param invoiceData
	 * @return
	 * @author Sohail Razvi
	 */
	@SelectProvider(type = FoodBazaarInvoiceQueryBuilder.class, method = "retrieveInvoiceListQuery")
	@Results({
		@Result(property="invoiceID",							column="frm_invoice_id"),
		@Result(property="dealerID",							column="frm_dealer_id"),
		@Result(property="dealerName",							column="frm_dealer_name"),
		@Result(property="invoiceNumber",						column="frm_invoice_number"),
		@Result(property="invoiceBillingDate",					column="frm_invoice_bill_date"),
		@Result(property="invoiceDeliveryDate",					column="frm_invoice_delivery_date"),
		@Result(property="invoiceAmount",						column="frm_invoice_amount"),
		@Result(property="invoiceMarketFees",					column="frm_invoice_market_fee"),
		@Result(property="invoiceTransportationUnloadingFees",	column="frm_invoice_transportation_unloading_fee"),
		@Result(property="invoicePaymentDueDate",				column="frm_invoice_payment_due_date"),
		@Result(property="invoiceEnteredBy",					column="frm_invoice_entered_by"),
		@Result(property="invoiceEnteredDate",					column="frm_invoice_entered_date"),
		@Result(property="invoiceUpdatedBy",					column="frm_invoice_updated_by"),
		@Result(property="invoiceUpdatedDate",					column="frm_invoice_updated_date")})
	
	public List<Invoice> retrieveInvoiceListDAO(@Param("invoiceData") Invoice invoiceData);
	
	/**
	 * 
	 * @param invoiceData
	 * @return
	 * @author Sohail Razvi
	 */
	@SelectProvider(type = FoodBazaarInvoiceQueryBuilder.class, method = "retrieveInvoiceDetailsQuery")
	@Results({
		@Result(property="invoiceNumber",						column="invoice_number"),
		@Result(property="invoiceBillingDate",					column="invoice_bill_date"),
		@Result(property="invoiceDeliveryDate",					column="invoice_delivery_date"),
		@Result(property="invoiceAmount",						column="invoice_amount"),
		@Result(property="invoiceMarketFees",					column="invoice_market_fee"),
		@Result(property="invoiceTransportationUnloadingFees",	column="invoice_transportation_charges"),
		@Result(property="invoicePaymentDueDate",				column="invoice_payment_due_date"),
		@Result(property="invoiceEnteredBy",					column="invoice_entered_by"),
		@Result(property="invoiceEnteredDate",					column="invoice_entered_date"),
		@Result(property="invoiceUpdatedBy",					column="invoice_updated_by"),
		@Result(property="invoiceUpdatedDate",					column="invoice_updated_date"),
		@Result(property="productName",							column="product_name"),
		@Result(property="productPackedQuantity",				column="product_packed_quantity"),
		@Result(property="productPackedUnit",					column="product_packed_unit"),
		@Result(property="productMRP",							column="product_mrp"),
		@Result(property="productUnitCost",						column="product_cost"),
		@Result(property="productPrimaryDiscount",				column="product_primary_discount"),
		@Result(property="productSecondaryDiscount",			column="product_secondary_discount"),
		@Result(property="productPaidQuantity",					column="product_paid_quantity"),
		@Result(property="productFreeQuantity",					column="product_free_quantity"),
		@Result(property="productTax",							column="product_tax"),
		@Result(property="productCess",							column="product_cess")})
	
	public List<Invoice> retrieveInvoiceDetailsDAO(@Param("invoiceData") Invoice invoiceData);
	
}
