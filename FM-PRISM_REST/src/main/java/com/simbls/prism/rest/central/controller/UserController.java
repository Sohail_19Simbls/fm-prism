package com.simbls.prism.rest.central.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.central.service.UserService;
import com.simbls.prism.rest.helper.PasswordEncodeDecodeGen;
import com.simbls.prism.rest.model.central.Login;
import com.simbls.prism.rest.model.central.User;

@Controller
@RequestMapping(value = "/user")
public class UserController {
	private static final Logger logger = Logger.getLogger(UserController.class);
	@Autowired private UserService userService;
	

	/**
	 * Implementation for Android Login validation
	 * Validating login for entered User Name and Password
	 * The following is the details of the User-type
	 * User Type - 1 ::: Role - Super Admin
	 * User Type - 2 ::: Role - Moderator
	 * User Type - 3 ::: Role - On Boarding Team
	 * User Type - 4 ::: Role - Admin
	 * User Type - 5 ::: Role - Accountant
	 * User Type - 6 ::: Role - Store Keeper
	 * User Type - 7 ::: Role - Salesman
	 * User Type - 8 ::: Role - Delivery Man
	 * User Type - 9 ::: Role - Customer
	 * ****************************************************
	 * IMPORTANT STATUS FLAGS - According to the way condition is checked
	 * ****************************************************
	 * Firm Row State ::: 0 - Firm is Active
	 * Firm Row State ::: 1 - Firm is Deleted
	 * ****************************************************
	 * Firm Approval State ::: 1 - Firm is Approved
	 * Firm Approval State ::: 0 - Firm is Not Approved
	 * ****************************************************
	 * User Approval Status ::: 1 - User is Approved
	 * User Approval Status ::: 0 - User is Not Approved
	 * ****************************************************
	 * User Row State ::: 0 - User is Active
	 * User Row State ::: 1 - User is Deleted
	 * ****************************************************
	 * User First Login ::: 0 - User is Logging in for the first time
	 * User First Login ::: 1 - User has Logged in previously
	 * ****************************************************
	 * @param Login Object
	 * @author Sohail Razvi
	 */
	
	@ResponseBody
	@RequestMapping(value = "/validateCredentials", method = RequestMethod.POST)
	public User validateCredentials(@RequestBody Login login, HttpServletResponse response) {
		logger.info(".... Continuing Credential Validation ::: REST ::: User Controller");
		User userObject = userService.validateCredentials(login);
		logger.info("Validating userType - " + userObject.toString() + " ::: REST ::: UserController");
		switch(userObject.getUserCategory()){
		case 1 : {
			logger.info("Successfully validated credentials ::: Logging in User ::: " + userObject.getUserName());
			logger.info("User type Super Admin");
			break;
		}case 2 : {
			logger.info("Successfully validated credentials ::: Logging in User ::: " + userObject.getUserName());
			logger.info("User type Moderator");
			break;
		}case 3 : {
			logger.info("Successfully validated credentials ::: Logging in User ::: " + userObject.getUserName());
			logger.info("User type On Board Team Member");
			break;
		}case 4 : {
			logger.info("Successfully validated credentials ::: Logging in User ::: " + userObject.getUserName());
			logger.info("User type Admin");
			break;
		}case 5 : {
			logger.info("Successfully validated credentials ::: Logging in User ::: " + userObject.getUserName());
			logger.info("User type Accountant");
			break;
		}case 6 : {
			logger.info("Successfully validated credentials ::: Logging in User ::: " + userObject.getUserName());
			logger.info("User type Store Keeper");
			break;
		}case 7 : {
			logger.info("Successfully validated credentials ::: Logging in User ::: " + userObject.getUserName());
			logger.info("User type Salesman");
			break;
		}case 8 : {
			logger.info("Successfully validated credentials ::: Logging in User ::: " + userObject.getUserName());
			logger.info("User type Delivery Man");
			break;
		}case 9 : {
			logger.info("Successfully validated credentials ::: Logging in User ::: " + userObject.getUserName());
			logger.info("User type Customer");
			break;
		}
		default : {
			logger.info("Validation Failed ::: REST ::: User Controller");
			break;
		}
		}
		logger.info("Processing user - " + userObject.getUserName() + " ::: REST ::: UserController");
		return userObject;
	}
	
	/**
	 * 
	 * @param resetPassword
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/resetPasswordFirstLogin", method=RequestMethod.POST)
	public User resetPasswordFirstLogin(@RequestBody Login resetPassword){		
		logger.info("Initiating password reset ::: REST ::: User logging in for the first time ::: UserID - "+ resetPassword.getUserId());
		User userObject = userService.getUserDetails(resetPassword.getUserId());
		String hashPassword = PasswordEncodeDecodeGen.getEncodedPassword(resetPassword.getUserPassword());
		userObject.setUserPassword(hashPassword);
		userObject.setUserFirstLogin(1);
		userService.resetPasswordFirstLogin(userObject);
		logger.info("Password successfully reset ::: @ REST ::: For the user - " + userObject.getUserName());
		logger.info("Initiating return to WEB ::: UserController ::: REST");
		return userObject;		
	}
}
