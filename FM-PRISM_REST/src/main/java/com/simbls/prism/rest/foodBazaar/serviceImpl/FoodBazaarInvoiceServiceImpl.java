package com.simbls.prism.rest.foodBazaar.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.dao.foodBazaar.FoodBazaarInvoiceDao;
import com.simbls.prism.rest.foodBazaar.service.FoodBazaarInvoiceService;
import com.simbls.prism.rest.model.industry.Invoice;

@Service
public class FoodBazaarInvoiceServiceImpl implements FoodBazaarInvoiceService {
	private static final Logger logger = Logger.getLogger(FoodBazaarInvoiceServiceImpl.class);
	@Autowired private FoodBazaarInvoiceDao foodBazaarInvoiceDAO;

	/**
	 * 
	 */
	public void invoiceRegister(Invoice invoiceData) {
		logger.info("Initiating insertion into Invoice Table - Target DB " + invoiceData.getFirmDBName() + " ::: REST ::: Invoice Service Impl");
		foodBazaarInvoiceDAO.invoiceRegisterDAO(invoiceData);
		logger.info("Invoice Successfully Populated ::: REST ::: Invoice Service Impl");
	}
	
	/**
	 * 
	 */
	public void invoiceUpdate(Invoice invoiceData) {
		logger.info("Initiating updation into Invoice Table - Target DB " + invoiceData.getFirmDBName() + " ::: REST ::: Invoice Service Impl");
		foodBazaarInvoiceDAO.invoiceUpdateDAO(invoiceData);
		logger.info("Invoice Successfully Updated ::: REST ::: Invoice Service Impl");
	}
	
	/**
	 * 
	 */
	public void invoiceDelete(Invoice invoiceData) {
		logger.info("Initiating deletion into Invoice and related Data - Target DB " + invoiceData.getFirmDBName() + " ::: REST ::: Invoice Service Impl");
		logger.info("Deleting Purchase Details ...");
		foodBazaarInvoiceDAO.productPurchaseDetailsDeleteDAO(invoiceData);
		logger.info("Purchase Details Successfully Deleted!");
		logger.info("Deleting Consolidated Purchase Details ...");
		foodBazaarInvoiceDAO.productPurchaseConsolidatedDetailsDeleteDAO(invoiceData);
		logger.info("Consolidated Purchase Details Successfully Deleted!");
		logger.info("Deleting Sale Details ...");
		foodBazaarInvoiceDAO.productSalesDetailsDeleteDAO(invoiceData);
		logger.info("Sale Details Successfully Deleted!");
		logger.info("Deleting Invoice Details ...");
		foodBazaarInvoiceDAO.invoiceDeleteDAO(invoiceData);
		logger.info("Invoice Details Successfully Deleted!");
		logger.info("Invoice Successfully Updated ::: REST ::: Invoice Service Impl");
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Invoice> retrieveInvoiceList(Invoice invoiceData) {
		logger.info("Initiating DB search to retrieve Invoice List ::: REST ::: FoodBazaarInvoiceService");
		List<Invoice> registeredInvoiceList = foodBazaarInvoiceDAO.retrieveInvoiceListDAO(invoiceData);
		logger.info("Successfully obtained - " + registeredInvoiceList.size() + " registered Invoices");
		logger.info("Returning to Invoice Controller ::: REST ::: Invoice Service IMPL");
		return registeredInvoiceList;
	}

	/**
	 * 
	 * @return
	 */
	public List<Invoice> retrieveInvoiceDetails(Invoice invoiceData) {
		logger.info("Initiating DB search to retrieve Products Purchased against Invoice ID ::: REST ::: FoodBazaarInvoiceService");
		List<Invoice> invoicePurchaseDetails = foodBazaarInvoiceDAO.retrieveInvoiceDetailsDAO(invoiceData);
		logger.info("Successfully obtained - " + invoicePurchaseDetails.size() + " registered Invoices");
		logger.info("Returning to Invoice Controller ::: REST ::: Invoice Service IMPL");
		return invoicePurchaseDetails;
	}
}
