package com.simbls.prism.rest.model.industry;

public class Expense {
	private long expenseID;
	private int expenseSubTypeID;
	private String expenseDate;
	private String expenseAmount;
	private String expenseEnteredBy;
	private String expenseEnteredDate;
	private String expenseUpdatedBy;
	private String expenseUpdated;
	
	private Expense expenseObject;

	public long getExpenseID() {
		return expenseID;
	}

	public void setExpenseID(long expenseID) {
		this.expenseID = expenseID;
	}

	public int getExpenseSubTypeID() {
		return expenseSubTypeID;
	}

	public void setExpenseSubTypeID(int expenseSubTypeID) {
		this.expenseSubTypeID = expenseSubTypeID;
	}

	public String getExpenseDate() {
		return expenseDate;
	}

	public void setExpenseDate(String expenseDate) {
		this.expenseDate = expenseDate;
	}

	public String getExpenseAmount() {
		return expenseAmount;
	}

	public void setExpenseAmount(String expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	public String getExpenseEnteredBy() {
		return expenseEnteredBy;
	}

	public void setExpenseEnteredBy(String expenseEnteredBy) {
		this.expenseEnteredBy = expenseEnteredBy;
	}

	public String getExpenseEnteredDate() {
		return expenseEnteredDate;
	}

	public void setExpenseEnteredDate(String expenseEnteredDate) {
		this.expenseEnteredDate = expenseEnteredDate;
	}

	public String getExpenseUpdatedBy() {
		return expenseUpdatedBy;
	}

	public void setExpenseUpdatedBy(String expenseUpdatedBy) {
		this.expenseUpdatedBy = expenseUpdatedBy;
	}

	public String getExpenseUpdated() {
		return expenseUpdated;
	}

	public void setExpenseUpdated(String expenseUpdated) {
		this.expenseUpdated = expenseUpdated;
	}

	public Expense getExpenseObject() {
		return expenseObject;
	}

	public void setExpenseObject(Expense expenseObject) {
		this.expenseObject = expenseObject;
	}

		
}
