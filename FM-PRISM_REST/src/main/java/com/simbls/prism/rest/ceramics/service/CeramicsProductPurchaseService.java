package com.simbls.prism.rest.ceramics.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.industry.ProductPurchase;

@Component
public interface CeramicsProductPurchaseService {
	/**
	 * Register Product Purchase Details
	 * @param productPurchaseData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<ProductPurchase> productPurchaseDetailsRegister(List<ProductPurchase> productPurchaseData) throws Exception;

	/**
	 * Retrieve all registered Dealers
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<ProductPurchase> retrieveProductPurchaseDetails(ProductPurchase productPurchaseData) throws Exception;
	
	/**
	 * Retrieve all registered Dealers
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public ProductPurchase checkInvoiceStatus(ProductPurchase productPurchaseData) throws Exception;
	
}
