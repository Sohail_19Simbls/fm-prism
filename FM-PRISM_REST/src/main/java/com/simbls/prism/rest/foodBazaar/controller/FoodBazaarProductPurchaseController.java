package com.simbls.prism.rest.foodBazaar.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.foodBazaar.service.FoodBazaarProductPurchaseService;
import com.simbls.prism.rest.model.industry.ProductPurchase;

@Controller
@RequestMapping(value = "/foodBazaar_productPurchase")
public class FoodBazaarProductPurchaseController {
	private static final Logger logger = Logger.getLogger(FoodBazaarProductPurchaseController.class);
	@Autowired private FoodBazaarProductPurchaseService foodBazaarProductPurchaseService;
	
	/**
	 * Product Purchase Registration
	 * @param productPurchaseData
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@ResponseBody
	@RequestMapping(value = "/productPurchaseDetailsRegister", method = RequestMethod.POST)
	public ProductPurchase productPurchaseDetailsRegister(@RequestBody ProductPurchase productPurchaseData) throws Exception{
		logger.info("... Continuing Product Purchase Registration for Product - " + productPurchaseData.getProductID() + "::: REST ::: FoodBazaarProductPurchaseController");
		ProductPurchase registredObject = foodBazaarProductPurchaseService.productPurchaseDetailsRegister(productPurchaseData);
		logger.info("Returning to WEB ::: REST ::: FoodBazaarProductPurchaseController");
		return registredObject;
	}
	
	/**
	 * Product Purchase Update
	 * @param productPurchaseData
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@ResponseBody
	@RequestMapping(value = "/productPurchaseDetailsUpdate", method = RequestMethod.POST)
	public ProductPurchase productPurchaseDetailsUpdate(@RequestBody ProductPurchase productPurchaseData) throws Exception{
		logger.info("... Continuing Product Purchase Updation for Product - " + productPurchaseData.getProductID() + "::: REST ::: FoodBazaarProductPurchaseController");
		ProductPurchase updatedObject = foodBazaarProductPurchaseService.productPurchaseDetailsUpdate(productPurchaseData);
		logger.info("Returning to WEB ::: REST ::: FoodBazaarProductPurchaseController");
		return updatedObject;
	}
	
	/**
	 * Product Purchase Update
	 * @param productPurchaseData
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@ResponseBody
	@RequestMapping(value = "/productPurchaseDetailsDelete", method = RequestMethod.POST)
	public ProductPurchase productPurchaseDetailsDelete(@RequestBody ProductPurchase productPurchaseData) throws Exception{
		logger.info("... Continuing Product Purchase Deletion ::: REST ::: FoodBazaarProductPurchaseController");
		foodBazaarProductPurchaseService.productPurchaseDetailsDelete(productPurchaseData);
		logger.info("Returning to WEB ::: REST ::: FoodBazaarProductPurchaseController");
		return productPurchaseData;
	}
	
	/**
	 * Retrieving list of all the Registered ProductPurchases
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value="/retrieveProductPurchaseDetails",method=RequestMethod.POST)
	public ResponseEntity<List<ProductPurchase>> retrieveProductPurchaseDetails(@RequestBody ProductPurchase productPurchaseData) throws Exception{
		logger.info("Continuing retrival of Product Purchase Details ::: REST ::: FoodBazaarProductPurchaseController");
		List<ProductPurchase> retrieveProductPurchaseList = foodBazaarProductPurchaseService.retrieveProductPurchaseDetails(productPurchaseData);
		logger.info("Returning Obtained Product Purchase Details of size " + retrieveProductPurchaseList.size() + " to WEB ::: REST ::: FoodBazaarProductPurchaseController");
		return ResponseEntity.accepted().body(retrieveProductPurchaseList);
	}
	
	/**
	 * Check if all the Purchased Product Total matches the Invoice Amount
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value="/checkInvoiceStatus",method=RequestMethod.POST)
	public ResponseEntity<ProductPurchase> checkInvoiceStatus(@RequestBody ProductPurchase productPurchaseData) throws Exception{
		logger.info("Continuing retrival of Product Purchase Details ::: REST ::: FoodBazaarProductPurchaseController");
		ProductPurchase productPurchaseDetails = foodBazaarProductPurchaseService.checkInvoiceStatus(productPurchaseData);
		logger.info("Returning Obtained Product Purchase Details to WEB ::: REST ::: FoodBazaarProductPurchaseController");
		return ResponseEntity.accepted().body(productPurchaseDetails);
	}

}
