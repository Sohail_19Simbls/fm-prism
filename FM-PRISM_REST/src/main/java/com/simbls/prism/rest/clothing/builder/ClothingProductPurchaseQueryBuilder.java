package com.simbls.prism.rest.clothing.builder;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.simbls.prism.rest.model.industry.ProductPurchase;
import com.simbls.prism.rest.model.industry.SalesBasket;

public class ClothingProductPurchaseQueryBuilder {
	private static final Logger logger = Logger.getLogger(ClothingProductPurchaseQueryBuilder.class);
	
	public String productPurchaseDetailsRegisterQuery(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Product Sales Basket ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		@SuppressWarnings("unchecked")
		List<ProductPurchase> productPurchaseData = (List<ProductPurchase>) parameters.get("productPurchaseData");
		logger.info("Retrieving Latest Sales Bill - Target DBName - " + productPurchaseData.get(0).getFirmDBName().toString());
		StringBuilder insertProductPurchaseDetailsQuery = new StringBuilder();
		insertProductPurchaseDetailsQuery.append("INSERT INTO " + productPurchaseData.get(0).getFirmDBName() + ".frm_product_purchase "
								+ " (frm_purchase_product_id,"
								+ " frm_purchase_dealer_id,"
								+ " frm_purchase_invoice_id, "
								+ " frm_purchase_product_barcode, "
								+ " frm_purchase_hsn, "
								+ " frm_purchase_mrp, "
								+ " frm_purchase_tax, "
								+ " frm_purchase_unit_cost, "
								+ " frm_purchase_paid_qty, "
								+ " frm_purchase_discount, "
								+ " frm_purchase_arrival_date, "
								+ " frm_purchase_entered_by, "
								+ " frm_purchase_entered_date, "							
								+ " frm_purchase_rowstate) "
						+ " VALUES ");
		for(ProductPurchase individualProductPurchaseData : productPurchaseData){
			insertProductPurchaseDetailsQuery.append(""
				+ " ("		+ individualProductPurchaseData.getProductID() + ", "
							+ individualProductPurchaseData.getDealerID() + ", "
							+ individualProductPurchaseData.getInvoiceID() + ", "
							+ "'" + individualProductPurchaseData.getProductPurchaseBarCode() + "', "
							+ "'" + individualProductPurchaseData.getProductPurchaseHSN() + "', "
							+ "'" + individualProductPurchaseData.getProductPurchaseMRP() + "', "
							+ "'" + individualProductPurchaseData.getProductPurchaseTax() + "', "
							+ "'" + individualProductPurchaseData.getProductPurchaseUnitCost() + "', "
							+ "'" + individualProductPurchaseData.getProductPurchasePaidQuantity() + "', "
							+ "'" + individualProductPurchaseData.getProductPurchaseDiscount() + "', "
							+ "'" + individualProductPurchaseData.getProductPurchaseArrivalDate() + "', "
							+ "'" + individualProductPurchaseData.getProductPurchaseEnteredBy() + "', "
							+ "'" + individualProductPurchaseData.getProductPurchaseEnteredDate() + "', "
							+ individualProductPurchaseData.getProductPurchaseRowState() + "), ");
		}
		insertProductPurchaseDetailsQuery = insertProductPurchaseDetailsQuery.deleteCharAt(insertProductPurchaseDetailsQuery.length() - 2);
		logger.info("Generated Query - " + insertProductPurchaseDetailsQuery.toString() + " ::: REST ::: FoodBazaarProductSalesQueryBuilder");
		return insertProductPurchaseDetailsQuery.toString();
	}
	
	
	public String productPurchaseConsolidated_UPSERT_Query(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Insertion of Consolidated Product Purchase Details ::: REST ::: FoodBazaarProductPurchaseQueryBuilder");
		ProductPurchase productPurchaseData = (ProductPurchase) parameters.get("productPurchaseData");
		logger.info("Retrieving Product Purchase Details - Target DBName - " + productPurchaseData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();

		if(productPurchaseData.getProductPurchase_For_0_Tax_Amount() == null){
			productPurchaseData.setProductPurchase_For_0_Tax_Amount("0"); 
		};
		if(productPurchaseData.getProductPurchase_For_5_Tax_Amount() == null){
			productPurchaseData.setProductPurchase_For_5_Tax_Amount("0");
		};
		if(productPurchaseData.getProductPurchase_For_12_Tax_Amount() == null){
			productPurchaseData.setProductPurchase_For_12_Tax_Amount("0");
		};
		if(productPurchaseData.getProductPurchase_For_18_Tax_Amount() == null){
			productPurchaseData.setProductPurchase_For_18_Tax_Amount("0");
		};
		if(productPurchaseData.getProductPurchase_For_28_Tax_Amount() == null){
			productPurchaseData.setProductPurchase_For_28_Tax_Amount("0");
		};
		if(productPurchaseData.getProductPurchase_Effective_CGST() == null){
			productPurchaseData.setProductPurchase_Effective_CGST("0");
		};
		if(productPurchaseData.getProductPurchase_Effective_SGST() == null){
			productPurchaseData.setProductPurchase_Effective_SGST("0");
		};
		if(productPurchaseData.getProductPurchase_Effective_CESS() == null){
			productPurchaseData.setProductPurchase_Effective_CESS("0");
		}
		SelectUserQuery.append("INSERT INTO " + productPurchaseData.getFirmDBName() + ".frm_purchase_bill_consolidated"
				+ " (frm_purchase_dealer_id,"
				+ " frm_purchase_invoice_id,"
				+ " frm_purchase_date,"
				+ " frm_purchase_gst_number,"
				+ " frm_purchase_0,"
				+ " frm_purchase_5,"
				+ " frm_purchase_12,"
				+ " frm_purchase_18,"
				+ " frm_purchase_28,"
				+ " frm_purchase_cgst,"
				+ " frm_purchase_sgst,"
				+ " frm_purchase_cess,"
				+ " frm_purchase_entered_date)"
		+ " VALUES ("
				+ productPurchaseData.getDealerID() + ", "
				+ productPurchaseData.getInvoiceID() + ", "
				+ "'" + productPurchaseData.getProductPurchaseEnteredDate() + "', "		
				+   "'', "		
				+ productPurchaseData.getProductPurchase_For_0_Tax_Amount() + ", "
				+ productPurchaseData.getProductPurchase_For_5_Tax_Amount() + ", "
				+ productPurchaseData.getProductPurchase_For_12_Tax_Amount() + ", "
				+ productPurchaseData.getProductPurchase_For_18_Tax_Amount() + ", "
				+ productPurchaseData.getProductPurchase_For_28_Tax_Amount() + ", "
				+ productPurchaseData.getProductPurchase_Effective_CGST() + ", "
				+ productPurchaseData.getProductPurchase_Effective_SGST() + ", "
				+ productPurchaseData.getProductPurchase_Effective_CESS() + ", "
				+ "'" + productPurchaseData.getProductPurchaseEnteredDate() + "') "
		+ " ON DUPLICATE KEY UPDATE "
				+ " frm_purchase_0 = frm_purchase_0 + VALUES(frm_purchase_0),"
				+ " frm_purchase_5 = frm_purchase_5 + VALUES(frm_purchase_5),"
				+ " frm_purchase_12 = frm_purchase_12 + VALUES(frm_purchase_12),"
				+ " frm_purchase_18 = frm_purchase_18 + VALUES(frm_purchase_18),"
				+ " frm_purchase_28 = frm_purchase_28 + VALUES(frm_purchase_28),"
				+ " frm_purchase_cgst = frm_purchase_cgst + VALUES(frm_purchase_cgst),"
				+ " frm_purchase_sgst = frm_purchase_sgst + VALUES(frm_purchase_sgst),"
				+ " frm_purchase_cess = frm_purchase_cess + VALUES(frm_purchase_cess)");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}


	/**
	 * 
	 * @param parameters
	 * @return
	 * @author Sohail Razvi
	 */
	public String retrieveProductPurchaseDetailsQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Product Purchase Details ::: REST ::: FoodBazaarProductPurchaseQueryBuilder");
		ProductPurchase productPurchaseData = (ProductPurchase) parameters.get("productPurchaseData");
		logger.info("Retrieving Product Purchase Details - Target DBName - " + productPurchaseData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("SELECT * from " + productPurchaseData.getFirmDBName() + ".frm_product_purchase");
		/**
		 * Validating for empty DealerID. If DealerID is empty, select all the Payment.
		 */
		if(productPurchaseData.getDealerID() != 0 && productPurchaseData.getInvoiceID() != 0 && productPurchaseData.getProductID() != 0){
			SelectUserQuery.append(" WHERE frm_purchase_dealer_id=" + productPurchaseData.getDealerID() 
								+ " AND frm_purchase_invoice_id=" + productPurchaseData.getInvoiceID() 
								+ " AND frm_purchase_product_id=" + productPurchaseData.getProductID());
		}
				
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	/**
	 * 
	 * @param parameters
	 * @return
	 * @author Sohail Razvi
	 */
	public String checkInvoiceStatusQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Product Purchase Details ::: REST ::: FoodBazaarProductPurchaseQueryBuilder");
		ProductPurchase productPurchaseData = (ProductPurchase) parameters.get("productPurchaseData");
		logger.info("Retrieving Product Purchase Details - Target DBName - " + productPurchaseData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("SELECT "
				+ " invoice.frm_invoice_id invoice_id, "
				+ " invoice.frm_invoice_dealer_id dealer_id, "
				+ " invoice.frm_invoice_number invoice_number, "
				+ " invoice.frm_invoice_amount invoice_amount, "
				+ " invoice.frm_invoice_market_fee market_fee, "
				+ " invoice.frm_invoice_transportation_unloading_fee extra_charges, "
				/* ------------------------------------- */ 
				+ " purchase.frm_purchase_product_id product_id, "
				+ " purchase.frm_purchase_tax product_tax, "
				+ " purchase.frm_purchase_cess product_cess, "
				+ " purchase.frm_purchase_primary_discount primary_discount, "
				+ " purchase.frm_purchase_secondary_discount secondary_discount, "
				+ " purchase.frm_purchase_unit_cost unit_cost, "
				+ " purchase.frm_purchase_paid_qty paid_quantity "
				/* ------------------------------------- */
				+ " FROM " + productPurchaseData.getFirmDBName() + ".frm_product_purchase purchase "
				+ " JOIN " + productPurchaseData.getFirmDBName() + ".frm_invoice invoice "
				+ " ON purchase.frm_purchase_dealer_id = " + productPurchaseData.getDealerID()
				+ " AND purchase.frm_purchase_invoice_id = " + productPurchaseData.getInvoiceID()
				+ " GROUP BY product_id");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarProductPurchaseQueryBuilder");
		return SelectUserQuery.toString();
	}
	


}
