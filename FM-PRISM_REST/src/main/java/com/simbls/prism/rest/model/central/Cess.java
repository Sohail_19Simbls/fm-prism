package com.simbls.prism.rest.model.central;

public class Cess {

	private long cessID;
	private String cess;
	
	private Cess cessObject;

	public long getCessID() {
		return cessID;
	}

	public void setCessID(long cessID) {
		this.cessID = cessID;
	}

	public String getCess() {
		return cess;
	}

	public void setCess(String cess) {
		this.cess = cess;
	}

	public Cess getCessObject() {
		return cessObject;
	}

	public void setCessObject(Cess cessObject) {
		this.cessObject = cessObject;
	}
	
}
