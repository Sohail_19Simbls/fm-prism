package com.simbls.prism.rest.ceramics.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.dao.ceramics.CeramicsProductDao;
import com.simbls.prism.rest.ceramics.service.CeramicsStockService;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Service
public class CeramicsStockServiceImpl implements CeramicsStockService {
	private static final Logger logger = Logger.getLogger(CeramicsStockServiceImpl.class);
	@Autowired private CeramicsProductDao ceramicsProductDAO;

	
	
	public List<SalesBasket> retrieveDetailProductList(SalesBasket salesBasketData) {
		logger.info("Initiating DB search to retrieve Product List. Target DB - " + salesBasketData.getFirmDBName() + "::: REST ::: CeramicsProductService");
		List<SalesBasket> retrievedDetailProductList = ceramicsProductDAO.retrieveDetailProductListDAO(salesBasketData);
		logger.info("Successfully obtained - " + retrievedDetailProductList.size() + " registered Products");
		logger.info("Returning to Product Controller ::: REST ::: CeramicsStockServiceImpl");
		return retrievedDetailProductList;
	}
}
