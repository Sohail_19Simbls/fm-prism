package com.simbls.prism.rest.dao.foodBazaar;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.simbls.prism.rest.foodBazaar.builder.FoodBazaarProductQueryBuilder;
import com.simbls.prism.rest.model.industry.Product;
import com.simbls.prism.rest.model.industry.ProductPurchase;
import com.simbls.prism.rest.model.industry.ProductSales;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Component("foodBazaarProductDAO")
public interface FoodBazaarProductDao {

	/**
	 * 
	 * @param productData
	 * @throws MySQLIntegrityConstraintViolationException
	 */
	@Insert("INSERT into ${firmDBName}.frm_product("
			+ "frm_product_company,"
			+ "frm_product_name,"
			+ "frm_product_packed_quantity,"
			+ "frm_product_packed_unit,"
			+ "frm_product_category_id,"
			+ "frm_product_sub_category_id,"
			+ "frm_product_barcode,"
			+ "frm_product_custom_flag,"
			+ "frm_product_entered_by,"
			+ "frm_product_entered_date)"
	+ "values("
			+ " #{productCompany},"
			+ " #{productName},"
			+ " #{productPackedQuantity},"
			+ " #{productUnit},"
			+ " #{productCategoryID},"
			+ " #{productSubCategoryID},"
			+ " #{productBarCode},"
			+ " #{productCustomBarCodeFlag},"
			+ " #{productEnteredBy},"
			+ " #{productEnteredDate})")
	public void productRegisterDAO(Product productData) throws MySQLIntegrityConstraintViolationException ;
	
	/**
	 * 
	 * @param productData
	 * @throws MySQLIntegrityConstraintViolationException
	 */
	@Update("UPDATE ${firmDBName}.frm_product "
			+ " SET "
			+ " frm_product_company = #{productCompany}, "
			+ " frm_product_name = #{productName}, "
			+ " frm_product_packed_quantity = #{productPackedQuantity}, "
			+ " frm_product_packed_unit = #{productUnit}, "
			+ " frm_product_category_id = #{productCategoryID}, "
			+ " frm_product_sub_category_id = #{productSubCategoryID}, "
			+ " frm_product_barcode = #{productBarCode}, "
			+ " frm_product_custom_flag = #{productCustomBarCodeFlag}, "
			+ " frm_product_updated_by = #{productUpdatedBy}, "
			+ " frm_product_updated_date = #{productUpdatedDate}"
			+ " WHERE frm_product_id = #{productID}")
	public void productUpdateDAO(Product productData) throws MySQLIntegrityConstraintViolationException;
	
	/**
	 * 
	 * @param productData
	 * @return
	 */
	@SelectProvider(type = FoodBazaarProductQueryBuilder.class, method = "retrieveProductByParameterQuery")
	@Results({
		@Result(property="productID",				column="frm_product_id"),
		@Result(property="productCompany",			column="frm_product_company"),
		@Result(property="productName",				column="frm_product_name"),
		@Result(property="productPackedQuantity",	column="frm_product_packed_quantity"),
		@Result(property="productUnit",				column="frm_product_packed_unit"),
		@Result(property="productCategoryObject",	column="frm_product_category_id",			one=@One(select = "com.simbls.prism.rest.dao.central.ProductCategoryDao.retrieveSpecificProductCategoriesDAO")),
		@Result(property="productSubCategoryObject",column="frm_product_sub_category_id",		one=@One(select = "com.simbls.prism.rest.dao.central.ProductSubCategoryDao.retrieveSpecificProductSubCategoriesDAO")),
		@Result(property="productBarCode",			column="frm_product_barcode"),
		@Result(property="productEnteredBy",		column="frm_product_entered_by"),
		@Result(property="productEnteredDate",		column="frm_product_entered_date"),
		@Result(property="productUpdatedBy",		column="frm_product_updated_by"),
		@Result(property="productUpdatedDate",		column="frm_product_updated_date")	
	})
	public List<Product> retrieveProductByParameterDAO(@Param("productData") Product productData);
	
	
	@SelectProvider(type = FoodBazaarProductQueryBuilder.class, method = "retrieveDetailProductListQuery")
	@Results({
		@Result(property="productPurchaseObject",				column="{productID=product_id, firmDBName=firm_DB_name}",				one=@One(select = "retrieveProductPurchaseDetailsDAO")),
		@Result(property="productSalesObject",					column="{productID=product_id, firmDBName=firm_DB_name}",				one=@One(select = "retrieveProductSalesDetailsDAO")),
		@Result(property="productCompany",						column="product_company"),
		@Result(property="productName",							column="product_name"),
		@Result(property="productPackedQuantity",				column="packed_product_quantity"),
		@Result(property="productUnit",							column="packed_product_unit"),
		@Result(property="productCategoryObject",				column="product_category",			one=@One(select = "com.simbls.prism.rest.dao.central.ProductCategoryDao.retrieveSpecificProductCategoriesDAO")),
		@Result(property="productSubCategoryObject",			column="product_sub_category",		one=@One(select = "com.simbls.prism.rest.dao.central.ProductSubCategoryDao.retrieveSpecificProductSubCategoriesDAO")),
		@Result(property="productBarCode",						column="product_barcode")
	})
	public Product retrieveProductPurchaseSaleDetailsDAO(@Param("productData") Product productData);
	
	@SelectProvider(type = FoodBazaarProductQueryBuilder.class, method = "retrieveProductPurchaseDetailsQuery")
	@Results({
		@Result(property="invoiceNumber",					column="invoice_number"),
		@Result(property="dealerName",						column="dealer_name"),
		@Result(property="productPurchaseMRP",				column="product_mrp"),
		@Result(property="productPurchaseTax",				column="product_tax"),
		@Result(property="productPurchaseCess",				column="product_cess"),
		@Result(property="productPurchasePackedQuantity",	column="product_packed_quantity"),
		@Result(property="productPurchaseUnit",				column="product_packed_unit"),
		@Result(property="productPurchaseUnitCost",			column="product_unit_cost"),
		@Result(property="productPurchasePaidQuantity",		column="product_paid_qty"),
		@Result(property="productPurchaseFreeQuantity",		column="product_free_qty"),
		@Result(property="productPurchaseThresholdQuantity",column="product_threshold_qty"),
		@Result(property="productPurchasePrimaryDiscount",	column="product_primary_disc"),
		@Result(property="productPurchaseSecondaryDiscount",column="product_secondary_disc"),
		@Result(property="productPurchaseArrivalDate",		column="product_arrival_date"),
		@Result(property="productPurchaseManufactureDate",	column="product_manufacture_date"),
		@Result(property="productPurchaseExpiryDate",		column="product_expiry_date")
	})
	public List<ProductPurchase> retrieveProductPurchaseDetailsDAO(@Param("productID") long productID, @Param("firmDBName") String firmDBName);
	
	@SelectProvider(type = FoodBazaarProductQueryBuilder.class, method = "retrieveProductSalesDetailsQuery")
	@Results({
		@Result(property="productSalesID",					column="sales_id"),
		@Result(property="dealerName",						column="dealer_name"),
		@Result(property="invoiceNumber",					column="invoice_number"),
		@Result(property="productSalesDiscount",			column="sales_discount"),
		@Result(property="productSalesPrice",				column="sales_price"),
		@Result(property="productSalesTax",					column="sales_tax"),
		@Result(property="productSalesCess",				column="sales_cess"),
		@Result(property="productSoldQuantity",				column="sold_qty")
	})
	public List<ProductSales> retrieveProductSalesDetailsDAO(@Param("productID") long productID, @Param("firmDBName") String firmDBName);

	
	
}
