package com.simbls.prism.rest.foodBazaar.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.dao.foodBazaar.FoodBazaarPaymentDao;
import com.simbls.prism.rest.foodBazaar.service.FoodBazaarPaymentService;
import com.simbls.prism.rest.model.industry.Invoice;
import com.simbls.prism.rest.model.industry.Payment;

@Service
public class FoodBazaarPaymentServiceImpl implements FoodBazaarPaymentService {
	private static final Logger logger = Logger.getLogger(FoodBazaarPaymentServiceImpl.class);
	@Autowired private FoodBazaarPaymentDao foodBazaarPaymentDAO;

	/**
	 * 
	 */
	public void paymentRegister(Payment paymentData) {
		logger.info("Initiating insertion into Payment Table - Target DB " + paymentData.getFirmDBName() + " ::: REST ::: Payment Service Impl");
		foodBazaarPaymentDAO.paymentRegisterDAO(paymentData);
		logger.info("Payment Successfully Populated ::: REST ::: Payment Service Impl");
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	public Payment retrieveTodaysPaymentDetails(Payment paymentData) {
		logger.info("Initiating DB search to retrieve Payment List. Target DB - " + paymentData.getFirmDBName() + "::: REST ::: FoodBazaarPaymentService");
		Payment returningPaymentObject = new Payment();
		List<Payment> todaysIssuedPaymentDetails = foodBazaarPaymentDAO.retrieveTodaysIssuedPaymentDetailsDAO(paymentData);
		logger.info("Successfully obtained - " + todaysIssuedPaymentDetails.size() + " todays Issued Payments");
		List<Payment> todaysEnCashedPaymentDetails  = foodBazaarPaymentDAO.retrieveTodaysEnCashedPaymentDetailsDAO(paymentData);
		logger.info("Successfully obtained - " + todaysEnCashedPaymentDetails.size() + " todays Encashed Payments");
		returningPaymentObject.setTodaysIssuedPaymentDetails(todaysIssuedPaymentDetails);
		returningPaymentObject.setTodaysEnCashedPaymentDetails(todaysEnCashedPaymentDetails);
		logger.info("Returning to Payment Controller ::: REST ::: Payment Service IMPL");
		return returningPaymentObject;
	}
	
	/**
	 * 
	 * @return
	 */
	public Payment retrieveConditionalPaymentDetails(Payment paymentData) {
		logger.info("Initiating DB search to retrieve Payment List. Starting date - " + paymentData.getBillingStartingDate() + " to Ending Date - " + paymentData.getBillingEndingDate() + " Target DB - " + paymentData.getFirmDBName() + "::: REST ::: FoodBazaarPaymentService");
		Payment returningPaymentObject = new Payment();
		List<Payment> conditionalIssuedPaymentDetails = foodBazaarPaymentDAO.retrieveConditionalIssuedPaymentDetailsDAO(paymentData);
		logger.info("Successfully obtained - " + conditionalIssuedPaymentDetails.size() + " todays Issued Payments");
		List<Payment> conditionalEnCashedPaymentDetails  = foodBazaarPaymentDAO.retrieveConditionalEnCashedPaymentDetailsDAO(paymentData);
		logger.info("Successfully obtained - " + conditionalEnCashedPaymentDetails.size() + " todays Encashed Payments");
		returningPaymentObject.setConditionalIssuedPaymentDetails(conditionalIssuedPaymentDetails);
		returningPaymentObject.setConditionalEnCashedPaymentDetails(conditionalEnCashedPaymentDetails);
		logger.info("Returning to Payment Controller ::: REST ::: Payment Service IMPL");
		return returningPaymentObject;
	}

}
