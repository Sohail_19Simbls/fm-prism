package com.simbls.prism.rest.ceramics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.ceramics.service.CeramicsProductPurchaseService;
import com.simbls.prism.rest.model.industry.ProductPurchase;

@Controller
@RequestMapping(value = "/ceramics_productPurchase")
public class CeramicsProductPurchaseController {
	private static final Logger logger = Logger.getLogger(CeramicsProductPurchaseController.class);
	@Autowired private CeramicsProductPurchaseService ceramicsProductPurchaseService;
	/**
	 * Product Purchase Registration
	 * @param productPurchaseData
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@ResponseBody
	@RequestMapping(value = "/productPurchaseDetailsRegister", method = RequestMethod.POST)
	public ResponseEntity<List<ProductPurchase>> productPurchaseDetailsRegister(@RequestBody List<ProductPurchase> productPurchaseData) throws Exception{
		logger.info("... Continuing Product Purchase Registration for Product of size- " + productPurchaseData.size() + "::: REST ::: CeramicsProductPurchaseController");
		List<ProductPurchase> returnedObject = ceramicsProductPurchaseService.productPurchaseDetailsRegister(productPurchaseData);
		logger.info("Returning to WEB ::: REST ::: CeramicsProductPurchaseController");
		return ResponseEntity.accepted().body(productPurchaseData);
	}
	
	/**
	 * Retrieving list of all the Registered ProductPurchases
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value="/retrieveProductPurchaseDetails",method=RequestMethod.POST)
	public ResponseEntity<List<ProductPurchase>> retrieveProductPurchaseDetails(@RequestBody ProductPurchase productPurchaseData) throws Exception{
		logger.info("Continuing retrival of Product Purchase Details ::: REST ::: CeramicsProductPurchaseController");
		List<ProductPurchase> retrieveProductPurchaseList = ceramicsProductPurchaseService.retrieveProductPurchaseDetails(productPurchaseData);
		logger.info("Returning Obtained Product Purchase Details of size " + retrieveProductPurchaseList.size() + " to WEB ::: REST ::: CeramicsProductPurchaseController");
		return ResponseEntity.accepted().body(retrieveProductPurchaseList);
	}
	
	/**
	 * Check if all the Purchased Product Total matches the Invoice Amount
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@RequestMapping(value="/checkInvoiceStatus",method=RequestMethod.POST)
	public ResponseEntity<ProductPurchase> checkInvoiceStatus(@RequestBody ProductPurchase productPurchaseData) throws Exception{
		logger.info("Continuing retrival of Product Purchase Details ::: REST ::: CeramicsProductPurchaseController");
		ProductPurchase productPurchaseDetails = ceramicsProductPurchaseService.checkInvoiceStatus(productPurchaseData);
		logger.info("Returning Obtained Product Purchase Details to WEB ::: REST ::: CeramicsProductPurchaseController");
		return ResponseEntity.accepted().body(productPurchaseDetails);
	}

}
