package com.simbls.prism.rest.central.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.central.service.ProductCategoryService;
import com.simbls.prism.rest.model.central.ProductCategory;

@Controller
@RequestMapping(value="/productCategory")
public class ProductCategoryController {
	private static final Logger logger = Logger.getLogger(ProductCategoryController.class);
	
	@Autowired private ProductCategoryService productCategoryService;
	
	/**
	 * Retrieve Product Categories from Central DB
	 * @return
	 * @author Sohail Razvi
	 */
	@ResponseBody
	@RequestMapping(value="/retrieveProductCategories", method = RequestMethod.POST)
	public ResponseEntity<List<ProductCategory>> retrieveProductCategories(@RequestBody ProductCategory productCategoryData) {
		logger.info("... Continuing retrival of Product Category from Central DB ::: REST ::: ProductCategoryController");
		List<ProductCategory> productCategoryList = productCategoryService.retrieveProductCategories(productCategoryData);
		logger.info("Returning obtained Product Categories, Size - " + productCategoryList.size() + " ::: WEB ::: ProductCategoryController");
		return ResponseEntity.accepted().body(productCategoryList);
	}

}
