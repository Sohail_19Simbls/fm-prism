package com.simbls.prism.rest.central.builder;

import java.util.Map;

import org.apache.log4j.Logger;

public class UserQueryBuilder {

	private static final Logger logger = Logger.getLogger(UserQueryBuilder.class);

	/**
	 * 
	 * @param parameters
	 * @return
	 */
	public String getUsersProvider(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to obtain User Object ::: REST ::: Firm Query Builder");
		Long userId =  (Long) parameters.get("userId");
		String userName = (String) parameters.get("userName");
		String userPassword = (String) parameters.get("userPassword");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("select * from fmprism_central.cnt_user");
		logger.info("UserID Value - " + userId.longValue());
		logger.info("UserName Value - " + userName.toString());
		logger.info("UserPassword Value - " + userPassword.toString());
					
		if((userId.longValue() == 0) && ((userName.toString() != null) && userPassword.toString() != null)){
			SelectUserQuery.append(" where cnt_user_username='" + userName + "' and cnt_user_password='" + userPassword + "'");
		}
		
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
}
