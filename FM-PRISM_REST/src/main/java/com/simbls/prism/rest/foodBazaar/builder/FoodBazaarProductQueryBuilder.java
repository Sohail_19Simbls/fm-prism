package com.simbls.prism.rest.foodBazaar.builder;

import java.util.Map;

import org.apache.log4j.Logger;

import com.simbls.prism.rest.model.industry.Product;
import com.simbls.prism.rest.model.industry.SalesBasket;

public class FoodBazaarProductQueryBuilder {

	private static final Logger logger = Logger.getLogger(FoodBazaarProductQueryBuilder.class);
	
	
	/**
	 * Building a query to retrieve Product.
	 * If the Product Name is null 
	 * @param parameters
	 * @return
	 * @author Sohail Razvi
	 */
	
	
	public String retrieveProductByParameterQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Product operations. Captured Object - " + parameters.toString() + ". ::: REST ::: FoodBazaarProductQueryBuilder");
		Product productData = (Product) parameters.get("productData");
		String firmDBName = productData.getFirmDBName();
		logger.info("Retrieving Product target DBName - " + productData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		/**
		 * Creating a condition-less SELECT query.
		 */
		SelectUserQuery.append("SELECT * FROM " + firmDBName + ".frm_product");
		/**
		 * Validation for productName name empty. If ProductName name is not empty, create Query.
		 */
		if(productData.getProductID() != 0){
			SelectUserQuery.append(" WHERE frm_product_id =" + productData.getProductID());
		}
		else if(productData.getProductName() != null && !productData.getProductName().isEmpty()){
			SelectUserQuery.append(" WHERE frm_product_name like '" + productData.getProductName() + "%'");
		} 
		/**
		 * Validation for barCode not empty. If BarCode is not empty, Create Query.
		 */
		else if(productData.getProductBarCode() != null && !productData.getProductBarCode().isEmpty()){
			SelectUserQuery.append(" WHERE frm_product_barcode = '" + productData.getProductBarCode() + "'");
		}
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarProductQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	
	public String retrieveDetailProductListQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Product operations. Captured Object - " + parameters.toString() + ". ::: REST ::: FoodBazaarProductQueryBuilder");
		Product productData = (Product) parameters.get("productData");
		String firmDBName = productData.getFirmDBName();
		logger.info("Retrieving Product target DBName - " + productData.getFirmDBName().toString());
		StringBuilder retrieveProductPurchaseSalesDetails = new StringBuilder();
		retrieveProductPurchaseSalesDetails.append("SELECT '"
				+ firmDBName + "' firm_DB_name, "
				+ " product.frm_product_id product_id, "
				+ " product.frm_product_company product_company, "
				+ " product.frm_product_name product_name, "
				+ " product.frm_product_packed_quantity packed_product_quantity, "
				+ " product.frm_product_packed_unit packed_product_unit, "
				+ " product.frm_product_category_id product_category, "
				+ " product.frm_product_sub_category_id product_sub_category, "
				+ " product.frm_product_barcode product_barcode "
				+ " FROM "
				+  firmDBName + ".frm_product product");
		/**
		 * Validation for productID. If productID is not empty, create Query.
		 */
		if(productData.getProductID() != 0){
			retrieveProductPurchaseSalesDetails.append(" WHERE frm_product_id =" + productData.getProductID() +
														" ORDER BY frm_product_id");
		}
		
		logger.info("Generated Query - " + retrieveProductPurchaseSalesDetails.toString() + " ::: REST ::: FoodBazaarProductQueryBuilder");
		return retrieveProductPurchaseSalesDetails.toString();
	}
	
	public String retrieveProductPurchaseDetailsQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Product operations. Captured Object - " + parameters.toString() + ". ::: REST ::: FoodBazaarProductQueryBuilder");
		long productID =  (Long) parameters.get("productID");
		String firmDBName = (String) parameters.get("firmDBName");
		logger.info("Retrieving Product Purchase Details from target DBName - " + firmDBName.toString());
		StringBuilder retrieveProductPurchaseDetailsQuery = new StringBuilder();
		
		retrieveProductPurchaseDetailsQuery.append("SELECT "
				+ " invoice.frm_invoice_number invoice_number, "
				+ " dealer.frm_dealer_name dealer_name, "
				+ " purchase.frm_purchase_mrp product_mrp, "
				+ " purchase.frm_purchase_tax product_tax, "
				+ " purchase.frm_purchase_cess product_cess, "
				+ " purchase.frm_purchase_packed_quantity product_packed_quantity, "
				+ " purchase.frm_purchase_unit product_packed_unit, "
				+ " purchase.frm_purchase_unit_cost product_unit_cost, "
				+ " purchase.frm_purchase_paid_qty product_paid_qty, "
				+ " purchase.frm_purchase_free_qty product_free_qty, "
				+ " purchase.frm_product_threshold_qty product_threshold_qty, "
				+ " purchase.frm_purchase_primary_discount product_primary_disc, "
				+ " purchase.frm_purchase_secondary_discount product_secondary_disc, "
				+ " purchase.frm_purchase_arrival_date product_arrival_date, "
				+ " purchase.frm_purchase_manufacture_date product_manufacture_date, "
				+ " purchase.frm_purchase_expiry_date product_expiry_date "
				+ " FROM " + firmDBName + ".frm_product_purchase purchase "
				+ " JOIN " + firmDBName + ".frm_invoice invoice "
				+ " JOIN " + firmDBName + ".frm_dealer dealer "
				+ " ON purchase.frm_purchase_invoice_id = invoice.frm_invoice_id "
				+ " AND purchase.frm_purchase_dealer_id = dealer.frm_dealer_id "
				+ " WHERE frm_purchase_product_id = " + productID);
		
		logger.info("Generated Query - " + retrieveProductPurchaseDetailsQuery.toString() + " ::: REST ::: FoodBazaarProductQueryBuilder");
		return retrieveProductPurchaseDetailsQuery.toString();
	}
	
	public String retrieveProductSalesDetailsQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Product operations. Captured Object - " + parameters.toString() + ". ::: REST ::: FoodBazaarProductQueryBuilder");
		long productID =  (Long) parameters.get("productID");
		String firmDBName = (String) parameters.get("firmDBName");
		logger.info("Retrieving Product Sales Details from target DBName - " + firmDBName.toString());
		StringBuilder retrieveSalesDetailsQuery = new StringBuilder();
		
		retrieveSalesDetailsQuery.append("SELECT "
				+ " sales.frm_sales_id sales_id, "
				+ " dealer.frm_dealer_name dealer_name, "
				+ " invoice.frm_invoice_number invoice_number, "
				+ " sales.frm_sales_discount sales_discount, "
				+ " sales.frm_sales_price sales_price, "
				+ " sales.frm_sales_tax sales_tax, "
				+ " sales.frm_sales_cess sales_cess, "
				+ " sales.frm_sales_sold_quantity sold_qty "
				+ " FROM " + firmDBName + ".frm_product_sales sales "
				+ " JOIN " + firmDBName + ".frm_dealer dealer "
				+ " JOIN " + firmDBName + ".frm_invoice invoice "
				+ " ON sales.frm_sales_dealer_id = dealer.frm_dealer_id "
				+ " AND sales.frm_sales_invoice_id = invoice.frm_invoice_id "
				+ " WHERE frm_sales_product_id =" + productID
				+ " ORDER BY sales.frm_sales_id");
		
		logger.info("Generated Query - " + retrieveSalesDetailsQuery.toString() + " ::: REST ::: FoodBazaarProductQueryBuilder");
		return retrieveSalesDetailsQuery.toString();
	}

}
