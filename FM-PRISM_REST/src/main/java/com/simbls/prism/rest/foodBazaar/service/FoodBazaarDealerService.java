package com.simbls.prism.rest.foodBazaar.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.industry.Dealer;

@Component
public interface FoodBazaarDealerService {
	/**
	 * Register Dealer details 
	 * @param dealer
	 * @throws Exception
	 */
	public Dealer dealerRegister(Dealer dealer) throws Exception;
	
	/**
	 * Update Dealer details 
	 * @param dealer
	 * @throws Exception
	 */
	public Dealer dealerUpdate(Dealer dealer) throws Exception;
	
	
	/**
	 * Update Dealer details 
	 * @param dealer
	 * @throws Exception
	 */
	public void dealerDelete(Dealer dealer) throws Exception;
	/**
	 * Retrieve List of Registered Dealers
	 * @return
	 */
	public List<Dealer> retrieveDealerList(Dealer dealerData);
	
	
	/**
	 * Retrieve List of Registered Dealers
	 * @return
	 */
	public Dealer retrieveDealerDetails(Dealer dealerData);
}
