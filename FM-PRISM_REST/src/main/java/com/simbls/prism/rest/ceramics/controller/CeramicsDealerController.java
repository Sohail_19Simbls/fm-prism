package com.simbls.prism.rest.ceramics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.ceramics.service.CeramicsDealerService;
import com.simbls.prism.rest.model.industry.Dealer;


@Controller
@RequestMapping(value = "/ceramics_dealer")
public class CeramicsDealerController {
	private static final Logger logger = Logger.getLogger(CeramicsDealerController.class);
	@Autowired private CeramicsDealerService ceramicsDealerService;
	
	/**
	 * Populating DB with Dealer Details
	 * @param dealer
	 * @param firmDBCredentials
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/dealerRegister", method = RequestMethod.POST)
	public Dealer dealerRegister(@RequestBody Dealer dealerData) throws Exception{
		logger.info("... Continuing Dealer Registration ::: REST ::: CeramicsDealerController");
		Dealer insertedDealer = ceramicsDealerService.dealerRegister(dealerData);
		logger.info("Returning to WEB ::: REST ::: Dealer Controller");
		return insertedDealer;
	}
	
	/**
	 * Updating DB with Dealer Details
	 * @param dealer
	 * @param firmDBCredentials
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/dealerUpdate", method = RequestMethod.POST)
	public Dealer dealerUpdate(@RequestBody Dealer dealerData) throws Exception{
		logger.info("... Continuing Dealer Updation ::: REST ::: CeramicsDealerController");
		Dealer updatedDealer = ceramicsDealerService.dealerUpdate(dealerData);
		logger.info("Returning to WEB ::: REST ::: Dealer Controller");
		return updatedDealer;
	}
	
	/**
	 * Retrieving list of all the Registered Dealers
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/retrieveDealerList",method=RequestMethod.POST)
	public ResponseEntity<List<Dealer>> retrieveDealerList(@RequestBody Dealer dealerData){
		logger.info("Continuing retrival of Dealer list ::: REST ::: CeramicsDealerController");
		List<Dealer> retrieveDealerList = ceramicsDealerService.retrieveDealerList(dealerData);
		logger.info("Returning Obtained Dealer list of size " + retrieveDealerList.size() + " to WEB ::: REST ::: CeramicsDealerController");
		return ResponseEntity.accepted().body(retrieveDealerList);
	}
	
	/**
	 * Retrieving list of all the Registered Dealers
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/retrieveDealerDetails",method=RequestMethod.POST)
	public ResponseEntity<Dealer> retrieveDealerDetails(@RequestBody Dealer dealerData){
		logger.info("Continuing retrival of Dealer list ::: REST ::: CeramicsDealerController");
		Dealer retrieveDealerDetails = ceramicsDealerService.retrieveDealerDetails(dealerData);
		logger.info("Returning Obtained Dealer details to WEB ::: REST ::: CeramicsDealerController");
		return ResponseEntity.accepted().body(retrieveDealerDetails);
	}
}
