package com.simbls.prism.rest.clothing.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.industry.Dealer;

@Component
public interface ClothingDealerService {
	/**
	 * Register Dealer details 
	 * @param dealer
	 * @throws Exception
	 */
	public Dealer dealerRegister(Dealer dealer) throws Exception;
	
	/**
	 * Update Dealer details 
	 * @param dealer
	 * @throws Exception
	 */
	public Dealer dealerUpdate(Dealer dealer) throws Exception;
	
	/**
	 * Retrieve List of Registered Dealers
	 * @return
	 */
	public List<Dealer> retrieveDealerList(Dealer dealerData);
	
	
	/**
	 * Retrieve List of Registered Dealers
	 * @return
	 */
	public Dealer retrieveDealerDetails(Dealer dealerData);
}
