package com.simbls.prism.rest.central.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.central.ProductCategory;

@Component
public interface ProductCategoryService {
	/**
	 * Auto Retrieve all the Product Categories
	 * @return List of Product Categories
	 * @author Sohail Razvi
	 */
	public List<ProductCategory> retrieveProductCategories(ProductCategory productCategoryData);

}
