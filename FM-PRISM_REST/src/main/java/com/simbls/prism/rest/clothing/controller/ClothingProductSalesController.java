package com.simbls.prism.rest.clothing.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.clothing.service.ClothingProductSalesService;
import com.simbls.prism.rest.model.industry.ProductSales;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Controller
@RequestMapping(value = "/clothing_productSales")
public class ClothingProductSalesController {
	private static final Logger logger = Logger.getLogger(ClothingProductSalesController.class);
	@Autowired private ClothingProductSalesService clothingProductSalesService;
	
	/**
	 * 
	 * @param productSalesData
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/productSalesRegister", method = RequestMethod.POST)
	public ProductSales productSalesRegister(@RequestBody ProductSales productSalesData) throws Exception{
		logger.info("... Continuing Product Sales Registration ::: REST ::: FoodBazaarProductSalesController");
		ProductSales productSalesObject = clothingProductSalesService.productSalesRegister(productSalesData);
		logger.info("Returning to WEB ::: REST ::: FoodBazaarProductSalesController");
		return productSalesObject;
	}
	
	/**
	 * 
	 * @param salesBasketData
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/productSalesBasket", method = RequestMethod.POST)
	public ResponseEntity<SalesBasket> productSalesBasket(@RequestBody SalesBasket salesBasketData) throws Exception{
		logger.info("... Continuing Product Sales into Sales Basket ::: REST ::: FoodBazaarProductSalesController");
		SalesBasket salesBasketObject = clothingProductSalesService.productSalesBasket(salesBasketData);
		logger.info("Returning to WEB ::: REST ::: FoodBazaarProductSalesController");
		return ResponseEntity.accepted().body(salesBasketObject);
	}
	
	/**
	 * Initiate population of Sale Bill and Sale Bill Consolidated Table 
	 * @param salesBasketData
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/generateSalesBasketBill", method = RequestMethod.POST)
	public ResponseEntity<List<SalesBasket>> generateSalesBasketBill(@RequestBody List<SalesBasket> salesBasketData) throws Exception{
		logger.info("... Continuing Product Sales into Sales Basket ::: REST ::: FoodBazaarProductSalesController");
		List<SalesBasket> salesBasketObject = clothingProductSalesService.generateSalesBasketBill(salesBasketData);
		logger.info("Returning to WEB ::: REST ::: FoodBazaarProductSalesController");
		return ResponseEntity.accepted().body(salesBasketObject);
	}

}
