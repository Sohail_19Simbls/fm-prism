package com.simbls.prism.rest.foodBazaar.builder;

import java.util.Map;

import org.apache.log4j.Logger;

import com.simbls.prism.rest.model.industry.Dealer;

public class FoodBazaarDealerQueryBuilder {

	private static final Logger logger = Logger.getLogger(FoodBazaarDealerQueryBuilder.class);
	
	/**
	 * Building a query to retrieve Dealer.
	 * If the Dealer Name is null 
	 * @param parameters
	 * @return
	 * @author Sohail Razvi
	 */
	
	
	public String retrieveDealerListQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Dealer operations. Captured Object - " + parameters.toString() + ". ::: REST ::: FoodBazaarDealerQueryBuilder");
		Dealer dealerData = (Dealer) parameters.get("dealerData");
		logger.info("Retrieving Delears target DBName - " + dealerData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("SELECT * from " + dealerData.getFirmDBName() + ".frm_dealer");
		/**
		 * Validation if dealer ID is not empty. Select Dealer based on DEaler ID.
		 */
		if(dealerData.getDealerID() != 0){
			SelectUserQuery.append(" WHERE frm_dealer_id =" + dealerData.getDealerID());
		}
		/**
		 * Validation for dealer name not empty. Select Dealer based on typed character.
		 */
		else if(dealerData.getDealerName() != null && !dealerData.getDealerName().isEmpty()){
			SelectUserQuery.append(" WHERE frm_dealer_name like '" + dealerData.getDealerName() + "%'");
		}
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}

}
