package com.simbls.prism.rest.dao.central;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Component;

import com.simbls.prism.rest.central.builder.ProductCategoryQueryBuilder;
import com.simbls.prism.rest.model.central.ProductCategory;

@Component("productCategoryDao")
public interface ProductCategoryDao {
	
	/**
	 * Select all the Product Categories from Central DB based on Industry Type
	 * @return List of Product Category Objects
	 * @author Sohail Razvi
	 */
	@SelectProvider(type = ProductCategoryQueryBuilder.class, method = "retrieveProductCategoriesQuery")
	@Results({
		@Result(property="productCategoryID",		column="cnt_product_category_id"),
		@Result(property="productCategory",			column="cnt_product_category")
	})
	public List<ProductCategory> retrieveProductCategoriesDAO(@Param("productCategoryData") ProductCategory productCategoryData);
	
	/**
	 * Select all the Product Categories from Central DB
	 * @return List of Product Category Objects
	 * @author Sohail Razvi
	 */
	@Select("SELECT * FROM cnt_product_category WHERE cnt_product_category_id = #{productCategoryID}")
	@Results({
		@Result(property="productCategoryID",		column="cnt_product_category_id"),
		@Result(property="productCategory",			column="cnt_product_category")
	})
	public ProductCategory retrieveSpecificProductCategoriesDAO(@Param("productCategoryID") int productCategoryID);


}
