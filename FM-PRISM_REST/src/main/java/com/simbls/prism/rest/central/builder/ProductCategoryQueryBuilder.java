package com.simbls.prism.rest.central.builder;

import java.util.Map;

import org.apache.log4j.Logger;

import com.simbls.prism.rest.model.central.ProductCategory;

public class ProductCategoryQueryBuilder {
	
	private static final Logger logger = Logger.getLogger(ProductCategoryQueryBuilder.class);

	public String retrieveProductCategoriesQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to obtain Product Category ::: REST ::: Firm Query Builder");
		ProductCategory productCategoryData =  (ProductCategory) parameters.get("productCategoryData");
		StringBuilder SelectProductCategoryQuery = new StringBuilder();
		SelectProductCategoryQuery.append("SELECT * FROM fmprism_central.cnt_product_category WHERE cnt_product_industry_id = " + productCategoryData.getProductIndustryID());		
		logger.info("Generated Query - " + SelectProductCategoryQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectProductCategoryQuery.toString();
	}
	
}
