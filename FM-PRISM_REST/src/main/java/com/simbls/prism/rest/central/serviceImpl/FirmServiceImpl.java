package com.simbls.prism.rest.central.serviceImpl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.central.service.FirmService;
import com.simbls.prism.rest.dao.central.FirmDao;
import com.simbls.prism.rest.model.central.Firm;


@Service
public class FirmServiceImpl implements FirmService {
	private static final Logger logger = Logger.getLogger(FirmServiceImpl.class);
	@Autowired private FirmDao firmDao;

	/**
	 * 
	 */
	public void firmRegister(Firm firmData) {
		logger.info("Initiating insertion into Firm Table - Target DB - FM-PRISM Central ::: REST ::: Firm Service Impl");
		firmDao.firmRegisterDAO(firmData);
		firmDao.createFirmDBDAO(firmData);
		firmDao.createFirmDealerTableDAO(firmData);
		firmDao.createFirmProductMapTableDAO(firmData);
		firmDao.createFirmDiscountTableDAO(firmData);
		firmDao.createFirmDiscountMapperTableDAO(firmData);
		firmDao.createFirmEmployeeTableDAO(firmData);
		firmDao.createFirmExpenseTableDAO(firmData);
		firmDao.createFirmInvoiceTableDAO(firmData);
		firmDao.createFirmPaymentTableDAO(firmData);
		firmDao.createFirmProductTableDAO(firmData);
		firmDao.createFirmProductMapperTableDAO(firmData);
		firmDao.createFirmProductPurchaseTableDAO(firmData);
		firmDao.createFirmProductSalesTableDAO(firmData);
		firmDao.createFirmPurchaseBillConsolidatedTableDAO(firmData);
		firmDao.createFirmSalesBillTableDAO(firmData);
		firmDao.createFirmSalesBillConsolidatedTableDAO(firmData);
		
		logger.info("Firm Successfully Populated ::: REST ::: Firm Service Impl");
	}
	

}
