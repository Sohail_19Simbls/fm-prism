package com.simbls.prism.rest.dao.central;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.central.ProductSubCategory;

@Component("productSubCategoryDao")
public interface ProductSubCategoryDao {
	
	/**
	 * Select all the Product Categories from Central DB
	 * @return List of Product Category Objects
	 * @author Sohail Razvi
	 */
	@Select("SELECT * from fmprism_central.cnt_product_subcategory WHERE cnt_product_category_id=#{productCategoryID}")
	@Results({
		@Result(property="productSubCategoryID",	column="cnt_product_subcategory_id"),
		@Result(property="productCategoryID",		column="cnt_product_category_id"),
		@Result(property="productSubCategory",		column="cnt_product_subcategory")	})
	public List<ProductSubCategory> retrieveProductSubCategoriesDAO(@Param("productCategoryID")  int productCategoryID);
	
	/**
	 * Select all the Product Categories from Central DB
	 * @return List of Product Category Objects
	 * @author Sohail Razvi
	 */
	@Select("SELECT * from fmprism_central.cnt_product_subcategory WHERE cnt_product_subcategory_id=#{productSubCategoryID}")
	@Results({
		@Result(property="productSubCategoryID",	column="cnt_product_subcategory_id"),
		@Result(property="productCategoryID",		column="cnt_product_category_id"),
		@Result(property="productSubCategory",		column="cnt_product_subcategory")})
	public ProductSubCategory retrieveSpecificProductSubCategoriesDAO(@Param("productCategoryID") int productSubCategoryID);

}
