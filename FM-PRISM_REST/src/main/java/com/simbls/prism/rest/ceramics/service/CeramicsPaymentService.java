package com.simbls.prism.rest.ceramics.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.industry.Invoice;
import com.simbls.prism.rest.model.industry.Payment;

@Component
public interface CeramicsPaymentService {
	/**
	 * Register Payment details 
	 * @param payment
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public void paymentRegister(Payment payment) throws Exception;
	
	
	/**
	 * Retrieve List of Payments For Today
	 * @return
	 * @author Sohail Razvi
	 */
	public Payment retrieveTodaysPaymentDetails(Payment paymentData);
	/**
	 * Retrieve List of Registered Payments
	 * @return
	 * @author Sohail Razvi
	 */
	public List<Invoice> retrievePaymentDetails(Invoice paymentData);
}
