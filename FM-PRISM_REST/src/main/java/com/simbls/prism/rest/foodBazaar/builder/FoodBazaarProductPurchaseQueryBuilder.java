package com.simbls.prism.rest.foodBazaar.builder;

import java.util.Map;

import org.apache.ibatis.annotations.Select;
import org.apache.log4j.Logger;

import com.simbls.prism.rest.model.industry.ProductPurchase;

public class FoodBazaarProductPurchaseQueryBuilder {

	private static final Logger logger = Logger.getLogger(FoodBazaarProductPurchaseQueryBuilder.class);
	
	
	public String productPurchaseConsolidated_UPSERT_Query(@SuppressWarnings("rawtypes") Map parameters){
		logger.info("Initiating query builder for Insertion of Consolidated Product Purchase Details ::: REST ::: FoodBazaarProductPurchaseQueryBuilder");
		ProductPurchase productPurchaseData = (ProductPurchase) parameters.get("productPurchaseData");
		logger.info("Retrieving Product Purchase Details - Target DBName - " + productPurchaseData.getFirmDBName().toString());
		StringBuilder purchaseConsolidatedUPSERTQuery = new StringBuilder();

		if(productPurchaseData.getProductPurchase_For_0_Tax_Amount() == null){
			productPurchaseData.setProductPurchase_For_0_Tax_Amount("0"); 
		};
		if(productPurchaseData.getProductPurchase_For_5_Tax_Amount() == null){
			productPurchaseData.setProductPurchase_For_5_Tax_Amount("0");
		};
		if(productPurchaseData.getProductPurchase_For_12_Tax_Amount() == null){
			productPurchaseData.setProductPurchase_For_12_Tax_Amount("0");
		};
		if(productPurchaseData.getProductPurchase_For_18_Tax_Amount() == null){
			productPurchaseData.setProductPurchase_For_18_Tax_Amount("0");
		};
		if(productPurchaseData.getProductPurchase_For_28_Tax_Amount() == null){
			productPurchaseData.setProductPurchase_For_28_Tax_Amount("0");
		};
		if(productPurchaseData.getProductPurchase_Effective_CGST() == null){
			productPurchaseData.setProductPurchase_Effective_CGST("0");
		};
		if(productPurchaseData.getProductPurchase_Effective_SGST() == null){
			productPurchaseData.setProductPurchase_Effective_SGST("0");
		};
		if(productPurchaseData.getProductPurchase_Effective_CESS() == null){
			productPurchaseData.setProductPurchase_Effective_CESS("0");
		}
		purchaseConsolidatedUPSERTQuery.append("INSERT INTO " + productPurchaseData.getFirmDBName() + ".frm_purchase_bill_consolidated"
				+ " (frm_purchase_dealer_id,"
				+ " frm_purchase_invoice_id,"
				+ " frm_purchase_date,"
				+ " frm_purchase_gst_number,"
				+ " frm_purchase_0,"
				+ " frm_purchase_5,"
				+ " frm_purchase_12,"
				+ " frm_purchase_18,"
				+ " frm_purchase_28,"
				+ " frm_purchase_cgst,"
				+ " frm_purchase_sgst,"
				+ " frm_purchase_cess,"
				+ " frm_purchase_entered_date)"
		+ " VALUES ("
				+ productPurchaseData.getDealerID() + ", "
				+ productPurchaseData.getInvoiceID() + ", "
				+ "'" + productPurchaseData.getProductPurchaseEnteredDate() + "', "		
				+   "'', "		
				+ productPurchaseData.getProductPurchase_For_0_Tax_Amount() + ", "
				+ productPurchaseData.getProductPurchase_For_5_Tax_Amount() + ", "
				+ productPurchaseData.getProductPurchase_For_12_Tax_Amount() + ", "
				+ productPurchaseData.getProductPurchase_For_18_Tax_Amount() + ", "
				+ productPurchaseData.getProductPurchase_For_28_Tax_Amount() + ", "
				+ productPurchaseData.getProductPurchase_Effective_CGST() + ", "
				+ productPurchaseData.getProductPurchase_Effective_SGST() + ", "
				+ productPurchaseData.getProductPurchase_Effective_CESS() + ", "
				+ "'" + productPurchaseData.getProductPurchaseEnteredDate() + "') "
		+ " ON DUPLICATE KEY UPDATE "
				+ " frm_purchase_0 = frm_purchase_0 + VALUES(frm_purchase_0),"
				+ " frm_purchase_5 = frm_purchase_5 + VALUES(frm_purchase_5),"
				+ " frm_purchase_12 = frm_purchase_12 + VALUES(frm_purchase_12),"
				+ " frm_purchase_18 = frm_purchase_18 + VALUES(frm_purchase_18),"
				+ " frm_purchase_28 = frm_purchase_28 + VALUES(frm_purchase_28),"
				+ " frm_purchase_cgst = frm_purchase_cgst + VALUES(frm_purchase_cgst),"
				+ " frm_purchase_sgst = frm_purchase_sgst + VALUES(frm_purchase_sgst),"
				+ " frm_purchase_cess = frm_purchase_cess + VALUES(frm_purchase_cess)");
		logger.info("Generated Query - " + purchaseConsolidatedUPSERTQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return purchaseConsolidatedUPSERTQuery.toString();
	}


	/**
	 * 
	 * @param parameters
	 * @return
	 * @author Sohail Razvi
	 */
	public String retrieveProductPurchaseDetailsQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder to retrieve Product Purchase Details ::: REST ::: FoodBazaarProductPurchaseQueryBuilder");
		ProductPurchase productPurchaseData = (ProductPurchase) parameters.get("productPurchaseData");
		logger.info("Retrieving Product Purchase Details - Target DBName - " + productPurchaseData.getFirmDBName().toString());
		StringBuilder retrieveProductPurchaseDetailsQuery = new StringBuilder();
		retrieveProductPurchaseDetailsQuery.append("SELECT "
				+ "'" + productPurchaseData.getFirmDBName() + "' firm_DB_name,"
				+ " purchase.frm_purchase_id purchase_id, "
				+ " purchase.frm_purchase_product_id product_id, "
				+ " purchase.frm_purchase_dealer_id dealer_id, "
				+ " purchase.frm_purchase_invoice_id invoice_id, "
				+ " purchase.frm_purchase_mrp purchase_mrp, "
				+ " purchase.frm_purchase_tax purchase_tax, "
				+ " purchase.frm_purchase_cess purchase_cess, "
				+ " purchase.frm_purchase_packed_quantity packed_quantity, "
				+ " purchase.frm_purchase_unit packed_unit, "
				+ " purchase.frm_purchase_unit_cost purchase_unit_cost, "
				+ " purchase.frm_purchase_paid_qty purchase_paid_qty, "
				+ " purchase.frm_purchase_free_qty purchase_free_qty, "
				+ " purchase.frm_product_threshold_qty threshold_qty, "
				+ " purchase.frm_purchase_primary_discount primary_discount, "
				+ " purchase.frm_purchase_secondary_discount secondary_discount "
				+ " FROM " + productPurchaseData.getFirmDBName() + ".frm_product_purchase purchase");
		/**
		 * Validating for empty DealerID. If DealerID is empty, select all the Payment.
		 */
		if(productPurchaseData.getDealerID() != 0 && productPurchaseData.getInvoiceID() != 0){
			retrieveProductPurchaseDetailsQuery.append(" WHERE frm_purchase_dealer_id=" + productPurchaseData.getDealerID() 
								+ " AND frm_purchase_invoice_id=" + productPurchaseData.getInvoiceID());
		}
				
		logger.info("Generated Query - " + retrieveProductPurchaseDetailsQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return retrieveProductPurchaseDetailsQuery.toString();
	}
	
	/**
	 * 
	 * @param parameters
	 * @return
	 */
	public String retrieveProductDetailsQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder to retrieve Product Purchase Details ::: REST ::: FoodBazaarProductPurchaseQueryBuilder");
		long productID = (Long) parameters.get("productID");
		String firmDBName = (String) parameters.get("firmDBName");
		logger.info("Retrieving Product Purchase Details - Target DBName - " + firmDBName);
		StringBuilder retrieveProductDetailsQuery = new StringBuilder();
		retrieveProductDetailsQuery.append("SELECT "
				+ " * FROM " + firmDBName + ".frm_product "
				+ "WHERE frm_product_id = " + productID);
		logger.info("Generated Query - " + retrieveProductDetailsQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return retrieveProductDetailsQuery.toString();
	}
	
	
	/**
	 * 
	 * @param parameters
	 * @return
	 * @author Sohail Razvi
	 */
	public String checkInvoiceStatusQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder to Check Invoice Status ::: REST ::: FoodBazaarProductPurchaseQueryBuilder");
		ProductPurchase productPurchaseData = (ProductPurchase) parameters.get("productPurchaseData");
		logger.info("Retrieving Product Purchase Details - Target DBName - " + productPurchaseData.getFirmDBName().toString());
		StringBuilder checkInvoiceStatusQuery = new StringBuilder();
		checkInvoiceStatusQuery.append("SELECT "
				+ " invoice.frm_invoice_id invoice_id, "
				+ " invoice.frm_invoice_dealer_id dealer_id, "
				+ " invoice.frm_invoice_number invoice_number, "
				+ " invoice.frm_invoice_amount invoice_amount, "
				+ " invoice.frm_invoice_market_fee market_fee, "
				+ " invoice.frm_invoice_transportation_unloading_fee extra_charges, "
				/* ------------------------------------- */ 
				+ " purchase.frm_purchase_product_id product_id, "
				+ " purchase.frm_purchase_tax product_tax, "
				+ " purchase.frm_purchase_cess product_cess, "
				+ " purchase.frm_purchase_primary_discount primary_discount, "
				+ " purchase.frm_purchase_secondary_discount secondary_discount, "
				+ " purchase.frm_purchase_unit_cost unit_cost, "
				+ " purchase.frm_purchase_paid_qty paid_quantity "
				/* ------------------------------------- */
				+ " FROM " + productPurchaseData.getFirmDBName() + ".frm_product_purchase purchase "
				+ " JOIN " + productPurchaseData.getFirmDBName() + ".frm_invoice invoice "
				+ " ON  invoice.frm_invoice_id = purchase.frm_purchase_invoice_id "
				+ " AND invoice.frm_invoice_dealer_id = purchase.frm_purchase_dealer_id "
				+ " AND   purchase.frm_purchase_dealer_id = " + productPurchaseData.getDealerID()
				+ " AND purchase.frm_purchase_invoice_id = " + productPurchaseData.getInvoiceID()
				+ " GROUP BY product_id");
		logger.info("Generated Query - " + checkInvoiceStatusQuery.toString() + " ::: REST ::: FoodBazaarProductPurchaseQueryBuilder");
		return checkInvoiceStatusQuery.toString();
	}
	


}
