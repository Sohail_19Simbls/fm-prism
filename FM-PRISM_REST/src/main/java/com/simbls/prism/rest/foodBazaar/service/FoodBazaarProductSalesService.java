package com.simbls.prism.rest.foodBazaar.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.industry.ProductSales;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Component
public interface FoodBazaarProductSalesService {
	/**
	 * Register Product Sales Details
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<ProductSales> productSalesRegister(List<ProductSales> productSalesData) throws Exception;
	
	/**
	 * 
	 * @param productSalesData
	 * @return
	 * @throws Exception
	 */
	public ProductSales productSalesDetailsUpdate(ProductSales productSalesData) throws Exception;
	
	/**
	 * Register Product Sales Details
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public SalesBasket productSalesBasket(SalesBasket salesBasketData) throws Exception;
	
	/**
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<SalesBasket> generateSalesBasketBill(List<SalesBasket> salesBasketData) throws Exception;
	
	
	/**
	 * Retrieve Todays Sales Details
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public SalesBasket retrieveTodaysSalesDetails(SalesBasket salesBasketData) throws Exception;
	
	/**
	 * Retrieve Customised Sales Details
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public SalesBasket retrieveCustomisedSalesDetails(SalesBasket salesBasketData) throws Exception;
}
