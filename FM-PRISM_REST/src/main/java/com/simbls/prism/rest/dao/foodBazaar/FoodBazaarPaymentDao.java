package com.simbls.prism.rest.dao.foodBazaar;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Component;

import com.simbls.prism.rest.foodBazaar.builder.FoodBazaarPaymentQueryBuilder;
import com.simbls.prism.rest.model.industry.Payment;

@Component("foodBazaarPaymentDAO")
public interface FoodBazaarPaymentDao {

	@Insert("INSERT into ${firmDBName}.frm_payment("
			+ "frm_payment_dealer_id,"
			+ "frm_payment_invoice_id,"
			+ "frm_payment_mode,"
			+ "frm_payment_transaction_number,"
			+ "frm_payment_issue_date,"
			+ "frm_payment_encash_date,"
			+ "frm_payment_amount,"
			+ "frm_payment_bank,"
			+ "frm_payment_entered_by,"
			+ "frm_payment_entered_date)"
	+ "values("
			+ " #{dealerID},"
			+ " #{invoiceID},"
			+ " #{paymentMode},"
			+ " #{paymentTransactionNumber},"
			+ " #{paymentIssuedDate},"
			+ " #{paymentEncashDate},"
			+ " #{paymentAmount},"
			+ " #{paymentBank},"
			+ " #{paymentEnteredBy},"
			+ " #{paymentEnteredDate})")
	
	public void paymentRegisterDAO(Payment paymentData);
	
	
	/**
	 * @param paymentData
	 * @return
	 */
	@SelectProvider(type = FoodBazaarPaymentQueryBuilder.class, method = "retrieveTodaysIssuedPaymentDetailsQuery")
	@Results({
		@Result(property="dealerName",							column="dealer_name"),
		@Result(property="invoiceID",							column="invoice_id"),
		@Result(property="invoiceNumber",						column="invoice_number"),
		@Result(property="invoiceBillingDate",					column="invoice_billing_date"),
		@Result(property="invoiceAmount",						column="invoice_amount"),
		@Result(property="invoiceMarketFees",					column="market_fee"),
		@Result(property="invoiceDeliveryDate",					column="invoice_delivery_date"),
		@Result(property="invoicePaymentDueDate",				column="payment_due_date"),
		@Result(property="paymentAmount",						column="amount_paid"),
		@Result(property="paymentTransactionNumber",			column="transaction_number"),
		@Result(property="paymentMode",							column="payment_mode"),
		@Result(property="paymentIssuedDate",					column="payment_issue_date"),
		@Result(property="paymentEncashDate",					column="payment_encash_date"),
		@Result(property="paymentStatus",						column="payment_status")
	})
	public List<Payment> retrieveTodaysIssuedPaymentDetailsDAO(@Param("paymentData") Payment paymentData);
	
	/**
	 * @param paymentData
	 * @return
	 */
	@SelectProvider(type = FoodBazaarPaymentQueryBuilder.class, method = "retrieveTodaysEnCashedPaymentDetailsQuery")
	@Results({
		@Result(property="dealerName",							column="dealer_name"),
		@Result(property="invoiceID",							column="invoice_id"),
		@Result(property="invoiceNumber",						column="invoice_number"),
		@Result(property="invoiceBillingDate",					column="invoice_billing_date"),
		@Result(property="invoiceAmount",						column="invoice_amount"),
		@Result(property="invoiceMarketFees",					column="market_fee"),
		@Result(property="invoiceDeliveryDate",					column="invoice_delivery_date"),
		@Result(property="invoicePaymentDueDate",				column="payment_due_date"),
		@Result(property="paymentAmount",						column="amount_paid"),
		@Result(property="paymentTransactionNumber",			column="transaction_number"),
		@Result(property="paymentMode",							column="payment_mode"),
		@Result(property="paymentIssuedDate",					column="payment_issue_date"),
		@Result(property="paymentEncashDate",					column="payment_encash_date"),
		@Result(property="paymentStatus",						column="payment_status")	
	})
	public List<Payment> retrieveTodaysEnCashedPaymentDetailsDAO(@Param("paymentData") Payment paymentData);
	
	/**
	 * @param paymentData
	 * @return
	 */
	@SelectProvider(type = FoodBazaarPaymentQueryBuilder.class, method = "retrieveConditionalIssuedPaymentDetailsQuery")
	@Results({
		@Result(property="dealerName",							column="dealer_name"),
		@Result(property="invoiceID",							column="invoice_id"),
		@Result(property="invoiceNumber",						column="invoice_number"),
		@Result(property="invoiceBillingDate",					column="invoice_billing_date"),
		@Result(property="invoiceAmount",						column="invoice_amount"),
		@Result(property="invoiceMarketFees",					column="market_fee"),
		@Result(property="invoiceDeliveryDate",					column="invoice_delivery_date"),
		@Result(property="invoicePaymentDueDate",				column="payment_due_date"),
		@Result(property="paymentAmount",						column="amount_paid"),
		@Result(property="paymentTransactionNumber",			column="transaction_number"),
		@Result(property="paymentMode",							column="payment_mode"),
		@Result(property="paymentIssuedDate",					column="payment_issue_date"),
		@Result(property="paymentEncashDate",					column="payment_encash_date"),
		@Result(property="paymentStatus",						column="payment_status")
	})
	public List<Payment> retrieveConditionalIssuedPaymentDetailsDAO(@Param("paymentData") Payment paymentData);
	
	/**
	 * @param paymentData
	 * @return
	 */
	@SelectProvider(type = FoodBazaarPaymentQueryBuilder.class, method = "retrieveConditionalEnCashedPaymentDetailsQuery")
	@Results({
		@Result(property="dealerName",							column="dealer_name"),
		@Result(property="invoiceID",							column="invoice_id"),
		@Result(property="invoiceNumber",						column="invoice_number"),
		@Result(property="invoiceBillingDate",					column="invoice_billing_date"),
		@Result(property="invoiceAmount",						column="invoice_amount"),
		@Result(property="invoiceMarketFees",					column="market_fee"),
		@Result(property="invoiceDeliveryDate",					column="invoice_delivery_date"),
		@Result(property="invoicePaymentDueDate",				column="payment_due_date"),
		@Result(property="paymentAmount",						column="amount_paid"),
		@Result(property="paymentTransactionNumber",			column="transaction_number"),
		@Result(property="paymentMode",							column="payment_mode"),
		@Result(property="paymentIssuedDate",					column="payment_issue_date"),
		@Result(property="paymentEncashDate",					column="payment_encash_date"),
		@Result(property="paymentStatus",						column="payment_status")	
	})
	public List<Payment> retrieveConditionalEnCashedPaymentDetailsDAO(@Param("paymentData") Payment paymentData);
	
	

}
