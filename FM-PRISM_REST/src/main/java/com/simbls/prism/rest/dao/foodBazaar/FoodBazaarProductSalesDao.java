package com.simbls.prism.rest.dao.foodBazaar;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.springframework.stereotype.Component;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.simbls.prism.rest.foodBazaar.builder.FoodBazaarProductSalesQueryBuilder;
import com.simbls.prism.rest.model.industry.ProductPurchase;
import com.simbls.prism.rest.model.industry.ProductSales;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Component("foodBazaarProductSalesDao")
public interface FoodBazaarProductSalesDao {

	@Insert("INSERT into ${firmDBName}.frm_product_sales("
			+ "frm_sales_product_id,"
			+ "frm_sales_dealer_id,"
			+ "frm_sales_invoice_id,"
			+ "frm_sales_discount,"
			+ "frm_sales_price,"
			+ "frm_sales_tax,"
			+ "frm_sales_cess,"
			+ "frm_sales_sold_quantity,"
			+ "frm_product_sales_entered_by,"
			+ "frm_product_sales_entered_date,"
			+ "frm_sales_rowstate)"
+ "values("
			+ " #{productID},"
			+ " #{dealerID},"
			+ " #{invoiceID},"
			+ " #{productSalesDiscount},"
			+ " #{productSalesPrice},"
			+ " #{productSalesTax},"
			+ " #{productSalesCess},"
			+ " #{productSoldQuantity},"
			+ " #{productSalesEnteredBy},"
			+ " #{productSalesEnteredDate},"
			+ " #{productSalesRowState})")
	
	public void productSalesRegisterDAO(ProductSales productSalesData)  throws MySQLIntegrityConstraintViolationException;
		
	/**
	 * 
	 * @param productSalesData
	 */
	@Update("UPDATE ${firmDBName}.frm_product_sales "
			+ " SET "
			+ " frm_sales_price = #{productSalesPrice}, "
			+ " frm_sales_sold_quantity = #{productSoldQuantity} "
			+ " WHERE frm_sales_id = #{productSalesID}")
	public void productSalesDetailsUpdateDAO(ProductSales productSalesData);
	
	/**
	 * 
	 * @param salesBasketData
	 * @return
	 */
	@InsertProvider(type = FoodBazaarProductSalesQueryBuilder.class, method = "generateSalesBasketBillQuery")
	public void generateSalesBasketBillDAO(@Param("salesBasketData") List<SalesBasket> salesBasketData);
	/**
	 * 
	 * @param salesBasketData
	 */
	@InsertProvider(type = FoodBazaarProductSalesQueryBuilder.class, method = "productSalesConsolidated_UPSERT_Query")
	public void productSalesConsolidated_UPSERT_DAO(@Param("salesBasketData") List<SalesBasket> salesBasketData);
	
	/**
	 * 
	 * @param salesBasketData
	 */
	@UpdateProvider(type = FoodBazaarProductSalesQueryBuilder.class, method = "updateSoldQuantityQuery")
	public void updateSoldQuantityDAO(@Param("salesBasketData") SalesBasket salesBasketData);
	
	/**
	 * 
	 * @param salesBasketData
	 */
	@UpdateProvider(type = FoodBazaarProductSalesQueryBuilder.class, method = "updatePurchaseRowStateQuery")
	public void updatePurchaseRowStateDAO(@Param("salesBasketData") SalesBasket salesBasketData);
	
	/**
	 * 
	 * @param salesBasketData
	 */
	@UpdateProvider(type = FoodBazaarProductSalesQueryBuilder.class, method = "updateSalesRowStateQuery")
	public void updateSalesRowStateDAO(@Param("salesBasketData") SalesBasket salesBasketData);
	
	/**
	 * 
	 * @param salesBasketData
	 * @return
	 */
	@SelectProvider(type = FoodBazaarProductSalesQueryBuilder.class, 	method = "productSalesBasketQuery")
	@Results({
		@Result(property="productID",					column="product_id"),
		@Result(property="dealerID",					column="dealer_id"),
		@Result(property="invoiceID",					column="invoice_id"),
		@Result(property="productCompanyName",			column="product_company"),
		@Result(property="productName",					column="product_name"),
		@Result(property="productBarCode",				column="product_barcode"),
		@Result(property="productCategory",				column="product_category"),
		@Result(property="productSubCategory",			column="product_sub_category"),
		@Result(property="productMRP",					column="product_mrp"),
		@Result(property="productPurchaseTax",			column="product_purchase_tax"),
		@Result(property="productPurchaseCess",			column="product_purchase_cess"),
		@Result(property="productPackedQuantity",		column="product_packed_quantity"),
		@Result(property="productPackedUnit",			column="product_unit"),
		@Result(property="productPurchasePrice",		column="product_purchase_price"),
		@Result(property="productInStock",				column="total_stock"),
		@Result(property="productThresholdQuantity",	column="product_threshold"),
		@Result(property="productPrimaryDiscount",		column="product_primary_discount"),
		@Result(property="productSecondaryDiscount",	column="product_secondary_discount"),
		@Result(property="productExpiryDate",			column="product_expiry_date"),
		@Result(property="productSalesDiscount",		column="product_sales_discount"),
		@Result(property="productSalesPrice",			column="product_sales_price"),
		@Result(property="productSalesTax",				column="product_sales_tax"),
		@Result(property="productSalesCess",			column="product_sales_cess"),
		@Result(property="productSoldQuantity",			column="product_sold_quantity"),
		@Result(property="dealerCategoryID",			column="dealer_category_id")
	})
	public List<SalesBasket> productSalesBasketDAO(@Param("salesBasketData") SalesBasket salesBasketData);
	
	/**
	 * 
	 * @param salesBasketData
	 * @return
	 */
	@SelectProvider(type = FoodBazaarProductSalesQueryBuilder.class, method = "getLatestSalesBillQuery")
	@Results({
		@Result(property="saleBillID",					column="frm_sales_bill_id"),
		@Result(property="saleBillIssuedDate",			column="frm_sales_bill_date"),
		@Result(property="saleBillNumber",				column="frm_sales_bill_number"),
		@Result(property="dealerID",					column="frm_sales_dealer_id"),
		@Result(property="invoiceID",					column="frm_sales_invoice_id"),
		@Result(property="productID",					column="frm_sales_product_id"),
		@Result(property="productSoldQuantity",			column="frm_sales_product_quantity")
	})
	public SalesBasket getLatestSalesBillQuery(@Param("salesBasketData") SalesBasket salesBasketData);
	
	/**
	 * 
	 * @param productPurchaseData
	 * @return
	 */
	@SelectProvider(type = FoodBazaarProductSalesQueryBuilder.class, method= "getLatestEntryInSalesDetailsQuery")
	@Results({
		@Result(property="productID",					column="frm_sales_product_id"),
		@Result(property="dealerID",					column="frm_sales_dealer_id"),
		@Result(property="invoiceID",					column="frm_sales_invoice_id"),
		@Result(property="productSalesDiscount",		column="frm_sales_discount"),
		@Result(property="productSalesPrice",			column="frm_sales_price"),
		@Result(property="productSalesTax",				column="frm_sales_tax"),
		@Result(property="productSalesCess",			column="frm_sales_cess")
	})
	public ProductSales getLatestEntryInSalesDetailsDAO(@Param("productPurchaseData") ProductPurchase productPurchaseData);
	
	
	/**
	 * 
	 * @param salesBasketData
	 * @return
	 */
	@SelectProvider(type = FoodBazaarProductSalesQueryBuilder.class, method= "retrieveCustomisedSalesDetailsQuery")
	@Results({
		@Result(property="saleBillNumber",					column="bill_number"),
		@Result(property="salesManName",					column="sales_man"),
		@Result(property="customerName",					column="customer_name"),
		@Result(property="customerContactNumber",			column="customer_number"),
		@Result(property="productCompanyName",				column="product_company"),
		@Result(property="productName",						column="product_name"),
		@Result(property="productPackedQuantity",			column="packed_quantity"),
		@Result(property="productPackedUnit",				column="packed_unit"),
		@Result(property="productSoldQuantity",				column="sold_quantity"),
		@Result(property="productSalesPrice",				column="sales_price"),
		@Result(property="productSalesDiscount",			column="sales_discount")
	})
	public List<SalesBasket> retrieveCustomisedSalesDetailsDAO(@Param("salesBasketData") SalesBasket salesBasketData);

	
}