package com.simbls.prism.rest.central.service;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.central.Firm;

@Component
public interface FirmService {
	
	/**
	 * Register Dealer details 
	 * @param dealer
	 * @throws Exception
	 */
	public void firmRegister(Firm firmData) throws Exception;
	
}
