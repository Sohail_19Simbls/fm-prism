package com.simbls.prism.rest.ceramics.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.ceramics.service.CeramicsProductSalesService;
import com.simbls.prism.rest.dao.ceramics.CeramicsProductDao;
import com.simbls.prism.rest.dao.ceramics.CeramicsProductSalesDao;
import com.simbls.prism.rest.model.industry.Product;
import com.simbls.prism.rest.model.industry.ProductSales;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Service
public class CeramicsProductSalesServiceImpl implements CeramicsProductSalesService {
	private static final Logger logger = Logger.getLogger(CeramicsProductSalesServiceImpl.class);
	@Autowired private CeramicsProductSalesDao ceramicsProductSalesDAO;
	@Autowired private CeramicsProductDao ceramicsProductDAO;
	
	/**
	 * 
	 */
	public ProductSales productSalesRegister(ProductSales productSalesData) throws Exception {
		logger.info("Initiating insertion into Product Sales Table - Target DB " + productSalesData.getFirmDBName() + " ::: REST ::: Product Sales Service Impl");
		ceramicsProductSalesDAO.productSalesRegisterDAO(productSalesData);
		logger.info("Product Successfully Populated ::: WEB ::: Product Sales Servie Impl");
		return productSalesData;
	}

	/**
	 * The following method works whenever a Product is scanned for selling. The logical explanation is as follows.
	 * 	Any Product that is to be sold at the counter is either searched with name or BarCode.
	 * 	The scanned product is searched from Product Table and product ID obtained.
	 * 	Retrieve latest list of entries from Purchase Details Table and Sales Details Table combined.
	 * 	An Instance variable of type List<SalesBasket> is created in Sales Basket POJO to append the obtained result.
	 * 	Any result obtained either 0, 1 or more than 1 are appended to the incoming object and returned.
	 * 	The query results in either 0 rows, 1 row or more than one.
	 * 	------------------------------------------------------------------------------------------------------------	
	 * 	If 0 rows are returned, then re-obtained the searched product and populate it in the incoming object and return.
	 * 	For 0 rows the Property "objectReturnedFromDB" of POJO SalesBasket remains empty. Check this at front-end.
	 * 	------------------------------------------------------------------------------------------------------------
	 * 	If 1 row is returned then populate the obtained result from DB in the incoming object and send.
	 * 	For 1 row the Property "ProductObject" of POJO SalesBasket remains empty. Check this at front end.
	 * 	------------------------------------------------------------------------------------------------------------
	 * 	If more than 1 row is returned then 
	 * 			Create an ArrayList of invoiceID's
	 * 			Create an ArrayList of M.R.P's
	 * 			Create an ArrayList of Expiry Dates
	 * 	Delete all the redundant elements and set the Arrays into ObjectReturnedFromDB  
	 * 
	 * 	Finally before the object is returned, Obtained the latest Bill Number and increment it by one, update the incoming SalesBasket object and return.
	 * @param salesBaskteData
	 * @return updated salesBsketData
	 * @author Sohail Razvi
	 */
	public SalesBasket productSalesBasket(SalesBasket salesBaskteData) throws Exception {
		logger.info("Initiating retrival of Product Sales Details for Sale Basket. Target DB -" + salesBaskteData.getFirmDBName() + " ::: REST ::: Product Sales Service Impl");
		List<SalesBasket> objectReturnedFromDB  = ceramicsProductSalesDAO.productSalesBasketDAO(salesBaskteData);
		if(objectReturnedFromDB.isEmpty()){
			logger.info("No Object obtained from DB. Return the Object with Product Object. ::: REST ::: Product Sales Service Impl");
			Product productData = new Product();
			if(salesBaskteData.getProductName() != null && !salesBaskteData.getProductName().isEmpty()){
				productData.setProductName(salesBaskteData.getProductName());
			}else if(salesBaskteData.getProductBarCode() != null && !salesBaskteData.getProductBarCode().isEmpty()){
				productData.setProductBarCode(salesBaskteData.getProductBarCode());
			}
			productData.setFirmDBName(salesBaskteData.getFirmDBName());
			List<Product> retrievedProduct = ceramicsProductDAO.retrieveProductByParameterDAO(productData);
			if(retrievedProduct.isEmpty()){
				logger.info("The Product searched does not exist. ::: REST::: Product Sales Service Impl");
			} else {
				logger.info("The Product searched is retrieved. ::: REST::: Product Sales Service Impl");
				salesBaskteData.setProductObject(retrievedProduct);
			}
			logger.info("Obtaining Latest Bill Number from sales Bill Table target DB - " + salesBaskteData.getFirmDBName() + " ::: REST ::: Product Sales Service Impl");
			SalesBasket billObject = ceramicsProductSalesDAO.getLatestSalesBillQuery(salesBaskteData);
			if(billObject == null){
				salesBaskteData.getObjectReturnedFromDB().get(0).setSaleBillNumber("1");
			} else{
				logger.info("Inserting the Bill Number - " + billObject.getSaleBillNumber() + "into returning Object ::: REST ::: Product Sales Service Impl");
				long billNumber = Integer.parseInt(billObject.getSaleBillNumber()) + 1;
				salesBaskteData.setSaleBillNumber(String.valueOf(billNumber));
			}
		} else if(objectReturnedFromDB.size() == 1){
			logger.info("Only one Object obtained from DB ::: REST ::: Product Sales Service Impl");
			salesBaskteData.setObjectReturnedFromDB(objectReturnedFromDB);
			
			ArrayList<Integer> invoiceIDArray = new ArrayList<Integer>();
			ArrayList<String> productMRPArray = new ArrayList<String>();
			ArrayList<String> productExpiryDateArray = new ArrayList<String>();
			for(int i = 0; i < objectReturnedFromDB.size(); i++){
				if(!invoiceIDArray.contains(objectReturnedFromDB.get(i).getInvoiceID())){
					invoiceIDArray.add((int) objectReturnedFromDB.get(i).getInvoiceID());
				}
				if(!productMRPArray.contains(objectReturnedFromDB.get(i).getProductMRP())){
					productMRPArray.add(objectReturnedFromDB.get(i).getProductMRP());
				}
				if(!productExpiryDateArray.contains(objectReturnedFromDB.get(i).getProductExpiryDate())){
					productExpiryDateArray.add(objectReturnedFromDB.get(i).getProductExpiryDate());
				}
			}
			
			for(int i = 0; i < objectReturnedFromDB.size(); i++){
				objectReturnedFromDB.get(i).setInvoiceIDArray(invoiceIDArray);
				objectReturnedFromDB.get(i).setProductMRPArray(productMRPArray);
				objectReturnedFromDB.get(i).setProductExpiryDateArray(productExpiryDateArray);
			}
			
			logger.info("Obtaining Latest Bill Number from sales Bill Table target DB - " + salesBaskteData.getFirmDBName() + " ::: REST ::: Product Sales Service Impl");
			SalesBasket billObject = ceramicsProductSalesDAO.getLatestSalesBillQuery(salesBaskteData);
			if(billObject == null){
				salesBaskteData.getObjectReturnedFromDB().get(0).setSaleBillNumber("1");
			} else{
				logger.info("Inserting the Bill Number - " + billObject.getSaleBillNumber() + "into returning Object ::: REST ::: Product Sales Service Impl");
				long billNumber = Integer.parseInt(billObject.getSaleBillNumber()) + 1;
				salesBaskteData.getObjectReturnedFromDB().get(0).setSaleBillNumber(String.valueOf(billNumber));
			}
		}else if (objectReturnedFromDB.size() > 1){
			logger.info( objectReturnedFromDB.size() + " Objects obtained from DB ::: REST ::: Product Sales Service Impl");
			logger.info("Condensing the Array into as few Rows as possible based on M.R.P.");
			ArrayList<Integer> invoiceIDArray = new ArrayList<Integer>();
			ArrayList<String> productMRPArray = new ArrayList<String>();
			ArrayList<String> productExpiryDateArray = new ArrayList<String>();
			for(int i = 0; i < objectReturnedFromDB.size(); i++){
				if(!invoiceIDArray.contains(objectReturnedFromDB.get(i).getInvoiceID())){
					invoiceIDArray.add((int) objectReturnedFromDB.get(i).getInvoiceID());
				}
				if(!productMRPArray.contains(objectReturnedFromDB.get(i).getProductMRP())){
					productMRPArray.add(objectReturnedFromDB.get(i).getProductMRP());
				}
				if(!productExpiryDateArray.contains(objectReturnedFromDB.get(i).getProductExpiryDate())){
					productExpiryDateArray.add(objectReturnedFromDB.get(i).getProductExpiryDate());
				}
			}
			
			for(int i = 0; i < objectReturnedFromDB.size(); i++){
				int productQuantityInStock = 0;
				for(int k = objectReturnedFromDB.size() - 1; k > 0; k--){
					if((i != k) && (objectReturnedFromDB.get(i).getProductMRP().equals(objectReturnedFromDB.get(k).getProductMRP()))){
						productQuantityInStock = Integer.parseInt(objectReturnedFromDB.get(i).getProductInStock()) + Integer.parseInt(objectReturnedFromDB.get(k).getProductInStock());
						objectReturnedFromDB.get(i).setProductInStock(String.valueOf(productQuantityInStock));
						objectReturnedFromDB.remove(k);
					}
				}
			}
			for(int i = 0; i < objectReturnedFromDB.size(); i++){
				objectReturnedFromDB.get(i).setInvoiceIDArray(invoiceIDArray);
				objectReturnedFromDB.get(i).setProductMRPArray(productMRPArray);
				objectReturnedFromDB.get(i).setProductExpiryDateArray(productExpiryDateArray);
			}
			
			logger.info("Condenced the Object into last - " + objectReturnedFromDB.size() + " Rows. Returning to FrontEnd ::: REST ::: Product Sales Service Impl");
			logger.info("Obtaining Latest Bill Number from sales Bill Table target DB - " + salesBaskteData.getFirmDBName() + " ::: REST ::: Product Sales Service Impl");
			SalesBasket billObject = ceramicsProductSalesDAO.getLatestSalesBillQuery(salesBaskteData);
			if(billObject == null){
				salesBaskteData.getObjectReturnedFromDB().get(0).setSaleBillNumber("1");
			} else{
				logger.info("Inserting the Bill Number - " + billObject.getSaleBillNumber() + "into returning Object ::: REST ::: Product Sales Service Impl");
				long billNumber = Integer.parseInt(billObject.getSaleBillNumber()) + 1;
				for(SalesBasket individualObjectReturned : objectReturnedFromDB){
					individualObjectReturned.setSaleBillNumber(String.valueOf(billNumber));
				}
			}
			salesBaskteData.setObjectReturnedFromDB(objectReturnedFromDB);
		}		
		logger.info("Obtained Data from DB. Returning back. ::: WEB ::: Product Sales Servie Impl");
		return salesBaskteData;
	}		

	
	/**
	 * The following method performs 3 major operations
	 * 		Populates the Sales Bill table (frm_sales_bill), with the necessary details.
	 * 		Updates the Product Sales Details Table (frm_product_sales) with the sold quantity.
	 * 		Finally populates the Consolidated Sales Bill Table (frm_sale_bill_consolidated) with the purchase details.
	 * @param salesBasketData
	 * @return
	 */
	public List<SalesBasket> generateSalesBasketBill(List<SalesBasket> salesBasketData) throws Exception {
		logger.info("Initiating insertion into Product Sales Table - Target DB " + salesBasketData.get(0).getFirmDBName() + " ::: REST ::: Product Sales Service Impl");
		ceramicsProductSalesDAO.generateSalesBasketBillDAO(salesBasketData);
		logger.info("Initiating updation of Sold quantity in Purchase Details Table ::: REST ::: Product Sales Service Impl");
		for(SalesBasket soldItemObject : salesBasketData){
			ceramicsProductSalesDAO.updateSoldQuantityDAO(soldItemObject);
			ceramicsProductSalesDAO.updatePurchaseRowStateDAO(soldItemObject);
			ceramicsProductSalesDAO.updateSalesRowStateDAO(soldItemObject);
		}
		logger.info("Initiating insertion into Sales Bill Consolidated Table. Target DB - " + salesBasketData.get(0).getFirmDBName() + " ::: REST ::: Product Purchase Service Impl");
		logger.info("Preparing data to be inserted into Sales Bill Consolidated Table.");
		for(SalesBasket soldItemObject : salesBasketData){
			String tempPercentage =  soldItemObject.getProductPurchaseTax();
			logger.info("Initiating Check for obtained Percentage - " + tempPercentage + " ::: REST ::: Product Purchase Impl");
			if(tempPercentage.equals("0.00")){
				soldItemObject.setProductSales_For_0_Tax_Amount(soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 0% being added. Amount - " + soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
			}else if(tempPercentage.equals("0.05")){
				soldItemObject.setProductSales_For_5_Tax_Amount(soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 5% being added. Amount - " + soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
			}else if(tempPercentage.equals("0.12")){
				soldItemObject.setProductSales_For_12_Tax_Amount(soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 12% being added. Amount - " + soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
			}else if(tempPercentage.equals("0.18")){
				soldItemObject.setProductSales_For_18_Tax_Amount(soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 18% being added. Amount - " + soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
			}else if(tempPercentage.equals("0.28")){
				soldItemObject.setProductSales_For_28_Tax_Amount(soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 28% being added. Amount - " + soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
			}
			soldItemObject.setProductSales_Effective_CGST(String.valueOf(Float.parseFloat(soldItemObject.getProductSales_TaxAmount())/2));
			soldItemObject.setProductSales_Effective_SGST(String.valueOf(Float.parseFloat(soldItemObject.getProductSales_TaxAmount())/2));
		}
		logger.info("Data prepared. Initiating inserton...");
		ceramicsProductSalesDAO.productSalesConsolidated_UPSERT_DAO(salesBasketData);
		logger.info("Product Successfully Populated ::: REST ::: Product Sales Servie Impl");
		return salesBasketData;
	}
	
}

