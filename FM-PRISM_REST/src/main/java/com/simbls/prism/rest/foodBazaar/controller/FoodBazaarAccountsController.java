package com.simbls.prism.rest.foodBazaar.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.foodBazaar.service.FoodBazaarAccountsService;
import com.simbls.prism.rest.model.industry.Accounts;

@Controller
@RequestMapping(value = "/foodBazaar_accounts")
public class FoodBazaarAccountsController {
	private static final Logger logger = Logger.getLogger(FoodBazaarAccountsController.class);
	@Autowired private FoodBazaarAccountsService foodBazaarAccountsService;

	/**
	 * 
	 * @param accountsData
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/retrieveConsolidatedPurchaseDetails",method=RequestMethod.POST)
	public ResponseEntity<List<Accounts>> retrieveConsolidatedPurchaseDetails(@RequestBody Accounts accountsData){
		logger.info("Continuing retrival of Consolidated Purchase Details for Accounts ::: REST ::: FoodBazaarAccountsController");
		List<Accounts> retrieveConsolidatedPurchaseDetails = foodBazaarAccountsService.retrieveConsolidatedPurchaseDetails(accountsData);
		logger.info("Returning Obtained Consolidated Purchase Details of size " + retrieveConsolidatedPurchaseDetails.size() + " to WEB ::: REST ::: FoodBazaarAccountsController");
		return ResponseEntity.accepted().body(retrieveConsolidatedPurchaseDetails);
	}
	
	/**
	 * 
	 * @param accountsData
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/retrieveConsolidatedSalesDetails",method=RequestMethod.POST)
	public ResponseEntity<List<Accounts>> retrieveConsolidatedSalesDetails(@RequestBody Accounts accountsData){
		logger.info("Continuing retrival of Consolidated Sales Details for Accounts ::: REST ::: FoodBazaarAccountsController");
		List<Accounts> retrieveConsolidatedSalesDetails = foodBazaarAccountsService.retrieveConsolidatedSalesDetails(accountsData);
		logger.info("Returning Obtained Consolidated Sales Details of size " + retrieveConsolidatedSalesDetails.size() + " to WEB ::: REST ::: FoodBazaarAccountsController");
		return ResponseEntity.accepted().body(retrieveConsolidatedSalesDetails);
	}
	
	
}
