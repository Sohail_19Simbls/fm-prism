package com.simbls.prism.rest.central.service;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.central.Login;
import com.simbls.prism.rest.model.central.User;

@Component
public interface UserService {
	
	/**
	 * 
	 * @param login
	 * @return User Object
	 * @throws Exception
	 */
	public User validateCredentials(Login login);
	/***************************************************************/
	
	/**
	 * Get details of individual user by user ID
	 * @return User Object
	 */
	public User getUserDetails(long userID);
 
    /**
     * Reset User Password upon First Login
     * @return void
     */
	public void resetPasswordFirstLogin(User user);
}
