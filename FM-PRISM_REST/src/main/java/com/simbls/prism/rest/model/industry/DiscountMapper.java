package com.simbls.prism.rest.model.industry;

public class DiscountMapper {
	private long discountMapperID;
	private String discountMapperProductID;
	private String discountMapperDiscountID;
	
	private DiscountMapper discountMapperObject;

	public long getDiscountMapperID() {
		return discountMapperID;
	}

	public void setDiscountMapperID(long discountMapperID) {
		this.discountMapperID = discountMapperID;
	}

	public String getDiscountMapperProductID() {
		return discountMapperProductID;
	}

	public void setDiscountMapperProductID(String discountMapperProductID) {
		this.discountMapperProductID = discountMapperProductID;
	}

	public String getDiscountMapperDiscountID() {
		return discountMapperDiscountID;
	}

	public void setDiscountMapperDiscountID(String discountMapperDiscountID) {
		this.discountMapperDiscountID = discountMapperDiscountID;
	}

	public DiscountMapper getDiscountMapperObject() {
		return discountMapperObject;
	}

	public void setDiscountMapperObject(DiscountMapper discountMapperObject) {
		this.discountMapperObject = discountMapperObject;
	}

	

}
