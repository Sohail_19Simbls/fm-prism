package com.simbls.prism.rest.ceramics.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.dao.ceramics.CeramicsInvoiceDao;
import com.simbls.prism.rest.ceramics.service.CeramicsInvoiceService;
import com.simbls.prism.rest.model.industry.Invoice;

@Service
public class CeramicsInvoiceServiceImpl implements CeramicsInvoiceService {
	private static final Logger logger = Logger.getLogger(CeramicsInvoiceServiceImpl.class);
	@Autowired private CeramicsInvoiceDao ceramicsInvoiceDAO;

	/**
	 * 
	 */
	public void invoiceRegister(Invoice invoiceData) {
		logger.info("Initiating insertion into Invoice Table - Target DB " + invoiceData.getFirmDBName() + " ::: REST ::: CeramicsInvoiceServiceImpl");
		ceramicsInvoiceDAO.invoiceRegisterDAO(invoiceData);
		logger.info("Invoice Successfully Populated ::: REST ::: CeramicsInvoiceServiceImpl");
	}
	
	/**
	 * 
	 */
	public void invoiceUpdate(Invoice invoiceData) {
		logger.info("Initiating updation into Invoice Table - Target DB " + invoiceData.getFirmDBName() + " ::: REST ::: CeramicsInvoiceServiceImpl");
		ceramicsInvoiceDAO.invoiceUpdateDAO(invoiceData);
		logger.info("Invoice Successfully Updated ::: REST ::: CeramicsInvoiceServiceImpl");
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Invoice> retrieveInvoiceList(Invoice invoiceData) {
		logger.info("Initiating DB search to retrieve Invoice List ::: REST ::: CeramicsInvoiceService");
		List<Invoice> registeredInvoiceList = ceramicsInvoiceDAO.retrieveInvoiceListDAO(invoiceData);
		logger.info("Successfully obtained - " + registeredInvoiceList.size() + " registered Invoices");
		logger.info("Returning to Invoice Controller ::: REST ::: CeramicsInvoiceServiceImpl");
		return registeredInvoiceList;
	}

	/**
	 * 
	 * @return
	 */
	public List<Invoice> retrieveInvoiceDetails(Invoice invoiceData) {
		logger.info("Initiating DB search to retrieve Products Purchased against Invoice ID ::: REST ::: CeramicsInvoiceService");
		List<Invoice> invoicePurchaseDetails = ceramicsInvoiceDAO.retrieveInvoiceDetailsDAO(invoiceData);
		logger.info("Successfully obtained - " + invoicePurchaseDetails.size() + " registered Invoices");
		logger.info("Returning to Invoice Controller ::: REST ::: CeramicsInvoiceServiceImpl");
		return invoicePurchaseDetails;
	}
}
