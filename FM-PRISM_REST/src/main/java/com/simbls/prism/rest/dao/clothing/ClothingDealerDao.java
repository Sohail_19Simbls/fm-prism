package com.simbls.prism.rest.dao.clothing;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.simbls.prism.rest.foodBazaar.builder.FoodBazaarDealerQueryBuilder;
import com.simbls.prism.rest.model.central.DealerCategory;
import com.simbls.prism.rest.model.industry.Dealer;

@Component("clothingDealerDAO")
public interface ClothingDealerDao {

	@Insert("INSERT into ${firmDBName}.frm_dealer("
			+ "frm_dealer_category_id,"
			+ "frm_dealer_name,"
			+ "frm_dealer_contact_one,"
			+ "frm_dealer_contact_two,"
			+ "frm_dealer_email,"
			+ "frm_dealer_address,"
			+ "frm_dealer_gst,"
			+ "frm_dealer_tin,"
			+ "frm_dealer_pan,"
			+ "frm_dealer_entered_by,"
			+ "frm_dealer_entered_date)"
	+ "values("
			+ " #{dealerCategoryID},"
			+ " #{dealerName},"
			+ " #{dealerContactOne},"
			+ " #{dealerContactTwo},"
			+ " #{dealerEmail},"
			+ " #{dealerAddress},"
			+ " #{dealerGST},"
			+ " #{dealerTIN},"
			+ " #{dealerPAN},"
			+ " #{dealerEnteredBy},"
			+ " #{dealerEnteredDate})")
	
	public void dealerRegisterDAO(Dealer dealer) throws MySQLIntegrityConstraintViolationException;
	
	@Update("UPDATE ${firmDBName}.frm_dealer "
			+ " SET "
			+ " frm_dealer_category_id = #{dealerCategoryID}, "
			+ " frm_dealer_name = #{dealerName}, "
			+ " frm_dealer_contact_one = #{dealerContactOne}, "
			+ " frm_dealer_contact_two = #{dealerContactTwo}, "
			+ " frm_dealer_email = #{dealerEmail}, "
			+ " frm_dealer_address = #{dealerAddress}, "
			+ " frm_dealer_gst = #{dealerGST}, "
			+ " frm_dealer_tin = #{dealerTIN}, "
			+ " frm_dealer_pan = #{dealerPAN}, "
			+ " frm_dealer_updated_by = #{dealerUpdatedBy}, "
			+ " frm_dealer_updated_date = #{dealerUpdatedDate} "
			+ " WHERE frm_dealer_id = #{dealerID}")
	public void dealerUpdateDAO(Dealer dealerData) throws MySQLIntegrityConstraintViolationException;
	
	@SelectProvider(type = FoodBazaarDealerQueryBuilder.class, method = "retrieveDealerListQuery")
	@Results({
		@Result(property="dealerID",				column="frm_dealer_id"),
		@Result(property="dealerCategoryObject",	column="frm_dealer_category_id",	one=@One(select="retrieveDealerCategoryDAO")),
		@Result(property="dealerName",				column="frm_dealer_name"),
		@Result(property="dealerContactOne",		column="frm_dealer_contact_one"),
		@Result(property="dealerContactTwo",		column="frm_dealer_contact_two"),
		@Result(property="dealerEmail",				column="frm_dealer_email"),
		@Result(property="dealerAddress",			column="frm_dealer_address"),
		@Result(property="dealerGST",				column="frm_dealer_gst"),
		@Result(property="dealerTIN",				column="frm_dealer_tin"),
		@Result(property="dealerPAN",				column="frm_dealer_pan")			
	})
	public List<Dealer> retrieveDealerListDAO(@Param("dealerData") Dealer dealerData);
	
	@Select("SELECT * from cnt_dealer_category where cnt_dealer_category_id=#{dealerCategoryID}")
	@Results({
		@Result(property="dealerCategoryID",		column="cnt_dealer_category_id"),
		@Result(property="dealerCategory",			column="cnt_dealer_category")
	})
	public DealerCategory retrieveDealerCategoryDAO(@Param("dealerCategoryID") int dealerCategoryID);
	
	
/*	@SelectProvider(type = FoodBazaarDealerQueryBuilder.class, method = "retrieveDealerDetailsQuery")
	@Results({
		@Result(property="dealerID",				column="frm_dealer_id"),
		@Result(property="dealerCategoryObject",	column="frm_dealer_category_id",	one=@One(select="retrieveDealerCategoryDAO")),
		@Result(property="dealerName",				column="frm_dealer_name"),
		@Result(property="dealerContactOne",		column="frm_dealer_contact_one"),
		@Result(property="dealerContactTwo",		column="frm_dealer_contact_two"),
		@Result(property="dealerEmail",				column="frm_dealer_email"),
		@Result(property="dealerAddress",			column="frm_dealer_address"),
		@Result(property="dealerGST",				column="frm_dealer_gst"),
		@Result(property="dealerTIN",				column="frm_dealer_tin"),
		@Result(property="dealerPAN",				column="frm_dealer_pan")			
	})
	public Dealer retrieveDealerDetailsDAO(@Param("dealerData") Dealer dealerData);*/
	
}
