package com.simbls.prism.rest.foodBazaar.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.foodBazaar.service.FoodBazaarProductSalesService;
import com.simbls.prism.rest.model.industry.ProductSales;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Controller
@RequestMapping(value = "/foodBazaar_productSales")
public class FoodBazaarProductSalesController {
	private static final Logger logger = Logger.getLogger(FoodBazaarProductSalesController.class);
	@Autowired private FoodBazaarProductSalesService foodBazaarProductSalesService;
	
	/**
	 * 
	 * @param productSalesData
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/productSalesRegister", method = RequestMethod.POST)
	public ResponseEntity<List<ProductSales>> productSalesRegister(@RequestBody List<ProductSales> productSalesData) throws Exception{
		logger.info("... Continuing Product Sales Registration ::: REST ::: FoodBazaarProductSalesController");
		List<ProductSales> productSalesObject = foodBazaarProductSalesService.productSalesRegister(productSalesData);
		logger.info("Returning to WEB ::: REST ::: FoodBazaarProductSalesController");
		return ResponseEntity.accepted().body(productSalesObject);
	}
	
	/**
	 * 
	 * @param productSalesData
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/productSalesDetailsUpdate", method = RequestMethod.POST)
	public ProductSales productSalesDetailsUpdate(@RequestBody ProductSales productSalesData) throws Exception{
		logger.info("... Continuing Product Sales Update ::: REST ::: FoodBazaarProductSalesController");
		ProductSales productSalesObject = foodBazaarProductSalesService.productSalesDetailsUpdate(productSalesData);
		logger.info("Returning to WEB ::: REST ::: FoodBazaarProductSalesController");
		return productSalesObject;
	}
	
	
	
	/**
	 * 
	 * @param salesBasketData
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/productSalesBasket", method = RequestMethod.POST)
	public ResponseEntity<SalesBasket> productSalesBasket(@RequestBody SalesBasket salesBasketData) throws Exception{
		logger.info("... Continuing Product Sales into Sales Basket ::: REST ::: FoodBazaarProductSalesController");
		SalesBasket salesBasketObject = foodBazaarProductSalesService.productSalesBasket(salesBasketData);
		logger.info("Returning to WEB ::: REST ::: FoodBazaarProductSalesController");
		return ResponseEntity.accepted().body(salesBasketObject);
	}
	
	/**
	 * Initiate population of Sale Bill and Sale Bill Consolidated Table 
	 * @param salesBasketData
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/generateSalesBasketBill", method = RequestMethod.POST)
	public ResponseEntity<List<SalesBasket>> generateSalesBasketBill(@RequestBody List<SalesBasket> salesBasketData) throws Exception{
		logger.info("... Continuing Product Sales into Sales Basket ::: REST ::: FoodBazaarProductSalesController");
		List<SalesBasket> salesBasketObject = foodBazaarProductSalesService.generateSalesBasketBill(salesBasketData);
		logger.info("Returning to WEB ::: REST ::: FoodBazaarProductSalesController");
		return ResponseEntity.accepted().body(salesBasketObject);
	}
	
	/**
	 * Initiate population of Todays Sale Details 
	 * @param salesBasketData
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/retrieveTodaysSalesDetails", method = RequestMethod.POST)
	public ResponseEntity<SalesBasket> retrieveTodaysSalesDetails(@RequestBody SalesBasket salesBasketData) throws Exception{
		logger.info("... Continuing retrieval of todays sales ::: REST ::: FoodBazaarProductSalesController");
		SalesBasket salesBasketObject = foodBazaarProductSalesService.retrieveTodaysSalesDetails(salesBasketData);
		logger.info("Returning to WEB ::: REST ::: FoodBazaarProductSalesController");
		return ResponseEntity.accepted().body(salesBasketObject);
	}
	
	/**
	 * Initiate population of Customised Sale Details 
	 * @param salesBasketData
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/retrieveCustomisedSalesDetails", method = RequestMethod.POST)
	public ResponseEntity<SalesBasket> retrieveCustomisedSalesDetails(@RequestBody SalesBasket salesBasketData) throws Exception{
		logger.info("... Continuing retrieval of Customised sales ::: REST ::: FoodBazaarProductSalesController");
		SalesBasket salesBasketObject = foodBazaarProductSalesService.retrieveCustomisedSalesDetails(salesBasketData);
		logger.info("Returning to WEB ::: REST ::: FoodBazaarProductSalesController");
		return ResponseEntity.accepted().body(salesBasketObject);
	}

}
