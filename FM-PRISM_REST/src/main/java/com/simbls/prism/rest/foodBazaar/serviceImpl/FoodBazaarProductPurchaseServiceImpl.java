package com.simbls.prism.rest.foodBazaar.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.simbls.prism.rest.dao.foodBazaar.FoodBazaarInvoiceDao;
import com.simbls.prism.rest.dao.foodBazaar.FoodBazaarProductPurchaseDao;
import com.simbls.prism.rest.dao.foodBazaar.FoodBazaarProductSalesDao;
import com.simbls.prism.rest.foodBazaar.service.FoodBazaarProductPurchaseService;
import com.simbls.prism.rest.helper.GSTPurchaseReportGenerator;
import com.simbls.prism.rest.model.industry.Invoice;
import com.simbls.prism.rest.model.industry.ProductPurchase;

@Service
public class FoodBazaarProductPurchaseServiceImpl implements FoodBazaarProductPurchaseService {
	private static final Logger logger = Logger.getLogger(FoodBazaarProductPurchaseServiceImpl.class);
	@Autowired private FoodBazaarProductPurchaseDao foodBazaarProductPurchaseDAO;
	@Autowired private FoodBazaarProductSalesDao foodBazaarProductSalesDao;
	@Autowired private FoodBazaarInvoiceDao foodBazaarInvoiceDao;
	@Autowired private GSTPurchaseReportGenerator GSTPurchaseReportGenerator;
	
	/**
	 * Product Details Register Targets two tables.
	 * First Table	:	frm_product_purchase - Inserts a new Row for each and every entry.
	 * Second Table :	frm_purchase_bill_consolidated - Inserts a new Row for each and every Invoice and keeps updating it.
	 * 					The Second table is used to generate a purchase Tax Invoice for the Accountant Dashboard.
	 * @param productPurchaseData
	 * @author Sohail Razvi
	 * 
	 */
	public ProductPurchase productPurchaseDetailsRegister(ProductPurchase productPurchaseData) throws Exception {
		logger.info("Initiating insertion into Product Purchase Table - Target DB " + productPurchaseData.getFirmDBName() + " ::: REST ::: Product Purchase Service Impl");
		ProductPurchase returningProductPurchaseData = new ProductPurchase();
		try{
			foodBazaarProductPurchaseDAO.productPurchaseDetailsRegisterDAO(productPurchaseData);
			logger.info("Purchase Details Successfully Populated ::: REST ::: Dealer Service Impl");
			returningProductPurchaseData = productPurchaseData;
		} catch (MySQLIntegrityConstraintViolationException  e){
			logger.info("Purchase Details were not Populated. ::: REST ::: Dealer Service Impl");
			logger.error("Constraint violation", e);
		} catch (Exception e){
			logger.info("Purchase Details were not Populated. ::: REST ::: Dealer Service Impl");
			logger.error("Constraint violation", e);
	    }
		logger.info("Check if user has requested autoupdate of the Sales price ::: REST ::: Product Purchase Service Impl");
		if(productPurchaseData.getSalesDetailsAsPreviousFlag() == 1){
			logger.info("Auto Update requested. Commencing Auto Update of Sales details of the product with Recent Details ::: REST ::: Product Purchase Service Impl");
			logger.info("Obtaining the most recent updated set for Product ID - " + productPurchaseData.getProductID()
															+ " and Dealer ID - " + productPurchaseData.getDealerID()
															+ " ::: REST ::: Product Purchase Service Impl");
			productPurchaseData.setProductSalesObject(foodBazaarProductSalesDao.getLatestEntryInSalesDetailsDAO(productPurchaseData));
			logger.info("Check if latest entry exists. ::: REST ::: Product Purchase Service Impl");
			if(productPurchaseData.getProductSalesObject() != null){
				logger.info("Entry Exists. Updating the Obtained entry. Set Invoice ID, Sales Entered Date and Updated Date to latest. ::: REST ::: Product Purchase Service Impl");
				productPurchaseData.getProductSalesObject().setInvoiceID(productPurchaseData.getInvoiceID());
				productPurchaseData.getProductSalesObject().setProductSoldQuantity("0");
				productPurchaseData.getProductSalesObject().setProductSalesEnteredBy(productPurchaseData.getProductPurchaseEnteredBy());
				productPurchaseData.getProductSalesObject().setProductSalesEnteredDate(productPurchaseData.getProductPurchaseEnteredDate());
//				productPurchaseData.getProductSalesObject().setProductSalesUpdatedDate(new Date());
				productPurchaseData.getProductSalesObject().setFirmDBName(productPurchaseData.getFirmDBName());
				productPurchaseData.getProductSalesObject().setFirmDBUserName(productPurchaseData.getFirmDBUserName());
				productPurchaseData.getProductSalesObject().setFirmDBPassword(productPurchaseData.getFirmDBPassword());
				logger.info("Initiating Sales Details Registration for latest Invoice with previous Details. ::: REST ::: Product Purchase Service Impl");
				foodBazaarProductSalesDao.productSalesRegisterDAO(productPurchaseData.getProductSalesObject());
				logger.info("Sales Details successfully auto updated ::: REST ::: Product Purchase Service Impl");
			}else{
				logger.info("Couldnt update Sales Details. Latest Entry is missing::: REST ::: Product Purchase Service Impl");
				productPurchaseData.setProductSalesObject(null);
			}
		}
		//	Organizing Purchase Data based on Tax
		GSTPurchaseReportGenerator.purchaseDetailsRegister(productPurchaseData);
		logger.info("Product Successfully Populated ::: REST ::: Product Purchase Service Impl");
		return returningProductPurchaseData;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public ProductPurchase productPurchaseDetailsUpdate(ProductPurchase productPurchaseData) {
		logger.info("Initiating updation into Product Purchase Table. Target DB - " + productPurchaseData.getFirmDBName() + "::: REST ::: FoodBazaarProductPurchaseService");
		foodBazaarProductPurchaseDAO.productPurchaseDetailsUpdateDAO(productPurchaseData);
		logger.info("Returning to ProductPurchase Controller ::: REST ::: FoodBazaarProductPurchaseService");
		return productPurchaseData;
	}

	/**
	 * 
	 * @return
	 */
	public void productPurchaseDetailsDelete(ProductPurchase productPurchaseData) {
		logger.info("Initiating deletion into Product Purchase Table. Target DB - " + productPurchaseData.getFirmDBName() + "::: REST ::: FoodBazaarProductPurchaseService");
		foodBazaarProductPurchaseDAO.productPurchaseDetailsDeleteDAO(productPurchaseData);
		logger.info("Returning to ProductPurchase Controller ::: REST ::: FoodBazaarProductPurchaseService");
	}
	
	/**
	 * 
	 * @return
	 */
	public List<ProductPurchase> retrieveProductPurchaseDetails(ProductPurchase productPurchaseData) {
		logger.info("Initiating DB search to retrieve ProductPurchase List. Target DB - " + productPurchaseData.getFirmDBName() + "::: REST ::: FoodBazaarProductPurchaseService");
		List<ProductPurchase> registeredProductPurchaseList = foodBazaarProductPurchaseDAO.retrieveProductPurchaseDetailsDAO(productPurchaseData);
		logger.info("Successfully obtained - " + registeredProductPurchaseList.size() + " registered ProductPurchases");
		logger.info("Returning to ProductPurchase Controller ::: REST ::: ProductPurchase Service IMPL");
		return registeredProductPurchaseList;
	}
	
	/**
	 * 
	 * @return
	 */
	public ProductPurchase checkInvoiceStatus(ProductPurchase productPurchaseData) {
		logger.info("Initiating DB check to verify ProductPurchase Amount with Invoice Amount. Target DB - " + productPurchaseData.getFirmDBName() + "::: REST ::: FoodBazaarProductPurchaseService");
		List<ProductPurchase> productPurchaseBasketDetails = foodBazaarProductPurchaseDAO.checkInvoiceStatusDAO(productPurchaseData);
		logger.info("Successfully obtained -  registered ProductPurchases");
		logger.info("Initiating calculations to check invoice Status ::: REST ::: FoodBazaarProductPurchaseService");
		logger.info("Creating a Return Object by name 'invoiceStatus' of Type ProductPurchase and initializing other Variables....");
		ProductPurchase invoiceStatus = new ProductPurchase();
		String invoiceAmount = null;
		String marketFees = null ;
		String unloadingAndTransportationCharges = null;
		double productUnitCost = 0.0;
		double productPurchaseQuantity = 0.0;
		double productFreeQuantity = 0.0;
		double productPurchaseTotalQuantity = 0.0;
		double productTotalCost = 0.0;
		double productPimaryDiscount = 0.0;
		double productSecondaryDiscount = 0.0;
		double productTotalDiscount = 0.0;
		double productTotalDiscountedCost = 0.0;
		double productTax = 0.0;
		double productCess = 0.0;
		double productTotalTax = 0.0; 
		double finalProductCost = 0.0;
		logger.info("Check if the obtained Object comprising of all the Invoice Details is Filled or Empty....");
		if(productPurchaseBasketDetails.isEmpty()){
			logger.info("Empty Object Detected. Initiating invoice details retrival Query...");
			logger.info("Creating invoiceData Object and populating it with DelearID and InvoiceID");
			Invoice invoiceData = new Invoice();
			invoiceData.setDealerID(productPurchaseData.getDealerID());
			invoiceData.setInvoiceID(productPurchaseData.getInvoiceID());
			invoiceData.setFirmDBName(productPurchaseData.getFirmDBName());
			logger.info("Performing search over Database to retrieve Invoice Details of Invoice Number -" + productPurchaseData.getInvoiceNumber() + " ::: REST ::: FoodBazaarProductPurchaseService");
			List<Invoice> retrivedInvoiceData = foodBazaarInvoiceDao.retrieveInvoiceListDAO(invoiceData);
			logger.info("Assigning the obtained values of InvoiceAmount, Market Fees and Transportation Charges ::: REST ::: FoodBazaarProductPurchaseService");
			invoiceAmount = retrivedInvoiceData.get(0).getInvoiceAmount();
			marketFees = retrivedInvoiceData.get(0).getInvoiceMarketFees();
			unloadingAndTransportationCharges = retrivedInvoiceData.get(0).getInvoiceTransportationUnloadingFees();
		} else {
			logger.info("Object of size - " + productPurchaseBasketDetails.size() + " detected ::: REST ::: FoodBazaarProductPurchaseService");
			logger.info("Initiating loop to calculate TotalCost and Final Cost of all the products ::: REST ::: FoodBazaarProductPurchaseService");
			for(int i=0; i<productPurchaseBasketDetails.size(); i++) {
				logger.info("--------------------------------------------------------------------------------------------------");
				productUnitCost = Double.parseDouble(productPurchaseBasketDetails.get(i).getProductPurchaseUnitCost());
				logger.info("Product Unit Cost - " + productUnitCost);
				productPurchaseQuantity = Double.parseDouble(productPurchaseBasketDetails.get(i).getProductPurchasePaidQuantity());
				logger.info("Product Purchased Quantity - " + productPurchaseQuantity);
				if(productPurchaseBasketDetails.get(i).getProductPurchaseFreeQuantity() != null){
					productFreeQuantity = Double.parseDouble(productPurchaseBasketDetails.get(i).getProductPurchaseFreeQuantity());
					logger.info("Product Free Quantity - " + productFreeQuantity);
				}
				productPurchaseTotalQuantity += (productPurchaseQuantity + productFreeQuantity);
				logger.info("Product Purchased Total Quantity - " + productPurchaseTotalQuantity);
				productTotalCost = productTotalCost + (productUnitCost * productPurchaseQuantity);
				logger.info("Product Total Cost - " + productTotalCost);
				productPimaryDiscount = Double.parseDouble(productPurchaseBasketDetails.get(i).getProductPurchasePrimaryDiscount());
				productSecondaryDiscount = Double.parseDouble(productPurchaseBasketDetails.get(i).getProductPurchaseSecondaryDiscount());
				productTotalDiscount = productPimaryDiscount + productSecondaryDiscount;
				logger.info("Product Total Discount - " + productTotalDiscount);
				logger.info("Initiating Check if the Primary and Secondary Discount are in Percentage or Rupees ::: REST ::: FoodBazaarProductPurchaseService");
				if(productPimaryDiscount < 1 && productSecondaryDiscount < 1) {
					logger.info("Primary and Secondary Discount are found to be in Percentage ::: REST ::: FoodBazaarProductPurchaseService");
					productTotalDiscountedCost = (productUnitCost * productPurchaseQuantity) * (1-productTotalDiscount);
					logger.info("Product Total Discounted Cost - " + productTotalDiscountedCost);
				} else {
					logger.info("Primary and Secondary Discount are found to be in Rupees ::: REST ::: FoodBazaarProductPurchaseService");
					productTotalDiscountedCost = (productUnitCost * productPurchaseQuantity) - productTotalDiscount;
					logger.info("Product Total Discounted Cost - " + productTotalDiscountedCost);
				}
				productTax = Double.parseDouble(productPurchaseBasketDetails.get(i).getProductPurchaseTax());
				productCess = Double.parseDouble(productPurchaseBasketDetails.get(i).getProductPurchaseCess());
				productTotalTax = productTax + productCess;
				logger.info("Total Product Tax - " + productTotalTax);
				finalProductCost = finalProductCost + (productTotalDiscountedCost * (1+ productTotalTax));
				logger.info("Final Product Cost - " + finalProductCost);
				invoiceAmount = productPurchaseBasketDetails.get(i).getInvoiceAmount();
				logger.info("Invoice Amount - " + invoiceAmount);
				marketFees = productPurchaseBasketDetails.get(i).getMarketFee();
				logger.info("Market Fees - " + marketFees);
				unloadingAndTransportationCharges = productPurchaseBasketDetails.get(i).getUnloadingAndTransportationCharges();
				logger.info("Unloading and Transportation Charges - " + unloadingAndTransportationCharges);
				logger.info("--------------------------------------------------------------------------------------------------");
			}
		}
	
		invoiceStatus.setProductTotalCost(productTotalCost);
		invoiceStatus.setProductPurchaseTotalQuantity(String.valueOf(productPurchaseTotalQuantity));
		invoiceStatus.setFinalProductCost(Math.ceil(finalProductCost));
		invoiceStatus.setInvoiceAmount(invoiceAmount);
		invoiceStatus.setMarketFee(marketFees);
		invoiceStatus.setUnloadingAndTransportationCharges(unloadingAndTransportationCharges);
		
		logger.info("Returning to ProductPurchase Controller ::: REST ::: ProductPurchase Service IMPL");
		return invoiceStatus;
	}



}
