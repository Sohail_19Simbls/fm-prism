package com.simbls.prism.rest.model.industry;

import java.util.ArrayList;
import java.util.List;

import com.simbls.prism.rest.model.central.ProductCategory;
import com.simbls.prism.rest.model.central.ProductSubCategory;


public class Product {
	/* ---------------------------------------- */
	/*	Parameters common to All Industry	*/
	/* ---------------------------------------- */
	private long productID;
	private String productCompany;
	private String productName;
	private int productCategoryID;
	private int productSubCategoryID;
	private String productBarCode;
	private Boolean productCustomBarCodeFlag;
	private String productEnteredBy;
	private String productEnteredDate;
	private String productUpdatedBy;
	private String productUpdatedDate;
	
	/* ---------------------------------------- */
	/*	Parameters related to FoodBazaar Industry	*/
	/* ---------------------------------------- */
	private String productPackedQuantity;
	private String productUnit;
	
	/* ---------------------------------------- */
	/*	Parameters related to Clothing Industry	*/
	/* ---------------------------------------- */
	private String productMaterial;
	private String productSize;
	
	/* ---------------------------------------- */
	/*	Additional Parameters	*/
	/* ---------------------------------------- */
	private ProductCategory productCategoryObject;
	private ProductSubCategory productSubCategoryObject;
	private Product productObject;
	private List<ProductPurchase> productPurchaseObject = new ArrayList<ProductPurchase>();
	private List<ProductSales> productSalesObject = new ArrayList<ProductSales>();
	
	/* ---------------------------------------- */
	/*	Parameters related to specific Industry and Firm	*/
	/* ---------------------------------------- */
	private String industryCategory;
	private String firmDBName;
	private String firmDBUserName;
	private String firmDBPassword;
	
	public long getProductID() {
		return productID;
	}
	public void setProductID(long productID) {
		this.productID = productID;
	}
	public String getProductCompany() {
		return productCompany;
	}
	public void setProductCompany(String productCompany) {
		this.productCompany = productCompany;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getProductCategoryID() {
		return productCategoryID;
	}
	public void setProductCategoryID(int productCategoryID) {
		this.productCategoryID = productCategoryID;
	}
	public int getProductSubCategoryID() {
		return productSubCategoryID;
	}
	public void setProductSubCategoryID(int productSubCategoryID) {
		this.productSubCategoryID = productSubCategoryID;
	}
	public String getProductBarCode() {
		return productBarCode;
	}
	public void setProductBarCode(String productBarCode) {
		this.productBarCode = productBarCode;
	}
	public Boolean getProductCustomBarCodeFlag() {
		return productCustomBarCodeFlag;
	}
	public void setProductCustomBarCodeFlag(Boolean productCustomBarCodeFlag) {
		this.productCustomBarCodeFlag = productCustomBarCodeFlag;
	}
	public String getProductEnteredBy() {
		return productEnteredBy;
	}
	public void setProductEnteredBy(String productEnteredBy) {
		this.productEnteredBy = productEnteredBy;
	}
	public String getProductEnteredDate() {
		return productEnteredDate;
	}
	public void setProductEnteredDate(String productEnteredDate) {
		this.productEnteredDate = productEnteredDate;
	}
	public String getProductUpdatedBy() {
		return productUpdatedBy;
	}
	public void setProductUpdatedBy(String productUpdatedBy) {
		this.productUpdatedBy = productUpdatedBy;
	}
	public String getProductUpdatedDate() {
		return productUpdatedDate;
	}
	public void setProductUpdatedDate(String productUpdatedDate) {
		this.productUpdatedDate = productUpdatedDate;
	}
	public String getProductPackedQuantity() {
		return productPackedQuantity;
	}
	public void setProductPackedQuantity(String productPackedQuantity) {
		this.productPackedQuantity = productPackedQuantity;
	}
	public String getProductUnit() {
		return productUnit;
	}
	public void setProductUnit(String productUnit) {
		this.productUnit = productUnit;
	}
	public String getProductMaterial() {
		return productMaterial;
	}
	public void setProductMaterial(String productMaterial) {
		this.productMaterial = productMaterial;
	}
	public String getProductSize() {
		return productSize;
	}
	public void setProductSize(String productSize) {
		this.productSize = productSize;
	}
	public ProductCategory getProductCategoryObject() {
		return productCategoryObject;
	}
	public void setProductCategoryObject(ProductCategory productCategoryObject) {
		this.productCategoryObject = productCategoryObject;
	}
	public ProductSubCategory getProductSubCategoryObject() {
		return productSubCategoryObject;
	}
	public void setProductSubCategoryObject(ProductSubCategory productSubCategoryObject) {
		this.productSubCategoryObject = productSubCategoryObject;
	}
	public Product getProductObject() {
		return productObject;
	}
	public void setProductObject(Product productObject) {
		this.productObject = productObject;
	}
	public List<ProductPurchase> getProductPurchaseObject() {
		return productPurchaseObject;
	}
	public void setProductPurchaseObject(List<ProductPurchase> productPurchaseObject) {
		this.productPurchaseObject = productPurchaseObject;
	}
	public List<ProductSales> getProductSalesObject() {
		return productSalesObject;
	}
	public void setProductSalesObject(List<ProductSales> productSalesObject) {
		this.productSalesObject = productSalesObject;
	}
	public String getIndustryCategory() {
		return industryCategory;
	}
	public void setIndustryCategory(String industryCategory) {
		this.industryCategory = industryCategory;
	}
	public String getFirmDBName() {
		return firmDBName;
	}
	public void setFirmDBName(String firmDBName) {
		this.firmDBName = firmDBName;
	}
	public String getFirmDBUserName() {
		return firmDBUserName;
	}
	public void setFirmDBUserName(String firmDBUserName) {
		this.firmDBUserName = firmDBUserName;
	}
	public String getFirmDBPassword() {
		return firmDBPassword;
	}
	public void setFirmDBPassword(String firmDBPassword) {
		this.firmDBPassword = firmDBPassword;
	}
	
}
