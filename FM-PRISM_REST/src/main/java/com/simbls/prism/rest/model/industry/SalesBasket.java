package com.simbls.prism.rest.model.industry;

import java.util.ArrayList;
import java.util.List;

import com.simbls.prism.rest.model.central.ProductCategory;
import com.simbls.prism.rest.model.central.ProductSubCategory;

public class SalesBasket {
	private long saleBasketID;
	
	private long productID;
	private long dealerID;
	private long invoiceID;
	
	private int dealerCategoryID;
	
	private String productCompanyName;
	private String productName;	
	private String productBarCode;
	private int productCategory;
	private int productSubCategory;
	private String productMRP;
	private String productPurchaseTax;
	private String productPurchaseCess;
	private String productPackedQuantity;
	private String productPackedUnit;
	private String productPurchasePrice;
	private String productInStock;
	private String productThresholdQuantity;
	private String productPrimaryDiscount;
	private String productSecondaryDiscount;
	private String productExpiryDate;
	private String productSalesDiscount;
	private String productSalesPrice;
	private String productSalesTax;
	private String productSalesCess;
	private String productSalesEnteredDate;
	private String productSalesUpdatedDate;
	
	private ProductCategory productCategoryObject;
	private ProductSubCategory productSubCategoryObject;
	
	private int productSalesRowState;
	private int productPurchaseRowState;
	/* ----------------------------- */
	/*	Following refer to the Sale Bill Table	*/
	/* ----------------------------- */
	private long saleBillID;
	private String saleBillIssuedDate;
	private String saleBillNumber;
	private String productSoldQuantity;
	
	/* ----------------------------- */
	/*	Following refer to the Sale Bill Consolidated Table	*/
	/* ----------------------------- */
	private String saleBillCompactDate;
	private String perProductSalesCostWithTax;
	private String totalSalesQuantityProductTotalCostWithoutTax;
	private String productSales_TaxAmount;
	private String productSales_For_0_Tax_Amount;
	private String productSales_For_5_Tax_Amount;
	private String productSales_For_12_Tax_Amount;
	private String productSales_For_18_Tax_Amount;
	private String productSales_For_28_Tax_Amount;
	private String productSales_Effective_CGST;
	private String productSales_Effective_SGST;
	private String productSales_Effective_CESS;
	
	/* ----------------------------- */
	private List<Product> productObject = new ArrayList<Product>();
	private List<ProductPurchase> productPurchaseObject = new ArrayList<ProductPurchase>();
	private List<ProductSales> productSalesObject = new ArrayList<ProductSales>();
	private List<Dealer> dealerObject = new ArrayList<Dealer>();
	private List<Invoice> invoiceObject = new ArrayList<Invoice>();
	
	/* ----------------------------- */
	/*	Following are Used in case of Multiple results from the DB	*/
	/* ----------------------------- */
	private List<Integer> invoiceIDArray = new ArrayList<Integer>();
	private List<String> productMRPArray = new ArrayList<String>();
	private List<String> productExpiryDateArray = new ArrayList<String>();
	/* ------------------------------------- */
	/* Object from DB Containing Scanned Details
	 * Object to DB Containing Selling Details
	/* ------------------------------------- */
	private List<SalesBasket> objectReturnedFromDB = new ArrayList<SalesBasket>();
	private List<SalesBasket> objectToDB = new ArrayList<SalesBasket>();
	
	/* ------------------------------------- */
	/* Following refer to Bill Specific Details */
	/* ------------------------------------- */
	
	private String customerName;
	private String customerContactNumber;
	private String salesManName;
	
	/* ------------------------------------- */
	/* Following refer to Sales Details */
	/* ------------------------------------- */
	
	private String todaysDate;
	private String salesStartDate;
	private String salesEndDate;
	private List<SalesBasket> todaysSales = new ArrayList<SalesBasket>();
	private List<SalesBasket> customisedSales = new ArrayList<SalesBasket>();
	
	/* ---------------------------------------- */
	/*	Parameters related to specific Industry and Firm	*/
	/* ---------------------------------------- */
	private String industryCategory;
	private String firmDBName;
	private String firmDBUserName;
	private String firmDBPassword;
	
	private List<SalesBasket> salesBasketObject = new ArrayList<SalesBasket>();

	public long getSaleBasketID() {
		return saleBasketID;
	}

	public void setSaleBasketID(long saleBasketID) {
		this.saleBasketID = saleBasketID;
	}

	public long getProductID() {
		return productID;
	}

	public void setProductID(long productID) {
		this.productID = productID;
	}

	public long getDealerID() {
		return dealerID;
	}

	public void setDealerID(long dealerID) {
		this.dealerID = dealerID;
	}

	public long getInvoiceID() {
		return invoiceID;
	}

	public void setInvoiceID(long invoiceID) {
		this.invoiceID = invoiceID;
	}

	public int getDealerCategoryID() {
		return dealerCategoryID;
	}

	public void setDealerCategoryID(int dealerCategoryID) {
		this.dealerCategoryID = dealerCategoryID;
	}

	public String getProductCompanyName() {
		return productCompanyName;
	}

	public void setProductCompanyName(String productCompanyName) {
		this.productCompanyName = productCompanyName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductBarCode() {
		return productBarCode;
	}

	public void setProductBarCode(String productBarCode) {
		this.productBarCode = productBarCode;
	}

	public int getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(int productCategory) {
		this.productCategory = productCategory;
	}

	public int getProductSubCategory() {
		return productSubCategory;
	}

	public void setProductSubCategory(int productSubCategory) {
		this.productSubCategory = productSubCategory;
	}

	public String getProductMRP() {
		return productMRP;
	}

	public void setProductMRP(String productMRP) {
		this.productMRP = productMRP;
	}

	public String getProductPurchaseTax() {
		return productPurchaseTax;
	}

	public void setProductPurchaseTax(String productPurchaseTax) {
		this.productPurchaseTax = productPurchaseTax;
	}

	public String getProductPurchaseCess() {
		return productPurchaseCess;
	}

	public void setProductPurchaseCess(String productPurchaseCess) {
		this.productPurchaseCess = productPurchaseCess;
	}

	public String getProductPackedQuantity() {
		return productPackedQuantity;
	}

	public void setProductPackedQuantity(String productPackedQuantity) {
		this.productPackedQuantity = productPackedQuantity;
	}

	public String getProductPackedUnit() {
		return productPackedUnit;
	}

	public void setProductPackedUnit(String productPackedUnit) {
		this.productPackedUnit = productPackedUnit;
	}

	public String getProductPurchasePrice() {
		return productPurchasePrice;
	}

	public void setProductPurchasePrice(String productPurchasePrice) {
		this.productPurchasePrice = productPurchasePrice;
	}

	public String getProductInStock() {
		return productInStock;
	}

	public void setProductInStock(String productInStock) {
		this.productInStock = productInStock;
	}

	public String getProductThresholdQuantity() {
		return productThresholdQuantity;
	}

	public void setProductThresholdQuantity(String productThresholdQuantity) {
		this.productThresholdQuantity = productThresholdQuantity;
	}

	public String getProductPrimaryDiscount() {
		return productPrimaryDiscount;
	}

	public void setProductPrimaryDiscount(String productPrimaryDiscount) {
		this.productPrimaryDiscount = productPrimaryDiscount;
	}

	public String getProductSecondaryDiscount() {
		return productSecondaryDiscount;
	}

	public void setProductSecondaryDiscount(String productSecondaryDiscount) {
		this.productSecondaryDiscount = productSecondaryDiscount;
	}

	public String getProductExpiryDate() {
		return productExpiryDate;
	}

	public void setProductExpiryDate(String productExpiryDate) {
		this.productExpiryDate = productExpiryDate;
	}

	public String getProductSalesDiscount() {
		return productSalesDiscount;
	}

	public void setProductSalesDiscount(String productSalesDiscount) {
		this.productSalesDiscount = productSalesDiscount;
	}

	public String getProductSalesPrice() {
		return productSalesPrice;
	}

	public void setProductSalesPrice(String productSalesPrice) {
		this.productSalesPrice = productSalesPrice;
	}

	public String getProductSalesTax() {
		return productSalesTax;
	}

	public void setProductSalesTax(String productSalesTax) {
		this.productSalesTax = productSalesTax;
	}

	public String getProductSalesCess() {
		return productSalesCess;
	}

	public void setProductSalesCess(String productSalesCess) {
		this.productSalesCess = productSalesCess;
	}

	public String getProductSalesEnteredDate() {
		return productSalesEnteredDate;
	}

	public void setProductSalesEnteredDate(String productSalesEnteredDate) {
		this.productSalesEnteredDate = productSalesEnteredDate;
	}

	public String getProductSalesUpdatedDate() {
		return productSalesUpdatedDate;
	}

	public void setProductSalesUpdatedDate(String productSalesUpdatedDate) {
		this.productSalesUpdatedDate = productSalesUpdatedDate;
	}

	public ProductCategory getProductCategoryObject() {
		return productCategoryObject;
	}

	public void setProductCategoryObject(ProductCategory productCategoryObject) {
		this.productCategoryObject = productCategoryObject;
	}

	public ProductSubCategory getProductSubCategoryObject() {
		return productSubCategoryObject;
	}

	public void setProductSubCategoryObject(ProductSubCategory productSubCategoryObject) {
		this.productSubCategoryObject = productSubCategoryObject;
	}

	public int getProductSalesRowState() {
		return productSalesRowState;
	}

	public void setProductSalesRowState(int productSalesRowState) {
		this.productSalesRowState = productSalesRowState;
	}

	public int getProductPurchaseRowState() {
		return productPurchaseRowState;
	}

	public void setProductPurchaseRowState(int productPurchaseRowState) {
		this.productPurchaseRowState = productPurchaseRowState;
	}

	public long getSaleBillID() {
		return saleBillID;
	}

	public void setSaleBillID(long saleBillID) {
		this.saleBillID = saleBillID;
	}

	public String getSaleBillIssuedDate() {
		return saleBillIssuedDate;
	}

	public void setSaleBillIssuedDate(String saleBillIssuedDate) {
		this.saleBillIssuedDate = saleBillIssuedDate;
	}

	public String getSaleBillNumber() {
		return saleBillNumber;
	}

	public void setSaleBillNumber(String saleBillNumber) {
		this.saleBillNumber = saleBillNumber;
	}

	public String getProductSoldQuantity() {
		return productSoldQuantity;
	}

	public void setProductSoldQuantity(String productSoldQuantity) {
		this.productSoldQuantity = productSoldQuantity;
	}

	public String getSaleBillCompactDate() {
		return saleBillCompactDate;
	}

	public void setSaleBillCompactDate(String saleBillCompactDate) {
		this.saleBillCompactDate = saleBillCompactDate;
	}

	public String getPerProductSalesCostWithTax() {
		return perProductSalesCostWithTax;
	}

	public void setPerProductSalesCostWithTax(String perProductSalesCostWithTax) {
		this.perProductSalesCostWithTax = perProductSalesCostWithTax;
	}

	public String getTotalSalesQuantityProductTotalCostWithoutTax() {
		return totalSalesQuantityProductTotalCostWithoutTax;
	}

	public void setTotalSalesQuantityProductTotalCostWithoutTax(String totalSalesQuantityProductTotalCostWithoutTax) {
		this.totalSalesQuantityProductTotalCostWithoutTax = totalSalesQuantityProductTotalCostWithoutTax;
	}

	public String getProductSales_TaxAmount() {
		return productSales_TaxAmount;
	}

	public void setProductSales_TaxAmount(String productSales_TaxAmount) {
		this.productSales_TaxAmount = productSales_TaxAmount;
	}

	public String getProductSales_For_0_Tax_Amount() {
		return productSales_For_0_Tax_Amount;
	}

	public void setProductSales_For_0_Tax_Amount(String productSales_For_0_Tax_Amount) {
		this.productSales_For_0_Tax_Amount = productSales_For_0_Tax_Amount;
	}

	public String getProductSales_For_5_Tax_Amount() {
		return productSales_For_5_Tax_Amount;
	}

	public void setProductSales_For_5_Tax_Amount(String productSales_For_5_Tax_Amount) {
		this.productSales_For_5_Tax_Amount = productSales_For_5_Tax_Amount;
	}

	public String getProductSales_For_12_Tax_Amount() {
		return productSales_For_12_Tax_Amount;
	}

	public void setProductSales_For_12_Tax_Amount(String productSales_For_12_Tax_Amount) {
		this.productSales_For_12_Tax_Amount = productSales_For_12_Tax_Amount;
	}

	public String getProductSales_For_18_Tax_Amount() {
		return productSales_For_18_Tax_Amount;
	}

	public void setProductSales_For_18_Tax_Amount(String productSales_For_18_Tax_Amount) {
		this.productSales_For_18_Tax_Amount = productSales_For_18_Tax_Amount;
	}

	public String getProductSales_For_28_Tax_Amount() {
		return productSales_For_28_Tax_Amount;
	}

	public void setProductSales_For_28_Tax_Amount(String productSales_For_28_Tax_Amount) {
		this.productSales_For_28_Tax_Amount = productSales_For_28_Tax_Amount;
	}

	public String getProductSales_Effective_CGST() {
		return productSales_Effective_CGST;
	}

	public void setProductSales_Effective_CGST(String productSales_Effective_CGST) {
		this.productSales_Effective_CGST = productSales_Effective_CGST;
	}

	public String getProductSales_Effective_SGST() {
		return productSales_Effective_SGST;
	}

	public void setProductSales_Effective_SGST(String productSales_Effective_SGST) {
		this.productSales_Effective_SGST = productSales_Effective_SGST;
	}

	public String getProductSales_Effective_CESS() {
		return productSales_Effective_CESS;
	}

	public void setProductSales_Effective_CESS(String productSales_Effective_CESS) {
		this.productSales_Effective_CESS = productSales_Effective_CESS;
	}

	public List<Product> getProductObject() {
		return productObject;
	}

	public void setProductObject(List<Product> productObject) {
		this.productObject = productObject;
	}

	public List<ProductPurchase> getProductPurchaseObject() {
		return productPurchaseObject;
	}

	public void setProductPurchaseObject(List<ProductPurchase> productPurchaseObject) {
		this.productPurchaseObject = productPurchaseObject;
	}

	public List<ProductSales> getProductSalesObject() {
		return productSalesObject;
	}

	public void setProductSalesObject(List<ProductSales> productSalesObject) {
		this.productSalesObject = productSalesObject;
	}

	public List<Dealer> getDealerObject() {
		return dealerObject;
	}

	public void setDealerObject(List<Dealer> dealerObject) {
		this.dealerObject = dealerObject;
	}

	public List<Invoice> getInvoiceObject() {
		return invoiceObject;
	}

	public void setInvoiceObject(List<Invoice> invoiceObject) {
		this.invoiceObject = invoiceObject;
	}

	public List<Integer> getInvoiceIDArray() {
		return invoiceIDArray;
	}

	public void setInvoiceIDArray(List<Integer> invoiceIDArray) {
		this.invoiceIDArray = invoiceIDArray;
	}

	public List<String> getProductMRPArray() {
		return productMRPArray;
	}

	public void setProductMRPArray(List<String> productMRPArray) {
		this.productMRPArray = productMRPArray;
	}

	public List<String> getProductExpiryDateArray() {
		return productExpiryDateArray;
	}

	public void setProductExpiryDateArray(List<String> productExpiryDateArray) {
		this.productExpiryDateArray = productExpiryDateArray;
	}

	public List<SalesBasket> getObjectReturnedFromDB() {
		return objectReturnedFromDB;
	}

	public void setObjectReturnedFromDB(List<SalesBasket> objectReturnedFromDB) {
		this.objectReturnedFromDB = objectReturnedFromDB;
	}

	public List<SalesBasket> getObjectToDB() {
		return objectToDB;
	}

	public void setObjectToDB(List<SalesBasket> objectToDB) {
		this.objectToDB = objectToDB;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerContactNumber() {
		return customerContactNumber;
	}

	public void setCustomerContactNumber(String customerContactNumber) {
		this.customerContactNumber = customerContactNumber;
	}

	public String getSalesManName() {
		return salesManName;
	}

	public void setSalesManName(String salesManName) {
		this.salesManName = salesManName;
	}

	public String getTodaysDate() {
		return todaysDate;
	}

	public void setTodaysDate(String todaysDate) {
		this.todaysDate = todaysDate;
	}

	public String getSalesStartDate() {
		return salesStartDate;
	}

	public void setSalesStartDate(String salesStartDate) {
		this.salesStartDate = salesStartDate;
	}

	public String getSalesEndDate() {
		return salesEndDate;
	}

	public void setSalesEndDate(String salesEndDate) {
		this.salesEndDate = salesEndDate;
	}

	public List<SalesBasket> getTodaysSales() {
		return todaysSales;
	}

	public void setTodaysSales(List<SalesBasket> todaysSales) {
		this.todaysSales = todaysSales;
	}

	public String getIndustryCategory() {
		return industryCategory;
	}

	public void setIndustryCategory(String industryCategory) {
		this.industryCategory = industryCategory;
	}

	public String getFirmDBName() {
		return firmDBName;
	}

	public void setFirmDBName(String firmDBName) {
		this.firmDBName = firmDBName;
	}

	public String getFirmDBUserName() {
		return firmDBUserName;
	}

	public void setFirmDBUserName(String firmDBUserName) {
		this.firmDBUserName = firmDBUserName;
	}

	public String getFirmDBPassword() {
		return firmDBPassword;
	}

	public void setFirmDBPassword(String firmDBPassword) {
		this.firmDBPassword = firmDBPassword;
	}

	public List<SalesBasket> getSalesBasketObject() {
		return salesBasketObject;
	}

	public void setSalesBasketObject(List<SalesBasket> salesBasketObject) {
		this.salesBasketObject = salesBasketObject;
	}

	public List<SalesBasket> getCustomisedSales() {
		return customisedSales;
	}

	public void setCustomisedSales(List<SalesBasket> customisedSales) {
		this.customisedSales = customisedSales;
	}
	
}
