package com.simbls.prism.rest.central.builder;

import java.util.Map;

import org.apache.log4j.Logger;

import com.simbls.prism.rest.model.central.Firm;

public class FirmQueryBuilder {
	
	private static final Logger logger = Logger.getLogger(FirmQueryBuilder.class);

	public String getFirmsProvider(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to obtain Firm Object ::: REST ::: Firm Query Builder");
		Long firmId =  (Long) parameters.get("firmId");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("SELECT * FROM fmprism_central.cnt_firm");
		logger.info("UserID Value - " + firmId.longValue());
		if (!firmId.equals("NA")) {
			SelectUserQuery.append(" WHERE cnt_firm_id ='" + firmId + "'");
		}
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	
	public String createFirmDBQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create new Firm DB ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE DATABASE " + firmObject.getFirmDBName());
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String createFirmDealerTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to Create Firm Dealer Table ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_dealer ("
				+ "frm_dealer_id bigint(20) NOT NULL AUTO_INCREMENT, "
				+ "frm_dealer_category_id int(11) DEFAULT NULL, "
				+ "frm_dealer_name varchar(45) DEFAULT NULL, "
				+ "frm_dealer_contact_one varchar(45) DEFAULT NULL, "
				+ "frm_dealer_contact_two varchar(45) DEFAULT NULL, "
				+ "frm_dealer_email varchar(45) DEFAULT NULL, "
				+ "frm_dealer_address varchar(100) DEFAULT NULL, "
				+ "frm_dealer_gst varchar(45) DEFAULT NULL, "
				+ "frm_dealer_tin varchar(45) DEFAULT NULL, "
				+ "frm_dealer_pan varchar(45) DEFAULT NULL, "
				+ "frm_dealer_entered_by varchar(45) DEFAULT NULL, "
				+ "frm_dealer_entered_date varchar(45) DEFAULT NULL, "
				+ "frm_dealer_updated_by varchar(45) DEFAULT NULL, "
				+ "frm_dealer_updated_date varchar(45) DEFAULT NULL, "
				+ "PRIMARY KEY (frm_dealer_id), "
				+ "UNIQUE KEY frm_dealer_gst_UNIQUE (frm_dealer_gst) "
				+ ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}

	public String createFirmProductMapTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create Product Mapper Table ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_dealer_product_map ("
				+ "frm_dealer_product_map_id bigint(20) NOT NULL AUTO_INCREMENT, "
				+ "frm_map_dealer_id bigint(20) DEFAULT NULL, "
				+ "frm_map_product_id bigint(20) DEFAULT NULL, "
				+ "PRIMARY KEY (frm_dealer_product_map_id) "
				+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String createFirmDiscountTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create Discount Table ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_discount ( "
				+ "frm_discount_id bigint(20) NOT NULL AUTO_INCREMENT, "
				+ "frm_discount_category varchar(45) DEFAULT NULL, "
				+ "frm_discount_default int(11) DEFAULT NULL, "
				+ "frm_discount varchar(45) DEFAULT NULL, "
				+ "frm_discount_start_date varchar(45) DEFAULT NULL, "
				+ "frm_discount_end_date varchar(45) DEFAULT NULL, "
				+ "frm_discount_entered_by varchar(45) DEFAULT NULL, "
				+ "frm_discount_entered_date varchar(45) DEFAULT NULL, "
				+ "frm_discount_updated_by varchar(45) DEFAULT NULL, "
				+ "frm_discount_updated_date varchar(45) DEFAULT NULL, "
				+ "frm_discount_rowstate varchar(45) DEFAULT NULL, "
				+ "PRIMARY KEY (frm_discount_id) "
				+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String createFirmDiscountMapperTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create Discount Mapper ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_discount_mapper ( "
				+ "frm_discount_mapper_id bigint(20) NOT NULL AUTO_INCREMENT, "
				+ "frm_discount_mapper_discount_id bigint(20) DEFAULT NULL, "
				+ "frm_discount_mapper_product_id bigint(20) DEFAULT NULL, "
				+ "frm_discount_mapper_purchase_id bigint(20) DEFAULT NULL, "
				+ "frm_discount_mapper_sales_id bigint(20) DEFAULT NULL, "
				+ "PRIMARY KEY (frm_discount_mapper_id), "
				+ "KEY frm_product_id_idx (frm_discount_mapper_product_id) "
				+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String createFirmEmployeeTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create employee Table ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_employee ( "
				+ "frm_employee_id bigint(20) NOT NULL AUTO_INCREMENT, "
				+ "frm_employee_name varchar(45) DEFAULT NULL, "
				+ "frm_employee_contact_one varchar(45) DEFAULT NULL, "
				+ "frm_employee_contact_two varchar(45) DEFAULT NULL, "
				+ "frm_employee_home_address varchar(150) DEFAULT NULL, "
				+ "frm_employee_aadhar_number varchar(45) DEFAULT NULL, "
				+ "frm_employee_joining_date varchar(45) DEFAULT NULL, "
				+ "frm_employee_salary varchar(45) DEFAULT NULL, "
				+ "frm_employee_incentive varchar(45) DEFAULT NULL, "
				+ "frm_employee_entered_by varchar(45) DEFAULT NULL, "
				+ "frm_employee_entered_date varchar(45) DEFAULT NULL, "
				+ "frm_employee_updated_by varchar(45) DEFAULT NULL, "
				+ "frm_employee_updated_date varchar(45) DEFAULT NULL, "
				+ "frm_employee_rowstate varchar(45) DEFAULT NULL, "
				+ "PRIMARY KEY (frm_employee_id) "
				+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String createFirmExpenseTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create Expense Table ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_expense ( "
				+ "frm_expense_id int(11) NOT NULL AUTO_INCREMENT, "
				+ "frm_expense_type_id int(11) DEFAULT NULL, "
				+ "frm_expense_subtype_id int(11) DEFAULT NULL, "
				+ "frm_expense_amount varchar(45) DEFAULT NULL, "
				+ "frm_expense_date varchar(45) DEFAULT NULL, "
				+ "frm_expense_entered_by varchar(45) DEFAULT NULL, "
				+ "frm_expense_entered_date varchar(45) DEFAULT NULL, "
				+ "frm_expense_updated_by varchar(45) DEFAULT NULL, "
				+ "frm_expense_updated_date varchar(45) DEFAULT NULL, "
				+ "PRIMARY KEY (frm_expense_id) "
				+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String createFirmInvoiceTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create Invoice Table ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_invoice ( "
				+ "frm_invoice_id bigint(20) NOT NULL AUTO_INCREMENT, "
				+ "frm_invoice_dealer_id bigint(20) DEFAULT NULL, "
				+ "frm_invoice_number varchar(45) DEFAULT NULL, "
				+ "frm_invoice_bill_date varchar(45) DEFAULT NULL, "
				+ "frm_invoice_delivery_date varchar(45) DEFAULT NULL, "
				+ "frm_invoice_amount varchar(45) DEFAULT NULL, "
				+ "frm_invoice_market_fee varchar(45) DEFAULT NULL, "
				+ "frm_invoice_transportation_unloading_fee varchar(45) DEFAULT NULL, "
				+ "frm_invoice_payment_due_date varchar(45) DEFAULT NULL, "
				+ "frm_invoice_entered_by varchar(45) DEFAULT NULL, "
				+ "frm_invoice_entered_date varchar(45) DEFAULT NULL, "
				+ "frm_invoice_updated_by varchar(45) DEFAULT NULL, "
				+ "frm_invoice_updated_date varchar(45) DEFAULT NULL, "
				+ "PRIMARY KEY (frm_invoice_id), "
				+ "KEY frm_dealer_id_idx (frm_invoice_dealer_id) "
				+ ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String createFirmPaymentTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create payment Table ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_payment ( "
				+ "frm_payment_id bigint(20) NOT NULL AUTO_INCREMENT, "
				+ "frm_payment_dealer_id bigint(20) DEFAULT NULL, "
				+ "frm_payment_invoice_id bigint(20) DEFAULT NULL, "
				+ "frm_payment_mode varchar(45) DEFAULT NULL, "
				+ "frm_payment_transaction_number varchar(45) DEFAULT NULL, "
				+ "frm_payment_amount_paid varchar(45) DEFAULT NULL, "
				+ "frm_payment_paid_date varchar(45) DEFAULT NULL, "
				+ "frm_payment_entered_by varchar(45) DEFAULT NULL, "
				+ "frm_payment_entered_date varchar(45) DEFAULT NULL, "
				+ "frm_payment_updated_by varchar(45) DEFAULT NULL, "
				+ "frm_payment_updated_date varchar(45) DEFAULT NULL, "
				+ "PRIMARY KEY (frm_payment_id), "
				+ "KEY frm_invoice_id_idx (frm_payment_invoice_id) "
				+ ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String createFirmProductTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create product Table ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_product ( "
				+ "frm_product_id bigint(20) NOT NULL AUTO_INCREMENT, "
				+ "frm_product_company varchar(45) DEFAULT NULL, "
				+ "frm_product_name varchar(45) DEFAULT NULL, "
				+ "frm_product_packed_quantity varchar(45) DEFAULT NULL, "
				+ "frm_product_packed_unit varchar(45) DEFAULT NULL, "
				+ "frm_product_category_id int(11) DEFAULT NULL, "
				+ "frm_product_sub_category_id int(11) DEFAULT NULL, "
				+ "frm_product_barcode varchar(45) DEFAULT NULL, "
				+ "frm_product_entered_by varchar(45) DEFAULT NULL, "
				+ "frm_product_entered_date varchar(45) DEFAULT NULL, "
				+ "frm_product_updated_by varchar(45) DEFAULT NULL, "
				+ "frm_product_updated_date varchar(45) DEFAULT NULL, "
				+ "PRIMARY KEY (frm_product_id), "
				+ "UNIQUE KEY frm_product_barcode_UNIQUE (frm_product_barcode) "
				+ ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String createFirmProductMapperTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create Product Mapper Table ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_product_mapper ( "
				+ "frm_product_mapper_id bigint(20) NOT NULL AUTO_INCREMENT, "
				+ "frm_product_id varchar(45) DEFAULT NULL, "
				+ "frm_purchase_id varchar(45) DEFAULT NULL, "
				+ "frm_sales_id varchar(45) DEFAULT NULL, "
				+ "PRIMARY KEY (frm_product_mapper_id) "
				+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String createFirmProductPurchaseTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create Product Purchase Details Table ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_product_purchase ( "
				+ "frm_purchase_id bigint(20) NOT NULL AUTO_INCREMENT, "
				+ "frm_purchase_product_id bigint(20) DEFAULT NULL, "
				+ "frm_purchase_dealer_id bigint(20) DEFAULT NULL, "
				+ "frm_purchase_invoice_id bigint(20) DEFAULT NULL, "
				+ "frm_purchase_mrp varchar(45) DEFAULT NULL, "
				+ "frm_purchase_tax varchar(45) DEFAULT NULL, "
				+ "frm_purchase_cess varchar(45) DEFAULT NULL, "
				+ "frm_purchase_packed_quantity varchar(45) DEFAULT NULL, "
				+ "frm_purchase_unit varchar(45) DEFAULT NULL, "
				+ "frm_purchase_unit_cost varchar(45) DEFAULT NULL, "
				+ "frm_purchase_paid_qty varchar(45) DEFAULT NULL, "
				+ "frm_purchase_free_qty varchar(45) DEFAULT NULL, "
				+ "frm_product_threshold_qty varchar(45) DEFAULT NULL, "
				+ "frm_purchase_primary_discount varchar(45) DEFAULT NULL, "
				+ "frm_purchase_secondary_discount varchar(45) DEFAULT NULL, "
				+ "frm_purchase_arrival_date varchar(45) DEFAULT NULL, "
				+ "frm_purchase_manufacture_date varchar(45) DEFAULT NULL, "
				+ "frm_purchase_expiry_date varchar(45) DEFAULT NULL, "
				+ "frm_purchase_entered_by varchar(45) DEFAULT NULL, "
				+ "frm_purchase_entered_date varchar(45) DEFAULT NULL, "
				+ "frm_purchase_updated_by varchar(45) DEFAULT NULL, "
				+ "frm_purchase_updated_date varchar(45) DEFAULT NULL, "
				+ "frm_purchase_rowstate int(11) DEFAULT NULL, "
				+ "PRIMARY KEY (frm_purchase_id) "
				+ ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String createFirmProductSalesTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create Product Sales Table ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_product_sales ( "
				+ "frm_sales_id bigint(20) NOT NULL AUTO_INCREMENT, "
				+ "frm_sales_product_id bigint(20) DEFAULT NULL, "
				+ "frm_sales_dealer_id bigint(20) DEFAULT NULL, "
				+ "frm_sales_invoice_id bigint(20) DEFAULT NULL, "
				+ "frm_sales_discount varchar(45) DEFAULT NULL, "
				+ "frm_sales_price varchar(45) DEFAULT NULL, "
				+ "frm_sales_tax varchar(45) DEFAULT NULL, "
				+ "frm_sales_cess varchar(45) DEFAULT NULL, "
				+ "frm_sales_sold_quantity varchar(45) DEFAULT NULL, "
				+ "frm_product_sales_entered_by varchar(45) DEFAULT NULL, "
				+ "frm_product_sales_entered_date varchar(45) DEFAULT NULL, "
				+ "frm_product_sales_updated_by varchar(45) DEFAULT NULL, "
				+ "frm_product_sales_updated_date varchar(45) DEFAULT NULL, "
				+ "frm_sales_rowstate int(11) DEFAULT NULL, "
				+ "PRIMARY KEY (frm_sales_id), "
				+ "KEY frm_product_id_idx (frm_sales_product_id) "
				+ ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String createFirmPurchaseBillConsolidatedTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create Purchase Bill Consolidated Table ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_purchase_bill_consolidated ( "
				+ "frm_purchase_bill_consolidated_id bigint(20) NOT NULL AUTO_INCREMENT, "
				+ "frm_purchase_dealer_id bigint(20) DEFAULT NULL, "
				+ "frm_purchase_invoice_id bigint(20) DEFAULT NULL, "
				+ "frm_purchase_date varchar(45) DEFAULT NULL, "
				+ "frm_purchase_gst_number varchar(45) DEFAULT NULL, "
				+ "frm_purchase_0 varchar(45) DEFAULT NULL, "
				+ "frm_purchase_5 varchar(45) DEFAULT NULL, "
				+ "frm_purchase_12 varchar(45) DEFAULT NULL, "
				+ "frm_purchase_18 varchar(45) DEFAULT NULL, "
				+ "frm_purchase_28 varchar(45) DEFAULT NULL, "
				+ "frm_purchase_cgst varchar(45) DEFAULT NULL, "
				+ "frm_purchase_sgst varchar(45) DEFAULT NULL, "
				+ "frm_purchase_cess varchar(45) DEFAULT NULL, "
				+ "frm_purchase_entered_date varchar(45) DEFAULT NULL, "
				+ "PRIMARY KEY (frm_purchase_bill_consolidated_id), "
				+ "UNIQUE KEY frm_purchase_invoice_UNIQUE (frm_purchase_invoice_id) "
				+ ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String createFirmSalesBillTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create Sales Bill Table ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_sales_bill ( "
				+ "frm_sales_bill_id bigint(20) NOT NULL AUTO_INCREMENT, "
				+ "frm_sales_bill_by varchar(45) DEFAULT NULL, "
				+ "frm_sales_bill_date varchar(45) DEFAULT NULL, "
				+ "frm_sales_bill_customer_name varchar(45) DEFAULT NULL, "
				+ "frm_sales_bill_customer_number varchar(45) DEFAULT NULL, "
				+ "frm_sales_bill_number varchar(45) DEFAULT NULL, "
				+ "frm_sales_dealer_id varchar(45) DEFAULT NULL, "
				+ "frm_sales_invoice_id varchar(45) DEFAULT NULL, "
				+ "frm_sales_product_id varchar(45) DEFAULT NULL, "
				+ "frm_sales_product_quantity varchar(45) DEFAULT NULL, "
				+ "frm_sales_bill_returned_by varchar(45) DEFAULT NULL, "
				+ "frm_sales_bill_returned_date varchar(45) DEFAULT NULL, "
				+ "  PRIMARY KEY (frm_sales_bill_id) "
				+ ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	public String createFirmSalesBillConsolidatedTableQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Building Query to create Sales Bill Consolidated ::: REST ::: Firm Query Builder");
		Firm firmObject = (Firm) parameters.get("firmData");
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("CREATE TABLE " + firmObject.getFirmDBName() + ".frm_sales_bill_consolidated ( "
				+ "frm_sales_bill_consolidated_id bigint(20) NOT NULL AUTO_INCREMENT, "
				+ "frm_sales_dealer_category_id bigint(20) DEFAULT NULL, "
				+ "frm_sales_date varchar(45) DEFAULT NULL, "
				+ "frm_sales_bill_start_number varchar(45) DEFAULT NULL, "
				+ "frm_sales_bill_end_number varchar(45) DEFAULT NULL, "
				+ "frm_sales_0 varchar(45) DEFAULT NULL, "
				+ "frm_sales_5 varchar(45) DEFAULT NULL, "
				+ "frm_sales_12 varchar(45) DEFAULT NULL, "
				+ "frm_sales_18 varchar(45) DEFAULT NULL, "
				+ "frm_sales_28 varchar(45) DEFAULT NULL, "
				+ "frm_sales_cgst varchar(45) DEFAULT NULL, "
				+ "frm_sales_sgst varchar(45) DEFAULT NULL, "
				+ "frm_sales_cess varchar(45) DEFAULT NULL, "
				+ "frm_sales_bill_updated_date varchar(45) DEFAULT NULL, "
				+ "PRIMARY KEY (frm_sales_bill_consolidated_id), "
				+ "UNIQUE KEY frm_sales_date_UNIQUE (frm_sales_date,frm_sales_dealer_category_id) "
				+ ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8");
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
}
