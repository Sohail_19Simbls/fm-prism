package com.simbls.prism.rest.clothing.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.industry.ProductSales;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Component
public interface ClothingProductSalesService {
	/**
	 * Register Product Sales Details
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public ProductSales productSalesRegister(ProductSales productSalesData) throws Exception;
	
	
	/**
	 * Register Product Sales Details
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public SalesBasket productSalesBasket(SalesBasket salesBasketData) throws Exception;
	
	/**
	 * Register Product Sales Details
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<SalesBasket> generateSalesBasketBill(List<SalesBasket> salesBasketData) throws Exception;
}
