package com.simbls.prism.rest.foodBazaar.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.industry.Accounts;

@Component
public interface FoodBazaarAccountsService {

	/**
	 * 
	 * @param accountsData
	 * @return
	 */
	public List<Accounts> retrieveConsolidatedPurchaseDetails(Accounts accountsData);
	
	/**
	 * 
	 * @param accountsData
	 * @return
	 */
	public List<Accounts> retrieveConsolidatedSalesDetails(Accounts accountsData);
	
}
