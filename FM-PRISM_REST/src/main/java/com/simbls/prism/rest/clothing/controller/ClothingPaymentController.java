package com.simbls.prism.rest.clothing.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.clothing.service.ClothingPaymentService;
import com.simbls.prism.rest.model.industry.Invoice;
import com.simbls.prism.rest.model.industry.Payment;


@Controller
@RequestMapping(value = "/clothing_payment")
public class ClothingPaymentController {
	private static final Logger logger = Logger.getLogger(ClothingPaymentController.class);
	@Autowired private ClothingPaymentService clothingPaymentService;
	
	/**
	 * Populating DB with Payment Details
	 * @param payment
	 * @param firmDBCredentials
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/paymentRegister", method = RequestMethod.POST)
	public Payment paymentRegister(@RequestBody Payment paymentData) throws Exception{
		logger.info("... Continuing Payment Registration ::: REST ::: ClothingPaymentController");
		clothingPaymentService.paymentRegister(paymentData);
		logger.info("Returning to WEB ::: REST ::: Payment Controller");
		return paymentData;
	}
	
	/**
	 * Retrieving list of all the Registered Payments
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/retrieveTodaysPaymentDetails",method=RequestMethod.POST)
	public ResponseEntity<Payment> retrieveTodaysPaymentDetails(@RequestBody Payment paymentData){
		logger.info("... Continuing retrival of Todays Payment Details ::: REST ::: ClothingPaymentController");
		Payment registeredInvoicePaymentList = clothingPaymentService.retrieveTodaysPaymentDetails(paymentData);
		logger.info("Returning Obtained Payment details to WEB ::: REST ::: ClothingPaymentController");
		return ResponseEntity.accepted().body(registeredInvoicePaymentList);
	}
	
	/**
	 * Retrieving list of all the Registered Payments
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/retrievePaymentDetails",method=RequestMethod.POST)
	public ResponseEntity<List<Invoice>> retrievePaymentDetails(@RequestBody Invoice invoiceData){
		logger.info("... Continuing retrival of Payment Details list ::: REST ::: ClothingPaymentController");
		List<Invoice> retrievePaymentList = clothingPaymentService.retrievePaymentDetails(invoiceData);
		logger.info("Returning Obtained Payment list of size " + retrievePaymentList.size() + " to WEB ::: REST ::: ClothingPaymentController");
		return ResponseEntity.accepted().body(retrievePaymentList);
	}
}
