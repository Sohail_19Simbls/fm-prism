package com.simbls.prism.rest.central.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.central.service.ProductSubCategoryService;
import com.simbls.prism.rest.dao.central.ProductSubCategoryDao;
import com.simbls.prism.rest.model.central.ProductSubCategory;

@Service
public class ProductSubCategoryServiceImpl implements ProductSubCategoryService {
	private static final Logger logger = Logger.getLogger(ProductSubCategoryServiceImpl.class);
	@Autowired private ProductSubCategoryDao productSubCategoryDao;

	public List<ProductSubCategory> retrieveProductSubCategories(ProductSubCategory productSubCategory) {
		logger.info("Retrieving Product Sub Categories from Central DB of Category - " + productSubCategory.getProductCategoryID() + " ::: REST ::: ProductSubCategoryServiceImpl");
		List<ProductSubCategory> productSubCategoryList = productSubCategoryDao.retrieveProductSubCategoriesDAO(productSubCategory.getProductCategoryID());
		logger.info("Successfully obtained Product Sub Categories size - " + productSubCategoryList.size() + " ::: REST ::: ProductSubCategoryServiceImpl");
		return productSubCategoryList;
	}

}
