package com.simbls.prism.rest.helper;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.dao.foodBazaar.FoodBazaarProductSalesDao;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Service
public class GSTSalesReportGenerator {
	private static final Logger logger = Logger.getLogger(GSTSalesReportGenerator.class);
	@Autowired private FoodBazaarProductSalesDao foodBazaarProductSalesDAO;
	
	public void salesDetailsRegister(List<SalesBasket> salesBasketData) throws Exception {
		logger.info("Preparing data to be inserted into Sales Bill Consolidated Table :: REST :: GSTSalesReportGenerator");
		for(SalesBasket soldItemObject : salesBasketData){
			String tempPercentage =  soldItemObject.getProductPurchaseTax();
			logger.info("Initiating Check for obtained Percentage - " + tempPercentage + " ::: REST ::: Product Purchase Impl");
			if(tempPercentage.equals("0.00")){
				soldItemObject.setProductSales_For_0_Tax_Amount(soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 0% being added. Amount - " + soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
			}else if(tempPercentage.equals("0.05")){
				soldItemObject.setProductSales_For_5_Tax_Amount(soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 5% being added. Amount - " + soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
			}else if(tempPercentage.equals("0.12")){
				soldItemObject.setProductSales_For_12_Tax_Amount(soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 12% being added. Amount - " + soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
			}else if(tempPercentage.equals("0.18")){
				soldItemObject.setProductSales_For_18_Tax_Amount(soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 18% being added. Amount - " + soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
			}else if(tempPercentage.equals("0.28")){
				soldItemObject.setProductSales_For_28_Tax_Amount(soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
				logger.info("Product under Tax Slab 28% being added. Amount - " + soldItemObject.getTotalSalesQuantityProductTotalCostWithoutTax());
			}
			soldItemObject.setProductSales_Effective_CGST(String.valueOf(Float.parseFloat(soldItemObject.getProductSales_TaxAmount())/2));
			soldItemObject.setProductSales_Effective_SGST(String.valueOf(Float.parseFloat(soldItemObject.getProductSales_TaxAmount())/2));
		}
		logger.info("Data prepared. Initiating inserton...");
		foodBazaarProductSalesDAO.productSalesConsolidated_UPSERT_DAO(salesBasketData);
		logger.info("Data successfully inserted into Sales Bill Consolidated Table :: REST :: GSTSalesReportGenerator");
	}

}
