package com.simbls.prism.rest.helper;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.dao.foodBazaar.FoodBazaarProductPurchaseDao;
import com.simbls.prism.rest.model.industry.ProductPurchase;

@Service
public class GSTPurchaseReportGenerator {
	private static final Logger logger = Logger.getLogger(GSTPurchaseReportGenerator.class);
	@Autowired private FoodBazaarProductPurchaseDao foodBazaarProductPurchaseDAO;
	
	public void purchaseDetailsRegister(ProductPurchase productPurchaseData) throws Exception {
		logger.info("Initiating insertion into Purchase Bill Consolidated Table. Target DB - " + productPurchaseData.getFirmDBName() + " ::: REST ::: Product Purchase Service Impl");
		String tempPercentage =  productPurchaseData.getProductPurchaseTax();
		logger.info("Initiating Check for obtained Percentage - " + tempPercentage + " ::: REST ::: Product Purchase Impl");
		if(tempPercentage.equals("0.00")){
			productPurchaseData.setProductPurchase_For_0_Tax_Amount(productPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
			logger.info("Product under Tax Slab 0% being added. Amount - " + productPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
		}else if(tempPercentage.equals("0.05")){
			productPurchaseData.setProductPurchase_For_5_Tax_Amount(productPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
			logger.info("Product under Tax Slab 5% being added. Amount - " + productPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
		}else if(tempPercentage.equals("0.12")){
			productPurchaseData.setProductPurchase_For_12_Tax_Amount(productPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
			logger.info("Product under Tax Slab 12% being added. Amount - " + productPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
		}else if(tempPercentage.equals("0.18")){
			productPurchaseData.setProductPurchase_For_18_Tax_Amount(productPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
			logger.info("Product under Tax Slab 18% being added. Amount - " + productPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
		}else if(tempPercentage.equals("0.28")){
			productPurchaseData.setProductPurchase_For_28_Tax_Amount(productPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
			logger.info("Product under Tax Slab 28% being added. Amount - " + productPurchaseData.getTotalQuantityProductTotalCostWithoutTax());
		}
		productPurchaseData.setProductPurchase_Effective_CGST(String.valueOf(Float.parseFloat(productPurchaseData.getProductPurchase_TaxAmount())/2));
		productPurchaseData.setProductPurchase_Effective_SGST(String.valueOf(Float.parseFloat(productPurchaseData.getProductPurchase_TaxAmount())/2));
		logger.info("Initiating insertion ... ");
		foodBazaarProductPurchaseDAO.productPurchaseConsolidated_UPSERT_DAO(productPurchaseData);
		logger.info("Successfully inserted GST Purchase Details ::: REST ::: GSTPurchaseReportGenerator");
	}
}
