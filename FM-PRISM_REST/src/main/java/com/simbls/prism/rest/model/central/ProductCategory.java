package com.simbls.prism.rest.model.central;

public class ProductCategory {

	private int productCategoryID;
	private int productIndustryID;
	private String productCategory;
	
	private ProductCategory productCategoryObject;

	public int getProductCategoryID() {
		return productCategoryID;
	}

	public void setProductCategoryID(int productCategoryID) {
		this.productCategoryID = productCategoryID;
	}

	public int getProductIndustryID() {
		return productIndustryID;
	}

	public void setProductIndustryID(int productIndustryID) {
		this.productIndustryID = productIndustryID;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public ProductCategory getProductCategoryObject() {
		return productCategoryObject;
	}

	public void setProductCategoryObject(ProductCategory productCategoryObject) {
		this.productCategoryObject = productCategoryObject;
	}

	
	
	
}
