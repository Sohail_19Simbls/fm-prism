package com.simbls.prism.rest.ceramics.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.industry.Product;

@Component
public interface CeramicsProductService {
	/**
	 * Register Product details
	 * @param productData
	 * @throws Exception
	 */
	public Product productRegister(Product productData) throws Exception;
	
	/**
	 * Register Product details
	 * @param productData
	 * @throws Exception
	 */
	public Product productUpdate(Product productData) throws Exception;
	
	/**
	 * Retrieve all Product by BarCode.
	 * @return
	 */
	public List<Product> retrieveProductByParameter(Product productData);
	
	
	
}
