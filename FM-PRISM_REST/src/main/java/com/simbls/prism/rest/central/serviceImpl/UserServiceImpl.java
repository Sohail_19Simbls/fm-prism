package com.simbls.prism.rest.central.serviceImpl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.rest.central.service.UserService;
import com.simbls.prism.rest.dao.central.UserDao;
import com.simbls.prism.rest.helper.PasswordEncodeDecodeGen;
import com.simbls.prism.rest.model.central.Login;
import com.simbls.prism.rest.model.central.User;


@Service
public class UserServiceImpl implements UserService {
	private static final Logger logger = Logger.getLogger(UserServiceImpl.class);
	@Autowired private UserDao userCentralDao;

	
	/**
	 * Login Credential Validation - Directed from UserService.java
	 * All the User Details are stored in the central DB
	 * Redirecting the DAO requests to the Central DAO
	 */
	
	public User validateCredentials(Login login) {
		logger.info("Performing Credential Validation .... ");
		logger.info("Initiated User-Name - " + login.getUserName() +
					" ::: Initiated Password - " + login.getUserPassword());
		User userObject = new User();
		userObject = userCentralDao.getPasswordDAO(login.getUserName());
		if (userObject != null) {
			logger.info("Subjecting entered password for validation  ::: REST ::: Serving from User Service Impl");
			if (PasswordEncodeDecodeGen.checkPW(login.getUserPassword(), userObject.getUserPassword())) {
				logger.info("User successfully validated ::: REST ::: For user - " + login.getUserName());		
				userObject = userCentralDao.retrieveUserDAO(login.getUserId(), login.getUserName(), userObject.getUserPassword());
				logger.info("Logging in User ::: " + userObject.getUserName() + " ::: REST ::: Serving from Service Impl" );
			} else {
				logger.info("User Validation FAILED for - " + login.getUserName() + " ::: REST ::: Serving from User Service Impl");
				userObject = new User();
			}
		}
		return userObject;
	}

	public User getUserDetails(long userID) {
		logger.info("Initiating Extraction of User Details based on User ID ::: REST ::: Initiating from User Service IMPL");
		logger.info("Initiating for the User with the ID ::: " + userID);
		User userObject = userCentralDao.retrieveUserDAO(userID, null, null);
		logger.info("Successfully Obtained User Details ::: REST ::: User Service Impl");
		return userObject;		
	}
	
	public void resetPasswordFirstLogin(User user) {
		logger.info("Initiating reset passowrd on First Login ::: REST ::: UserServiceImpl");
		String resetUserPasswordFirstLoginQuery = "SET 	cnt_user_id = #{" + user.getUserId() + "}, "
														+ 	"cnt_user_password = #{" + user.getUserPassword() + "}, "
														+ 	"cnt_user_first_login = #{" + user.getUserFirstLogin() + "} ";
		userCentralDao.updateUserDAO(resetUserPasswordFirstLoginQuery);
		logger.info("Pasword for User with userID - " + user.getUserId() + " ::: REST ::: UserServiceImpl");
	}
}
