package com.simbls.prism.rest.dao.central;

import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import com.simbls.prism.rest.central.builder.UserQueryBuilder;
import com.simbls.prism.rest.model.central.User;

@Component("userCentralDao")
public interface UserDao {

	/**
	 * 
	 * @param userUsername
	 * @return UserObject with Password
	 * @author Sohail Razvi
	 */

	@Select("Select * from cnt_user where cnt_user_username = #{cnt_user_username}")
	@Results({ @Result(property = "userPassword", 	column = "cnt_user_password") })
	public User getPasswordDAO(@Param("cnt_user_username") String userName);

	/**
	 * Master searchQuery to search individual User
	 * @param PreparedQuery
	 * @return Complete User Object
	 * @author Sohail Razvi
	 */
	@SelectProvider(type = UserQueryBuilder.class, method = "getUsersProvider")
	@Results({ 
		@Result(property = "userId",				column = "cnt_user_id"),
		@Result(property = "firmObject",			column = "cnt_user_firm_id",		one=@One(select="com.simbls.prism.rest.dao.central.FirmDao.getFirmDetailsByIdDAO")),
		@Result(property = "userFirstName",			column = "cnt_user_first_name"),
		@Result(property = "userLastName",			column = "cnt_user_last_name"),
		@Result(property = "userName",				column = "cnt_user_username"),
		@Result(property = "userPassword",			column = "cnt_user_password"),
		@Result(property = "userFirstLogin",		column = "cnt_user_first_login"),
		@Result(property = "userContactOne",		column = "cnt_user_contact_one"),
		@Result(property = "userContactTwo",		column = "cnt_user_contact_two"),
		@Result(property = "userHouseNo",			column = "cnt_user_house_no"),
		@Result(property = "userHouseName",			column = "cnt_user_building_name"),
		@Result(property = "userStreet",			column = "cnt_user_street"),
		@Result(property = "userLocality",			column = "cnt_user_locality"),
		@Result(property = "userCity",				column = "cnt_user_city"),
		@Result(property = "userPincode",			column = "cnt_user_pincode"),
		@Result(property = "userState",				column = "cnt_user_state"),
		@Result(property = "userCategory",			column = "cnt_user_category"),
		@Result(property = "userParentId",			column = "cnt_user_parent_id"),
		@Result(property = "userAddedBy",			column = "cnt_user_added_by"),
		@Result(property = "userAddedDate",			column = "cnt_user_added_date"),
		@Result(property = "userUpdatedBy",			column = "cnt_user_updated_by"),
		@Result(property = "userUpdatedDate",		column = "cnt_user_updated_date"),
		@Result(property = "userApprovedState",		column = "cnt_user_approved_state"),
		@Result(property = "userApprovedBy",		column = "cnt_user_approved_by"),
		@Result(property = "userApprovedDate",		column = "cnt_user_approved_date"),
		@Result(property = "userDeletedDate",		column = "cnt_user_deleted_date"),
		@Result(property = "userRowState",			column = "cnt_user_row_state")
	})
	public User retrieveUserDAO(@Param("userId") long userId, @Param("userName") String userName, @Param("userPassword") String userPassword);

	/**
	 * Update User details
	 * @param conditionalQuery
	 */
	@Update("${conditionalQuery}")
	public void updateUserDAO(@Param("conditionalQuery") Object conditionalQuery);

	
}
