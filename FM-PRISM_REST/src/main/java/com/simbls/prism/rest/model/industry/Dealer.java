package com.simbls.prism.rest.model.industry;

import java.util.ArrayList;
import java.util.List;

import com.simbls.prism.rest.model.central.DealerCategory;


public class Dealer {
	private long dealerID;
	private int dealerCategoryID;
	private String dealerName;
	private String dealerContactOne;
	private String dealerContactTwo;
	private String dealerEmail;
	private String dealerAddress;
	private String dealerGST;
	private String dealerTIN;
	private String dealerPAN;
	private String dealerEnteredBy;
	private String dealerEnteredDate;
	private String dealerUpdatedBy;
	private String dealerUpdatedDate;
	
	/* ---------------------------------------- */
	/*	Parameters related to specific Industry and Firm	*/
	/* ---------------------------------------- */
	private String industryCategory;
	private String firmDBName;
	private String firmDBUserName;
	private String firmDBPassword;
	
	private DealerCategory dealerCategoryObject;
	private Dealer dealerObject;
	
	/* ---------------------------------------- */
	/*	Parameters related to Dealer Details	*/
	/* ---------------------------------------- */
	private List<Invoice> invoiceList = new ArrayList<Invoice>();

	public long getDealerID() {
		return dealerID;
	}

	public void setDealerID(long dealerID) {
		this.dealerID = dealerID;
	}

	public int getDealerCategoryID() {
		return dealerCategoryID;
	}

	public void setDealerCategoryID(int dealerCategoryID) {
		this.dealerCategoryID = dealerCategoryID;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getDealerContactOne() {
		return dealerContactOne;
	}

	public void setDealerContactOne(String dealerContactOne) {
		this.dealerContactOne = dealerContactOne;
	}

	public String getDealerContactTwo() {
		return dealerContactTwo;
	}

	public void setDealerContactTwo(String dealerContactTwo) {
		this.dealerContactTwo = dealerContactTwo;
	}

	public String getDealerEmail() {
		return dealerEmail;
	}

	public void setDealerEmail(String dealerEmail) {
		this.dealerEmail = dealerEmail;
	}

	public String getDealerAddress() {
		return dealerAddress;
	}

	public void setDealerAddress(String dealerAddress) {
		this.dealerAddress = dealerAddress;
	}

	public String getDealerGST() {
		return dealerGST;
	}

	public void setDealerGST(String dealerGST) {
		this.dealerGST = dealerGST;
	}

	public String getDealerTIN() {
		return dealerTIN;
	}

	public void setDealerTIN(String dealerTIN) {
		this.dealerTIN = dealerTIN;
	}

	public String getDealerPAN() {
		return dealerPAN;
	}

	public void setDealerPAN(String dealerPAN) {
		this.dealerPAN = dealerPAN;
	}

	public String getDealerEnteredBy() {
		return dealerEnteredBy;
	}

	public void setDealerEnteredBy(String dealerEnteredBy) {
		this.dealerEnteredBy = dealerEnteredBy;
	}

	public String getDealerEnteredDate() {
		return dealerEnteredDate;
	}

	public void setDealerEnteredDate(String dealerEnteredDate) {
		this.dealerEnteredDate = dealerEnteredDate;
	}

	public String getDealerUpdatedBy() {
		return dealerUpdatedBy;
	}

	public void setDealerUpdatedBy(String dealerUpdatedBy) {
		this.dealerUpdatedBy = dealerUpdatedBy;
	}

	public String getDealerUpdatedDate() {
		return dealerUpdatedDate;
	}

	public void setDealerUpdatedDate(String dealerUpdatedDate) {
		this.dealerUpdatedDate = dealerUpdatedDate;
	}

	public String getIndustryCategory() {
		return industryCategory;
	}

	public void setIndustryCategory(String industryCategory) {
		this.industryCategory = industryCategory;
	}

	public String getFirmDBName() {
		return firmDBName;
	}

	public void setFirmDBName(String firmDBName) {
		this.firmDBName = firmDBName;
	}

	public String getFirmDBUserName() {
		return firmDBUserName;
	}

	public void setFirmDBUserName(String firmDBUserName) {
		this.firmDBUserName = firmDBUserName;
	}

	public String getFirmDBPassword() {
		return firmDBPassword;
	}

	public void setFirmDBPassword(String firmDBPassword) {
		this.firmDBPassword = firmDBPassword;
	}

	public DealerCategory getDealerCategoryObject() {
		return dealerCategoryObject;
	}

	public void setDealerCategoryObject(DealerCategory dealerCategoryObject) {
		this.dealerCategoryObject = dealerCategoryObject;
	}

	public Dealer getDealerObject() {
		return dealerObject;
	}

	public void setDealerObject(Dealer dealerObject) {
		this.dealerObject = dealerObject;
	}

	public List<Invoice> getInvoiceList() {
		return invoiceList;
	}

	public void setInvoiceList(List<Invoice> invoiceList) {
		this.invoiceList = invoiceList;
	}
	
}
