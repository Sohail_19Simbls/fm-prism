package com.simbls.prism.rest.foodBazaar.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.foodBazaar.service.FoodBazaarPaymentService;
import com.simbls.prism.rest.model.industry.Payment;


@Controller
@RequestMapping(value = "/foodBazaar_payment")
public class FoodBazaarPaymentController {
	private static final Logger logger = Logger.getLogger(FoodBazaarPaymentController.class);
	@Autowired private FoodBazaarPaymentService foodBazaarPaymentService;
	
	/**
	 * Populating DB with Payment Details
	 * @param payment
	 * @param firmDBCredentials
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/paymentRegister", method = RequestMethod.POST)
	public Payment paymentRegister(@RequestBody Payment paymentData) throws Exception{
		logger.info("... Continuing Payment Registration ::: REST ::: FoodBazaarPaymentController");
		foodBazaarPaymentService.paymentRegister(paymentData);
		logger.info("Returning to WEB ::: REST ::: Payment Controller");
		return paymentData;
	}
	
	/**
	 * Retrieving list of all the Registered Payments
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/retrieveTodaysPaymentDetails",method=RequestMethod.POST)
	public ResponseEntity<Payment> retrieveTodaysPaymentDetails(@RequestBody Payment paymentData){
		logger.info("... Continuing retrival of Todays Payment Details ::: REST ::: FoodBazaarPaymentController");
		Payment registeredInvoicePaymentList = foodBazaarPaymentService.retrieveTodaysPaymentDetails(paymentData);
		logger.info("Returning Obtained Payment details to WEB ::: REST ::: FoodBazaarPaymentController");
		return ResponseEntity.accepted().body(registeredInvoicePaymentList);
	}
	
	/**
	 * Retrieving list of all the Registered Payments
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/retrieveConditionalPaymentDetails",method=RequestMethod.POST)
	public ResponseEntity<Payment> retrieveConditionalPaymentDetails(@RequestBody Payment paymentData){
		logger.info("... Continuing retrival of Payment Details list ::: REST ::: FoodBazaarPaymentController");
		Payment retrieveConditionalPaymentList = foodBazaarPaymentService.retrieveConditionalPaymentDetails(paymentData);
		logger.info("Returning Obtained Payment details to WEB ::: REST ::: FoodBazaarPaymentController");
		return ResponseEntity.accepted().body(retrieveConditionalPaymentList);
	}
}
