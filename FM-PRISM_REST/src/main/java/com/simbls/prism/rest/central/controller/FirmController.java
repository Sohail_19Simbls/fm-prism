package com.simbls.prism.rest.central.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.central.service.FirmService;
import com.simbls.prism.rest.model.central.Firm;

@Controller
@RequestMapping(value = "/firm")
public class FirmController {
	private static final Logger logger = Logger.getLogger(FirmController.class);
	@Autowired private FirmService firmService;
	

	/**
	 * Registring Firm. Firm registration is performed by On Boarding Team or SuperAdmin.
	 * The following is the details of categories of Firms
	 * Firm Category - 1 ::: Role - Manufacturer
	 * Firm Category - 2 ::: Role - Super Stockist
	 * Firm Category - 3 ::: Role - Firm
	 * Firm Category - 4 ::: Role - Retailer
	 * ****************************************************
	 * IMPORTANT STATUS FLAGS - According to the way condition is checked
	 * ****************************************************
	 * Firm Row State ::: 0 - Firm is Active
	 * Firm Row State ::: 1 - Firm is Deleted
	 * ****************************************************
	 * Firm Approval State ::: 1 - Firm is Approved
	 * Firm Approval State ::: 0 - Firm is Not Approved
	 * ****************************************************
	 * @param Firm Object
	 * @return Success Message
	 * @author Mudasser Nawaz
	 */
	
	@ResponseBody
	@RequestMapping(value = "/firmRegister", method = RequestMethod.POST)
	public Firm firmRegister(@RequestBody Firm firmData) throws Exception{
		logger.info("... Continuing Firm Registration ::: REST ::: FirmController");
		firmService.firmRegister(firmData);
		logger.info("Returning to WEB ::: REST ::: Firm Controller");
		return firmData;
	}


}
