package com.simbls.prism.rest.dao.foodBazaar;

import java.util.List;

import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Component;

import com.simbls.prism.rest.foodBazaar.builder.FoodBazaarAccountsQueryBuilder;
import com.simbls.prism.rest.model.central.DealerCategory;
import com.simbls.prism.rest.model.industry.Accounts;
import com.simbls.prism.rest.model.industry.Dealer;
import com.simbls.prism.rest.model.industry.Invoice;

@Component("foodBazaarAccountsDAO")
public interface FoodBazaarAccountsDao {

	/**
	 * 
	 * @param accountsData
	 * @return
	 */
	@SelectProvider(type = FoodBazaarAccountsQueryBuilder.class, method = "retrieveConsolidatedPurchaseDetailsQuery")
	@Results({
		@Result(property="consolidatedPurchaseID",		column="purchase_consolidated_id"),
		@Result(property="dealerObject",				column="{purchaseDealerID=dealer_id, firmDBName=firm_DB_name}",				one=@One(select = "retrieveDealerDAO")),
		@Result(property="invoiceObject",				column="{purchaseInvoiceID=invoice_id, firmDBName=firm_DB_name}",			one=@One(select = "retrieveInvoiceDAO")),
		@Result(property="purchaseDate", 				column="purchase_date"),
		@Result(property="purchase_0_slab",				column="purchase_0"),
		@Result(property="purchase_5_slab",				column="purchase_5"),
		@Result(property="purchase_12_slab",			column="purchase_12"),
		@Result(property="purchase_18_slab",			column="purchase_18"),
		@Result(property="purchase_28_slab",			column="purchase_28"),
		@Result(property="purchase_CGST",				column="purchase_CGST"),
		@Result(property="purchase_SGST",				column="purchase_SGST"),
		@Result(property="purchase_CESS",				column="purchase_CESS"),
		@Result(property="purchaseEnteredDate",			column="purchase_entered_date")		
	})
	public List<Accounts> retrieveConsolidatedPurchaseDetailsDAO(@Param("accountsData") Accounts accountsData);
	
	/**
	 * 
	 * @param accountsData
	 * @return
	 */
	@SelectProvider(type = FoodBazaarAccountsQueryBuilder.class, method = "retrieveConsolidatedSalesDetailsQuery")
	@Results({
		@Result(property="consolidatedSalesID",		column="sales_consolidated_id"),
		@Result(property="salesDealerCategoryID",	column="sales_dealer_category_id"),
		@Result(property="salesDate",				column="sales_date"),
		@Result(property="salesBillEndNumber", 		column="sales_bill_end_number"),
		@Result(property="sales_0_slab",			column="sales_0"),
		@Result(property="sales_5_slab",			column="sales_5"),
		@Result(property="sales_12_slab",			column="sales_12"),
		@Result(property="sales_18_slab",			column="sales_18"),
		@Result(property="sales_28_slab",			column="sales_28"),
		@Result(property="sales_CGST",				column="sales_CGST"),
		@Result(property="sales_SGST",				column="sales_SGST"),
		@Result(property="sales_CESS",				column="sales_CESS")	
	})
	public List<Accounts> retrieveConsolidatedSalesDetailsDAO(@Param("accountsData") Accounts accountsData);
	
	
	@SelectProvider(type = FoodBazaarAccountsQueryBuilder.class, method = "retrieveDealerQuery")
	@Results({
		@Result(property="dealerID",				column="frm_dealer_id"),
		@Result(property="dealerCategoryObject",	column="frm_dealer_category_id",	one=@One(select="retrieveDealerCategoryDAO")),
		@Result(property="dealerName",				column="frm_dealer_name"),
		@Result(property="dealerContactOne",		column="frm_dealer_contact_one"),
		@Result(property="dealerContactTwo",		column="frm_dealer_contact_two"),
		@Result(property="dealerEmail",				column="frm_dealer_email"),
		@Result(property="dealerAddress",			column="frm_dealer_address"),
		@Result(property="dealerGST",				column="frm_dealer_gst"),
		@Result(property="dealerTIN",				column="frm_dealer_tin"),
		@Result(property="dealerPAN",				column="frm_dealer_pan")			
	})
	public Dealer retrieveDealerDAO(@Param("purchaseDealerID") long purchaseDealerID, @Param("firmDBName") String firmDBName);
	
	@Select("SELECT * from cnt_dealer_category where cnt_dealer_category_id=#{dealerCategoryID}")
	@Results({
		@Result(property="dealerCategoryID",		column="cnt_dealer_category_id"),
		@Result(property="dealerCategory",			column="cnt_dealer_category")
	})
	public DealerCategory retrieveDealerCategoryDAO(@Param("dealerCategoryID") int dealerCategoryID);
	
	@SelectProvider(type = FoodBazaarAccountsQueryBuilder.class, method = "retrieveInvoiceQuery")
	@Results({
		@Result(property="invoiceID",							column="frm_invoice_id"),
		@Result(property="dealerID",							column="frm_dealer_id"),
		@Result(property="dealerName",							column="frm_dealer_name"),
		@Result(property="invoiceNumber",						column="frm_invoice_number"),
		@Result(property="invoiceBillingDate",					column="frm_invoice_bill_date"),
		@Result(property="invoiceDeliveryDate",					column="frm_invoice_delivery_date"),
		@Result(property="invoiceAmount",						column="frm_invoice_amount"),
		@Result(property="invoiceMarketFees",					column="frm_invoice_market_fee"),
		@Result(property="invoiceTransportationUnloadingFees",	column="frm_invoice_transportation_unloading_fee"),
		@Result(property="invoicePaymentDueDate",				column="frm_invoice_payment_due_date"),
		@Result(property="invoiceEnteredBy",					column="frm_invoice_entered_by"),
		@Result(property="invoiceEnteredDate",					column="frm_invoice_entered_date"),
		@Result(property="invoiceUpdatedBy",					column="frm_invoice_updated_by"),
		@Result(property="invoiceUpdatedDate",					column="frm_invoice_updated_date")
	})
	public Invoice retrieveInvoiceDAO(@Param("purchaseInvoiceID") long purchaseInvoiceID, @Param("firmDBName") String firmDBName);
	
	
}
