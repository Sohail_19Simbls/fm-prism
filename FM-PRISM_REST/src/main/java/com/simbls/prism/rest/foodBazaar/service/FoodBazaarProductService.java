package com.simbls.prism.rest.foodBazaar.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.rest.model.industry.Product;

@Component
public interface FoodBazaarProductService {
	/**
	 * Register Product details
	 * @param productData
	 * @throws Exception
	 */
	public Product productRegister(Product productData) throws Exception;
	
	/**
	 * Register Product details
	 * @param productData
	 * @throws Exception
	 */
	public Product productUpdate(Product productData) throws Exception;
	
	/**
	 * Retrieve all Product by BarCode.
	 * @return
	 */
	public List<Product> retrieveProductByParameter(Product productData);
	
	/**
	 * Retrieve Product Purchase and Sales Details
	 * @return
	 */
	
	public Product retrieveProductPurchaseSaleDetails(Product productData);
	
}
