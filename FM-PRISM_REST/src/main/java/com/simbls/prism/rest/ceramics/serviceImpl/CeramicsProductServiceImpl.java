package com.simbls.prism.rest.ceramics.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.simbls.prism.rest.dao.ceramics.CeramicsProductDao;
import com.simbls.prism.rest.ceramics.service.CeramicsProductService;
import com.simbls.prism.rest.model.industry.Product;

@Service
public class CeramicsProductServiceImpl implements CeramicsProductService {
	private static final Logger logger = Logger.getLogger(CeramicsProductServiceImpl.class);
	@Autowired private CeramicsProductDao ceramicsProductDAO;

	/**
	 * 
	 */
	public Product productRegister(Product productData) {
		logger.info("Initiating insertion into Product Table - Target DB " + productData.getFirmDBName() + " ::: REST ::: CeramicsProductServiceImpl");
		Product insertingProduct = new Product();
		try{
			ceramicsProductDAO.productRegisterDAO(productData);
			logger.info("Product Successfully Populated ::: REST ::: CeramicsProductServiceImpl");
			insertingProduct = productData;
		} catch (MySQLIntegrityConstraintViolationException  e){
			logger.info("Product - " + insertingProduct.getProductName() + " was not Populated. ::: REST ::: CeramicsProductServiceImpl");
			logger.error("Constraint violation", e);
		} catch (Exception e){
			logger.info("Product - " + insertingProduct.getProductName() + " was not Populated. ::: REST ::: CeramicsProductServiceImpl");
			logger.error("Constraint violation", e);
	    }
		logger.info("Returning Product with ProductName - " + insertingProduct.getProductName() + " ::: REST ::: CeramicsProductServiceImpl");
		return insertingProduct;
	}
	
	/**
	 * 
	 */
	public Product productUpdate(Product productData) {
		logger.info("Initiating updation of Product Table - Target DB " + productData.getFirmDBName() + " ::: REST ::: CeramicsProductServiceImpl");
		Product updatingProduct = new Product();
		try{
			ceramicsProductDAO.productUpdateDAO(productData);
			logger.info("Product Successfully Updated ::: REST ::: CeramicsProductServiceImpl");
			updatingProduct = productData;
		} catch (MySQLIntegrityConstraintViolationException  e){
			logger.info("Product - " + updatingProduct.getProductName() + " was not Updated. ::: REST ::: CeramicsProductServiceImpl");
			logger.error("Constraint violation", e);
		} catch (Exception e){
			logger.info("Product - " + updatingProduct.getProductName() + " was not Updated. ::: REST ::: CeramicsProductServiceImpl");
			logger.error("Constraint violation", e);
	    }
		logger.info("Returning Product with ProductName - " + updatingProduct.getProductName() + " ::: REST ::: CeramicsProductServiceImpl");
		return updatingProduct;
	}
	
	public List<Product> retrieveProductByParameter(Product productData) {
		logger.info("Initiating DB search to retrieve Product List. Target DB - " + productData.getFirmDBName() + "::: REST ::: CeramicsProductService");
		List<Product> retrievedProduct = ceramicsProductDAO.retrieveProductByParameterDAO(productData);
		logger.info("Successfully obtained - " + retrievedProduct.size() + " registered Products");
		logger.info("Returning to Product Controller ::: REST ::: CeramicsProductServiceImpl");
		return retrievedProduct;
	}

	
}
