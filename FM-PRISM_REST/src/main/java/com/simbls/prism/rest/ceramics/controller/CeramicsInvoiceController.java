package com.simbls.prism.rest.ceramics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.rest.ceramics.service.CeramicsInvoiceService;
import com.simbls.prism.rest.model.industry.Invoice;


@Controller
@RequestMapping(value = "/ceramics_invoice")
public class CeramicsInvoiceController {
	private static final Logger logger = Logger.getLogger(CeramicsInvoiceController.class);
	@Autowired private CeramicsInvoiceService ceramicsInvoiceService;
	
	/**
	 * Populating DB with Invoice Details
	 * @param invoice
	 * @param firmDBCredentials
	 * @param modelMap
	 * @return invoiceData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@ResponseBody
	@RequestMapping(value = "/invoiceRegister", method = RequestMethod.POST)
	public Invoice invoiceRegister(@RequestBody Invoice invoiceData) throws Exception{
		logger.info("... Continuing Invoice Registration ::: REST ::: CeramicsInvoiceController");
		ceramicsInvoiceService.invoiceRegister(invoiceData);
		logger.info("Returning to WEB ::: REST ::: Invoice Controller");
		return invoiceData;
	}
	
	/**
	 * Updating DB with Invoice Details
	 * @param invoice
	 * @param firmDBCredentials
	 * @param modelMap
	 * @return invoiceData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@ResponseBody
	@RequestMapping(value = "/invoiceUpdate", method = RequestMethod.POST)
	public Invoice invoiceUpdate(@RequestBody Invoice invoiceData) throws Exception{
		logger.info("... Continuing Invoice Updation ::: REST ::: CeramicsInvoiceController");
		ceramicsInvoiceService.invoiceUpdate(invoiceData);
		logger.info("Returning to WEB ::: REST ::: Invoice Controller");
		return invoiceData;
	}
	
	/**
	 * Retrieving list of all the Registered Invoices
	 * @return List of all the Registered Invoices
	 * @author Sohail Razvi
	 */
	@ResponseBody
	@RequestMapping(value="/retrieveInvoiceList",method=RequestMethod.POST)
	public ResponseEntity<List<Invoice>> retrieveInvoiceList(@RequestBody Invoice invoiceData){
		logger.info("... Continuing retrival of Invoice list ::: REST ::: CeramicsInvoiceController");
		List<Invoice> retrieveInvoiceList = ceramicsInvoiceService.retrieveInvoiceList(invoiceData);
		logger.info("Returning Obtained Invoice list of size " + retrieveInvoiceList.size() + " to WEB ::: REST ::: CeramicsInvoiceController");
		return ResponseEntity.accepted().body(retrieveInvoiceList);
	}
	
	/**
	 * Retrieve Invoice Details Along with Products Purchased Against a Specific to Invoice
	 * By Invoice ID from Firm DB
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@ResponseBody
	@RequestMapping(value="/retrieveInvoiceDetails", method=RequestMethod.POST)
	public ResponseEntity<List<Invoice>> retrieveInvoiceDetails(@RequestBody Invoice invoiceData) {
		logger.info("... Continuing retrival of all the Products Purchased against InvoiceID - " + invoiceData.getInvoiceID() + " ::: REST ::: Invoice Controller");
		List<Invoice> registeredInvoiceDetails = ceramicsInvoiceService.retrieveInvoiceDetails(invoiceData);
		logger.info("Successfully retrieved Invoice List from DB - " + invoiceData.getFirmDBName() + " ::: WEB ::: Invoice Controller");
		return ResponseEntity.accepted().body(registeredInvoiceDetails);
	}
	
}
