package com.simbls.prism.rest.foodBazaar.builder;

import java.util.Map;

import org.apache.log4j.Logger;

import com.simbls.prism.rest.model.industry.Invoice;

public class FoodBazaarInvoiceQueryBuilder {

	private static final Logger logger = Logger.getLogger(FoodBazaarInvoiceQueryBuilder.class);

	/**
	 * 
	 * @param parameters
	 * @return
	 * @author Sohail Razvi
	 */
		
	public String retrieveInvoiceListQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Invoice operations ::: REST ::: FoodBazaarInvoiceQueryBuilder");
		Invoice invoiceData = (Invoice) parameters.get("invoiceData");
		logger.info("Retrieving Invoice target DBName - " + invoiceData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("SELECT "
				+ " invoice.frm_invoice_id, "
				+ " dealer.frm_dealer_id, "
				+ " dealer.frm_dealer_name, "
				+ " invoice.frm_invoice_number, "
				+ " invoice.frm_invoice_bill_date, "
				+ " invoice.frm_invoice_delivery_date, "
				+ " invoice.frm_invoice_amount, "
				+ " invoice.frm_invoice_market_fee, "
				+ " invoice.frm_invoice_transportation_unloading_fee, "
				+ " invoice.frm_invoice_payment_due_date, "
				+ " invoice.frm_invoice_entered_by, "
				+ " invoice.frm_invoice_entered_date, "
				+ " invoice.frm_invoice_updated_by, "
				+ " invoice.frm_invoice_updated_date "
			+ " FROM "
				+ invoiceData.getFirmDBName() + ".frm_invoice invoice "
			+ " JOIN "
				+ invoiceData.getFirmDBName() + ".frm_dealer dealer "
			+ " WHERE "
				+ " invoice.frm_invoice_dealer_id = dealer.frm_dealer_id");
		/**
		 * Validating for InvoiceID not null. If InvoiceID is not null, select Invoices specific to the InvoiceID
		 */
		if(invoiceData.getDealerID() == 0 && invoiceData.getInvoiceID() != 0){
			SelectUserQuery.append(" AND invoice.frm_invoice_id=" + invoiceData.getInvoiceID());
		}
		/**
		 * Validating for DealerID not null. If DealerID is not null, select Invoices specific to the dealerID
		 */
		else if(invoiceData.getDealerID() != 0 && invoiceData.getInvoiceID() == 0){
			SelectUserQuery.append(" AND invoice.frm_invoice_dealer_id=" + invoiceData.getDealerID() +
									" ORDER BY invoice.frm_invoice_id DESC");
		}
		/**
		 * Validating for DealerID and InvoiceID not null. If DealerID and InvoiceID is not null, select Invoices specific to the dealerID and InvoiceID
		 */
		else if(invoiceData.getDealerID() != 0 && invoiceData.getInvoiceID() != 0){
			SelectUserQuery.append(" AND invoice.frm_invoice_dealer_id=" + invoiceData.getDealerID()
								+ " AND invoice.frm_invoice_id=" + invoiceData.getInvoiceID());
		}else{
			SelectUserQuery.append(" ORDER BY invoice.frm_invoice_id");
		}
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarInvoiceQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	
	/**
	 * 
	 * @param parameters
	 * @return
	 * @author Sohail Razvi
	 */
		
	public String retrieveInvoiceDetailsQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Invoice operations ::: REST ::: FoodBazaarInvoiceQueryBuilder");
		Invoice invoiceData = (Invoice) parameters.get("invoiceData");
		logger.info("Retrieving Invoice target DBName - " + invoiceData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("SELECT "
				+ " invoice.frm_invoice_number invoice_number, "
				+ " invoice.frm_invoice_bill_date invoice_bill_date, "
				+ " invoice.frm_invoice_delivery_date invoice_delivery_date, "
				+ " invoice.frm_invoice_amount invoice_amount, "
				+ " invoice.frm_invoice_market_fee invoice_market_fee, "
				+ " invoice.frm_invoice_transportation_unloading_fee invoice_transportation_charges, "
				+ " invoice.frm_invoice_payment_due_date invoice_payment_due_date, "
				+ " invoice.frm_invoice_entered_by invoice_entered_by, "
				+ " invoice.frm_invoice_entered_date invoice_entered_date, "
				+ " invoice.frm_invoice_updated_by invoice_updated_by, "
				+ " invoice.frm_invoice_updated_date invoice_updated_date, "
				/* ---------------------------------- */
				+ " product.frm_product_name product_name, "
				+ " product.frm_product_packed_quantity product_packed_quantity, "
				+ " product.frm_product_packed_unit product_packed_unit, "
				/* ---------------------------------- */
				+ " purchase.frm_purchase_mrp product_mrp, "
				+ " purchase.frm_purchase_unit_cost product_cost, "
				+ " purchase.frm_purchase_primary_discount product_primary_discount, "
				+ " purchase.frm_purchase_secondary_discount product_secondary_discount, "
				+ " purchase.frm_purchase_paid_qty product_paid_quantity, "
				+ " purchase.frm_purchase_free_qty product_free_quantity, "
				+ " purchase.frm_purchase_tax product_tax, "
				+ " purchase.frm_purchase_cess product_cess "
				/* ---------------------------------- */
				+ " FROM " + invoiceData.getFirmDBName() + ".frm_invoice invoice "
				+ " JOIN " + invoiceData.getFirmDBName() + ".frm_product_purchase purchase "
				+ " JOIN " + invoiceData.getFirmDBName() + ".frm_product product "
				+ " ON purchase.frm_purchase_product_id = product.frm_product_id "
				+ " WHERE purchase.frm_purchase_invoice_id =" + invoiceData.getInvoiceID()
				+ " AND invoice.frm_invoice_id =" + invoiceData.getInvoiceID());
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: FoodBazaarInvoiceQueryBuilder");
		return SelectUserQuery.toString();
	}

}
