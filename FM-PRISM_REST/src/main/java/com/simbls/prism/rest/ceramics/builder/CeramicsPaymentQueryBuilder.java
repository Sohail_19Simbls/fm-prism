package com.simbls.prism.rest.ceramics.builder;

import java.util.Map;

import org.apache.log4j.Logger;

import com.simbls.prism.rest.model.industry.Invoice;
import com.simbls.prism.rest.model.industry.Payment;

public class CeramicsPaymentQueryBuilder {

	private static final Logger logger = Logger.getLogger(CeramicsPaymentQueryBuilder.class);

	/**
	 * Payment Details are obtained against Invoice. Query targets Invoice table.
	 * @param parameters
	 * @return
	 * @author Sohail Razvi
	 */
	
	
	public String retrieveTodaysIssuedPaymentDetailsQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Payment operations ::: REST ::: FoodBazaarPaymentQueryBuilder");
		Payment paymentData = (Payment) parameters.get("paymentData");
		logger.info("Retrieving Todays Issued Payment Details target DBName - " + paymentData.getFirmDBName().toString());
		StringBuilder SelectPaymentDetailsQuery = new StringBuilder();
		SelectPaymentDetailsQuery.append("SELECT "
				+ " dealer.frm_dealer_name dealer_name, "
				/* ----------------------- */
				+ " invoice.frm_invoice_id invoice_id, "
				+ " invoice.frm_invoice_number invoice_number, "
				+ " invoice.frm_invoice_bill_date invoice_billing_date, "
				+ " invoice.frm_invoice_amount invoice_amount, "
				+ " invoice.frm_invoice_market_fee market_fee, "
				+ " invoice.frm_invoice_delivery_date invoice_delivery_date, "
				+ " invoice.frm_invoice_payment_due_date payment_due_date, "
				/* ----------------------- */
				+ " payment.frm_payment_amount amount_paid, "
				+ " payment.frm_payment_transaction_number transaction_number, "
				+ " payment.frm_payment_mode payment_mode, "
				+ " payment.frm_payment_issue_date payment_issue_date, "
				+ " payment.frm_payment_encash_date payment_encash_date, "
				+ " payment.frm_payment_completed payment_status "
				/* ----------------------- */
				+ " FROM " + paymentData.getFirmDBName() +".frm_invoice invoice "
				+ " JOIN " + paymentData.getFirmDBName() +".frm_payment payment "
				+ " JOIN " + paymentData.getFirmDBName() +".frm_dealer dealer "
				+ " ON invoice.frm_invoice_id = payment.frm_payment_invoice_id "
				+ " AND dealer.frm_dealer_id = invoice.frm_invoice_dealer_id ");
		
		if(paymentData.getTodaysDate() != null){
			SelectPaymentDetailsQuery.append(" WHERE payment.frm_payment_issue_date = '" + paymentData.getTodaysDate() + "'");
		}
				
		logger.info("Generated Query - " + SelectPaymentDetailsQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectPaymentDetailsQuery.toString();
	}
	
	/**
	 * Payment Details are obtained against Invoice. Query targets Invoice table.
	 * @param parameters
	 * @return
	 * @author Sohail Razvi
	 */
	
	
	public String retrieveTodaysEnCashedPaymentDetailsQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Payment operations ::: REST ::: FoodBazaarPaymentQueryBuilder");
		Payment paymentData = (Payment) parameters.get("paymentData");
		logger.info("Retrieving Todays Issued Payment Details target DBName - " + paymentData.getFirmDBName().toString());
		StringBuilder SelectPaymentDetailsQuery = new StringBuilder();
		SelectPaymentDetailsQuery.append("SELECT "
				+ " dealer.frm_dealer_name dealer_name, "
				/* ----------------------- */
				+ " invoice.frm_invoice_id invoice_id, "
				+ " invoice.frm_invoice_number invoice_number, "
				+ " invoice.frm_invoice_bill_date invoice_billing_date, "
				+ " invoice.frm_invoice_amount invoice_amount, "
				+ " invoice.frm_invoice_market_fee market_fee, "
				+ " invoice.frm_invoice_delivery_date invoice_delivery_date, "
				+ " invoice.frm_invoice_payment_due_date payment_due_date, "
				/* ----------------------- */
				+ " payment.frm_payment_amount amount_paid, "
				+ " payment.frm_payment_transaction_number transaction_number, "
				+ " payment.frm_payment_mode payment_mode, "
				+ " payment.frm_payment_issue_date payment_issue_date, "
				+ " payment.frm_payment_encash_date payment_encash_date, "
				+ " payment.frm_payment_completed payment_status "
				/* ----------------------- */
				+ " FROM " + paymentData.getFirmDBName() +".frm_invoice invoice "
				+ " JOIN " + paymentData.getFirmDBName() +".frm_payment payment "
				+ " JOIN " + paymentData.getFirmDBName() +".frm_dealer dealer "
				+ " ON invoice.frm_invoice_id = payment.frm_payment_invoice_id "
				+ " AND dealer.frm_dealer_id = invoice.frm_invoice_dealer_id ");
		
		if(paymentData.getTodaysDate() != null){
			SelectPaymentDetailsQuery.append(" WHERE payment.frm_payment_encash_date = '" + paymentData.getTodaysDate() + "'");
		}
				
		logger.info("Generated Query - " + SelectPaymentDetailsQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectPaymentDetailsQuery.toString();
	}
	
	
	
/*	public String retrieveInvoicePaymentDetailsQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Payment operations ::: REST ::: FoodBazaarPaymentQueryBuilder");
		Invoice invoiceData = (Invoice) parameters.get("invoiceData");
		logger.info("Retrieving Invoice Details target DBName - " + invoiceData.getFirmDBName().toString());
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("SELECT "
				+ "'" + invoiceData.getFirmDBName() + "' firm_DB_name,"
				+ " dealer.frm_dealer_name dealer_name, "
				 ---------------------------------- 
				+ " invoice.frm_invoice_id invoice_id, "
				+ " invoice.frm_invoice_number invoice_number, "
				+ " invoice.frm_invoice_bill_date billing_date, "
				+ " invoice.frm_invoice_delivery_date delivery_date, "
				+ " invoice.frm_invoice_amount invoice_amount, "
				+ " invoice.frm_invoice_market_fee market_fee, "
				+ " invoice.frm_invoice_transportation_unloading_fee unloading_charges, "
				+ " invoice.frm_invoice_payment_due_date payment_due "
				 ---------------------------------- 
				+ " FROM " + invoiceData.getFirmDBName() +".frm_invoice invoice "
				+ " JOIN " + invoiceData.getFirmDBName() +".frm_dealer dealer "
				+ " ON  dealer.frm_dealer_id = invoice.frm_invoice_dealer_id");
		
		if(invoiceData.getDealerID() != 0 && (invoiceData.getBillingStartingDate() == null && invoiceData.getBillingEndingDate() == null )){
			SelectUserQuery.append(" AND invoice.frm_invoice_dealer_id = " + invoiceData.getDealerID());
		} else if(invoiceData.getDealerID() == 0 && (invoiceData.getBillingStartingDate() != null && invoiceData.getBillingEndingDate() != null )){
			SelectUserQuery.append(" AND invoice.frm_invoice_bill_date BETWEEN '" + invoiceData.getBillingStartingDate() + "' AND '" + invoiceData.getBillingEndingDate() + "'");
		}
				
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}
	
	
	public String retrievePaymentDetailsQuery(@SuppressWarnings("rawtypes") Map parameters) {
		logger.info("Initiating query builder for Payment operations ::: REST ::: FoodBazaarPaymentQueryBuilder");
		long invoiceID =  (Long) parameters.get("invoiceID");
		String firmDBName =  (String) parameters.get("firmDBName");
		logger.info("Retrieving Payment target DBName - " + firmDBName);
		StringBuilder SelectUserQuery = new StringBuilder();
		SelectUserQuery.append("SELECT "
				+ " payment.frm_payment_mode payment_mode, "
				+ " payment.frm_payment_transaction_number transaction_number, "
				+ " payment.frm_payment_amount_paid amount_paid, "
				+ " payment.frm_payment_paid_date amount_paid_date "
				+ " FROM " + firmDBName +".frm_payment payment"
				+ " WHERE payment.frm_payment_invoice_id = " + invoiceID);
				
		logger.info("Generated Query - " + SelectUserQuery.toString() + " ::: REST ::: UserQueryBuilder");
		return SelectUserQuery.toString();
	}*/

}
