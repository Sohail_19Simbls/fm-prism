package com.simbls.prism.rest.ceramics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.simbls.prism.rest.ceramics.service.CeramicsStockService;
import com.simbls.prism.rest.model.industry.SalesBasket;

@Controller
@RequestMapping(value = "/ceramics_product")
public class CeramicsStockController {
	private static final Logger logger = Logger.getLogger(CeramicsStockController.class);
	@Autowired private CeramicsStockService ceramicsStockService;
	
	
	/**
	 * Retrieve Detailed Product List of all the Products Present in Stock.
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveDetailProductList", method=RequestMethod.POST)
	public ResponseEntity<List<SalesBasket>> retrieveDetailProductList(@RequestBody SalesBasket salesBasketData){
		logger.info("Continuing retrival of Product list ::: REST ::: CeramicsProductController");
		List<SalesBasket> retrievedDetailProductList = ceramicsStockService.retrieveDetailProductList(salesBasketData);
		logger.info("Returning Obtained Product  " + retrievedDetailProductList.size() + " to WEB ::: REST ::: CeramicsProductController");
		return ResponseEntity.accepted().body(retrievedDetailProductList);
	}

}
