package com.simbls.prism.web.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

@Component
public class RestClient {
	@Inject
	private RestClientHelper restClientHelper;

	// private static final Logger logger = Logger.getLogger(RestClient.class);

	@SuppressWarnings("rawtypes")
	public HttpEntity<?> getRequestEntity(Map<String, String> headers, String requestPayload) {
		HttpHeaders requestHeaders = new HttpHeaders();
		HttpEntity<?> requestEntity = null;
		if (headers != null && !headers.isEmpty()) {
			Set<String> keySet = headers.keySet();
			if (!keySet.isEmpty()) {
				Iterator it = keySet.iterator();
				while (it.hasNext()) {
					String headerName = (String) it.next();
					requestHeaders.add(headerName, headers.get(headerName));
				}
			}
			if (requestPayload != null && !requestPayload.isEmpty()) {
				requestEntity = new HttpEntity<Object>(requestPayload, requestHeaders);
			} else {
				requestEntity = new HttpEntity<Object>(requestHeaders);
			}
		}
		return requestEntity;
	}

	public ResponseEntity<String> getDataJson(String url, Map<String, String> headers, String requestPayload)
			throws RestClientException, Exception {
		ResponseEntity<String> response = null;
		response = restClientHelper.restTemplate().exchange(url, HttpMethod.GET,
				getRequestEntity(headers, requestPayload), String.class);
		// logger.info("value ::: " + response.getBody());
		return response;

	}

	public ResponseEntity<String> postDataJson(String url, Map<String, String> headers, String requestPayload) throws RestClientException, Exception {
		ResponseEntity<String> response = null;
		try {
			response = restClientHelper.restTemplate().exchange(url, HttpMethod.POST, getRequestEntity(headers, requestPayload), String.class);
		} catch (HttpClientErrorException e) {
			e.printStackTrace();
			return response;
		}
		return response;
	}

	
	public Response putDataJson(String url, Map<String, String> headers, String requestPayload)
			throws RestClientException, Exception {
		ResponseEntity<String> response = restClientHelper.restTemplate().exchange(url, HttpMethod.PUT,
				getRequestEntity(headers, requestPayload), String.class);
		return Response.status(Status.OK).entity(response.getBody()).build();
	}

	public Response deleteDataJson(String url, Map<String, String> headers, String requestPayload)
			throws RestClientException, Exception {
		ResponseEntity<String> response = restClientHelper.restTemplate().exchange(url, HttpMethod.DELETE,
				getRequestEntity(headers, requestPayload), String.class);
		return Response.status(Status.OK).entity(response.getBody()).build();
	}
}
