package com.simbls.prism.web.foodBazaar.serviceImpl;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.foodBazaar.service.FoodBazaarPaymentService;
import com.simbls.prism.web.industry.model.Payment;
import com.simbls.prism.web.util.FoodBazaarRestUtil;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class FoodBazaarPaymentServiceImpl implements FoodBazaarPaymentService {
	private static final Logger logger = Logger.getLogger(FoodBazaarPaymentServiceImpl.class);
	@Autowired private FoodBazaarRestUtil foodBazaarRestUtil;
	@Inject RestClient restClient;
	
	
	/**
	 * 
	 */
	public void paymentRegister(Payment payment) throws Exception {
		logger.info("Initiating Payment Registration ::: WEB ::: Payment Service Impl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.PAYMENT_REGISTER;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(payment));
		mapper.readValue(httpResponse.getBody().toString(), Payment.class);	
		logger.info("Successfully received control from REST ::: WEB ::: Payment Servie Impl");
	}

	
	/**
	 * 
	 */
	public Payment retrieveTodaysPaymentDetails(Payment paymentData) throws Exception {
		logger.info("Continuing retrival of all the payments Details ::: WEB ::: Payment Service Impl ");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.RETRIEVE_TODAYS_PAYMENT_DETAILS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(paymentData));
		Payment registeredInvoicePaymentList = mapper.readValue(httpResponse.getBody().toString(), Payment.class);
		logger.info("Obtained Payment Details from REST ::: WEB ::: CeramicsPaymentServiceImpl");
		return registeredInvoicePaymentList;
	}
	
	/**
	 * 
	 */
	public Payment retrieveConditionalPaymentDetails(Payment paymentData) throws Exception {
		logger.info("Continuing retrival of all the payments Details ::: WEB ::: Payment Service Impl ");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.RETRIEVE_CONDITIONAL_PAYMENT_DETAILS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(paymentData));
		Payment registeredInvoicePaymentList = mapper.readValue(httpResponse.getBody().toString(), Payment.class);
		logger.info("Obtained Payment Details from REST ::: WEB ::: CeramicsPaymentServiceImpl");
		return registeredInvoicePaymentList;
	}

}
