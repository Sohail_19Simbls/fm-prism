package com.simbls.prism.web.central.model;

public class DiscountCategory {
	private int discountCategoryID;
	private String discountCategory;
	
	public int getDiscountCategoryID() {
		return discountCategoryID;
	}
	public void setDiscountCategoryID(int discountCategoryID) {
		this.discountCategoryID = discountCategoryID;
	}
	public String getDiscountCategory() {
		return discountCategory;
	}
	public void setDiscountCategory(String discountCategory) {
		this.discountCategory = discountCategory;
	}
	
	

}
