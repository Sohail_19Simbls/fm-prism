package com.simbls.prism.web.industry.model;

public class Invoice {
	private long invoiceID;
	private long dealerID;
	private String invoiceNumber;
	private String invoiceBillingDate;
	private String invoiceDeliveryDate;
	private String invoiceAmount;
	private String invoiceMarketFees;
	private String invoiceTransportationUnloadingFees;
	private String invoicePaymentDueDate;
	private String invoiceEnteredBy;
	private String invoiceEnteredDate;
	private String invoiceUpdatedBy;
	private String invoiceUpdatedDate;
	
	/* ---------------------------------------- */
	/*	Parameters related to specific Industry and Firm	*/
	/* ---------------------------------------- */
	private String industryCategory;
	private String firmDBName;
	private String firmDBUserName;
	private String firmDBPassword;

	private String dealerName;
	private Dealer dealerObject = new Dealer();
	
	/* ------------------------------------- */
	/* Following refer to Invoice Details */
	/* ------------------------------------- */
	private String productName;
	private String productPackedQuantity;
	private String productPackedUnit;
	private String productMRP;
	private String productUnitCost;
	private String productPrimaryDiscount;
	private String productSecondaryDiscount;
	private String productPaidQuantity;
	private String productFreeQuantity;
	private String productTax;
	private String productCess;
	
	private Invoice invoiceObject;

	public long getInvoiceID() {
		return invoiceID;
	}

	public void setInvoiceID(long invoiceID) {
		this.invoiceID = invoiceID;
	}

	public long getDealerID() {
		return dealerID;
	}

	public void setDealerID(long dealerID) {
		this.dealerID = dealerID;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getInvoiceBillingDate() {
		return invoiceBillingDate;
	}

	public void setInvoiceBillingDate(String invoiceBillingDate) {
		this.invoiceBillingDate = invoiceBillingDate;
	}

	public String getInvoiceDeliveryDate() {
		return invoiceDeliveryDate;
	}

	public void setInvoiceDeliveryDate(String invoiceDeliveryDate) {
		this.invoiceDeliveryDate = invoiceDeliveryDate;
	}

	public String getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(String invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public String getInvoiceMarketFees() {
		return invoiceMarketFees;
	}

	public void setInvoiceMarketFees(String invoiceMarketFees) {
		this.invoiceMarketFees = invoiceMarketFees;
	}

	public String getInvoiceTransportationUnloadingFees() {
		return invoiceTransportationUnloadingFees;
	}

	public void setInvoiceTransportationUnloadingFees(String invoiceTransportationUnloadingFees) {
		this.invoiceTransportationUnloadingFees = invoiceTransportationUnloadingFees;
	}

	public String getInvoicePaymentDueDate() {
		return invoicePaymentDueDate;
	}

	public void setInvoicePaymentDueDate(String invoicePaymentDueDate) {
		this.invoicePaymentDueDate = invoicePaymentDueDate;
	}

	public String getInvoiceEnteredBy() {
		return invoiceEnteredBy;
	}

	public void setInvoiceEnteredBy(String invoiceEnteredBy) {
		this.invoiceEnteredBy = invoiceEnteredBy;
	}

	public String getInvoiceEnteredDate() {
		return invoiceEnteredDate;
	}

	public void setInvoiceEnteredDate(String invoiceEnteredDate) {
		this.invoiceEnteredDate = invoiceEnteredDate;
	}

	public String getInvoiceUpdatedBy() {
		return invoiceUpdatedBy;
	}

	public void setInvoiceUpdatedBy(String invoiceUpdatedBy) {
		this.invoiceUpdatedBy = invoiceUpdatedBy;
	}

	public String getInvoiceUpdatedDate() {
		return invoiceUpdatedDate;
	}

	public void setInvoiceUpdatedDate(String invoiceUpdatedDate) {
		this.invoiceUpdatedDate = invoiceUpdatedDate;
	}

	public String getIndustryCategory() {
		return industryCategory;
	}

	public void setIndustryCategory(String industryCategory) {
		this.industryCategory = industryCategory;
	}

	public String getFirmDBName() {
		return firmDBName;
	}

	public void setFirmDBName(String firmDBName) {
		this.firmDBName = firmDBName;
	}

	public String getFirmDBUserName() {
		return firmDBUserName;
	}

	public void setFirmDBUserName(String firmDBUserName) {
		this.firmDBUserName = firmDBUserName;
	}

	public String getFirmDBPassword() {
		return firmDBPassword;
	}

	public void setFirmDBPassword(String firmDBPassword) {
		this.firmDBPassword = firmDBPassword;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public Dealer getDealerObject() {
		return dealerObject;
	}

	public void setDealerObject(Dealer dealerObject) {
		this.dealerObject = dealerObject;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductPackedQuantity() {
		return productPackedQuantity;
	}

	public void setProductPackedQuantity(String productPackedQuantity) {
		this.productPackedQuantity = productPackedQuantity;
	}

	public String getProductPackedUnit() {
		return productPackedUnit;
	}

	public void setProductPackedUnit(String productPackedUnit) {
		this.productPackedUnit = productPackedUnit;
	}

	public String getProductMRP() {
		return productMRP;
	}

	public void setProductMRP(String productMRP) {
		this.productMRP = productMRP;
	}

	public String getProductUnitCost() {
		return productUnitCost;
	}

	public void setProductUnitCost(String productUnitCost) {
		this.productUnitCost = productUnitCost;
	}

	public String getProductPrimaryDiscount() {
		return productPrimaryDiscount;
	}

	public void setProductPrimaryDiscount(String productPrimaryDiscount) {
		this.productPrimaryDiscount = productPrimaryDiscount;
	}

	public String getProductSecondaryDiscount() {
		return productSecondaryDiscount;
	}

	public void setProductSecondaryDiscount(String productSecondaryDiscount) {
		this.productSecondaryDiscount = productSecondaryDiscount;
	}

	public String getProductPaidQuantity() {
		return productPaidQuantity;
	}

	public void setProductPaidQuantity(String productPaidQuantity) {
		this.productPaidQuantity = productPaidQuantity;
	}

	public String getProductFreeQuantity() {
		return productFreeQuantity;
	}

	public void setProductFreeQuantity(String productFreeQuantity) {
		this.productFreeQuantity = productFreeQuantity;
	}

	public String getProductTax() {
		return productTax;
	}

	public void setProductTax(String productTax) {
		this.productTax = productTax;
	}

	public String getProductCess() {
		return productCess;
	}

	public void setProductCess(String productCess) {
		this.productCess = productCess;
	}

	public Invoice getInvoiceObject() {
		return invoiceObject;
	}

	public void setInvoiceObject(Invoice invoiceObject) {
		this.invoiceObject = invoiceObject;
	}	
}
