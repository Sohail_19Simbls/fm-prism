package com.simbls.prism.web.ceramics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.web.ceramics.service.CeramicsDealerService;
import com.simbls.prism.web.common.DateTimeGeneratorService;
import com.simbls.prism.web.industry.model.Dealer;

@Controller
@RequestMapping(value = "/ceramics")
public class CeramicsDealerController {
	private static final Logger logger = Logger.getLogger(CeramicsDealerController.class);
	@Autowired private CeramicsDealerService ceramicsDealerService;
	@Autowired private DateTimeGeneratorService dateTimeGeneratorService; 
	public static String compactDate;
	
	/**
	 * Register Dealer from Owner Page
	 * @param dealerData
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/dealerRegister", method=RequestMethod.POST)
	public String dealerRegister(Dealer dealerData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + dealerData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Dealer Registration with GST - " + dealerData.getDealerGST() + " ::: WEB ::: CeramicsDealerController");
		compactDate = dateTimeGeneratorService.compactDate();
		dealerData.setDealerEnteredDate(compactDate);
		Dealer insertedDealerData = ceramicsDealerService.dealerRegister(dealerData);
		if(dealerData.getDealerGST().equals(insertedDealerData.getDealerGST())){
			logger.info("Dealer with GST - " + dealerData.getDealerGST() + " successfully registered ::: WEB ::: CeramicsDealerController");
		} else {
			logger.info("Dealer with GST - " + dealerData.getDealerGST() + " already registered ::: WEB ::: CeramicsDealerController");
			insertedDealerData = new Dealer();
		}
		modelMap.addAttribute("insertedDealerData", insertedDealerData);
		logger.info("Successfully registered Dealer ::: WEB ::: CeramicsDealerController");
		logger.info("------------------------->>>" + dealerData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = dealerData.getIndustryCategory() + "/firmTabPages/ownerDealerTab :: DealerTabAddDealerFragment";
		return returnChunk;
	}
	
	/**
	 * Update Dealer
	 * @param product
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/dealerUpdate", method = RequestMethod.POST)
	public String dealerUpdate(Dealer dealerData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + dealerData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Dealer Update with GST - " + dealerData.getDealerGST() + " ::: WEB ::: CeramicsDealerController");
		compactDate = dateTimeGeneratorService.compactDate();
		dealerData.setDealerUpdatedDate(compactDate);
		Dealer insertedDealerData = ceramicsDealerService.dealerUpdate(dealerData);
		if(dealerData.getDealerGST().equals(insertedDealerData.getDealerGST())){
			logger.info("Dealer with GST - " + dealerData.getDealerGST() + " successfully registered ::: WEB ::: CeramicsDealerController");
		} else {
			logger.info("Dealer with GST - " + dealerData.getDealerGST() + " already registered ::: WEB ::: CeramicsDealerController");
			insertedDealerData = new Dealer();
		}
		modelMap.addAttribute("insertedDealerData", insertedDealerData);
		logger.info("Successfully registered Dealer ::: WEB ::: CeramicsDealerController");
		logger.info("------------------------->>>" + dealerData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = dealerData.getIndustryCategory() + "/firmUpdateForms/updateDealerForm :: UpdateDealerSuccessModalChunk";
		return returnChunk;
	}
	
	/**
	 * Retrieves entire list of Dealers registerd.
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveDealerList", method=RequestMethod.GET)
	public String retrieveDealerList(Dealer dealerData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + dealerData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of all the Registered Dealers ::: WEB ::: CeramicsDealerController");
		List<Dealer> registeredDealersList = ceramicsDealerService.retrieveDealerList(dealerData);
		modelMap.addAttribute("registeredDealersList", registeredDealersList);
		logger.info("Successfully retrieved Dealer List from ::: WEB ::: Dealer Controller");
		logger.info("------------------------->>>" + dealerData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = dealerData.getIndustryCategory() + "/firmTabPages/ownerDealerTab :: DealerTabDealerListFragment";
		return returnChunk;
	}
	
	/**
	 * Retrieves dealer by typed character. (Dealer By Name)
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveDealerName", method=RequestMethod.GET)
	public @ResponseBody List<Dealer> retrieveDealerName(Dealer dealerData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + dealerData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of all the Registered Dealers ::: WEB ::: CeramicsDealerController");
		List<Dealer> registeredDealersList = ceramicsDealerService.retrieveDealerList(dealerData);
		modelMap.addAttribute("registeredDealersList", registeredDealersList);
		logger.info("Successfully retrieved Dealer List from DB - " + dealerData.getFirmDBName() + "::: WEB ::: CeramicsDealerController");
		logger.info("------------------------->>>" + dealerData.getIndustryCategory() + "<<<-------------------------");
		return registeredDealersList;
	}
	
	/**
	 * Retrieves dealer Details by Dealer ID
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveDealerDetails", method=RequestMethod.GET)
	public String retrieveDealerDetails(Dealer dealerData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + dealerData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of all the details of Dealer ::: WEB ::: CeramicsDealerController");
		Dealer registeredDealerDetails = ceramicsDealerService.retrieveDealerDetails(dealerData);
		modelMap.addAttribute("registeredDealerDetails", registeredDealerDetails);
		logger.info("Successfully retrieved Dealer List from DB - " + dealerData.getFirmDBName() + "::: WEB ::: CeramicsDealerController");
		logger.info("------------------------->>>" + dealerData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = dealerData.getIndustryCategory() + "/firmTabPages/ownerDealerTab :: DealerTabDealerListFragment";
		return returnChunk;
	}
	
}
