package com.simbls.prism.web.ceramics.serviceImpl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.ceramics.service.CeramicsInvoiceService;
import com.simbls.prism.web.industry.model.Invoice;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.CeramicsRestUtil;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class CeramicsInvoiceServiceImpl implements CeramicsInvoiceService {
	private static final Logger logger = Logger.getLogger(CeramicsInvoiceServiceImpl.class);
	@Autowired private CeramicsRestUtil ceramicsRestUtil;
	@Inject RestClient restClient;
	
	
	/**
	 * 
	 */
	public void invoiceRegister(Invoice invoiceData) throws Exception {
		logger.info("Continuing Invoice Registration ::: WEB ::: CeramicsInvoiceServiceImpl");
		String url = ceramicsRestUtil.getUrl() + CeramicsRestUtil.INVOICE_REGISTER;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(invoiceData));
		mapper.readValue(httpResponse.getBody().toString(), Invoice.class);	
		logger.info("Successfully received control from REST ::: WEB ::: CeramicsInvoiceServiceImpl");
	}
	
	/**
	 * 
	 */
	public void invoiceUpdate(Invoice invoiceData) throws Exception {
		logger.info("Continuing Invoice Updation ::: WEB ::: CeramicsInvoiceServiceImpl");
		String url = ceramicsRestUtil.getUrl() + CeramicsRestUtil.INVOICE_UPDATE;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(invoiceData));
		mapper.readValue(httpResponse.getBody().toString(), Invoice.class);	
		logger.info("Successfully received control from REST ::: WEB ::: CeramicsInvoiceServiceImpl");
	}

	/**
	 * 
	 */
	public List<Invoice> retrieveInvoiceList(Invoice invoiceData) throws Exception {
		logger.info("Continuing retrival of all the invoices ::: WEB ::: CeramicsInvoiceServiceImpl ");
		String url = ceramicsRestUtil.getUrl() + CeramicsRestUtil.RETRIEVE_INVOICE_LIST;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(invoiceData));
		Invoice[] invoices = mapper.readValue(httpResponse.getBody().toString(), Invoice[].class);
		List<Invoice> invoiceList = Arrays.asList(invoices);
		logger.info("Obtained Invoice list of Size -" + invoiceList.size() + " from REST ::: WEB ::: CeramicsInvoiceServiceImpl");
		return invoiceList;
	}
	
	/**
	 * 
	 */
	public List<Invoice> retrieveInvoiceDetails(Invoice invoiceData) throws Exception {
		logger.info("Continuing retrival of all the Products Purchased against invoiceID - " + invoiceData.getInvoiceID() + " ::: WEB ::: CeramicsInvoiceServiceImpl ");
		String url = ceramicsRestUtil.getUrl() + CeramicsRestUtil.RETRIEVE_INVOICE_DETAILS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(invoiceData));
		Invoice[] invoiceDetailsArray = mapper.readValue(httpResponse.getBody().toString(), Invoice[].class);
		List<Invoice> invoicePurchaseDetails = Arrays.asList(invoiceDetailsArray);
		logger.info("Obtained Invoice list of Size -" + invoicePurchaseDetails.size() + " from REST ::: WEB ::: CeramicsInvoiceServiceImpl");
		return invoicePurchaseDetails;
	}
	
}
