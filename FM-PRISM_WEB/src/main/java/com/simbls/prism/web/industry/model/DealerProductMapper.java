package com.simbls.prism.web.industry.model;

public class DealerProductMapper {
	private long dealerProductMapperID;
	private long dealerID;
	private long productID;
	
	private DealerProductMapper dealerProductMapperObject;

	public long getDealerProductMapperID() {
		return dealerProductMapperID;
	}

	public void setDealerProductMapperID(long dealerProductMapperID) {
		this.dealerProductMapperID = dealerProductMapperID;
	}

	public long getDealerID() {
		return dealerID;
	}

	public void setDealerID(long dealerID) {
		this.dealerID = dealerID;
	}

	public long getProductID() {
		return productID;
	}

	public void setProductID(long productID) {
		this.productID = productID;
	}

	public DealerProductMapper getDealerProductMapperObject() {
		return dealerProductMapperObject;
	}

	public void setDealerProductMapperObject(DealerProductMapper dealerProductMapperObject) {
		this.dealerProductMapperObject = dealerProductMapperObject;
	}

	
	
}
