package com.simbls.prism.web.central.serviceImpl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.central.model.ProductSubCategory;
import com.simbls.prism.web.central.service.ProductSubCategoryService;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.CentralRestUtil;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class ProductSubCategoryServiceImpl implements ProductSubCategoryService {
	
	private static final Logger logger = Logger.getLogger(ProductSubCategoryServiceImpl.class);
	@Autowired private CentralRestUtil centralRestUtil;
	@Inject	RestClient restClient;	

	public List<ProductSubCategory> retrieveProductSubCategories(ProductSubCategory productSubCategory) throws Exception {
		logger.info("Continuing retrieval of Product Sub Categories from Central DB ::: WEB ::: CeramicsProductServiceImpl");
		String url = centralRestUtil.getUrl() + CentralRestUtil.RETRIEVE_PRODUCT_SUB_CATEGORY;	
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productSubCategory));
		ProductSubCategory[] productSubCategoryObject = mapper.readValue(httpResponse.getBody().toString(), ProductSubCategory[].class);
		List<ProductSubCategory> retrievedSubProductCategoryList = Arrays.asList(productSubCategoryObject);
		logger.info("Successfully obtained Product Sub Category List from REST ::: WEB ::: CeramicsProductServiceImpl");
		return retrievedSubProductCategoryList;
	}

}
