package com.simbls.prism.web.ceramics.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.industry.model.Invoice;
import com.simbls.prism.web.industry.model.Payment;

@Component
public interface CeramicsPaymentService {
	/**
	 * Register Payment details 
	 * @param payment
	 * @param dbCredentials
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public void paymentRegister(Payment paymentData) throws Exception;
	
	/**
	 * Retrieve all registered Payments
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public Payment retrieveTodaysPaymentDetails(Payment paymentData) throws Exception;
	
	/**
	 * Retrieve all registered Payments
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<Invoice> retrievePaymentDetails(Invoice invoiceData) throws Exception;
	

}