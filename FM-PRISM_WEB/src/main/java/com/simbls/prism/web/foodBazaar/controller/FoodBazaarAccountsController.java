package com.simbls.prism.web.foodBazaar.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.simbls.prism.web.foodBazaar.service.FoodBazaarAccountsService;
import com.simbls.prism.web.industry.model.Accounts;

@Controller
@RequestMapping(value = "/foodBazaar")
public class FoodBazaarAccountsController {
	private static final Logger logger = Logger.getLogger(FoodBazaarAccountsController.class);
	@Autowired private FoodBazaarAccountsService foodBazaarAccountsService;
	

	/**
	 * Retrieve Consolidated Purchase Details
	 * @param accountsData
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/retrieveConsolidatedPurchaseDetails", method=RequestMethod.GET)
	public String retrieveConsolidatedPurchaseDetails(Accounts accountsData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + accountsData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of Consolidated Purchase Details for Accounts ::: WEB ::: FoodBazaarAccountsController");
		List<Accounts> retrievedConsolidatedPurchaseDetails = foodBazaarAccountsService.retrieveConsolidatedPurchaseDetails(accountsData);
		modelMap.addAttribute("retrievedConsolidatedPurchaseDetails", retrievedConsolidatedPurchaseDetails);
		logger.info("Successfully retrieved Consolidated Purchase Details from DB - " + retrievedConsolidatedPurchaseDetails.size() + "::: WEB ::: FoodBazaarAccountsController");
		logger.info("------------------------->>>" + accountsData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = accountsData.getIndustryCategory() + "/firmAccounts/consolidatedPurchaseDetails :: ViewConsolidatedPurchaseTableChunk";
		return returnChunk;
	}
	
	/**
	 * Retrieve Consolidated Sales Details
	 * @param accountsData
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/retrieveConsolidatedSalesDetails", method=RequestMethod.GET)
	public String retrieveConsolidatedSalesDetails(Accounts accountsData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + accountsData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of Consolidated Sales Details for Accounts ::: WEB ::: FoodBazaarAccountsController");
		List<Accounts> retrievedConsolidatedSalesDetails = foodBazaarAccountsService.retrieveConsolidatedSalesDetails(accountsData);
		modelMap.addAttribute("retrievedConsolidatedSalesDetails", retrievedConsolidatedSalesDetails);
		logger.info("Successfully retrieved Consolidated Sales Details from DB - " + retrievedConsolidatedSalesDetails.size() + "::: WEB ::: FoodBazaarAccountsController");
		logger.info("------------------------->>>" + accountsData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = accountsData.getIndustryCategory() + "/firmAccounts/consolidatedSalesDetails :: ViewConsolidatedSalesTableChunk";
		return returnChunk;
	}
	

}
