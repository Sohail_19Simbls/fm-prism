package com.simbls.prism.web.clothing.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.simbls.prism.web.clothing.service.ClothingPaymentService;
import com.simbls.prism.web.common.DateTimeGeneratorService;
import com.simbls.prism.web.industry.model.Invoice;
import com.simbls.prism.web.industry.model.Payment;


@Controller
@RequestMapping(value = "/clothing")
public class ClothingPaymentController {
	private static final Logger logger = Logger.getLogger(ClothingPaymentController.class);
	@Autowired private ClothingPaymentService clothingPaymentService;
	@Autowired private DateTimeGeneratorService dateTimeGeneratorService; 
	public static String compactDate;
	
	/**
	 * Register Payment from Owner Page
	 * @param paymentData
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/paymentRegister", method=RequestMethod.POST)
	public String paymentRegister(Payment paymentData, ModelMap map) throws Exception{
		logger.info("------------------------->>>" + paymentData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Payment Registration for InvoiceID - " + paymentData.getInvoiceID() 
				    + ". Target DB - " + paymentData.getFirmDBName() + "::: WEB ::: CeramicsPaymentController");
		compactDate = dateTimeGeneratorService.compactDate();
		paymentData.setPaymentEnteredDate(compactDate);
		clothingPaymentService.paymentRegister(paymentData);
		logger.info("Successfully Registered Payment ::: WEB ::: CeramicsPaymentController");
		logger.info("------------------------->>>" + paymentData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = paymentData.getIndustryCategory() + "/firmTabPages/ownerInvoiceTab :: InvoiceTabInvoicePaymentFragment";
		return returnChunk;
	}
	
	
	/**
	 * Retrieves entire list of Payments registered.
	 * Payment Details are obtained against Invoice 
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveTodaysPaymentDetails", method=RequestMethod.GET)
	public String retrieveTodaysPaymentDetails(Payment paymentData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + paymentData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of today's Registered Payment Details ::: WEB ::: Payment Controller");
		Payment registeredInvoicePaymentList = clothingPaymentService.retrieveTodaysPaymentDetails(paymentData);
		modelMap.addAttribute("registeredInvoicePaymentList", registeredInvoicePaymentList);
		logger.info("Successfully retrieved Payment Details from DB ::: WEB ::: Payment Controller");
		logger.info("------------------------->>>" + paymentData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = paymentData.getIndustryCategory() + "/firmLists/paymentList :: ViewPaymentListChunk";
		return returnChunk;
	}
	
	/**
	 * Retrieves entire list of Payments registered.
	 * Payment Details are obtained against Invoice 
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrievePaymentDetails", method=RequestMethod.GET)
	public String retrievePaymentDetails(Invoice invoiceData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + invoiceData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of all the Registered Payment Details ::: WEB ::: Payment Controller");
		List<Invoice> registeredPaymentsList = clothingPaymentService.retrievePaymentDetails(invoiceData);
		modelMap.addAttribute("registeredPaymentsList", registeredPaymentsList);
		logger.info("Successfully retrieved Payment Details from DB ::: WEB ::: Payment Controller");
		logger.info("------------------------->>>" + invoiceData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = invoiceData.getIndustryCategory() + "/firmTabPages/ownerPaymentTab :: PaymentTabPaymentListFragment";
		return returnChunk;
	}
	
	
}
