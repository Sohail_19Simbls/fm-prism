package com.simbls.prism.web.ceramics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.simbls.prism.web.ceramics.service.CeramicsStockService;
import com.simbls.prism.web.industry.model.SalesBasket;

@Controller
@RequestMapping(value = "/ceramics")
public class CeramicsStockController {
	private static final Logger logger = Logger.getLogger(CeramicsStockController.class);
	@Autowired private CeramicsStockService ceramicsStockService;
	
	
	
	/**
	 * Retrieve Detailed Product List of all the Products Present in Stock.
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveDetailProductList", method=RequestMethod.GET)
	public String retrieveDetailProductList(SalesBasket salesBasketData, ModelMap modelMap) throws Exception {
		logger.info("Initiating retrival of Product Details present in Stock ::: WEB ::: CeramicsStockController");
		List<SalesBasket> retrievedDetailProductList = ceramicsStockService.retrieveDetailProductList(salesBasketData);
		modelMap.addAttribute("retrievedDetailProductList", retrievedDetailProductList);
		logger.info("Successfully retrieved Product from DB - " + retrievedDetailProductList.size() + "::: WEB ::: CeramicsStockController");
		return "firmDashboard/firmTabPages/ownerProductTab :: ProductTabProductListFragment";
	}
	

}
