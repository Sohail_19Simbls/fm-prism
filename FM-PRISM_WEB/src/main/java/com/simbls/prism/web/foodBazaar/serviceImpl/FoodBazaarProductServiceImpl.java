package com.simbls.prism.web.foodBazaar.serviceImpl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.foodBazaar.service.FoodBazaarProductService;
import com.simbls.prism.web.industry.model.Product;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.FoodBazaarRestUtil;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class FoodBazaarProductServiceImpl implements FoodBazaarProductService {
	private static final Logger logger = Logger.getLogger(FoodBazaarProductServiceImpl.class);
	@Autowired private FoodBazaarRestUtil foodBazaarRestUtil;
	@Inject RestClient restClient;
	
	/**
	 * 
	 */
	public Product productRegister(Product productData) throws Exception {
		logger.info("Continuing Product Registration ::: WEB ::: FoodBazaarProductServiceImpl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.PRODUCT_REGISTER;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productData));
		Product obtainedObject = mapper.readValue(httpResponse.getBody().toString(), Product.class);
		logger.info("Successfully received control from REST ::: WEB ::: Product Servie Impl");
		return obtainedObject;
	}
	
	/**
	 * 
	 */
	public Product productUpdate(Product productData) throws Exception {
		logger.info("Continuing Product Updation ::: WEB ::: FoodBazaarProductServiceImpl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.PRODUCT_UPDATE;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productData));
		Product obtainedObject = mapper.readValue(httpResponse.getBody().toString(), Product.class);
		logger.info("Successfully received control from REST ::: WEB ::: Product Servie Impl");
		return obtainedObject;
	}

	/**
	 * 
	 */
	public List<Product> retrieveProductByParameter(Product productData) throws Exception {
		logger.info("Initiating retrival of all the products ::: WEB ::: FoodBazaarProductServiceImpl ");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.RETRIEVE_PRODUCT_BY_PARAMETER;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productData));
		Product[] retrievedProduct = mapper.readValue(httpResponse.getBody().toString(), Product[].class);
		List<Product> retrievedProductList = Arrays.asList(retrievedProduct);
		logger.info("Obtained Details of Product -" + retrievedProductList.size() + " from REST ::: WEB ::: CeramicsProductServiceImpl");
		return retrievedProductList;
	}

	
	/**
	 * 
	 */
	public Product retrieveProductPurchaseSaleDetails(Product productData) throws Exception {
		logger.info("Initiating retrival of product purchase and sales details ::: WEB ::: FoodBazaarProductServiceImpl ");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.RETRIEVE_PRODUCT_PURCHASE_SALES_DETAILS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productData));
		Product retrievedProductPurchaseSalesDetails = mapper.readValue(httpResponse.getBody().toString(), Product.class);
		logger.info("Obtained Details of Product. Purchase Details - " + 
												retrievedProductPurchaseSalesDetails.getProductPurchaseObject().size() +
												" Sales Details - " +
												retrievedProductPurchaseSalesDetails.getProductSalesObject().size() +
												" from REST ::: WEB ::: CeramicsProductServiceImpl");
		return retrievedProductPurchaseSalesDetails;
	}
	

}
