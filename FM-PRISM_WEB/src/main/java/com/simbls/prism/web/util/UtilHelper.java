package com.simbls.prism.web.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class UtilHelper {

	public static Date convertStringToDate(String stringDate) {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date formattedDate = null;
		try {
			System.out.println("date before format ::: " + stringDate);
			formattedDate = formatter.parse(stringDate);
			System.out.println("date after format ::: " + formattedDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formattedDate;
	}

	public static String convertDateToString(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String stringDate = dateFormat.format(date);
		return stringDate;

	}

	public static Map<String, String> getCommonHeader() {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		return headers;
	}

	public static boolean compareDates(String d1) {
		boolean result = false;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String d2 = "2017-07-01";
			Date date1 = sdf.parse(d1);
			Date date2 = sdf.parse(d2);
			if (date1.before(date2)) {
				result = true;
			}

		} catch (ParseException ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
