package com.simbls.prism.web.industry.model;

public class ProductPurchase {
	/* ------------------------------------- */
	/* Following refer to Product Purchase Details Table */
	/* ------------------------------------- */
	private long productPurchaseDetailsID;
	private long productID;
	private long dealerID;
	private long invoiceID;
	private String productPurchaseMRP;
	private String productPurchaseTax;
	private String productPurchaseCess;
	private String productPurchaseUnitCost;
	private String productPurchasePaidQuantity;
	private String productPurchaseArrivalDate;
	private String productPurchaseEnteredBy;
	private String productPurchaseEnteredDate;
	private String productPurchaseUpdatedBy;
	private String productPurchaseUpdatedDate;
	private int productPurchaseRowState;
	
	/* -------------------------------------------------------------------------- */
	/* Following refer to Product Purchase Details FoodBazaar Industry Table */
	/* -------------------------------------------------------------------------- */
	private String productPurchasePackedQuantity;
	private String productPurchaseUnit;
	private String productPurchaseFreeQuantity;
	private String productPurchaseTotalQuantity;
	private String productPurchaseThresholdQuantity;
	private String productPurchasePrimaryDiscount;
	private String productPurchaseSecondaryDiscount;
	private String productPurchaseManufactureDate;
	private String productPurchaseExpiryDate;
	private int salesDetailsAsPreviousFlag;
	
	/* -------------------------------------------------------------------------- */
	/* Following refer to Product Purchase Details Clothing Industry Table */
	/* -------------------------------------------------------------------------- */
	private String productPurchaseBarCode;
	private String productPurchaseDiscount;
	private String productPurchaseHSN;
	private String productPurchaseTaxableCost;
	
	/* ------------------------------------- */
	/* Following refer to Invoice Table */
	/* ------------------------------------- */
	private String invoiceNumber;
	private String invoiceAmount;
	private String marketFee;
	private String unloadingAndTransportationCharges;
	private String calculatedInvoiceAmount;
	private String dealerName;
	
	/* ------------------------------------- */
	/* Following refer to checkInvoiceStatus Method */
	/* ------------------------------------- */
	private double productTotalCost;
	private double finalProductCost;
		
	/* -------------------------------------- */
	/* Following are used for Purchase Consolidated Table */
	/* -------------------------------------- */
	private String perProductCostWithTax;
	private String totalQuantityProductTotalCostWithoutTax;
	private String productPurchase_TaxAmount;
	private String productPurchase_For_0_Tax_Amount;
	private String productPurchase_For_5_Tax_Amount;
	private String productPurchase_For_12_Tax_Amount;
	private String productPurchase_For_18_Tax_Amount;
	private String productPurchase_For_28_Tax_Amount;
	private String productPurchase_Effective_CGST;
	private String productPurchase_Effective_SGST;
	private String productPurchase_Effective_CESS;
	
	/* ---------------------------------------- */
	/*	Parameters related to specific Industry and Firm	*/
	/* ---------------------------------------- */
	private String industryCategory;
	private String firmDBName;
	private String firmDBUserName;
	private String firmDBPassword;
	
	/* ------------------------------------- */
	/* Following refer to Sales Details Object */
	/* ------------------------------------- */
	private Product productObject;
	private ProductSales productSalesObject;
	private ProductPurchase productPurchaseObject;
	
	public long getProductPurchaseDetailsID() {
		return productPurchaseDetailsID;
	}
	public void setProductPurchaseDetailsID(long productPurchaseDetailsID) {
		this.productPurchaseDetailsID = productPurchaseDetailsID;
	}
	public long getProductID() {
		return productID;
	}
	public void setProductID(long productID) {
		this.productID = productID;
	}
	public long getDealerID() {
		return dealerID;
	}
	public void setDealerID(long dealerID) {
		this.dealerID = dealerID;
	}
	public long getInvoiceID() {
		return invoiceID;
	}
	public void setInvoiceID(long invoiceID) {
		this.invoiceID = invoiceID;
	}
	public String getProductPurchaseMRP() {
		return productPurchaseMRP;
	}
	public void setProductPurchaseMRP(String productPurchaseMRP) {
		this.productPurchaseMRP = productPurchaseMRP;
	}
	public String getProductPurchaseTax() {
		return productPurchaseTax;
	}
	public void setProductPurchaseTax(String productPurchaseTax) {
		this.productPurchaseTax = productPurchaseTax;
	}
	public String getProductPurchaseCess() {
		return productPurchaseCess;
	}
	public void setProductPurchaseCess(String productPurchaseCess) {
		this.productPurchaseCess = productPurchaseCess;
	}
	public String getProductPurchaseUnitCost() {
		return productPurchaseUnitCost;
	}
	public void setProductPurchaseUnitCost(String productPurchaseUnitCost) {
		this.productPurchaseUnitCost = productPurchaseUnitCost;
	}
	public String getProductPurchasePaidQuantity() {
		return productPurchasePaidQuantity;
	}
	public void setProductPurchasePaidQuantity(String productPurchasePaidQuantity) {
		this.productPurchasePaidQuantity = productPurchasePaidQuantity;
	}
	public String getProductPurchaseArrivalDate() {
		return productPurchaseArrivalDate;
	}
	public void setProductPurchaseArrivalDate(String productPurchaseArrivalDate) {
		this.productPurchaseArrivalDate = productPurchaseArrivalDate;
	}
	public String getProductPurchaseEnteredBy() {
		return productPurchaseEnteredBy;
	}
	public void setProductPurchaseEnteredBy(String productPurchaseEnteredBy) {
		this.productPurchaseEnteredBy = productPurchaseEnteredBy;
	}
	public String getProductPurchaseEnteredDate() {
		return productPurchaseEnteredDate;
	}
	public void setProductPurchaseEnteredDate(String productPurchaseEnteredDate) {
		this.productPurchaseEnteredDate = productPurchaseEnteredDate;
	}
	public String getProductPurchaseUpdatedBy() {
		return productPurchaseUpdatedBy;
	}
	public void setProductPurchaseUpdatedBy(String productPurchaseUpdatedBy) {
		this.productPurchaseUpdatedBy = productPurchaseUpdatedBy;
	}
	public String getProductPurchaseUpdatedDate() {
		return productPurchaseUpdatedDate;
	}
	public void setProductPurchaseUpdatedDate(String productPurchaseUpdatedDate) {
		this.productPurchaseUpdatedDate = productPurchaseUpdatedDate;
	}
	public int getProductPurchaseRowState() {
		return productPurchaseRowState;
	}
	public void setProductPurchaseRowState(int productPurchaseRowState) {
		this.productPurchaseRowState = productPurchaseRowState;
	}
	public String getProductPurchasePackedQuantity() {
		return productPurchasePackedQuantity;
	}
	public void setProductPurchasePackedQuantity(String productPurchasePackedQuantity) {
		this.productPurchasePackedQuantity = productPurchasePackedQuantity;
	}
	public String getProductPurchaseUnit() {
		return productPurchaseUnit;
	}
	public void setProductPurchaseUnit(String productPurchaseUnit) {
		this.productPurchaseUnit = productPurchaseUnit;
	}
	public String getProductPurchaseFreeQuantity() {
		return productPurchaseFreeQuantity;
	}
	public void setProductPurchaseFreeQuantity(String productPurchaseFreeQuantity) {
		this.productPurchaseFreeQuantity = productPurchaseFreeQuantity;
	}
	public String getProductPurchaseTotalQuantity() {
		return productPurchaseTotalQuantity;
	}
	public void setProductPurchaseTotalQuantity(String productPurchaseTotalQuantity) {
		this.productPurchaseTotalQuantity = productPurchaseTotalQuantity;
	}
	public String getProductPurchaseThresholdQuantity() {
		return productPurchaseThresholdQuantity;
	}
	public void setProductPurchaseThresholdQuantity(String productPurchaseThresholdQuantity) {
		this.productPurchaseThresholdQuantity = productPurchaseThresholdQuantity;
	}
	public String getProductPurchasePrimaryDiscount() {
		return productPurchasePrimaryDiscount;
	}
	public void setProductPurchasePrimaryDiscount(String productPurchasePrimaryDiscount) {
		this.productPurchasePrimaryDiscount = productPurchasePrimaryDiscount;
	}
	public String getProductPurchaseSecondaryDiscount() {
		return productPurchaseSecondaryDiscount;
	}
	public void setProductPurchaseSecondaryDiscount(String productPurchaseSecondaryDiscount) {
		this.productPurchaseSecondaryDiscount = productPurchaseSecondaryDiscount;
	}
	public String getProductPurchaseManufactureDate() {
		return productPurchaseManufactureDate;
	}
	public void setProductPurchaseManufactureDate(String productPurchaseManufactureDate) {
		this.productPurchaseManufactureDate = productPurchaseManufactureDate;
	}
	public String getProductPurchaseExpiryDate() {
		return productPurchaseExpiryDate;
	}
	public void setProductPurchaseExpiryDate(String productPurchaseExpiryDate) {
		this.productPurchaseExpiryDate = productPurchaseExpiryDate;
	}
	public int getSalesDetailsAsPreviousFlag() {
		return salesDetailsAsPreviousFlag;
	}
	public void setSalesDetailsAsPreviousFlag(int salesDetailsAsPreviousFlag) {
		this.salesDetailsAsPreviousFlag = salesDetailsAsPreviousFlag;
	}
	public String getProductPurchaseBarCode() {
		return productPurchaseBarCode;
	}
	public void setProductPurchaseBarCode(String productPurchaseBarCode) {
		this.productPurchaseBarCode = productPurchaseBarCode;
	}
	public String getProductPurchaseDiscount() {
		return productPurchaseDiscount;
	}
	public void setProductPurchaseDiscount(String productPurchaseDiscount) {
		this.productPurchaseDiscount = productPurchaseDiscount;
	}
	public String getProductPurchaseHSN() {
		return productPurchaseHSN;
	}
	public void setProductPurchaseHSN(String productPurchaseHSN) {
		this.productPurchaseHSN = productPurchaseHSN;
	}
	public String getProductPurchaseTaxableCost() {
		return productPurchaseTaxableCost;
	}
	public void setProductPurchaseTaxableCost(String productPurchaseTaxableCost) {
		this.productPurchaseTaxableCost = productPurchaseTaxableCost;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(String invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public String getMarketFee() {
		return marketFee;
	}
	public void setMarketFee(String marketFee) {
		this.marketFee = marketFee;
	}
	public String getUnloadingAndTransportationCharges() {
		return unloadingAndTransportationCharges;
	}
	public void setUnloadingAndTransportationCharges(String unloadingAndTransportationCharges) {
		this.unloadingAndTransportationCharges = unloadingAndTransportationCharges;
	}
	public String getCalculatedInvoiceAmount() {
		return calculatedInvoiceAmount;
	}
	public void setCalculatedInvoiceAmount(String calculatedInvoiceAmount) {
		this.calculatedInvoiceAmount = calculatedInvoiceAmount;
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public double getProductTotalCost() {
		return productTotalCost;
	}
	public void setProductTotalCost(double productTotalCost) {
		this.productTotalCost = productTotalCost;
	}
	public double getFinalProductCost() {
		return finalProductCost;
	}
	public void setFinalProductCost(double finalProductCost) {
		this.finalProductCost = finalProductCost;
	}
	public String getPerProductCostWithTax() {
		return perProductCostWithTax;
	}
	public void setPerProductCostWithTax(String perProductCostWithTax) {
		this.perProductCostWithTax = perProductCostWithTax;
	}
	public String getTotalQuantityProductTotalCostWithoutTax() {
		return totalQuantityProductTotalCostWithoutTax;
	}
	public void setTotalQuantityProductTotalCostWithoutTax(String totalQuantityProductTotalCostWithoutTax) {
		this.totalQuantityProductTotalCostWithoutTax = totalQuantityProductTotalCostWithoutTax;
	}
	public String getProductPurchase_TaxAmount() {
		return productPurchase_TaxAmount;
	}
	public void setProductPurchase_TaxAmount(String productPurchase_TaxAmount) {
		this.productPurchase_TaxAmount = productPurchase_TaxAmount;
	}
	public String getProductPurchase_For_0_Tax_Amount() {
		return productPurchase_For_0_Tax_Amount;
	}
	public void setProductPurchase_For_0_Tax_Amount(String productPurchase_For_0_Tax_Amount) {
		this.productPurchase_For_0_Tax_Amount = productPurchase_For_0_Tax_Amount;
	}
	public String getProductPurchase_For_5_Tax_Amount() {
		return productPurchase_For_5_Tax_Amount;
	}
	public void setProductPurchase_For_5_Tax_Amount(String productPurchase_For_5_Tax_Amount) {
		this.productPurchase_For_5_Tax_Amount = productPurchase_For_5_Tax_Amount;
	}
	public String getProductPurchase_For_12_Tax_Amount() {
		return productPurchase_For_12_Tax_Amount;
	}
	public void setProductPurchase_For_12_Tax_Amount(String productPurchase_For_12_Tax_Amount) {
		this.productPurchase_For_12_Tax_Amount = productPurchase_For_12_Tax_Amount;
	}
	public String getProductPurchase_For_18_Tax_Amount() {
		return productPurchase_For_18_Tax_Amount;
	}
	public void setProductPurchase_For_18_Tax_Amount(String productPurchase_For_18_Tax_Amount) {
		this.productPurchase_For_18_Tax_Amount = productPurchase_For_18_Tax_Amount;
	}
	public String getProductPurchase_For_28_Tax_Amount() {
		return productPurchase_For_28_Tax_Amount;
	}
	public void setProductPurchase_For_28_Tax_Amount(String productPurchase_For_28_Tax_Amount) {
		this.productPurchase_For_28_Tax_Amount = productPurchase_For_28_Tax_Amount;
	}
	public String getProductPurchase_Effective_CGST() {
		return productPurchase_Effective_CGST;
	}
	public void setProductPurchase_Effective_CGST(String productPurchase_Effective_CGST) {
		this.productPurchase_Effective_CGST = productPurchase_Effective_CGST;
	}
	public String getProductPurchase_Effective_SGST() {
		return productPurchase_Effective_SGST;
	}
	public void setProductPurchase_Effective_SGST(String productPurchase_Effective_SGST) {
		this.productPurchase_Effective_SGST = productPurchase_Effective_SGST;
	}
	public String getProductPurchase_Effective_CESS() {
		return productPurchase_Effective_CESS;
	}
	public void setProductPurchase_Effective_CESS(String productPurchase_Effective_CESS) {
		this.productPurchase_Effective_CESS = productPurchase_Effective_CESS;
	}
	public String getIndustryCategory() {
		return industryCategory;
	}
	public void setIndustryCategory(String industryCategory) {
		this.industryCategory = industryCategory;
	}
	public String getFirmDBName() {
		return firmDBName;
	}
	public void setFirmDBName(String firmDBName) {
		this.firmDBName = firmDBName;
	}
	public String getFirmDBUserName() {
		return firmDBUserName;
	}
	public void setFirmDBUserName(String firmDBUserName) {
		this.firmDBUserName = firmDBUserName;
	}
	public String getFirmDBPassword() {
		return firmDBPassword;
	}
	public void setFirmDBPassword(String firmDBPassword) {
		this.firmDBPassword = firmDBPassword;
	}
	public Product getProductObject() {
		return productObject;
	}
	public void setProductObject(Product productObject) {
		this.productObject = productObject;
	}
	public ProductSales getProductSalesObject() {
		return productSalesObject;
	}
	public void setProductSalesObject(ProductSales productSalesObject) {
		this.productSalesObject = productSalesObject;
	}
	public ProductPurchase getProductPurchaseObject() {
		return productPurchaseObject;
	}
	public void setProductPurchaseObject(ProductPurchase productPurchaseObject) {
		this.productPurchaseObject = productPurchaseObject;
	}
}
