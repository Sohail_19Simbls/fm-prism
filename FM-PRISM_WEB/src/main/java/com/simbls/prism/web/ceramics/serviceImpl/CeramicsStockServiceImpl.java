package com.simbls.prism.web.ceramics.serviceImpl;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simbls.prism.web.ceramics.service.CeramicsStockService;
import com.simbls.prism.web.industry.model.SalesBasket;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.CeramicsRestUtil;

@Service
public class CeramicsStockServiceImpl implements CeramicsStockService {
	private static final Logger logger = Logger.getLogger(CeramicsStockServiceImpl.class);
	@Autowired private CeramicsRestUtil ceramicsRestUtil;
	@Inject RestClient restClient;
	

	/**
	 * 
	 */
	public List<SalesBasket> retrieveDetailProductList(SalesBasket salesBasketData) throws Exception {
		/*logger.info("Initiating retrival of all the products ::: WEB ::: Product Service Impl ");
		String url = ceramicsRestUtil.getUrl() + CeramicsRestUtil.RETRIEVE_DETAILED_PRODUCT_LIST;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(salesBasketData));
		SalesBasket[] retrievedDetailProductArray = mapper.readValue(httpResponse.getBody().toString(), SalesBasket[].class);
		List<SalesBasket> retrievedDetailProductList = Arrays.asList(retrievedDetailProductArray);
		logger.info("Obtained Details of Product -" + retrievedDetailProductList.size() + " from REST ::: WEB ::: CeramicsProductServiceImpl");
		return retrievedDetailProductList;*/
		return null;
	}

}
