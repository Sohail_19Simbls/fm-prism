package com.simbls.prism.web.central.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.central.model.ProductSubCategory;

@Component
public interface ProductSubCategoryService {
	/**
	 * Retrieve Product Sub Category List from Central DB
	 * @return
	 * @throws Exception
	 */
	public List<ProductSubCategory> retrieveProductSubCategories(ProductSubCategory productSubCategory) throws Exception;

}
