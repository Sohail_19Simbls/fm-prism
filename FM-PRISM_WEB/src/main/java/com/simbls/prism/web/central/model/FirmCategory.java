package com.simbls.prism.web.central.model;

public class FirmCategory {
	private int firmTypeID;
	private String firmType;
	
	private FirmCategory firmTypeObject;

	public int getFirmTypeID() {
		return firmTypeID;
	}

	public void setFirmTypeID(int firmTypeID) {
		this.firmTypeID = firmTypeID;
	}

	public String getFirmType() {
		return firmType;
	}

	public void setFirmType(String firmType) {
		this.firmType = firmType;
	}

	public FirmCategory getFirmTypeObject() {
		return firmTypeObject;
	}

	public void setFirmTypeObject(FirmCategory firmTypeObject) {
		this.firmTypeObject = firmTypeObject;
	}
}
