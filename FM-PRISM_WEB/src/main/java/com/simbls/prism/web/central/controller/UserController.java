package com.simbls.prism.web.central.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.simbls.prism.web.central.model.Login;
import com.simbls.prism.web.central.model.User;
import com.simbls.prism.web.central.service.UserService;

@Controller
@SessionAttributes("userObject")
public class UserController {

	private static final Logger logger = Logger.getLogger(UserController.class);
	@Autowired private UserService userService;
	
	/**
	 * Validating login for entered User Name and Password
	 * The following is the details of the User-type
	 * User Type - 1 ::: Role - Super Admin
	 * User Type - 2 ::: Role - Moderator
	 * User Type - 3 ::: Role - On Boarding Team
	 * User Type - 4 ::: Role - Admin
	 * User Type - 5 ::: Role - Accountant
	 * User Type - 6 ::: Role - Store Keeper
	 * User Type - 7 ::: Role - Salesman
	 * User Type - 8 ::: Role - Delivery Man
	 * User Type - 9 ::: Role - Customer
	 * ****************************************************
	 * IMPORTANT STATUS FLAGS - According to the way condition is checked
	 * ****************************************************
	 * Firm Industry Category - 1 ::: Industry - Food Bazaar
	 * Firm Industry Category - 2 ::: Industry - Clothing
	 * Firm Industry Category - 3 ::: Industry - Ceramics
	 * Firm Industry Category - 4 ::: Industry - Restaurant
	 * Firm Industry Category - 5 ::: Industry - Pharmaceutical
	 * Firm Industry Category - 6 ::: Industry - Automobile
	 * ****************************************************
	 * Firm Row State ::: 0 - Firm is Active
	 * Firm Row State ::: 1 - Firm is Deleted
	 * ****************************************************
	 * Firm Approval State ::: 1 - Firm is Approved
	 * Firm Approval State ::: 0 - Firm is Not Approved
	 * ****************************************************
	 * User Row State ::: 0 - User is Active
	 * User Row State ::: 1 - User is Deleted
	 * ****************************************************
	 * User Approval Status ::: 1 - User is Approved
	 * User Approval Status ::: 0 - User is Not Approved
	 * ****************************************************
	 * User First Login ::: 0 - User is Logging in for the first time
	 * User First Login ::: 1 - User has Logged in previously
	 * ****************************************************
	 * @param Login Object
	 * @author Sohail Razvi
	 */
	

	@RequestMapping(value = "/validateCredentials", method = RequestMethod.POST)
	public String validateCredentials(Login login, ModelMap map) throws Exception {
		logger.info("Initiating user login ....");
		logger.info("Initiating login Validation ::: WEB ::: User Controller");
		logger.info("UserName - " + login.getUserName() + " ::: Password - " + login.getUserPassword());
		User userObject = userService.validateCredentials(login);
		logger.info("Preparing Return Code Chunk ::: WEB ::: User Controller");
		logger.info("Industry Category - " + userObject.getFirmObject().getIndustryObject().getIndustryID() + ". Assigning industry parameter ... ");
		String industryName = "";
		switch (userObject.getFirmObject().getIndustryObject().getIndustryID()){
		case 1:{
			logger.info("Food Bazaar Industry detected ::: WEB ::: User Controller");
			industryName = "foodBazaar";
			break;
		}case 2:{
			logger.info("Ceramics Industry detected ::: WEB ::: User Controller");
			industryName = "clothing";
			break;
		}case 3:{
			logger.info("Hardware Industry detected ::: WEB ::: User Controller");
			industryName = "ceramics";
			break;
		}case 4:{
			logger.info("Restaurant Industry detected ::: WEB ::: User Controller");
			industryName = "restaurant";
			break;
		}case 5:{
			logger.info("Pharmaceutical Industry detected ::: WEB ::: User Controller");
			industryName = "pharmaceutical";
			break;
		}case 6:{
			logger.info("Pharmaceutical Industry detected ::: WEB ::: User Controller");
			industryName = "automobile";
			break;
		}
		}
		String returnChunk = "";
		logger.info("Validating if the Firm is Active or Deleted ::: WEB ::: User Controller");
		if(userObject.getFirmObject().getFirmRowState() == 0){
			logger.info("Active Firm detected.");
			logger.info("Validating if the Firm is Approved or Approval is pending ::: WEB ::: User Controller");
			if(userObject.getFirmObject().getFirmApprovalStatus() == 1){
				logger.info("Approved Firm detected.");
				// Check for Firm Expiry here
				if(userObject.getUserCategory() >= 1 && userObject.getUserCategory() <= 9){
					logger.info("Successfully validated credentials ::: Redirecting ...");
					if (userObject.getUserRowState() == 0){
						logger.info("Obtaining User - " + userObject.getUserName() + " approval status ....");
						logger.info("Users current approval status is - " + userObject.getUserApprovedState() + " ::: WEB ::: User Controller");
						if(userObject.getUserApprovedState() == 1){
							logger.info("Obtaining Login Status flag ::: " + userObject.getUserFirstLogin() + " ::: User Controller");
							if (userObject.getUserFirstLogin() != -1) {
								map.addAttribute("userObject", userObject);
								switch (userObject.getUserCategory()) {
								case 1: {
									logger.info("Redirecting to SuperAdmin Dashboard ::: WEB ::: User Controller");
									returnChunk = "centralDashboard/superAdminDashboard :: SuperAdminDashboardMain";
									break;
								}case 2: {
									logger.info("Redirecting to Moderator Dashboard ::: WEB ::: User Controller");
									returnChunk =  "centralDashboard/moderatorDashboard :: ModeratorDashboardMain";
									break;
								}case 3: {
									logger.info("Redirecting to On Board Team Dashboard ::: WEB ::: User Controller");
									returnChunk =  "centralDashboard/onboardDashboard :: OnboardDashboardMain";
									break;
								}case 4: {
									logger.info("Redirecting to OwnerAdmin Dashboard ::: WEB ::: User Controller");
									returnChunk =  "industryNavigator/" + industryName + "/ownerDashBoard :: OwnerDashBoardMain";
									break;
								}case 5: {
									logger.info("Redirecting to Accountant Dashboard ::: WEB ::: User Controller");
									returnChunk =  "industryNavigator/" + industryName + "/accountantDashBoard :: AccountantDashBoardMain";
									break;
								}case 6: {
									logger.info("Redirecting to Store Keeper Dashboard ::: WEB ::: User Controller");
									returnChunk = "industryNavigator/" + industryName + "/storeKeeperDashBoard :: StoreKeeperDashboardMain";
									break;
								}case 7: {
									logger.info("Redirecting to SalesMan Dashboard ::: WEB ::: User Controller");
									returnChunk = "industryNavigator/" + industryName + "/salesmanDashboard :: SalesmanDashboardMain";
									break;
								}case 8: {
									logger.info("Redirecting to DeliveryMan Dashboard ::: WEB ::: User Controller");
									returnChunk = "industryNavigator/" + industryName + "/deliverymanDashboard :: DeliverymanDashboardMain";
									break;
								}case 9: {
									logger.info("Redirecting to Customer Dashboard ::: WEB ::: User Controller");
									returnChunk = "industryNavigator/" + industryName + "/customerDashboard :: CustomerDashboardMain";
									break;
								}
								}
								return returnChunk;
							}else {
								logger.info("User Logging in for the first time ::: Redirecting to Reset Password Page");
								map.addAttribute("user", userObject);
								returnChunk = "firsttimelogin :: main";
							}
							return returnChunk;
						}else{
							logger.info("User is not yet approved ::: Contact Administrator ::: WEB ::: User Controller");
							returnChunk = "loginPage :: userNotApproved";
						}
						return returnChunk;
					}else {
						logger.info("User has been soft deleted ::: Contact Administrator ::: WEB ::: User Controller");
						returnChunk =  "loginPage :: userExpired";
					}
					return returnChunk;
				} else {
					logger.info("User has entered wrong credentials ::: WEB ::: User Controller");
					returnChunk = "loginPage :: wrongCredentials";
				}
			} else {
				logger.info("Firm Approval pending ::: WEB ::: User Controller");
				returnChunk = "loginPage :: firmPendingApproval";
			}
		} else {
			logger.info("Firm Has been deleted ::: WEB ::: User Controller");
			returnChunk = "loginPage :: firmDeleted";
		}
		
	return returnChunk;
}

	/* *********************************************************************************** */

	/**
	 * 
	 * @param resetPassword
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/resetPasswordFirstLogin",method = RequestMethod.POST)
	public String resetPasswordFirstLogin(Login resetPassword, ModelMap map) throws Exception{
		logger.info("Initiating password reset ::: WEB ::: User logging in for the first time");
		logger.info("Initiated by User ::: " + resetPassword.getUserId() + " ::: New Password ::: " + resetPassword.getNewPassword());
		User userObject = userService.resetPasswordFirstLogin(resetPassword);		
		map.addAttribute("user", userObject);
		logger.info("Password successfully reset ::: WEB ::: For the user - " + userObject.getUserName());
		return "loginPage :: mainLogin";
	}

}
