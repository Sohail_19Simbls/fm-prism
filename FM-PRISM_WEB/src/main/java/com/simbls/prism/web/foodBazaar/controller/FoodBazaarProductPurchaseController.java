package com.simbls.prism.web.foodBazaar.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.web.common.DateTimeGeneratorService;
import com.simbls.prism.web.foodBazaar.service.FoodBazaarProductPurchaseService;
import com.simbls.prism.web.industry.model.ProductPurchase;

@Controller
@RequestMapping(value = "/foodBazaar")
public class FoodBazaarProductPurchaseController {
	private static final Logger logger = Logger.getLogger(FoodBazaarProductPurchaseController.class);
	@Autowired private FoodBazaarProductPurchaseService foodBazaarProductPurchaseService;
	@Autowired private DateTimeGeneratorService dateTimeGeneratorService; 
	public static String compactDate;
	
	/**
	 * Register Product Purchase Details.
	 * @param productPurchaseData
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/productPurchaseDetailsRegister", method=RequestMethod.POST)
	public String productPurchaseDetailsRegister(ProductPurchase productPurchaseData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Product Purchase Details Registration for Product - " + productPurchaseData.getProductID() + "::: WEB ::: FoodBazaarProductPurchaseController");
		compactDate = dateTimeGeneratorService.compactDate();
		productPurchaseData.setProductPurchaseEnteredDate(compactDate);
		productPurchaseData.setProductPurchaseRowState(0);
		ProductPurchase registeredObject = foodBazaarProductPurchaseService.productPurchaseDetailsRegister(productPurchaseData);
		if(registeredObject != null){
			logger.info("Product Purchase Details Successfully Registered ::: WEB ::: FoodBazaarProductPurchaseController");
			if(registeredObject.getProductSalesObject() != null){
				logger.info("Product Sales Details auto populated as requested::: WEB ::: FoodBazaarProductPurchaseController");
			} else {
				logger.info("Product Sales Details auto population was not requested::: WEB ::: FoodBazaarProductPurchaseController");
			}
		}
		modelMap.addAttribute("registeredObject", registeredObject);
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productPurchaseData.getIndustryCategory() + "/firmTabPages/ownerProductTab :: ProductTabProductPurchaseDetailsFragment";
		return returnChunk;
	}
	
	/**
	 * Update Product Purchase Details.
	 * @param productPurchaseData
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/productPurchaseDetailsUpdate", method=RequestMethod.POST)
	public String productPurchaseDetailsUpdate(ProductPurchase productPurchaseData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Product Purchase Details Update for Product - " + productPurchaseData.getProductID() + "::: WEB ::: FoodBazaarProductPurchaseController");
		compactDate = dateTimeGeneratorService.compactDate();
		productPurchaseData.setProductPurchaseUpdatedDate(compactDate);
		ProductPurchase updatedObject = foodBazaarProductPurchaseService.productPurchaseDetailsUpdate(productPurchaseData);
		modelMap.addAttribute("updatedObject", updatedObject);
		logger.info("Product Purchase Details Successfully Updated ::: WEB ::: FoodBazaarProductPurchaseController");
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productPurchaseData.getIndustryCategory() + "/firmUpdateForms/updateProductPurchaseDetailsForm :: UpdateProductPurchaseDetailsSuccessModalChunk";
		return returnChunk;
	}
	
	/**
	 * Delete Product Purchase Details.
	 * @param productPurchaseData
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/productPurchaseDetailsDelete", method=RequestMethod.POST)
	public String productPurchaseDetailsDelete(ProductPurchase productPurchaseData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Product Purchase Details Deletion ::: WEB ::: FoodBazaarProductPurchaseController");
		foodBazaarProductPurchaseService.productPurchaseDetailsDelete(productPurchaseData);
		logger.info("Product Purchase Details Successfully Deleted ::: WEB ::: FoodBazaarProductPurchaseController");
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productPurchaseData.getIndustryCategory() + "/firmConfirmation/deleteConfirmation :: DeleteSuccessfulModalChunk";
		return returnChunk;
	}
	
	/**
	 * Retrieves Product Purchase Details by Selected Invoice ID
	 * @param productPurchaseData, modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveProductPurchaseDetails", method=RequestMethod.GET)
	public @ResponseBody List<ProductPurchase> retrieveProductPurchaseDetails(ProductPurchase productPurchaseData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of Product Purchase Details based on Invoice ::: WEB ::: FoodBazaarProductPurchaseController");
		List<ProductPurchase> productPurchaseDetailsList = foodBazaarProductPurchaseService.retrieveProductPurchaseDetails(productPurchaseData);
		modelMap.addAttribute("productPurchaseDetailsList", productPurchaseDetailsList);
		logger.info("Successfully retrieved Product Purcase Details from DB - " + productPurchaseData.getFirmDBName() + "::: WEB ::: FoodBazaarProductPurchaseController");
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		return productPurchaseDetailsList;
	}
	
	/**
	 * Retrieves Product Purchase Details by Selected Invoice ID
	 * @param productPurchaseData, modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveProductPurchaseDetailsList", method=RequestMethod.GET)
	public String retrieveProductPurchaseDetailsList(ProductPurchase productPurchaseData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of Product Purchase Details based on Invoice ::: WEB ::: FoodBazaarProductPurchaseController");
		List<ProductPurchase> productPurchaseDetailsList = foodBazaarProductPurchaseService.retrieveProductPurchaseDetails(productPurchaseData);
		modelMap.addAttribute("productPurchaseDetailsList", productPurchaseDetailsList);
		logger.info("Successfully retrieved Product Purcase Details from DB - " + productPurchaseData.getFirmDBName() + "::: WEB ::: FoodBazaarProductPurchaseController");
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productPurchaseData.getIndustryCategory() + "/firmLists/productPurchaseList :: ViewProductPurchaseDetailsListChunk";
		return returnChunk;
	}
	

	
	/**
	 * Check if all the Purchased Product Total matches the Invoice Amount
	 * @param productPurchaseData, modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/checkInvoiceStatus", method=RequestMethod.GET)
	public @ResponseBody ProductPurchase checkInvoiceStatus(ProductPurchase productPurchaseData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating check if all the Purchased Products Details Sum equals Invoice Total ::: WEB ::: FoodBazaarProductPurchaseController");
		ProductPurchase productPurchaseDetails = foodBazaarProductPurchaseService.checkInvoiceStatus(productPurchaseData);
		logger.info("Successfully Obtained Invoice Details ::: WEB ::: FoodBazaarProductPurchaseController");
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		return productPurchaseDetails;
	}

}
