package com.simbls.prism.web.util;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@SuppressWarnings("deprecation")
@Component("restClientHelper")
public class RestClientHelper {
	
	
	public RestTemplate restTemplate() throws Exception {
		return new RestTemplate(this.httpComponentsClientHttpRequestFactory());
	}
	
	public HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory() throws Exception {
		return new HttpComponentsClientHttpRequestFactory(this.httpClient());
	}
	
	public DefaultHttpClient httpClient() throws Exception {
		return new DefaultHttpClient(this.poolingClientConnectionManager());
	}
	
	public PoolingClientConnectionManager poolingClientConnectionManager() throws Exception {
		//return new PoolingClientConnectionManager(this.httpsSchemaRegistry());
		return new PoolingClientConnectionManager();
	}
	
	/*public SchemeRegistry httpsSchemaRegistry() throws Exception {
		SchemeRegistry schemeRegistry= new SchemeRegistry();
		Map<String,Scheme> map = new HashMap<String,Scheme>();
		Scheme httpsScheme = new Scheme("https", 443, this.sslSocketFactory());
		map.put("https", httpsScheme);
		Scheme httpScheme = new Scheme("http", 80, this.plainSocketFactory());
		map.put("http", httpScheme);
		schemeRegistry.setItems(map);
		return schemeRegistry;
		
	}
	
	public SSLSocketFactory sslSocketFactory() throws Exception {
		TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
			@Override
			public boolean isTrusted(X509Certificate[] certificate, String authType){
				return true;
			}	        
	    };
		return new SSLSocketFactory(acceptingTrustStrategy, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
	}
	
	public PlainSocketFactory plainSocketFactory() {
		return PlainSocketFactory.getSocketFactory();
	}*/
	
	
	

}
