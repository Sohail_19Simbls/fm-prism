package com.simbls.prism.web.central.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.web.central.model.ProductSubCategory;
import com.simbls.prism.web.central.service.ProductSubCategoryService;

@Controller
public class ProductSubCategoryController {
	private static final Logger logger = Logger.getLogger(ProductSubCategoryController.class);
	@Autowired private ProductSubCategoryService productSubCategoryService;
	
	/**
	 * 
	 * @param productSubCategory
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/retrieveProductSubCategories", method = RequestMethod.GET)
	public @ResponseBody List<ProductSubCategory> retrieveProductSubCategories(ProductSubCategory productSubCategory, ModelMap modelMap) throws Exception {
		logger.info("Initiating Auto Retrieval of Product Sub Category from Central DB ::: WEB ::: ProductSubCategoryController");
		List<ProductSubCategory> productSubCategoryList = productSubCategoryService.retrieveProductSubCategories(productSubCategory);
		logger.info("Successfully populated Product Sub Category List from Central DB ::: WEB ::: ProductSubCategoryController");
		return productSubCategoryList;
	}

}
