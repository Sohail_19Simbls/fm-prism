package com.simbls.prism.web.central.model;

public class DealerCategory {
	private int dealerCategoryID;
	private String dealerCategory;
	
	public int getDealerCategoryID() {
		return dealerCategoryID;
	}
	public void setDealerCategoryID(int dealerCategoryID) {
		this.dealerCategoryID = dealerCategoryID;
	}
	public String getDealerCategory() {
		return dealerCategory;
	}
	public void setDealerCategory(String dealerCategory) {
		this.dealerCategory = dealerCategory;
	}

}
