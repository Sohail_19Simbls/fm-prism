package com.simbls.prism.web.clothing.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.industry.model.Product;

@Component
public interface ClothingProductService {
	/**
	 * Register Product Details
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public Product productRegister(Product productData) throws Exception;
	
	/**
	 * Update Product Details
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public Product productUpdate(Product productData) throws Exception;
	
	/**
	 * Retrieves Product by Any Parameter present in Product Model.
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<Product> retrieveProductByParameter(Product dealerData) throws Exception;
	
	
}
