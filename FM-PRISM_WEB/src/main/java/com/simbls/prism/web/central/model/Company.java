package com.simbls.prism.web.central.model;

public class Company {
	private long companyID;
	private String company;
	
	private Company companyObject;

	public long getCompanyID() {
		return companyID;
	}

	public void setCompanyID(long companyID) {
		this.companyID = companyID;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Company getCompanyObject() {
		return companyObject;
	}

	public void setCompanyObject(Company companyObject) {
		this.companyObject = companyObject;
	}
	
	

}
