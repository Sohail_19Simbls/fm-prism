package com.simbls.prism.web.foodBazaar.serviceImpl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.foodBazaar.service.FoodBazaarAccountsService;
import com.simbls.prism.web.industry.model.Accounts;
import com.simbls.prism.web.util.FoodBazaarRestUtil;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class FoodBazaarAccountsServiceImpl implements FoodBazaarAccountsService {
	private static final Logger logger = Logger.getLogger(FoodBazaarAccountsServiceImpl.class);
	@Autowired private FoodBazaarRestUtil foodBazaarRestUtil;
	@Inject RestClient restClient;
	

	/**
	 * 
	 */
	public List<Accounts> retrieveConsolidatedPurchaseDetails(Accounts accountsData) throws Exception {
		logger.info("Initiating retrival of Consolidated Purchase Details ::: WEB ::: FoodBazaarAccountsServiceImpl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.RETRIEVE_CONSOLIDATED_PURCHASE_DETAILS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(accountsData));
		Accounts[] retrievedConsolidatedPurchase = mapper.readValue(httpResponse.getBody().toString(), Accounts[].class);
		List<Accounts> retrievedConsolidatedPurchaseDetails = Arrays.asList(retrievedConsolidatedPurchase);
		logger.info("Obtained Accounts list of Size -" + retrievedConsolidatedPurchaseDetails.size() + " from REST ::: WEB ::: FoodBazaarAccountsServiceImpl");
		return retrievedConsolidatedPurchaseDetails;
	}
	
	

	/**
	 * 
	 */
	public List<Accounts> retrieveConsolidatedSalesDetails(Accounts accountsData) throws Exception {
		logger.info("Initiating retrival of Consolidated Sales Details ::: WEB ::: FoodBazaarAccountsServiceImpl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.RETRIEVE_CONSOLIDATED_SALES_DETAILS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(accountsData));
		Accounts[] retrievedConsolidatedSales = mapper.readValue(httpResponse.getBody().toString(), Accounts[].class);
		List<Accounts> retrievedConsolidatedSalesDetails = Arrays.asList(retrievedConsolidatedSales);
		logger.info("Obtained Accounts list of Size -" + retrievedConsolidatedSalesDetails.size() + " from REST ::: WEB ::: FoodBazaarAccountsServiceImpl");
		return retrievedConsolidatedSalesDetails;
	}
	
	
}
