package com.simbls.prism.web.foodBazaar.service;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.industry.model.Payment;

@Component
public interface FoodBazaarPaymentService {
	/**
	 * Register Payment details 
	 * @param payment
	 * @param dbCredentials
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public void paymentRegister(Payment paymentData) throws Exception;
	
	/**
	 * Retrieve all registered Payments
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public Payment retrieveTodaysPaymentDetails(Payment paymentData) throws Exception;
	
	/**
	 * Retrieve all registered Payments
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public Payment retrieveConditionalPaymentDetails(Payment paymentData) throws Exception;
	

}