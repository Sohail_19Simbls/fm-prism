package com.simbls.prism.web.util;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component("clothingRestUtil")
public class ClothingRestUtil {

	private static final Logger logger = Logger.getLogger(ClothingRestUtil.class);
	
	@Value("${rest.client.connectionTimeoutMillis}")
	private int restClientConnectionTimeoutMillis;

	@Value("${rest.client.readTimeoutMillis}")
	private int restClientReadTimeoutMillis;

	@Value("${rest.client.maxConnectionsPerHost}")
	private int restClientMaxConnectionsPerHost;

	@Value("${rest.client.maxTotalConnections}")
	private int restClientMaxTotalConnections;

	@Value("${rest.fmprism.url}")
	private String url;

	private RestTemplate restTemplate;
	
	/**
	 * Firm Operations
	 */
	public static final String FIRM_REGISTER					=	"/firm/firmRegister";
	
	/**
	 * Login Validation - Validation of Credentials
	 */
	public static final String VALIDATE_CREDENTIALS				=	"/user/validateCredentials";
	public static final String RESET_USER_PASSWORD 				=	"/user/resetPasswordFirstLogin";
	
	/**
	 * Dealer Category Operations
	 */
	public static final String RETRIEVE_DEALER_CATEGORY			=	"/dealerCategory/retrieveDealerCategories";
	
	/**
	 * Dealer Operations
	 */
	public static final String DEALER_REGISTER					=	"/clothing_dealer/dealerRegister";
	public static final String DEALER_UPDATE					=	"/clothing_dealer/dealerUpdate";
	public static final String RETRIEVE_DEALER_LIST 			= 	"/clothing_dealer/retrieveDealerList";
	public static final String RETRIEVE_DEALER_DETAILS			=	"/clothing_dealer/retrieveDealerDetails";
	
	/**
	 * Invoice Operations
	 */
	public static final String INVOICE_REGISTER					=	"/clothing_invoice/invoiceRegister";
	public static final String INVOICE_UPDATE					=	"/clothing_invoice/invoiceUpdate";
	public static final String RETRIEVE_INVOICE_LIST			=	"/clothing_invoice/retrieveInvoiceList";
	public static final String RETRIEVE_INVOICE_DETAILS			=	"/clothing_invoice/retrieveInvoiceDetails";
	
	/**
	 * Payment Operations
	 */
	public static final String PAYMENT_REGISTER					=	"/clothing_payment/paymentRegister";
	public static final String RETRIEVE_TODAYS_PAYMENT_DETAILS	=	"/clothing_payment/retrieveTodaysPaymentDetails";
	public static final String RETRIEVE_PAYMENT_DETAILS			=	"/clothing_payment/retrievePaymentDetails";
	
	/**
	 * Product Category Operations
	 */
	public static final String RETRIEVE_PRODUCT_CATEGORY 		=	"/productCategory/retrieveProductCategories";
	
	/**
	 * Product Sub Category Operations
	 */
	public static final String RETRIEVE_PRODUCT_SUB_CATEGORY 	=	"/productSubCategory/retrieveProductSubCategories";
	
	/**
	 * Discount Operations
	 */
	public static final String RETRIEVE_DISCOUNT_CATEGORY		=	"/discount/retrieveDiscountCategories";
	
	/**
	 * Product Operations
	 */
	public static final String PRODUCT_REGISTER					=	"/clothing_product/productRegister";
	public static final String PRODUCT_UPDATE					=	"/clothing_product/productUpdate";
	public static final String RETRIEVE_PRODUCT_BY_PARAMETER	=	"/clothing_product/retrieveProductByParameter"; 	
	public static final String PRODUCT_PURCHSE_DETAILS_REGISTER	=	"/clothing_productPurchase/productPurchaseDetailsRegister";
	public static final String RETRIEVE_PRODUCT_PURCHSE_DETAILS	=	"/clothing_productPurchase/retrieveProductPurchaseDetails";
	public static final String CHECK_INVOICE_STATUS				=	"/clothing_productPurchase/checkInvoiceStatus";
	public static final String PRODUCT_SALES_REGISTER			=	"/clothing_productSales/productSalesRegister";
	public static final String PRODUCT_SALES_BASKET				=	"/clothing_productSales/productSalesBasket";
	public static final String GENERATE_SALES_BASKET_BILL		=	"/clothing_productSales/generateSalesBasketBill";
	
	
	/**
	 * Expense Category Operations
	 */
	public static final String RETRIEVE_EXPENSE_CATEGORY 	=	"/expenseCategory/retrieveExpenseCategories";
	
	/**
	 * Expense Sub Category Operations
	 */
	public static final String RETRIEVE_EXPENSE_SUB_CATEGORY=	"/expenseSubCategory/retrieveExpenseSubCategories";
	
	
	public static final String USER_REGISTER = "/user/register";
	public static final String FORGOT_PASSWORD = "/user/forgotpassword";
	public static final String RESET_USER_FORGOT_PASSWORD = "/user/resetForgotPassword";
	
	


	

	private HttpClient getHttpClient() {
		final PoolingClientConnectionManager connectionManager = new PoolingClientConnectionManager();
		connectionManager.setDefaultMaxPerRoute(restClientMaxConnectionsPerHost);
		connectionManager.setMaxTotal(restClientMaxTotalConnections);
		final DefaultHttpClient httpClient = new DefaultHttpClient(connectionManager);
		return httpClient;
	}

	private ClientHttpRequestFactory getClientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(getHttpClient());
		factory.setConnectTimeout(restClientConnectionTimeoutMillis);
		factory.setReadTimeout(restClientReadTimeoutMillis);

		return factory;

	}

	public RestTemplate getRestTemplate() {
		if (this.restTemplate == null) {
			restTemplate = new RestTemplate(getClientHttpRequestFactory());
			/* decorateRestTemplate(); */
		}
		return restTemplate;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		logger.info("Retrieving URL " + url + " ::: WEB ::: RESTUtil");
		return url;
	}
}
