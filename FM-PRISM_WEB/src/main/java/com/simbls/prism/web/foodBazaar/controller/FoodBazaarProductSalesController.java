package com.simbls.prism.web.foodBazaar.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.web.common.DateTimeGeneratorService;
import com.simbls.prism.web.foodBazaar.service.FoodBazaarProductSalesService;
import com.simbls.prism.web.industry.model.ProductSales;
import com.simbls.prism.web.industry.model.SalesBasket;

@Controller
@RequestMapping(value = "/foodBazaar")
public class FoodBazaarProductSalesController {
	private static final Logger logger = Logger.getLogger(FoodBazaarProductSalesController.class);
	@Autowired private FoodBazaarProductSalesService productSaleService;
	@Autowired private DateTimeGeneratorService dateTimeGeneratorService; 
	public static String compactDate;
	public static String currentTime;
	
	/**
	 * 
	 * @param productSalesData
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/productSalesRegister", method = RequestMethod.POST)
	public String productSalesRegister(@RequestBody List<ProductSales> productSalesData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + productSalesData.get(0).getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Product Sales Details Registration ::: WEB ::: ProductSaleController");
		compactDate = dateTimeGeneratorService.compactDate();
		for(ProductSales individualProductSalesData : productSalesData){
			individualProductSalesData.setProductSalesEnteredDate(compactDate);
			individualProductSalesData.setProductSoldQuantity("0");
			individualProductSalesData.setProductSalesRowState(0);
		}
		List<ProductSales> productSalesRegisteredObject = productSaleService.productSalesRegister(productSalesData);
		modelMap.addAttribute("productSalesRegisteredObject", productSalesRegisteredObject);
		logger.info("Product Sales Details Successfully Registered ::: WEB ::: ProductSaleController");
		logger.info("------------------------->>>" + productSalesData.get(0).getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productSalesData.get(0).getIndustryCategory() + "/firmTabPages/ownerProductTab :: ProductTabProductSalesDetailsFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @param productSalesData
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/productSalesDetailsUpdate", method = RequestMethod.POST)
	public String productSalesDetailsUpdate(ProductSales productSalesData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + productSalesData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Product Sales Details Update ::: WEB ::: ProductSaleController");
		compactDate = dateTimeGeneratorService.compactDate();
		productSalesData.setProductSalesUpdatedDate(compactDate);
		ProductSales productSalesUpdatedObject = productSaleService.productSalesDetailsUpdate(productSalesData);
		modelMap.addAttribute("productSalesUpdatedObject", productSalesUpdatedObject);
		logger.info("Product Sales Details Successfully Updated ::: WEB ::: ProductSaleController");
		logger.info("------------------------->>>" + productSalesData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productSalesData.getIndustryCategory() + "/firmUpdateForms/updateProductSalesDetailsForm :: UpdateProductSalesDetailsSuccessModalChunk";
		return returnChunk;
	}
	
	/**
	 * Initiating Product Sales. When the Shop Owner scans barcode or types product to initiate sale
	 * This method is called.
	 * @param saleBasketData
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/productSalesBasket", method = RequestMethod.GET)
	public @ResponseBody SalesBasket productSalesBasket(SalesBasket salesBasketData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + salesBasketData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Product Sales. Sale Basket ::: WEB ::: ProductSaleController");
		SalesBasket customerPurchasedObject = productSaleService.productSalesBasket(salesBasketData);
		modelMap.addAttribute("customerPurchasedObject", customerPurchasedObject);
		logger.info("Product Sales Details Successfully Obtained into Sale Basket of Size - " + customerPurchasedObject 
					+ " for Bill No - " + customerPurchasedObject.getSaleBillNumber() + " ::: WEB ::: ProductSaleController");
		logger.info("------------------------->>>" + salesBasketData.getIndustryCategory() + "<<<-------------------------");
		return customerPurchasedObject;
	}
	
	/**
	 * Initiate population of Sale Bill and Sale Bill Consolidated Table.
	 * Compact Date and Time is obtained to ensure unique entry is populated in Consolidated table for each day.
	 * @param saleBasketData
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/generateSalesBasketBill", method = RequestMethod.POST)
	public String generateSalesBasketBill(@RequestBody List<SalesBasket> salesBasketData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + salesBasketData.get(0).getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating population of Sale Bill and Sale Bill Consolidated Table. Customer Purchased items Bill Size - " + salesBasketData.size() + " ::: WEB ::: ProductSaleController");
		logger.info("Set Todays date into each Item. Compact Date - " + compactDate + ". Billing Date and Time - " + compactDate + " " + currentTime);	
		compactDate = dateTimeGeneratorService.compactDate();
		currentTime = dateTimeGeneratorService.compactTime();
		for ( SalesBasket soldProducts : salesBasketData){
			soldProducts.setSaleBillCompactDate(compactDate);
			soldProducts.setSaleBillIssuedDate(compactDate + " " + currentTime);
			logger.info("Bill Issued Date - " + soldProducts.getSaleBillIssuedDate());
		}
		
		List<SalesBasket> customerPurchasedObject = productSaleService.generateSalesBasketBill(salesBasketData);
		modelMap.addAttribute("customerPurchasedObject", customerPurchasedObject);
		logger.info("Product Sales Details Successfully Obtained into Sale Basket of Size -  ::: WEB ::: ProductSaleController");
		logger.info("------------------------->>>" + salesBasketData.get(0).getIndustryCategory() + "<<<-------------------------");
		String returnChunk = salesBasketData.get(0).getIndustryCategory() + "/firmTabPages/ownerSalesTab :: SalesTabHomeFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 */
	
	@RequestMapping(value = "/retrieveTodaysSalesDetails", method = RequestMethod.GET)
	public String retrieveTodaysSalesDetails(SalesBasket salesBasketData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + salesBasketData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of todays Sales. Date - " + salesBasketData.getTodaysDate() + " ::: WEB ::: ProductSaleController");
		SalesBasket todaysSalesDetails = productSaleService.retrieveTodaysSalesDetails(salesBasketData);
		modelMap.addAttribute("todaysSalesDetails", todaysSalesDetails);
		logger.info("Product Sales Details Successfully Obtained into Sale Basket of Size -  ::: WEB ::: ProductSaleController");
		logger.info("------------------------->>>" + salesBasketData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = salesBasketData.getIndustryCategory() + "/firmDetails/firmSalesList/salesDetails :: ViewSalesDetailsTableChunk";
		return returnChunk;
	}
	
	
	/**
	 * 
	 */
	
	@RequestMapping(value = "/retrieveCustomisedSalesDetails", method = RequestMethod.GET)
	public String retrieveCustomisedSalesDetails(SalesBasket salesBasketData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + salesBasketData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of Customised Sales. Date - " + salesBasketData.getTodaysDate() + " ::: WEB ::: ProductSaleController");
		SalesBasket customisedSalesDetails = productSaleService.retrieveCustomisedSalesDetails(salesBasketData);
		modelMap.addAttribute("customisedSalesDetails", customisedSalesDetails);
		logger.info("Product Sales Details Successfully Obtained into Sale Basket of Size -  ::: WEB ::: ProductSaleController");
		logger.info("------------------------->>>" + salesBasketData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = salesBasketData.getIndustryCategory() + "/firmDetails/firmSalesList/salesDetails :: ViewCustomisedSalesDetailsTableChunk";
		return returnChunk;
	}

}
