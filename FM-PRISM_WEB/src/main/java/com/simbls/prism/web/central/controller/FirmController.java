package com.simbls.prism.web.central.controller;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.simbls.prism.web.central.model.Firm;
import com.simbls.prism.web.central.service.FirmService;

@Controller

public class FirmController {

	private static final Logger logger = Logger.getLogger(FirmController.class);
	@Autowired private FirmService firmService;
	
	/**
	 * Registring Firm. Firm registration is performed by On Boarding Team or SuperAdmin.
	 * The following is the details of categories of Firms
	 * Firm Category - 1 ::: Role - Manufacturer
	 * Firm Category - 2 ::: Role - Super Stockist
	 * Firm Category - 3 ::: Role - Firm
	 * Firm Category - 4 ::: Role - Retailer
	 * ****************************************************
	 * IMPORTANT STATUS FLAGS - According to the way condition is checked
	 * ****************************************************
	 * Firm Row State ::: 0 - Firm is Active
	 * Firm Row State ::: 1 - Firm is Deleted
	 * ****************************************************
	 * Firm Approval State ::: 1 - Firm is Approved
	 * Firm Approval State ::: 0 - Firm is Not Approved
	 * ****************************************************
	 * @param Firm Object
	 * @return Success Message
	 * @author Mudasser Nawaz
	 */
	

	@RequestMapping(value = "/firmRegister", method = RequestMethod.POST)
	public String firmRegister(Firm firmData, ModelMap map) throws Exception {
		logger.info("Initiating Firm Registration ::: WEB ::: FirmController");
		logger.info("Set Firm Added date to current Date.");
		firmData.setFirmAddedDate(new Date());
		logger.info("Set Firm RowState to 0. Indicating Active Status.");
		firmData.setFirmRowState(0);
		logger.info("Set Firm Approval Status to 0. Waiting for Firm to be approved by Super Admin");
		firmData.setFirmApprovalStatus(0);
		firmService.firmRegister(firmData);
		logger.info("Successfully Registered Firm ::: WEB ::: FirmController");
		return "centralDashboard/centralTabPages/superAdminFirmsTab :: FirmTabAddFirmFragment";
	}



}
