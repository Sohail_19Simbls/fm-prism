package com.simbls.prism.web.foodBazaar.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.web.common.DateTimeGeneratorService;
import com.simbls.prism.web.foodBazaar.service.FoodBazaarProductService;
import com.simbls.prism.web.industry.model.Product;

@Controller
@RequestMapping(value = "/foodBazaar")
public class FoodBazaarProductController {
	private static final Logger logger = Logger.getLogger(FoodBazaarProductController.class);
	@Autowired private FoodBazaarProductService foodBazaarProductService;
	@Autowired private DateTimeGeneratorService dateTimeGeneratorService; 
	public static String compactDate;
	
	/**
	 * Register Product
	 * @param product
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/productRegister", method = RequestMethod.POST)
	public String productRegister(Product productData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Product Registration ::: WEB ::: FoodBazaarProductController");
		compactDate = dateTimeGeneratorService.compactDate();
		productData.setProductEnteredDate(compactDate);
		Product insertedProductData = foodBazaarProductService.productRegister(productData);
		if(productData.getProductName().equals(insertedProductData.getProductName())){
			logger.info("Product - " + productData.getProductID()  + " Successfully Registered ::: WEB ::: FoodBazaarProductController");
		}else {
			logger.info("Product - " + productData.getProductID() + " Already Registered ::: WEB ::: FoodBazaarProductController");
			insertedProductData = new Product();
		}
		modelMap.addAttribute("insertedProductData", insertedProductData);
		logger.info("Returning control to UI. Add Product Form ::: WEB ::: FoodBazaarProductController");
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productData.getIndustryCategory() + "/firmTabPages/ownerProductTab :: ProductTabAddProductFragment";
		return returnChunk;
	}
	
	/**
	 * Update Product
	 * @param product
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/productUpdate", method = RequestMethod.POST)
	public String productUpdate(Product productData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Product Update ::: WEB ::: FoodBazaarProductController");
		compactDate = dateTimeGeneratorService.compactDate();
		productData.setProductUpdatedDate(compactDate);
		Product updatedProductData = foodBazaarProductService.productUpdate(productData);
		if(productData.getProductName().equals(updatedProductData.getProductName())){
			logger.info("Product - " + productData.getProductID()  + " Successfully Updated ::: WEB ::: FoodBazaarProductController");
		}else {
			logger.info("Product - " + productData.getProductID() + " Already Registered ::: WEB ::: FoodBazaarProductController");
			updatedProductData = new Product();
		}
		modelMap.addAttribute("insertedProductData", updatedProductData);
		logger.info("Returning control to UI. Add Product Form ::: WEB ::: FoodBazaarProductController");
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productData.getIndustryCategory() + "/firmUpdateForms/updateProductForm :: UpdateProductSuccessModalChunk";
		return returnChunk;
	}
	
	/**
	 * Delete Product
	 * @param product
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/productDelete", method = RequestMethod.POST)
	public String productDelete(Product productData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Product Deletion ::: WEB ::: FoodBazaarProductController");
//		foodBazaarProductService.productDelete(productData);
		logger.info("Successfully Deleted Product ::: WEB ::: FoodBazaarProductController");
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productData.getIndustryCategory() + "/firmConfirmation/deleteConfirmation :: DeleteSuccessfulModalChunk";
		return returnChunk;
	}
	
	/**
	 * Generate BarCode and Register Product from Owner Page
	 * @param product
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/productGenerateBarCodeProductRegister", method = RequestMethod.POST)
	public String productGenerateBarCodeProductRegister(Product productData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating BarCode generation and Product Registration ::: WEB ::: FoodBazaarProductController");
		compactDate = dateTimeGeneratorService.compactDate();
		productData.setProductEnteredDate(compactDate);
		Product insertedProductData = foodBazaarProductService.productRegister(productData);
		if(productData.getProductName().equals(insertedProductData.getProductName())){
			logger.info("Product - " + productData.getProductID()  + " Successfully Registered ::: WEB ::: FoodBazaarProductController");
		}else {
			logger.info("Product - " + productData.getProductID() + " Already Registered ::: WEB ::: FoodBazaarProductController");
			insertedProductData = new Product();
		}
		modelMap.addAttribute("insertedProductData", insertedProductData);
		logger.info("Returning control to UI. Generate BarCode and Add Product Form ::: WEB ::: FoodBazaarProductController");
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productData.getIndustryCategory() + "/firmTabPages/ownerProductTab :: ProductTabCreateBarCodeRegisterProductFragment";
		return returnChunk;
	}
	
	/**
	 * Retrieves Product by Any Parameter present in Product Model.
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveProductByParameter", method=RequestMethod.GET)
	public @ResponseBody List<Product> retrieveProductByParameter(Product productData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of Registered Product based on BarCode ::: WEB ::: FoodBazaarProductController");
		List<Product> retrievedProduct = foodBazaarProductService.retrieveProductByParameter(productData);
		modelMap.addAttribute("retrievedProduct", retrievedProduct);
		logger.info("Successfully retrieved Product from DB - " + retrievedProduct.size() + "::: WEB ::: FoodBazaarProductController");
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		return retrievedProduct;
	}
	
	/**
	 * Retrieve Brief Product List of all the Products Registered.
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveProductList", method=RequestMethod.GET)
	public String retrieveProductList(Product productData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of Registered Product based on BarCode ::: WEB ::: FoodBazaarProductController");
		List<Product> retrievedProduct = foodBazaarProductService.retrieveProductByParameter(productData);
		modelMap.addAttribute("retrievedProduct", retrievedProduct);
		logger.info("Successfully retrieved Product from DB - " + retrievedProduct.size() + "::: WEB ::: FoodBazaarProductController");
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productData.getIndustryCategory() + "/firmTabPages/ownerProductTab :: ProductTabProductListFragment";
		return returnChunk;
	}
	
	/**
	 * Retrieve Product Purchase and Sales Details
	 * @param productData
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/retrieveProductPurchaseSaleDetails", method=RequestMethod.GET)
	public String retrieveProductPurchaseSaleDetails(Product productData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of Registered Product Purchase and Sales Details ::: WEB ::: FoodBazaarProductController");
		Product retrievedProductPurchaseSaleDetails = foodBazaarProductService.retrieveProductPurchaseSaleDetails(productData);
		modelMap.addAttribute("retrievedProductPurchaseSaleDetails", retrievedProductPurchaseSaleDetails);
		logger.info("Successfully retrieved Product from DB ::: WEB ::: FoodBazaarProductController");
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productData.getIndustryCategory() + "/firmDetails/productDetails :: ViewProductPurchaseSalesDetailsChunk";
		return returnChunk;
	}
	

}
