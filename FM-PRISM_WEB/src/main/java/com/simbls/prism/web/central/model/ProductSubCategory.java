package com.simbls.prism.web.central.model;

public class ProductSubCategory {
	
	private int productSubCategoryID;
	private int productCategoryID;
	private String productSubCategory;

	public int getProductSubCategoryID() {
		return productSubCategoryID;
	}

	public void setProductSubCategoryID(int productSubCategoryID) {
		this.productSubCategoryID = productSubCategoryID;
	}

	public int getProductCategoryID() {
		return productCategoryID;
	}

	public void setProductCategoryID(int productCategoryID) {
		this.productCategoryID = productCategoryID;
	}

	public String getProductSubCategory() {
		return productSubCategory;
	}

	public void setProductSubCategory(String productSubCategory) {
		this.productSubCategory = productSubCategory;
	}
	
}
