package com.simbls.prism.web.common;


import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes({ "loggedUser", "parentId" })
public class HomeController {

	private static final Logger logger = Logger.getLogger(HomeController.class);

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		logger.info("Redirecting to Landing Page ::: WEB ::: Home Controller");
		return "index";
	}

	
	/**
	* SuperAdmin Home Sub Tab
	* @author 
	*/
	@RequestMapping(value = "/superAdminDashboardHomeTab", method = RequestMethod.GET)
	public String superAdminDashboardHomeTab() {
	logger.info("Redirect to Super Admin Home Tab ::: WEB ::: Home Controller");
	return "centralDashboard/superAdminDashboard :: SuperAdminDashboardMain";
	}

	/***********************************************************************************************************************/
	/** SUPER ADMIN DASHBOARD >> USER TAB >> TEMPLATE AJAX CALLS **/
	/***********************************************************************************************************************/
	
	
	/**
	* SuperAdmin User Home Sub Tab
	* @return
	*/
	@RequestMapping(value = "/superAdminDashboardUserTab", method = RequestMethod.GET)
	public String superAdminDashboardUserTab() {
		logger.info("Redirect to Super Admin User Tab >> User Home ::: WEB ::: Home Controller");
		return "centralDashboard/centralTabPages/superAdminUserTab :: UserTabHomeFragment";
	}

	/**
	* SuperAdmin User Home Sub Tab
	* @return
	*/
	@RequestMapping(value = "/superAdminDashboardUserHomeSubTab", method = RequestMethod.GET)
	public String superAdminDashboardUserHomeSubTab() {
		logger.info("Redirect to Super Admin User Home Tab >> User Home ::: WEB ::: Home Controller");
		return "centralDashboard/centralTabPages/superAdminUserTab :: UserTabHomeFragment";
	}
	
	/**
	* SuperAdmin User >> Add User Sub Tab
	* @return
	*/
	@RequestMapping(value = "/superAdminDashboardAddUserSubTab", method = RequestMethod.GET)
	public String superAdminDashboardAddUserSubTab() {
		logger.info("Redirect to Super Admin User Home >> User Home >> Add User Tab ::: WEB ::: Home Controller");
		return "centralDashboard/centralTabPages/superAdminUserTab :: UserTabAddUserFragment";
	}
	
	/***********************************************************************************************************************/
	/** SUPER ADMIN DASHBOARD >> FIRM TAB >> TEMPLATE AJAX CALLS **/
	/***********************************************************************************************************************/
	
	/**
	* SuperAdmin Firm Sub Tab
	* @return
	*/
	@RequestMapping(value = "/superAdminDashboardFirmTab", method = RequestMethod.GET)
	public String superAdminDashboardFirmTab() {
		logger.info("Redirect to Super Admin Firm Tab ::: WEB ::: Home Controller");
		return "centralDashboard/centralTabPages/superAdminFirmsTab :: FirmTabHomeFragment";
	}
	
	/**
	* SuperAdmin Firm Sub Tab
	* @return
	*/
	@RequestMapping(value = "/superAdminDashboardFirmHomeSubTab", method = RequestMethod.GET)
	public String superAdminDashboardFirmHomeSubTab() {
		logger.info("Redirect to Super Admin Firm Tab ::: WEB ::: Home Controller");
		return "centralDashboard/centralTabPages/superAdminFirmsTab :: FirmTabHomeFragment";
	}
	
	/**
	* SuperAdmin Firm Sub Tab
	* @return
	*/
	@RequestMapping(value = "/superAdminDashboardAddFirmSubTab", method = RequestMethod.GET)
	public String superAdminDashboardAddFirmSubTab() {
		logger.info("Redirect to Super Admin Firm Home >> Firm Home >> Add Firm Tab ::: WEB ::: Home Controller");
		return "centralDashboard/centralTabPages/superAdminFirmsTab :: FirmTabAddFirmFragment";
	}
	

	
	/***********************************************************************************************************************/
	/** OWNER DASHBOARD >> TEMPLATE AJAX CALLS **/
	/***********************************************************************************************************************/
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardHomeTab", method = RequestMethod.GET)
	public String ownerDashboardHomeTab() {
		logger.info("Redirect to OwnerDashboard Home Tab ::: WEB ::: Home Controller");
		return "firmTabPages/ownerDashBoard :: OwnerDashBoardMain";
	}
	
	/***********************************************************************************************************************/
	/** OWNER DASHBOARD >> DEALER TAB >> TEMPLATE AJAX CALLS **/
	/***********************************************************************************************************************/
	
	/****************************************************************/
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardDealerTab", method = RequestMethod.GET)
	public String ownerDashboardDealerTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard Dealer Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerDealerTab :: DealerTabHomeFragment";
		return returnChunk;
	}
	/****************************************************************/
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardDealerHomeSubTab", method = RequestMethod.GET)
	public String ownerDashboardDealerHomeSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Dealer Tab >> DealerHome Sub Tab::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerDealerTab :: DealerTabHomeFragment";
		return returnChunk;
	}
	
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardAddDealerSubTab", method = RequestMethod.GET)
	public String ownerDashboardAddDealerSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Dealer Tab >> Add Dealer Sub Tab::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerDealerTab :: DealerTabAddDealerFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/editDealerDetails", method = RequestMethod.GET)
	public String editDealerDetails(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Dealer Tab >> Update Dealer Details ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmUpdateForms/updateDealerForm :: UpdateDealerFormChunk";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardDealerProfitSubTab", method = RequestMethod.GET)
	public String ownerDashboardDealerProfitSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Dealer Tab >> Dealer Profit Sub Tab::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerDealerTab :: DealerTabDealerProfitFragment";
		return returnChunk;
	}
	
	/***********************************************************************************************************************/
	/** OWNER DASHBOARD >> INVOICE TAB >> TEMPLATE AJAX CALLS **/
	/***********************************************************************************************************************/
	
	/****************************************************************/
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardInvoiceTab", method = RequestMethod.GET)
	public String ownerDashboardInvoiceTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard Invoice Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerInvoiceTab :: InvoiceTabHomeFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardInvoiceHomeSubTab", method = RequestMethod.GET)
	public String ownerDashboardInvoiceHomeSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Invoice Tab >> Invoice Home Sub Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerInvoiceTab :: InvoiceTabHomeFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardAddInvoiceSubTab", method = RequestMethod.GET)
	public String ownerDashboardAddInvoiceSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Invoice Tab >> Add Invoice Sub Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerInvoiceTab :: InvoiceTabAddInvoiceFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/editInvoiceDetails", method = RequestMethod.GET)
	public String editInvoiceDetails(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Product Tab >> Update Product Details ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmUpdateForms/updateInvoiceForm :: UpdateInvoiceFormChunk";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardInvoicePaymentSubTab", method = RequestMethod.GET)
	public String ownerDashboardInvoicePaymentSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Invoice Tab >> Add Invoice Payment Sub Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerInvoiceTab :: InvoiceTabInvoicePaymentFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardInvoicePaymentStatusSubTab", method = RequestMethod.GET)
	public String ownerDashboardInvoicePaymentStatusSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Invoice Tab >> Invoice Payment Status Sub Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerInvoiceTab :: InvoiceTabInvoicePaymentStatusFragment";
		return returnChunk;
	}
	
	
		
	/***********************************************************************************************************************/
	/** OWNER DASHBOARD >> PRODUCT TAB >> TEMPLATE AJAX CALLS **/
	/***********************************************************************************************************************/
	
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardProductTab", method = RequestMethod.GET)
	public String ownerDashboardProductTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard Product Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerProductTab :: ProductTabHomeFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardProductHomeSubTab", method = RequestMethod.GET)
	public String ownerDashboardProductHomeSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Product Tab >> Product Home Sub Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerProductTab :: ProductTabHomeFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardAddProductSubTab", method = RequestMethod.GET)
	public String ownerDashboardAddProductSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Product Tab >> Add Product Sub Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerProductTab :: ProductTabAddProductFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/editProductDetails", method = RequestMethod.GET)
	public String editProductDetails(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Product Tab >> Update Product Details ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmUpdateForms/updateProductForm :: UpdateProductFormChunk";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardGenerateBarcodeSubTab", method = RequestMethod.GET)
	public String ownerDashboardGenerateBarcodeSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Product Tab >> Generate BarCode Sub Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerProductTab :: ProductTabCreateBarCodeRegisterProductFragment";
		return returnChunk;
	}
	
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardProductPurchaseDetailsSubTab", method = RequestMethod.GET)
	public String ownerDashboardProductPurchaseDetailsSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Product Tab >> Product Purchase Details Sub Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerProductTab :: ProductTabProductPurchaseDetailsFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardProductPurchaseDetailsByProductSubTab", method = RequestMethod.GET)
	public String ownerDashboardProductPurchaseDetailsByProductSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Product Tab >> Product Purchase Details Sub Tab >> Product Purchase Details by Product ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmForms/addProductPurchaseDetailsForm::AddProductPurchaseDetailsByProductChunk";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardProductPurchaseDetailsByInvoiceSubTab", method = RequestMethod.GET)
	public String ownerDashboardProductPurchaseDetailsByInvoiceSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Product Tab >> Product Purchase Details Sub Tab >> Product Purchase Details by Invoice ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmForms/addProductPurchaseDetailsForm::AddProductPurchaseDetailsByInvoiceChunk";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardProductSalesDetailsSubTab", method = RequestMethod.GET)
	public String ownerDashboardProductSalesDetailsSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Product Tab >> Product Sales Details Sub Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerProductTab :: ProductTabProductSalesDetailsFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/editProductSalesDetails", method = RequestMethod.GET)
	public String editProductSalesDetails(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Dealer Tab >> Update Dealer Details ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmUpdateForms/updateProductSalesDetailsForm :: UpdateProductSalesDetailsFormChunk";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/editProductPurchaseDetails", method = RequestMethod.GET)
	public String editProductPurchaseDetails(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Dealer Tab >> Update Dealer Details ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmUpdateForms/updateProductPurchaseDetailsForm :: UpdateProductPurchaseDetailsFormChunk";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardProductListSubTab", method = RequestMethod.GET)
	public String ownerDashboardProductListSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Product Tab >> Product List Sub Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmLists/productList :: ViewProductListChunk";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardProductPurchaseListSubTab", method = RequestMethod.GET)
	public String ownerDashboardProductPurchaseListSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Product Tab >> Product Purchase Details List Sub Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerProductTab :: ProductTabProductPurchaseDetailsListFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/ownerDashboardProductSalesListSubTab", method = RequestMethod.GET)
	public String ownerDashboardProductSalesListSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Product Tab >> Product Sales Details List Sub Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerProductTab :: ProductTabProductSalesDetailsListFragment";
		return returnChunk;
	}
	
	/***********************************************************************************************************************/
	/** OWNER DASHBOARD >> SALES TAB >> TEMPLATE AJAX CALLS **/
	/***********************************************************************************************************************/
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardSalesTab", method = RequestMethod.GET)
	public String ownerDashboardSalesTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Sales Home Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerSalesTab :: SalesTabHomeFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardSalesHomeSubTab", method = RequestMethod.GET)
	public String ownerDashboardSalesHomeSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Sales >> Sales Home Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerSalesTab :: SalesTabHomeFragment";
		return returnChunk;
	}
	
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/generateSalesPrintableBill", method = RequestMethod.GET)
	public String generateSalesPrintableBill(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Sales >> Print Sales Bill Modal ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmPrints/customerSalesBill :: CustomerSalesFormPrintChunk";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardSalesHistorySubTab", method = RequestMethod.GET)
	public String ownerDashboardSalesHistorySubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Sales >> Sales History Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerSalesTab :: SalesTabSalesHistoryFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardSalesReturnsSubTab", method = RequestMethod.GET)
	public String ownerDashboardSalesReturnsSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Sales >> Sales Returns Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerSalesTab :: SalesTabSalesReturnsFragment";
		return returnChunk;
	}
	
	/***********************************************************************************************************************/
	/** OWNER DASHBOARD >> WHOLESALE TAB >> TEMPLATE AJAX CALLS **/
	/***********************************************************************************************************************/
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardWholeSaleTab", method = RequestMethod.GET)
	public String ownerDashboardWholeSaleTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> WholeSale Home Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerWholeSaleTab :: WholeSaleTabHomeFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardWholeSaleHomeSubTab", method = RequestMethod.GET)
	public String ownerDashboardWholeSaleHomeSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> WholeSale >> WholeSale Home Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerWholeSaleTab :: WholeSaleTabHomeFragment";
		return returnChunk;
	}
	
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardWholeSaleReturnSubTab", method = RequestMethod.GET)
	public String ownerDashboardWholeSaleReturnSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Sales >> WholeSale Returns Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerWholeSaleTab :: WholeSaleTabSalesReturnsFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardWholeSaleHistorySubTab", method = RequestMethod.GET)
	public String ownerDashboardWholeSaleHistorySubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect " + industryCategory + " to OwnerDashboard >> Sales >> WholeSale History Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerWholeSaleTab :: WholeSaleTabWholeSaleHistoryFragment";
		return returnChunk;
	}

	
	/***********************************************************************************************************************/
	/** OWNER DASHBOARD >> STOCK TAB >> TEMPLATE AJAX CALLS **/
	/***********************************************************************************************************************/
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardStockTab", method = RequestMethod.GET)
	public String ownerDashboardStockTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Stock Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerStockTab :: StockTabHomeFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardStockHomeSubTab", method = RequestMethod.GET)
	public String ownerDashboardStockHomeSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Stock Tab >> Stock Home ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerStockTab :: StockTabHomeFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardProductStockSubTab", method = RequestMethod.GET)
	public String ownerDashboardProductStockSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Stock Tab >> Product Stock ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerStockTab :: StockTabProductStockFragment";
		return returnChunk;
	}
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardStockInwardSubTab", method = RequestMethod.GET)
	public String ownerDashboardStockInwardSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Stock Tab >> Stock Inward ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerStockTab :: StockTabStockInwardFragment";
		return returnChunk;
	}
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardStockOutwardSubTab", method = RequestMethod.GET)
	public String ownerDashboardStockOutwardSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to " + industryCategory + " OwnerDashboard >> Stock Tab >> Stock Outward ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerStockTab :: StockTabStockOutwardFragment";
		return returnChunk;
	}
	
	/***********************************************************************************************************************/
	/** OWNER DASHBOARD >> ACCOUNTS TAB >> TEMPLATE AJAX CALLS **/
	/***********************************************************************************************************************/
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardAccountsTab", method = RequestMethod.GET)
	public String ownerDashboardAccountsTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to OwnerDashboard >> Accounts Tab ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerAccountsTab :: AccountsTabHomeFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardAccountsHomeSubTab", method = RequestMethod.GET)
	public String ownerDashboardAccountsHomeSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to OwnerDashboard >> Accounts Tab >> Accounts Home ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerAccountsTab :: AccountsTabHomeFragment";
		return returnChunk;
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardPurchaseAccountsSubTab", method = RequestMethod.GET)
	public String ownerDashboardPurchaseAccountsSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to OwnerDashboard >> Accounts Tab >> Purchase Accounts ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerAccountsTab :: AccountsTabPurchaseAccountsFragment";
		return returnChunk;
	}
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardSalesAccountsSubTab", method = RequestMethod.GET)
	public String ownerDashboardSalesAccountsSubTab(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to OwnerDashboard >> Accounts Tab >> Sales Accounts ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmTabPages/ownerAccountsTab :: AccountsTabSalesAccountsFragment";
		return returnChunk;
	}


	/***********************************************************************************************************************/
	/** OWNER DASHBOARD >> PROFIT TAB >> TEMPLATE AJAX CALLS **/
	/***********************************************************************************************************************/
	
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ownerDashboardProfitTab", method = RequestMethod.GET)
	public String ownerDashboardProfitTab() {
		logger.info("Redirect to OwnerDashboard Profit Tab ::: WEB ::: Home Controller");
		return "firmDashboard/firmTabPages/ownerProfitTab :: UserTabMainFragment";
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/logout")
	public String logout(HttpServletRequest request) {
		logger.info("Initiating logout for User - ::: WEB ::: HomeController");
		request.getSession().invalidate();
		logger.info("Redirecting to login page ::: WEB ::: HomeController");
		return "index";
	}
	
	/***********************************************************************************************************************/
	/** CONFIRMATION DIALOGUE BOX **/
	/***********************************************************************************************************************/
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/deleteConfirmation")
	public String deleteConfirmation(@RequestParam ("industryCategory") String industryCategory) {
		logger.info("Redirect to firmConfirmation >> DeleteConfirmation Pop-Up ::: WEB ::: Home Controller");
		String returnChunk = industryCategory + "/firmConfirmation/deleteConfirmation :: DeleteConfirmationModalChunk";
		return returnChunk;
	}
	
}
