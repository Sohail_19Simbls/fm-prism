package com.simbls.prism.web.common;

public class MultipleObjectJSON {
	private String objectName;
	private Object objectContent;
	
	public String getObjectName() {
		return objectName;
	}
	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
	public Object getObjectContent() {
		return objectContent;
	}
	public void setObjectContent(Object objectContent) {
		this.objectContent = objectContent;
	}
}
