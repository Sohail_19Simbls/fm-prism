package com.simbls.prism.web.foodBazaar.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.industry.model.ProductSales;
import com.simbls.prism.web.industry.model.SalesBasket;

@Component
public interface FoodBazaarProductSalesService {
	/**
	 * Register Product Sales Details
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<ProductSales> productSalesRegister(List<ProductSales> productSalesData) throws Exception;
	
	/**
	 * 
	 * @param productSalesData
	 * @return
	 * @throws Exception
	 */
	public ProductSales productSalesDetailsUpdate(ProductSales productSalesData) throws Exception;
	
	/**
	 * Populate Sale Basket
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public SalesBasket productSalesBasket(SalesBasket salesBasketData) throws Exception;
	
	/**
	 * Initiate population of Sale Bill and Sale Bill Consolidated Table 
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<SalesBasket> generateSalesBasketBill(List<SalesBasket> salesBasketData) throws Exception;
	
	
	/**
	 * Initiate population Todays Sale Details
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public SalesBasket retrieveTodaysSalesDetails(SalesBasket salesBasketData) throws Exception;
	
	/**
	 * Initiate population Customised Sale Details
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public SalesBasket retrieveCustomisedSalesDetails(SalesBasket salesBasketData) throws Exception;

}
