package com.simbls.prism.web.ceramics.serviceImpl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.ceramics.service.CeramicsProductPurchaseService;
import com.simbls.prism.web.industry.model.ProductPurchase;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.CeramicsRestUtil;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class CeramicsProductPurchaseServiceImpl implements CeramicsProductPurchaseService {
	private static final Logger logger = Logger.getLogger(CeramicsProductPurchaseServiceImpl.class);
	@Autowired private CeramicsRestUtil ceramicsRestUtil;
	@Inject RestClient restClient;
	
	/**
	 * 
	 */
	public List<ProductPurchase> productPurchaseDetailsRegister(List<ProductPurchase> productPurchaseData) throws Exception {
		logger.info("Continuing Product Purchase Registration ::: WEB ::: CeramicsProductPurchaseServiceImpl");
		String url = ceramicsRestUtil.getUrl() + CeramicsRestUtil.PRODUCT_PURCHSE_DETAILS_REGISTER;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productPurchaseData));
		ProductPurchase[] productPurchases = mapper.readValue(httpResponse.getBody().toString(), ProductPurchase[].class);
		List<ProductPurchase> productPurchaseList = Arrays.asList(productPurchases);
		logger.info("Successfully received control from REST ::: WEB ::: CeramicsProductPurchaseServiceImpl");
		return productPurchaseList;
	}
	
	/**
	 * 
	 */
	public List<ProductPurchase> retrieveProductPurchaseDetails(ProductPurchase productPurchaseData) throws Exception {
		logger.info("Continuing retrival of all Product Purchase Details ::: WEB ::: CeramicsProductPurchaseServiceImpl");
		String url = ceramicsRestUtil.getUrl() + CeramicsRestUtil.RETRIEVE_PRODUCT_PURCHSE_DETAILS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productPurchaseData));
		ProductPurchase[] productPurchases = mapper.readValue(httpResponse.getBody().toString(), ProductPurchase[].class);
		List<ProductPurchase> productPurchaseList = Arrays.asList(productPurchases);
		logger.info("Obtained ProductPurchase details of Size -" + productPurchaseList.size() + " from REST ::: WEB ::: CeramicsProductPurchaseServiceImpl");
		return productPurchaseList;
	}

	
	/**
	 * 
	 */
	public ProductPurchase checkInvoiceStatus(ProductPurchase productPurchaseData) throws Exception {
		logger.info("Continuing retrival of all Product Purchase Details ::: WEB ::: CeramicsProductPurchaseServiceImpl");
		String url = ceramicsRestUtil.getUrl() + CeramicsRestUtil.CHECK_INVOICE_STATUS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productPurchaseData));
		ProductPurchase productPurchaseDetails = mapper.readValue(httpResponse.getBody().toString(), ProductPurchase.class);
		logger.info("Obtained ProductPurchase details of Size - from REST ::: WEB ::: CeramicsProductPurchaseServiceImpl");
		return productPurchaseDetails;
	}

}
