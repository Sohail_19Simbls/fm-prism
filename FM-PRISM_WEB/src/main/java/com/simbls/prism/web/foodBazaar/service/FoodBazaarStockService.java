package com.simbls.prism.web.foodBazaar.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.industry.model.SalesBasket;

@Component
public interface FoodBazaarStockService {

	/**
	 * Retrieve Detailed Product List of all the Products Present in Stock.
	 * @param dealerData
	 * @return
	 * @throws Exception
	 */
	public List<SalesBasket> retrieveDetailProductList(SalesBasket salesBasketData) throws Exception;

}
