package com.simbls.prism.web.clothing.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.web.clothing.service.ClothingProductPurchaseService;
import com.simbls.prism.web.common.DateTimeGeneratorService;
import com.simbls.prism.web.industry.model.ProductPurchase;

@Controller
@RequestMapping(value = "/clothing")
public class ClothingProductPurchaseController {
	private static final Logger logger = Logger.getLogger(ClothingProductPurchaseController.class);
	@Autowired private ClothingProductPurchaseService clothingProductPurchaseService;
	@Autowired private DateTimeGeneratorService dateTimeGeneratorService; 
	public static String compactDate;
	
	/**
	 * Register Product Purchase Details.
	 * @param productPurchaseData
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/productPurchaseDetailsRegister", method=RequestMethod.POST)
	public String productPurchaseDetailsRegister(@RequestBody List<ProductPurchase> productPurchaseData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + productPurchaseData.get(0).getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Product Purchase Details Registration for Products of size - " + productPurchaseData.size() + "::: WEB ::: CeramicsProductPurchaseController");
		String compactDate = dateTimeGeneratorService.compactDate();
		for(ProductPurchase individualPurchasedProduct : productPurchaseData){
			individualPurchasedProduct.setProductPurchaseEnteredDate(compactDate);
			individualPurchasedProduct.setProductPurchaseRowState(0);
		}
		List<ProductPurchase> registeredObject = clothingProductPurchaseService.productPurchaseDetailsRegister(productPurchaseData);
		/*if(registeredObject != null){
			logger.info("Product Purchase Details Successfully Registered ::: WEB ::: CeramicsProductPurchaseController");
			if(registeredObject.getProductSalesObject() != null){
				logger.info("Product Sales Details auto populated as requested::: WEB ::: CeramicsProductPurchaseController");
			} else {
				logger.info("Product Sales Details auto population was not requested::: WEB ::: CeramicsProductPurchaseController");
			}
		}
		modelMap.addAttribute("registeredObject", registeredObject);*/
		logger.info("------------------------->>>" + productPurchaseData.get(0).getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productPurchaseData.get(0).getIndustryCategory() + "/firmTabPages/ownerProductTab :: ProductTabProductPurchaseDetailsFragment";
		return returnChunk;
	}
	
	
	/**
	 * Retrieves Product Purchase Details by Selected Product
	 * @param productPurchaseData, modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveProductPurchaseDetails", method=RequestMethod.GET)
	public @ResponseBody List<ProductPurchase> retrieveProductPurchaseDetails(ProductPurchase productPurchaseData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of Product Purchase Details based on Invoice ::: WEB ::: CeramicsProductPurchaseController");
		List<ProductPurchase> productPurchaseDetailsList = clothingProductPurchaseService.retrieveProductPurchaseDetails(productPurchaseData);
		modelMap.addAttribute("productPurchaseDetailsList", productPurchaseDetailsList);
		logger.info("Successfully retrieved Product Purcase Details from DB - " + productPurchaseData.getFirmDBName() + "::: WEB ::: CeramicsProductPurchaseController");
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		return productPurchaseDetailsList;
	}
	
	/**
	 * Check if all the Purchased Product Total matches the Invoice Amount
	 * @param productPurchaseData, modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/checkInvoiceStatus", method=RequestMethod.GET)
	public @ResponseBody ProductPurchase checkInvoiceStatus(ProductPurchase productPurchaseData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating check if all the Purchased Products Details Sum equals Invoice Total ::: WEB ::: CeramicsProductPurchaseController");
		ProductPurchase productPurchaseDetails = clothingProductPurchaseService.checkInvoiceStatus(productPurchaseData);
		logger.info("Successfully Obtained Invoice Details ::: WEB ::: CeramicsProductPurchaseController");
		logger.info("------------------------->>>" + productPurchaseData.getIndustryCategory() + "<<<-------------------------");
		return productPurchaseDetails;
	}

}
