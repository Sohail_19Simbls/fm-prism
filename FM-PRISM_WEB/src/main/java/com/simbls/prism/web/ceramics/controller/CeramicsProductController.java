package com.simbls.prism.web.ceramics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.web.ceramics.service.CeramicsProductService;
import com.simbls.prism.web.common.DateTimeGeneratorService;
import com.simbls.prism.web.industry.model.Product;

@Controller
@RequestMapping(value = "/ceramics")
public class CeramicsProductController {
	private static final Logger logger = Logger.getLogger(CeramicsProductController.class);
	@Autowired private CeramicsProductService ceramicsProductService;
	@Autowired private DateTimeGeneratorService dateTimeGeneratorService; 
	public static String compactDate;
	
	/**
	 * Register Product
	 * @param product
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/productRegister", method = RequestMethod.POST)
	public String productRegister(Product productData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Product Registration ::: WEB ::: CeramicsProductController");
		compactDate = dateTimeGeneratorService.compactDate();
		productData.setProductEnteredDate(compactDate);
		Product insertedProductData = ceramicsProductService.productRegister(productData);
		if(productData.getProductName().equals(insertedProductData.getProductName())){
			logger.info("Product - " + productData.getProductID()  + " Successfully Registered ::: WEB ::: CeramicsProductController");
		}else {
			logger.info("Product - " + productData.getProductID() + " Already Registered ::: WEB ::: CeramicsProductController");
			insertedProductData = new Product();
		}
		modelMap.addAttribute("insertedProductData", insertedProductData);
		logger.info("Returning control to UI. Add Product Form ::: WEB ::: CeramicsProductController");
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productData.getIndustryCategory() + "/firmTabPages/ownerProductTab :: ProductTabAddProductFragment";
		return returnChunk;
	}
	
	/**
	 * Update Product
	 * @param product
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/productUpdate", method = RequestMethod.POST)
	public String productUpdate(Product productData, ModelMap modelMap) throws Exception{
		logger.info("--------------------------------------------------");
		logger.info("Initiating Product Update ::: WEB ::: CeramicsProductController");
		compactDate = dateTimeGeneratorService.compactDate();
		productData.setProductUpdatedDate(compactDate);
		Product updatedProductData = ceramicsProductService.productUpdate(productData);
		if(productData.getProductName().equals(updatedProductData.getProductName())){
			logger.info("Product - " + productData.getProductID()  + " Successfully Updated ::: WEB ::: CeramicsProductController");
		}else {
			logger.info("Product - " + productData.getProductID() + " Already Registered ::: WEB ::: CeramicsProductController");
			updatedProductData = new Product();
		}
		modelMap.addAttribute("insertedProductData", updatedProductData);
		logger.info("Returning control to UI. Add Product Form ::: WEB ::: CeramicsProductController");
		logger.info("--------------------------------------------------");
		return "firmDashboard/firmUpdateForms/updateProductForm :: UpdateProductSuccessModalChunk";
	}
	
	/**
	 * Generate BarCode and Register Product from Owner Page
	 * @param product
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value = "/productGenerateBarCodeProductRegister", method = RequestMethod.POST)
	public String productGenerateBarCodeProductRegister(Product productData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating BarCode generation and Product Registration ::: WEB ::: CeramicsProductController");
		compactDate = dateTimeGeneratorService.compactDate();
		productData.setProductEnteredDate(compactDate);
		Product insertedProductData = ceramicsProductService.productRegister(productData);
		if(productData.getProductName().equals(insertedProductData.getProductName())){
			logger.info("Product - " + productData.getProductID()  + " Successfully Registered ::: WEB ::: CeramicsProductController");
		}else {
			logger.info("Product - " + productData.getProductID() + " Already Registered ::: WEB ::: CeramicsProductController");
			insertedProductData = new Product();
		}
		modelMap.addAttribute("insertedProductData", insertedProductData);
		logger.info("Returning control to UI. Generate BarCode and Add Product Form ::: WEB ::: CeramicsProductController");
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productData.getIndustryCategory() + "/firmTabPages/ownerProductTab :: ProductTabCreateBarCodeRegisterProductFragment";
		return returnChunk;
	}
	
	/**
	 * Retrieves Product by Any Parameter present in Product Model.
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveProductByParameter", method=RequestMethod.GET)
	public @ResponseBody List<Product> retrieveProductByParameter(Product productData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of Registered Product based on BarCode ::: WEB ::: CeramicsProductController");
		List<Product> retrievedProduct = ceramicsProductService.retrieveProductByParameter(productData);
		modelMap.addAttribute("retrievedProduct", retrievedProduct);
		logger.info("Successfully retrieved Product from DB - " + retrievedProduct.size() + "::: WEB ::: CeramicsProductController");
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		return retrievedProduct;
	}
	
	/**
	 * Retrieve Brief Product List of all the Products Registered.
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveProductList", method=RequestMethod.GET)
	public String retrieveProductList(Product productData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of Registered Product based on BarCode ::: WEB ::: CeramicsProductController");
		List<Product> retrievedProduct = ceramicsProductService.retrieveProductByParameter(productData);
		modelMap.addAttribute("retrievedProduct", retrievedProduct);
		logger.info("Successfully retrieved Product from DB - " + retrievedProduct.size() + "::: WEB ::: CeramicsProductController");
		logger.info("------------------------->>>" + productData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productData.getIndustryCategory() + "/firmTabPages/ownerProductTab :: ProductTabProductListFragment";
		return returnChunk;
	}

}
