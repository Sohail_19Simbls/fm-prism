package com.simbls.prism.web.central.model;

public class ProductMaster {
	private long productMasterID;
	private String productMasterCompany;
	private String productMasterProduct;
	private String productMasterCategory;
	private String productMasterType;
	private String productMasterBarcode;
	private String productMasterMrp;
	private String productMasterTax;
	private String productMasterCess;
	private String productMasterQuantity;
	private String productMasterUnit;
	private String productMasterUpdatedBy;
	private String productMasterUpdatedDate;
	private String productMasterRowstate;
	
	private ProductMaster productMasterObject;

	public long getProductMasterID() {
		return productMasterID;
	}

	public void setProductMasterID(long productMasterID) {
		this.productMasterID = productMasterID;
	}

	public String getProductMasterCompany() {
		return productMasterCompany;
	}

	public void setProductMasterCompany(String productMasterCompany) {
		this.productMasterCompany = productMasterCompany;
	}

	public String getProductMasterProduct() {
		return productMasterProduct;
	}

	public void setProductMasterProduct(String productMasterProduct) {
		this.productMasterProduct = productMasterProduct;
	}

	public String getProductMasterCategory() {
		return productMasterCategory;
	}

	public void setProductMasterCategory(String productMasterCategory) {
		this.productMasterCategory = productMasterCategory;
	}

	public String getProductMasterType() {
		return productMasterType;
	}

	public void setProductMasterType(String productMasterType) {
		this.productMasterType = productMasterType;
	}

	public String getProductMasterBarcode() {
		return productMasterBarcode;
	}

	public void setProductMasterBarcode(String productMasterBarcode) {
		this.productMasterBarcode = productMasterBarcode;
	}

	public String getProductMasterMrp() {
		return productMasterMrp;
	}

	public void setProductMasterMrp(String productMasterMrp) {
		this.productMasterMrp = productMasterMrp;
	}

	public String getProductMasterTax() {
		return productMasterTax;
	}

	public void setProductMasterTax(String productMasterTax) {
		this.productMasterTax = productMasterTax;
	}

	public String getProductMasterCess() {
		return productMasterCess;
	}

	public void setProductMasterCess(String productMasterCess) {
		this.productMasterCess = productMasterCess;
	}

	public String getProductMasterQuantity() {
		return productMasterQuantity;
	}

	public void setProductMasterQuantity(String productMasterQuantity) {
		this.productMasterQuantity = productMasterQuantity;
	}

	public String getProductMasterUnit() {
		return productMasterUnit;
	}

	public void setProductMasterUnit(String productMasterUnit) {
		this.productMasterUnit = productMasterUnit;
	}

	public String getProductMasterUpdatedBy() {
		return productMasterUpdatedBy;
	}

	public void setProductMasterUpdatedBy(String productMasterUpdatedBy) {
		this.productMasterUpdatedBy = productMasterUpdatedBy;
	}

	public String getProductMasterUpdatedDate() {
		return productMasterUpdatedDate;
	}

	public void setProductMasterUpdatedDate(String productMasterUpdatedDate) {
		this.productMasterUpdatedDate = productMasterUpdatedDate;
	}

	public String getProductMasterRowstate() {
		return productMasterRowstate;
	}

	public void setProductMasterRowstate(String productMasterRowstate) {
		this.productMasterRowstate = productMasterRowstate;
	}

	public ProductMaster getProductMasterObject() {
		return productMasterObject;
	}

	public void setProductMasterObject(ProductMaster productMasterObject) {
		this.productMasterObject = productMasterObject;
	}
	
	
}
