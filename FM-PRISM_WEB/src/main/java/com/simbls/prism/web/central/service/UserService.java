package com.simbls.prism.web.central.service;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.central.model.Login;
import com.simbls.prism.web.central.model.User;

@Component
public interface UserService {
	/**
	 * 
	 * @param login
	 * @return User Object
	 * @throws Exception
	 */
	public User validateCredentials(Login login) throws Exception;
	/***************************************************************/
	
	/**
	 * Reset user password for First Login
	 * @param login
	 * @return
	 * @throws Exception
	 */
	public User resetPasswordFirstLogin(Login login) throws Exception;
	

}