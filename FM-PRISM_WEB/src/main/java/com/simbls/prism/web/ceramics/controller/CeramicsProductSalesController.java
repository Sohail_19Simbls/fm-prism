package com.simbls.prism.web.ceramics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.web.ceramics.service.CeramicsProductSalesService;
import com.simbls.prism.web.common.DateTimeGeneratorService;
import com.simbls.prism.web.industry.model.ProductSales;
import com.simbls.prism.web.industry.model.SalesBasket;

@Controller
@RequestMapping(value = "/ceramics")
public class CeramicsProductSalesController {
	private static final Logger logger = Logger.getLogger(CeramicsProductSalesController.class);
	@Autowired private CeramicsProductSalesService ceramicsProductSaleService;
	@Autowired private DateTimeGeneratorService dateTimeGeneratorService; 
	public static String compactDate;
	public static String currentTime;
	
	/**
	 * 
	 * @param productSalesData
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/productSalesRegister", method = RequestMethod.POST)
	public String productSalesRegister(ProductSales productSalesData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + productSalesData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Product Sales Details Registration ::: WEB ::: CeramicsProductSaleController");
		compactDate = dateTimeGeneratorService.compactDate();
		productSalesData.setProductSalesEnteredDate(compactDate);
		productSalesData.setProductSoldQuantity("0");
		productSalesData.setProductSalesRowState(0);
		ProductSales productSalesRegisteredObject = ceramicsProductSaleService.productSalesRegister(productSalesData);
		modelMap.addAttribute("productSalesRegisteredObject", productSalesRegisteredObject);
		logger.info("Product Sales Details Successfully Registered ::: WEB ::: ProductSaleController");
		logger.info("------------------------->>>" + productSalesData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = productSalesData.getIndustryCategory() + "/firmTabPages/ownerProductTab :: ProductTabProductSalesDetailsFragment";
		return returnChunk;
	}
	
	/**
	 * Initiating Product Sales. When the Shop Owner scans barcode or types product to initiate sale
	 * This method is called.
	 * @param saleBasketData
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/productSalesBasket", method = RequestMethod.GET)
	public @ResponseBody SalesBasket productSalesBasket(SalesBasket salesBasketData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + salesBasketData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Product Sales. Sale Basket ::: WEB ::: CeramicsProductSaleController");
		SalesBasket customerPurchasedObject = ceramicsProductSaleService.productSalesBasket(salesBasketData);
		modelMap.addAttribute("customerPurchasedObject", customerPurchasedObject);
		logger.info("Product Sales Details Successfully Obtained into Sale Basket of Size - " + customerPurchasedObject 
					+ " for Bill No - " + customerPurchasedObject.getSaleBillNumber() + " ::: WEB ::: CeramicsProductSaleController");
		logger.info("------------------------->>>" + salesBasketData.getIndustryCategory() + "<<<-------------------------");
		return customerPurchasedObject;
	}
	
	/**
	 * Initiate population of Sale Bill and Sale Bill Consolidated Table.
	 * Compact Date and Time is obtained to ensure unique entry is populated in Consolidated table for each day.
	 * @param saleBasketData
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/generateSalesBasketBill", method = RequestMethod.POST)
	public String generateSalesBasketBill(@RequestBody List<SalesBasket> salesBasketData, ModelMap modelMap) throws Exception{
		logger.info("------------------------->>>" + salesBasketData.get(0).getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating population of Sale Bill and Sale Bill Consolidated Table. Customer Purchased items Bill Size - " + salesBasketData.size() + " ::: WEB ::: CeramicsProductSaleController");
		compactDate = dateTimeGeneratorService.compactDate();
		currentTime = dateTimeGeneratorService.compactTime();
		logger.info("Set Todays date into each Item. Compact Date - " + compactDate + ". Billing Date and Time - " + compactDate + " " + currentTime);	
		for ( SalesBasket soldProducts : salesBasketData){
			soldProducts.setSaleBillCompactDate(compactDate);
			soldProducts.setSaleBillIssuedDate(compactDate + " " + currentTime);
			logger.info("Bill Issued Date - " + soldProducts.getSaleBillIssuedDate());
		}
		
		List<SalesBasket> customerPurchasedObject = ceramicsProductSaleService.generateSalesBasketBill(salesBasketData);
		modelMap.addAttribute("customerPurchasedObject", customerPurchasedObject);
		logger.info("Product Sales Details Successfully Obtained into Sale Basket of Size -  ::: WEB ::: CeramicsProductSaleController");
		logger.info("------------------------->>>" + salesBasketData.get(0).getIndustryCategory() + "<<<-------------------------");
		String returnChunk = salesBasketData.get(0).getIndustryCategory() + "/firmTabPages/ownerSalesTab :: SalesTabHomeFragment";
		return returnChunk;
	}

}
