package com.simbls.prism.web.foodBazaar.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.industry.model.ProductPurchase;

@Component
public interface FoodBazaarProductPurchaseService {
	/**
	 * Register Product Purchase Details
	 * @param productPurchaseData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public ProductPurchase productPurchaseDetailsRegister(ProductPurchase productPurchaseData) throws Exception;
	
	/**
	 * Update Product Purchase Details
	 * @param productPurchaseData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public ProductPurchase productPurchaseDetailsUpdate(ProductPurchase productPurchaseData) throws Exception;
	
	/**
	 * Delete Product Purchase Details
	 * @param productPurchaseData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public void productPurchaseDetailsDelete(ProductPurchase productPurchaseData) throws Exception;
	
	/**
	 * Retrieves Product Purchase Details by Selected Product
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<ProductPurchase> retrieveProductPurchaseDetails(ProductPurchase productPurchaseData) throws Exception;
	
	/**
	 * Check if all the Purchased Product Total matches the Invoice Amount
	 * @param productPurchaseData
	 * @return
	 * @throws Exception
	 */
	public ProductPurchase checkInvoiceStatus(ProductPurchase productPurchaseData) throws Exception;
	

}
