package com.simbls.prism.web.foodBazaar.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.simbls.prism.web.common.DateTimeGeneratorService;
import com.simbls.prism.web.foodBazaar.service.FoodBazaarPaymentService;
import com.simbls.prism.web.industry.model.Payment;


@Controller
@RequestMapping(value = "/foodBazaar")
public class FoodBazaarPaymentController {
	private static final Logger logger = Logger.getLogger(FoodBazaarPaymentController.class);
	@Autowired private FoodBazaarPaymentService foodBazaarPaymentService;
	@Autowired private DateTimeGeneratorService dateTimeGeneratorService; 
	public static String compactDate;
	
	/**
	 * Register Payment from Owner Page
	 * @param paymentData
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/paymentRegister", method=RequestMethod.POST)
	public String paymentRegister(Payment paymentData, ModelMap map) throws Exception{
		logger.info("------------------------->>>" + paymentData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Payment Registration for InvoiceID - " + paymentData.getInvoiceID() 
				    + ". Target DB - " + paymentData.getFirmDBName() + "::: WEB ::: FoodBazaarPaymentController");
		compactDate = dateTimeGeneratorService.compactDate();
		paymentData.setPaymentEnteredDate(compactDate);
		foodBazaarPaymentService.paymentRegister(paymentData);
		logger.info("Successfully Registered Payment ::: WEB ::: FoodBazaarPaymentController");
		logger.info("------------------------->>>" + paymentData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = paymentData.getIndustryCategory() + "/firmTabPages/ownerInvoiceTab :: InvoiceTabInvoicePaymentFragment";
		return returnChunk;
	}
	
	
	/**
	 * Retrieves entire list of Payments registered.
	 * Payment Details are obtained against Invoice 
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveTodaysPaymentDetails", method=RequestMethod.GET)
	public String retrieveTodaysPaymentDetails(Payment paymentData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + paymentData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of today's Registered Payment Details ::: WEB ::: Payment Controller");
		Payment registeredInvoicePaymentList = foodBazaarPaymentService.retrieveTodaysPaymentDetails(paymentData);
		modelMap.addAttribute("registeredInvoicePaymentList", registeredInvoicePaymentList);
		logger.info("Successfully retrieved Payment Details from DB ::: WEB ::: Payment Controller");
		logger.info("------------------------->>>" + paymentData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = paymentData.getIndustryCategory() + "/firmLists/paymentList :: ViewPaymentListChunk";
		return returnChunk;
	}
	
	/**
	 * Retrieves entire list of Payments registered based on Dealer ID and Starting and Ending Date.
	 * Payment Details are obtained against Invoice 
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveConditionalPaymentDetails", method=RequestMethod.GET)
	public String retrievePaymentDetails(Payment paymentData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + paymentData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of all the Registered Payment Details ::: WEB ::: Payment Controller");
		Payment registeredInvoicePaymentList = foodBazaarPaymentService.retrieveConditionalPaymentDetails(paymentData);
		modelMap.addAttribute("registeredInvoicePaymentList", registeredInvoicePaymentList);
		logger.info("Successfully retrieved Payment Details from DB ::: WEB ::: Payment Controller");
		logger.info("------------------------->>>" + paymentData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = paymentData.getIndustryCategory() + "/firmLists/paymentList :: ViewConditionalPaymentListChunk";
		return returnChunk;
	}
	
	
}
