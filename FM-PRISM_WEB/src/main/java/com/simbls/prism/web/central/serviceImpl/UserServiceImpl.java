package com.simbls.prism.web.central.serviceImpl;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.central.model.Login;
import com.simbls.prism.web.central.model.User;
import com.simbls.prism.web.central.service.UserService;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.CentralRestUtil;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class UserServiceImpl implements UserService {

	@Autowired private CentralRestUtil centralRestUtil;
	@Inject RestClient restClient;
	private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

	/**
	 * Login Credential Validation - Directed from UserService.java
	 */
	public User validateCredentials(Login login) throws Exception {
		logger.info("Initiating Login Validation from User Service ::: WEB ::: User Service Impl For User " + login.getUserName());
		String url = centralRestUtil.getUrl() + CentralRestUtil.VALIDATE_CREDENTIALS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url, UtilHelper.getCommonHeader(), mapper.writeValueAsString(login));
		User userObject = mapper.readValue(httpResponse.getBody().toString(), User.class);
		logger.info("Redirecting to User Service ::: WEB ::: From User Service Impl");
		return userObject;
	}
	
	/**
	 * Reset Password for first login - Directed from UserService.java
	 */
	public User resetPasswordFirstLogin(Login resetPassword) throws Exception {
		logger.info("Continuing password reset for First Login ::: WEB ::: UserService Impl");
		String url = centralRestUtil.getUrl() + CentralRestUtil.RESET_USER_PASSWORD;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url, UtilHelper.getCommonHeader(), mapper.writeValueAsString(resetPassword));
		User userObject = mapper.readValue(httpResponse.getBody().toString(), User.class);
		logger.info("User Service successful for password reset ::: WEB ::: For the user - " + userObject.getUserName());
		return userObject;		
	}

}
