package com.simbls.prism.web.clothing.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.industry.model.ProductPurchase;

@Component
public interface ClothingProductPurchaseService {
	/**
	 * Register Product Purchase Details
	 * @param productPurchaseData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<ProductPurchase> productPurchaseDetailsRegister(List<ProductPurchase> productPurchaseData) throws Exception;
	
	/**
	 * Retrieves Product Purchase Details by Selected Product
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<ProductPurchase> retrieveProductPurchaseDetails(ProductPurchase productPurchaseData) throws Exception;
	
	/**
	 * Check if all the Purchased Product Total matches the Invoice Amount
	 * @param productPurchaseData
	 * @return
	 * @throws Exception
	 */
	public ProductPurchase checkInvoiceStatus(ProductPurchase productPurchaseData) throws Exception;
	

}
