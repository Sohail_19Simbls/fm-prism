package com.simbls.prism.web.industry.model;

import java.util.ArrayList;
import java.util.List;

public class ProductSales {
	private long productSalesID;
	private long productID;
	private long productPurchaseID;
	private long dealerID;
	private long invoiceID;
	private String productSalesPrice;
	private String productSalesTax;
	private String productSalesCess;
	private String productSalesDiscount;
	private String productSoldQuantity;
	private String productSalesEnteredBy;
	private String productSalesEnteredDate;
	private String productSalesUpdatedBy;
	private String productSalesUpdatedDate;
	private int productSalesRowState;
	/* ---------------------------------------- */
	/*	Additional Parameters */
	/* ---------------------------------------- */
	private String dealerName;
	private String invoiceNumber;
	
	/* ----------------------------- */
	private List<Product> productObject = new ArrayList<Product>();
	private List<ProductPurchase> productPurchaseObject = new ArrayList<ProductPurchase>();
	private List<ProductSales> productSalesObject = new ArrayList<ProductSales>();
	private List<Dealer> dealerObject = new ArrayList<Dealer>();
	private List<Invoice> invoiceObject = new ArrayList<Invoice>();
	
	/* ---------------------------------------- */
	/*	Parameters related to specific Industry and Firm	*/
	/* ---------------------------------------- */
	private String industryCategory;
	private String firmDBName;
	private String firmDBUserName;
	private String firmDBPassword;
	
	public long getProductSalesID() {
		return productSalesID;
	}
	public void setProductSalesID(long productSalesID) {
		this.productSalesID = productSalesID;
	}
	public long getProductID() {
		return productID;
	}
	public void setProductID(long productID) {
		this.productID = productID;
	}
	public long getProductPurchaseID() {
		return productPurchaseID;
	}
	public void setProductPurchaseID(long productPurchaseID) {
		this.productPurchaseID = productPurchaseID;
	}
	public long getDealerID() {
		return dealerID;
	}
	public void setDealerID(long dealerID) {
		this.dealerID = dealerID;
	}
	public long getInvoiceID() {
		return invoiceID;
	}
	public void setInvoiceID(long invoiceID) {
		this.invoiceID = invoiceID;
	}
	public String getProductSalesPrice() {
		return productSalesPrice;
	}
	public void setProductSalesPrice(String productSalesPrice) {
		this.productSalesPrice = productSalesPrice;
	}
	public String getProductSalesTax() {
		return productSalesTax;
	}
	public void setProductSalesTax(String productSalesTax) {
		this.productSalesTax = productSalesTax;
	}
	public String getProductSalesCess() {
		return productSalesCess;
	}
	public void setProductSalesCess(String productSalesCess) {
		this.productSalesCess = productSalesCess;
	}
	public String getProductSalesDiscount() {
		return productSalesDiscount;
	}
	public void setProductSalesDiscount(String productSalesDiscount) {
		this.productSalesDiscount = productSalesDiscount;
	}
	public String getProductSoldQuantity() {
		return productSoldQuantity;
	}
	public void setProductSoldQuantity(String productSoldQuantity) {
		this.productSoldQuantity = productSoldQuantity;
	}
	public String getProductSalesEnteredBy() {
		return productSalesEnteredBy;
	}
	public void setProductSalesEnteredBy(String productSalesEnteredBy) {
		this.productSalesEnteredBy = productSalesEnteredBy;
	}
	public String getProductSalesEnteredDate() {
		return productSalesEnteredDate;
	}
	public void setProductSalesEnteredDate(String productSalesEnteredDate) {
		this.productSalesEnteredDate = productSalesEnteredDate;
	}
	public String getProductSalesUpdatedBy() {
		return productSalesUpdatedBy;
	}
	public void setProductSalesUpdatedBy(String productSalesUpdatedBy) {
		this.productSalesUpdatedBy = productSalesUpdatedBy;
	}
	public String getProductSalesUpdatedDate() {
		return productSalesUpdatedDate;
	}
	public void setProductSalesUpdatedDate(String productSalesUpdatedDate) {
		this.productSalesUpdatedDate = productSalesUpdatedDate;
	}
	public int getProductSalesRowState() {
		return productSalesRowState;
	}
	public void setProductSalesRowState(int productSalesRowState) {
		this.productSalesRowState = productSalesRowState;
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public List<Product> getProductObject() {
		return productObject;
	}
	public void setProductObject(List<Product> productObject) {
		this.productObject = productObject;
	}
	public List<ProductPurchase> getProductPurchaseObject() {
		return productPurchaseObject;
	}
	public void setProductPurchaseObject(List<ProductPurchase> productPurchaseObject) {
		this.productPurchaseObject = productPurchaseObject;
	}
	public List<ProductSales> getProductSalesObject() {
		return productSalesObject;
	}
	public void setProductSalesObject(List<ProductSales> productSalesObject) {
		this.productSalesObject = productSalesObject;
	}
	public List<Dealer> getDealerObject() {
		return dealerObject;
	}
	public void setDealerObject(List<Dealer> dealerObject) {
		this.dealerObject = dealerObject;
	}
	public List<Invoice> getInvoiceObject() {
		return invoiceObject;
	}
	public void setInvoiceObject(List<Invoice> invoiceObject) {
		this.invoiceObject = invoiceObject;
	}
	public String getIndustryCategory() {
		return industryCategory;
	}
	public void setIndustryCategory(String industryCategory) {
		this.industryCategory = industryCategory;
	}
	public String getFirmDBName() {
		return firmDBName;
	}
	public void setFirmDBName(String firmDBName) {
		this.firmDBName = firmDBName;
	}
	public String getFirmDBUserName() {
		return firmDBUserName;
	}
	public void setFirmDBUserName(String firmDBUserName) {
		this.firmDBUserName = firmDBUserName;
	}
	public String getFirmDBPassword() {
		return firmDBPassword;
	}
	public void setFirmDBPassword(String firmDBPassword) {
		this.firmDBPassword = firmDBPassword;
	}
	
}
