package com.simbls.prism.web.clothing.serviceImpl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.clothing.service.ClothingPaymentService;
import com.simbls.prism.web.industry.model.Invoice;
import com.simbls.prism.web.industry.model.Payment;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.ClothingRestUtil;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class ClothingPaymentServiceImpl implements ClothingPaymentService {
	private static final Logger logger = Logger.getLogger(ClothingPaymentServiceImpl.class);
	@Autowired private ClothingRestUtil clothingRestUtil;
	@Inject RestClient restClient;
	
	
	/**
	 * 
	 */
	public void paymentRegister(Payment payment) throws Exception {
		logger.info("Initiating Payment Registration ::: WEB ::: Payment Service Impl");
		String url = clothingRestUtil.getUrl() + ClothingRestUtil.PAYMENT_REGISTER;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(payment));
		mapper.readValue(httpResponse.getBody().toString(), Payment.class);	
		logger.info("Successfully received control from REST ::: WEB ::: Payment Servie Impl");
	}

	
	/**
	 * 
	 */
	public Payment retrieveTodaysPaymentDetails(Payment paymentData) throws Exception {
		logger.info("Continuing retrival of all the payments Details ::: WEB ::: Payment Service Impl ");
		String url = clothingRestUtil.getUrl() + ClothingRestUtil.RETRIEVE_TODAYS_PAYMENT_DETAILS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(paymentData));
		Payment registeredInvoicePaymentList = mapper.readValue(httpResponse.getBody().toString(), Payment.class);
		logger.info("Obtained Payment Details from REST ::: WEB ::: CeramicsPaymentServiceImpl");
		return registeredInvoicePaymentList;
	}
	
	/**
	 * 
	 */
	public List<Invoice> retrievePaymentDetails(Invoice invoiceData) throws Exception {
		logger.info("Continuing retrival of all the payments Details ::: WEB ::: Payment Service Impl ");
		String url = clothingRestUtil.getUrl() + ClothingRestUtil.RETRIEVE_PAYMENT_DETAILS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(invoiceData));
		Invoice[] paymentDetails = mapper.readValue(httpResponse.getBody().toString(), Invoice[].class);
		List<Invoice> paymentDetailsList = Arrays.asList(paymentDetails);
		logger.info("Obtained Payment list of Size -" + paymentDetailsList.size() + " from REST ::: WEB ::: CeramicsPaymentServiceImpl");
		return paymentDetailsList;
	}

}
