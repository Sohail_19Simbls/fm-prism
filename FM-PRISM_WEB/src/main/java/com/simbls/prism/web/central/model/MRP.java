package com.simbls.prism.web.central.model;

public class MRP {

	private long mrpID;
	private String mrp;
	
	private MRP mrpObject;

	public long getMrpID() {
		return mrpID;
	}

	public void setMrpID(long mrpID) {
		this.mrpID = mrpID;
	}

	public String getMrp() {
		return mrp;
	}

	public void setMrp(String mrp) {
		this.mrp = mrp;
	}

	public MRP getMrpObject() {
		return mrpObject;
	}

	public void setMrpObject(MRP mrpObject) {
		this.mrpObject = mrpObject;
	}
	
	
}
