package com.simbls.prism.web.clothing.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.industry.model.ProductSales;
import com.simbls.prism.web.industry.model.SalesBasket;

@Component
public interface ClothingProductSalesService {
	/**
	 * Register Product Sales Details
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public ProductSales productSalesRegister(ProductSales productSalesData) throws Exception;
	
	/**
	 * Populate Sale Basket
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public SalesBasket productSalesBasket(SalesBasket salesBasketData) throws Exception;
	
	/**
	 * Initiate population of Sale Bill and Sale Bill Consolidated Table 
	 * @param productData
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<SalesBasket> generateSalesBasketBill(List<SalesBasket> salesBasketData) throws Exception;

}
