package com.simbls.prism.web.central.model;

public class Barcode {
	private long barcodeID;
	private String barcode;
	
	private Barcode barcodeObject;

	public long getBarcodeID() {
		return barcodeID;
	}

	public void setBarcodeID(long barcodeID) {
		this.barcodeID = barcodeID;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Barcode getBarcodeObject() {
		return barcodeObject;
	}

	public void setBarcodeObject(Barcode barcodeObject) {
		this.barcodeObject = barcodeObject;
	}
	
	
}
