package com.simbls.prism.web.central.model;

public class Bank {

	private long bankID;
	private String bankParentName;
	private String bankIFSC;
	private String bankMICR;
	private String bankName;
	private String bankAddress;
	private String bankCity;
	private String bankDistrict;
	private String bankState;
	private String bankContact;
	
	private Bank bankObject;

	public long getBankID() {
		return bankID;
	}

	public void setBankID(long bankID) {
		this.bankID = bankID;
	}

	public String getBankParentName() {
		return bankParentName;
	}

	public void setBankParentName(String bankParentName) {
		this.bankParentName = bankParentName;
	}

	public String getBankIFSC() {
		return bankIFSC;
	}

	public void setBankIFSC(String bankIFSC) {
		this.bankIFSC = bankIFSC;
	}

	public String getBankMICR() {
		return bankMICR;
	}

	public void setBankMICR(String bankMICR) {
		this.bankMICR = bankMICR;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public String getBankCity() {
		return bankCity;
	}

	public void setBankCity(String bankCity) {
		this.bankCity = bankCity;
	}

	public String getBankDistrict() {
		return bankDistrict;
	}

	public void setBankDistrict(String bankDistrict) {
		this.bankDistrict = bankDistrict;
	}

	public String getBankState() {
		return bankState;
	}

	public void setBankState(String bankState) {
		this.bankState = bankState;
	}

	public String getBankContact() {
		return bankContact;
	}

	public void setBankContact(String bankContact) {
		this.bankContact = bankContact;
	}

	public Bank getBankObject() {
		return bankObject;
	}

	public void setBankObject(Bank bankObject) {
		this.bankObject = bankObject;
	}
	
}
