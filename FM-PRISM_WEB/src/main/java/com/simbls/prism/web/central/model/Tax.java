package com.simbls.prism.web.central.model;

public class Tax {

	private int taxID;
	private String tax;
	
	private Tax taxObject;

	public int getTaxID() {
		return taxID;
	}

	public void setTaxID(int taxID) {
		this.taxID = taxID;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public Tax getTaxObject() {
		return taxObject;
	}

	public void setTaxObject(Tax taxObject) {
		this.taxObject = taxObject;
	}
	
	
}
