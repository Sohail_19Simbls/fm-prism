package com.simbls.prism.web.central.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.web.central.model.ProductCategory;
import com.simbls.prism.web.central.service.ProductCategoryService;

@Controller
public class ProductCategoryController {
	
	private static final Logger logger = Logger.getLogger(ProductCategoryController.class);
	@Autowired private ProductCategoryService productCategoryService;
	
	/**
	 * Auto Populate Product Categories wherever needed from Central DB
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	
	@RequestMapping(value = "/retrieveProductCategories", method=RequestMethod.GET)
	public @ResponseBody List<ProductCategory> retrieveProductCategories(ProductCategory productCategoryData, ModelMap map) throws Exception {
		logger.info("Initiating Auto Retrieval of Product Category from Central DB ::: WEB ::: ProductCategoryController");
		List<ProductCategory> productCategoryList = productCategoryService.retrieveProductCategories(productCategoryData);
		logger.info("Successfully populated Product Category List from Central DB ::: WEB ::: ProductCategoryController");
		return productCategoryList;
	}

}
