package com.simbls.prism.web.central.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.central.model.ProductCategory;

@Component
public interface ProductCategoryService {
	/**
	 * Retrieving Product Category List from Central DB
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<ProductCategory> retrieveProductCategories(ProductCategory productCategoryData) throws Exception;


}
