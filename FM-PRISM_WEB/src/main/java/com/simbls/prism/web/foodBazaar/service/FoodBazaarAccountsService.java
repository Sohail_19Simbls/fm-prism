package com.simbls.prism.web.foodBazaar.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.industry.model.Accounts;

@Component
public interface FoodBazaarAccountsService {
	/**
	 * 
	 * @param accountsData
	 * @return
	 * @throws Exception
	 */
	public List<Accounts> retrieveConsolidatedPurchaseDetails(Accounts accountsData) throws Exception;

	/**
	 * 
	 * @param accountsData
	 * @return
	 * @throws Exception
	 */
	public List<Accounts> retrieveConsolidatedSalesDetails(Accounts accountsData) throws Exception;

}