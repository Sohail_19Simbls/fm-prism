package com.simbls.prism.web.central.service;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.central.model.Firm;

@Component
public interface FirmService {
	/**
	 * Registration of New Firm
	 * @param login
	 * @return 
	 * @throws Exception
	 */
	public void firmRegister(Firm firmData) throws Exception;
	
}