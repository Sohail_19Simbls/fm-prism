package com.simbls.prism.web.util;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component("foodBazaarRestUtil")
public class FoodBazaarRestUtil {

	private static final Logger logger = Logger.getLogger(FoodBazaarRestUtil.class);
	
	@Value("${rest.client.connectionTimeoutMillis}")
	private int restClientConnectionTimeoutMillis;

	@Value("${rest.client.readTimeoutMillis}")
	private int restClientReadTimeoutMillis;

	@Value("${rest.client.maxConnectionsPerHost}")
	private int restClientMaxConnectionsPerHost;

	@Value("${rest.client.maxTotalConnections}")
	private int restClientMaxTotalConnections;

	@Value("${rest.fmprism.url}")
	private String url;

	private RestTemplate restTemplate;
	
	/**
	 * Firm Operations
	 */
	public static final String FIRM_REGISTER					=	"/firm/firmRegister";
	
	/**
	 * Login Validation - Validation of Credentials
	 */
	public static final String VALIDATE_CREDENTIALS				=	"/user/validateCredentials";
	public static final String RESET_USER_PASSWORD 				=	"/user/resetPasswordFirstLogin";
	
	/**
	 * Dealer Category Operations
	 */
	public static final String RETRIEVE_DEALER_CATEGORY			=	"/dealerCategory/retrieveDealerCategories";
	
	/**
	 * Dealer Operations
	 */
	public static final String DEALER_REGISTER					=	"/foodBazaar_dealer/dealerRegister";
	public static final String DEALER_UPDATE					=	"/foodBazaar_dealer/dealerUpdate";
	public static final String DEALER_DELETE					=	"/foodBazaar_dealer/dealerDelete";
	public static final String RETRIEVE_DEALER_LIST 			= 	"/foodBazaar_dealer/retrieveDealerList";
	public static final String RETRIEVE_DEALER_DETAILS			=	"/foodBazaar_dealer/retrieveDealerDetails";
	
	/**
	 * Invoice Operations
	 */
	public static final String INVOICE_REGISTER					=	"/foodBazaar_invoice/invoiceRegister";
	public static final String INVOICE_UPDATE					=	"/foodBazaar_invoice/invoiceUpdate";
	public static final String INVOICE_DELETE					=	"/foodBazaar_invoice/invoiceDelete";
	public static final String RETRIEVE_INVOICE_LIST			=	"/foodBazaar_invoice/retrieveInvoiceList";
	public static final String RETRIEVE_INVOICE_DETAILS			=	"/foodBazaar_invoice/retrieveInvoiceDetails";
	
	/**
	 * Payment Operations
	 */
	public static final String PAYMENT_REGISTER							=	"/foodBazaar_payment/paymentRegister";
	public static final String RETRIEVE_TODAYS_PAYMENT_DETAILS			=	"/foodBazaar_payment/retrieveTodaysPaymentDetails";
	public static final String RETRIEVE_CONDITIONAL_PAYMENT_DETAILS		=	"/foodBazaar_payment/retrieveConditionalPaymentDetails";
	
	/**
	 * Product Category Operations
	 */
	public static final String RETRIEVE_PRODUCT_CATEGORY 		=	"/productCategory/retrieveProductCategories";
	
	/**
	 * Product Sub Category Operations
	 */
	public static final String RETRIEVE_PRODUCT_SUB_CATEGORY 	=	"/productSubCategory/retrieveProductSubCategories";
	
	/**
	 * Discount Operations
	 */
	public static final String RETRIEVE_DISCOUNT_CATEGORY		=	"/discount/retrieveDiscountCategories";
	
	/**
	 * Product Operations
	 */
	public static final String PRODUCT_REGISTER							=	"/foodBazaar_product/productRegister";
	public static final String PRODUCT_UPDATE							=	"/foodBazaar_product/productUpdate";
	public static final String RETRIEVE_PRODUCT_BY_PARAMETER			=	"/foodBazaar_product/retrieveProductByParameter"; 
	public static final String RETRIEVE_PRODUCT_PURCHASE_SALES_DETAILS	= 	"/foodBazaar_product/retrieveProductPurchaseSaleDetails";
	public static final String PRODUCT_PURCHSE_DETAILS_REGISTER			=	"/foodBazaar_productPurchase/productPurchaseDetailsRegister";
	public static final String PRODUCT_PURCHSE_DETAILS_UPDATE			=	"/foodBazaar_productPurchase/productPurchaseDetailsUpdate";
	public static final String PRODUCT_PURCHSE_DETAILS_DELETE			=	"/foodBazaar_productPurchase/productPurchaseDetailsDelete";
	public static final String RETRIEVE_PRODUCT_PURCHSE_DETAILS			=	"/foodBazaar_productPurchase/retrieveProductPurchaseDetails";
	public static final String CHECK_INVOICE_STATUS						=	"/foodBazaar_productPurchase/checkInvoiceStatus";
	public static final String PRODUCT_SALES_REGISTER					=	"/foodBazaar_productSales/productSalesRegister";
	public static final String PRODUCT_SALES_UPDATE						=	"/foodBazaar_productSales/productSalesDetailsUpdate";
	public static final String PRODUCT_SALES_BASKET						=	"/foodBazaar_productSales/productSalesBasket";
	public static final String GENERATE_SALES_BASKET_BILL				=	"/foodBazaar_productSales/generateSalesBasketBill";
	public static final String RETRIEVE_TODAYS_SALES_DETAILS			=	"/foodBazaar_productSales/retrieveTodaysSalesDetails";
	public static final String RETRIEVE_CUSTOMISED_SALES_DETAILS		=	"/foodBazaar_productSales/retrieveCustomisedSalesDetails";
	
	
	/**
	 * Accounts Operations
	 */
	public static final String RETRIEVE_CONSOLIDATED_PURCHASE_DETAILS	=	"/foodBazaar_accounts/retrieveConsolidatedPurchaseDetails";
	public static final String RETRIEVE_CONSOLIDATED_SALES_DETAILS		=	"/foodBazaar_accounts/retrieveConsolidatedSalesDetails";
	
	/**
	 * Expense Category Operations
	 */
	public static final String RETRIEVE_EXPENSE_CATEGORY 	=	"/expenseCategory/retrieveExpenseCategories";
	
	/**
	 * Expense Sub Category Operations
	 */
	public static final String RETRIEVE_EXPENSE_SUB_CATEGORY=	"/expenseSubCategory/retrieveExpenseSubCategories";
	
	
	public static final String USER_REGISTER = "/user/register";
	public static final String FORGOT_PASSWORD = "/user/forgotpassword";
	public static final String RESET_USER_FORGOT_PASSWORD = "/user/resetForgotPassword";
	
	


	

	private HttpClient getHttpClient() {
		final PoolingClientConnectionManager connectionManager = new PoolingClientConnectionManager();
		connectionManager.setDefaultMaxPerRoute(restClientMaxConnectionsPerHost);
		connectionManager.setMaxTotal(restClientMaxTotalConnections);
		final DefaultHttpClient httpClient = new DefaultHttpClient(connectionManager);
		return httpClient;
	}

	private ClientHttpRequestFactory getClientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(getHttpClient());
		factory.setConnectTimeout(restClientConnectionTimeoutMillis);
		factory.setReadTimeout(restClientReadTimeoutMillis);

		return factory;

	}

	public RestTemplate getRestTemplate() {
		if (this.restTemplate == null) {
			restTemplate = new RestTemplate(getClientHttpRequestFactory());
			/* decorateRestTemplate(); */
		}
		return restTemplate;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		logger.info("Retrieving URL " + url + " ::: WEB ::: RESTUtil");
		return url;
	}
}
