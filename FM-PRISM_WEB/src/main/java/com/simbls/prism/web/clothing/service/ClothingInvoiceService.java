package com.simbls.prism.web.clothing.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.industry.model.Invoice;

@Component
public interface ClothingInvoiceService {
	/**
	 * Register Invoice details 
	 * @param invoice
	 * @param dbCredentials
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public void invoiceRegister(Invoice invoiceData) throws Exception;
	
	/**
	 * Update Invoice details 
	 * @param invoice
	 * @param dbCredentials
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public void invoiceUpdate(Invoice invoiceData) throws Exception;
	
	
	/**
	 * Retrieve all registered Invoices
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<Invoice> retrieveInvoiceList(Invoice invoiceData) throws Exception;
	
	/**
	 * 	Retrieve Invoice Details Along with Products Purchased Against a Specific to Invoice
	 * By Invoice ID from Firm DB
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	List<Invoice> retrieveInvoiceDetails(Invoice invoiceData) throws Exception;

}