package com.simbls.prism.web.foodBazaar.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.industry.model.Dealer;

@Component
public interface FoodBazaarDealerService {
	/**
	 * Register Dealer details 
	 * @param dealer
	 * @param dbCredentials
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public Dealer dealerRegister(Dealer dealerData) throws Exception;
	
	/**
	 * Update Dealer details 
	 * @param dealer
	 * @param dbCredentials
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public Dealer dealerUpdate(Dealer dealerData) throws Exception;
	
	/**
	 * Delete Dealer details 
	 * @param dealer
	 * @param dbCredentials
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public void dealerDelete(Dealer dealerData) throws Exception;
	
	/**
	 * Retrieve all registered Dealers
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<Dealer> retrieveDealerList(Dealer dealerData) throws Exception;
	
	
	/**
	 * Retrieve all registered Dealers
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public Dealer retrieveDealerDetails(Dealer dealerData) throws Exception;
	

}