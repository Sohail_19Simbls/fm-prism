package com.simbls.prism.web.clothing.serviceImpl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.clothing.service.ClothingDealerService;
import com.simbls.prism.web.industry.model.Dealer;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.ClothingRestUtil;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class ClothingDealerServiceImpl implements ClothingDealerService {
	private static final Logger logger = Logger.getLogger(ClothingDealerServiceImpl.class);
	@Autowired private ClothingRestUtil clothingRestUtil;
	@Inject RestClient restClient;
	
	
	/**
	 * 
	 */
	public Dealer dealerRegister(Dealer dealer) throws Exception {
		logger.info("Continuing Dealer Registration ::: WEB ::: Dealer Service Impl");
		String url = clothingRestUtil.getUrl() + ClothingRestUtil.DEALER_REGISTER;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(dealer));
		Dealer obtainedObject = mapper.readValue(httpResponse.getBody().toString(), Dealer.class);	
		logger.info("Successfully received control from REST ::: WEB ::: Dealer Servie Impl");
		return obtainedObject;
	}
	
	/**
	 * 
	 */
	public Dealer dealerUpdate(Dealer dealer) throws Exception {
		logger.info("Continuing Dealer Updation ::: WEB ::: Dealer Service Impl");
		String url = clothingRestUtil.getUrl() + ClothingRestUtil.DEALER_UPDATE;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(dealer));
		Dealer obtainedObject = mapper.readValue(httpResponse.getBody().toString(), Dealer.class);	
		logger.info("Successfully received control from REST ::: WEB ::: Dealer Servie Impl");
		return obtainedObject;
	}

	/**
	 * 
	 */
	public List<Dealer> retrieveDealerList(Dealer dealerData) throws Exception {
		logger.info("Initiating retrival of all the dealers ::: WEB ::: Dealer Service Impl ");
		String url = clothingRestUtil.getUrl() + ClothingRestUtil.RETRIEVE_DEALER_LIST;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(dealerData));
		Dealer[] dealers = mapper.readValue(httpResponse.getBody().toString(), Dealer[].class);
		List<Dealer> dealerList = Arrays.asList(dealers);
		logger.info("Obtained Dealer list of Size -" + dealerList.size() + " from REST ::: WEB ::: CeramicsDealerServiceImpl");
		return dealerList;
	}

	
	/**
	 * 
	 */
	public Dealer retrieveDealerDetails(Dealer dealerData) throws Exception {
		logger.info("Initiating retrival of all the dealers ::: WEB ::: Dealer Service Impl ");
		String url = clothingRestUtil.getUrl() + ClothingRestUtil.RETRIEVE_DEALER_DETAILS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(dealerData));
		Dealer retrieveDealerDetails = mapper.readValue(httpResponse.getBody().toString(), Dealer.class);
		logger.info("Obtained Dealer details from REST ::: WEB ::: CeramicsDealerServiceImpl");
		return retrieveDealerDetails;
	}
}
