package com.simbls.prism.web.central.serviceImpl;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.central.model.Firm;
import com.simbls.prism.web.central.service.FirmService;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.CentralRestUtil;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class FirmServiceImpl implements FirmService {

	@Autowired private CentralRestUtil centralRestUtil;
	@Inject RestClient restClient;
	private static final Logger logger = Logger.getLogger(FirmServiceImpl.class);


	/**
	 * Registration of New firm - Directed from UserService.java
	 */

	public void firmRegister(Firm firmData) throws Exception {
		logger.info("Continuing Firm Registration ::: WEB ::: Firm Service Imp");
		String url = centralRestUtil.getUrl() + CentralRestUtil.FIRM_REGISTER;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url, UtilHelper.getCommonHeader(), mapper.writeValueAsString(firmData));
		mapper.readValue(httpResponse.getBody().toString(), Firm.class);
		logger.info("Successfully received control from REST ::: WEB ::: Firm Servie Impl");
		
	}

}
