package com.simbls.prism.web.foodBazaar.serviceImpl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.foodBazaar.service.FoodBazaarProductPurchaseService;
import com.simbls.prism.web.industry.model.ProductPurchase;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.FoodBazaarRestUtil;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class FoodBazaarProductPurchaseServiceImpl implements FoodBazaarProductPurchaseService {
	private static final Logger logger = Logger.getLogger(FoodBazaarProductPurchaseServiceImpl.class);
	@Autowired private FoodBazaarRestUtil foodBazaarRestUtil;
	@Inject RestClient restClient;
	
	/**
	 * 
	 */
	public ProductPurchase productPurchaseDetailsRegister(ProductPurchase productPurchaseData) throws Exception {
		logger.info("Continuing Product Purchase Registration ::: WEB ::: FoodBazaarProductPurchaseServiceImpl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.PRODUCT_PURCHSE_DETAILS_REGISTER;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productPurchaseData));
		ProductPurchase registeredObject = mapper.readValue(httpResponse.getBody().toString(), ProductPurchase.class);
		logger.info("Successfully received control from REST ::: WEB ::: FoodBazaarProductPurchaseServiceImpl");
		return registeredObject;
	}
	
	

	/**
	 * 
	 */
	public ProductPurchase productPurchaseDetailsUpdate(ProductPurchase productPurchaseData) throws Exception {
		logger.info("Continuing Product Purchase Registration ::: WEB ::: FoodBazaarProductPurchaseServiceImpl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.PRODUCT_PURCHSE_DETAILS_UPDATE;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productPurchaseData));
		ProductPurchase updatedObject = mapper.readValue(httpResponse.getBody().toString(), ProductPurchase.class);
		logger.info("Successfully received control from REST ::: WEB ::: FoodBazaarProductPurchaseServiceImpl");
		return updatedObject;
	}
	
	/**
	 * 
	 */
	public void productPurchaseDetailsDelete(ProductPurchase productPurchaseData) throws Exception {
		logger.info("Continuing Product Purchase Registration ::: WEB ::: FoodBazaarProductPurchaseServiceImpl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.PRODUCT_PURCHSE_DETAILS_DELETE;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productPurchaseData));
		mapper.readValue(httpResponse.getBody().toString(), ProductPurchase.class);
		logger.info("Successfully received control from REST ::: WEB ::: FoodBazaarProductPurchaseServiceImpl");
	}
	
	
	/**
	 * 
	 */
	public List<ProductPurchase> retrieveProductPurchaseDetails(ProductPurchase productPurchaseData) throws Exception {
		logger.info("Continuing retrival of all Product Purchase Details ::: WEB ::: FoodBazaarProductPurchaseServiceImpl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.RETRIEVE_PRODUCT_PURCHSE_DETAILS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productPurchaseData));
		ProductPurchase[] productPurchases = mapper.readValue(httpResponse.getBody().toString(), ProductPurchase[].class);
		List<ProductPurchase> productPurchaseList = Arrays.asList(productPurchases);
		logger.info("Obtained ProductPurchase details of Size -" + productPurchaseList.size() + " from REST ::: WEB ::: FoodBazaarProductPurchaseServiceImpl");
		return productPurchaseList;
	}

	
	/**
	 * 
	 */
	public ProductPurchase checkInvoiceStatus(ProductPurchase productPurchaseData) throws Exception {
		logger.info("Continuing retrival of all Product Purchase Details ::: WEB ::: FoodBazaarProductPurchaseServiceImpl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.CHECK_INVOICE_STATUS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productPurchaseData));
		ProductPurchase productPurchaseDetails = mapper.readValue(httpResponse.getBody().toString(), ProductPurchase.class);
		logger.info("Obtained ProductPurchase details of Size - from REST ::: WEB ::: FoodBazaarProductPurchaseServiceImpl");
		return productPurchaseDetails;
	}

}
