package com.simbls.prism.web.central.model;

import java.util.Date;

public class Firm {
	
	private long firmId;
	private String firmName;
	private String firmAbbrivation;
	private int firmIndustryCategory;
	private int firmApprovalStatus;
	private String firmGSTNumber;
	private String firmPANNumber;
	private String firmTINNumber;
	private String firmPropriterName;
	private String firmCategory;
	private String firmPrimaryContact;
	private String firmSecondaryContact;
	private String firmShopNumber;
	private String firmAddress;
	private String firmStreet;
	private String firmArea;
	private String firmCity;
	private String firmPincode;
	private String firmState;
	private String firmBankOne;
	private String firmAccountOne;
	private String firmBankTwo;
	private String firmAccountTwo;
	private String firmBankThree;
	private String firmAccountThree;
	private String firmAddedBy;
	private Date firmAddedDate;
	private String firmUpdatedBy;
	private Date firmUpdatedDate;
	private String firmApprovedBy;
	private Date firmApprovedDate;
	private Date firmExpiryDate;
	private Date firmDeletedDate;
	private String firmDBName;
	private String firmDBUserName;
	private String firmDBPassword;
	
	private int firmRowState;
	
	private Firm firmObject;

	private Industry industryObject;

	public long getFirmId() {
		return firmId;
	}

	public void setFirmId(long firmId) {
		this.firmId = firmId;
	}

	public String getFirmName() {
		return firmName;
	}

	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}

	public String getFirmAbbrivation() {
		return firmAbbrivation;
	}

	public void setFirmAbbrivation(String firmAbbrivation) {
		this.firmAbbrivation = firmAbbrivation;
	}

	public int getFirmIndustryCategory() {
		return firmIndustryCategory;
	}

	public void setFirmIndustryCategory(int firmIndustryCategory) {
		this.firmIndustryCategory = firmIndustryCategory;
	}

	public int getFirmApprovalStatus() {
		return firmApprovalStatus;
	}

	public void setFirmApprovalStatus(int firmApprovalStatus) {
		this.firmApprovalStatus = firmApprovalStatus;
	}

	public String getFirmGSTNumber() {
		return firmGSTNumber;
	}

	public void setFirmGSTNumber(String firmGSTNumber) {
		this.firmGSTNumber = firmGSTNumber;
	}

	public String getFirmPANNumber() {
		return firmPANNumber;
	}

	public void setFirmPANNumber(String firmPANNumber) {
		this.firmPANNumber = firmPANNumber;
	}

	public String getFirmTINNumber() {
		return firmTINNumber;
	}

	public void setFirmTINNumber(String firmTINNumber) {
		this.firmTINNumber = firmTINNumber;
	}

	public String getFirmPropriterName() {
		return firmPropriterName;
	}

	public void setFirmPropriterName(String firmPropriterName) {
		this.firmPropriterName = firmPropriterName;
	}

	public String getFirmCategory() {
		return firmCategory;
	}

	public void setFirmCategory(String firmCategory) {
		this.firmCategory = firmCategory;
	}

	public String getFirmPrimaryContact() {
		return firmPrimaryContact;
	}

	public void setFirmPrimaryContact(String firmPrimaryContact) {
		this.firmPrimaryContact = firmPrimaryContact;
	}

	public String getFirmSecondaryContact() {
		return firmSecondaryContact;
	}

	public void setFirmSecondaryContact(String firmSecondaryContact) {
		this.firmSecondaryContact = firmSecondaryContact;
	}

	public String getFirmShopNumber() {
		return firmShopNumber;
	}

	public void setFirmShopNumber(String firmShopNumber) {
		this.firmShopNumber = firmShopNumber;
	}

	public String getFirmAddress() {
		return firmAddress;
	}

	public void setFirmAddress(String firmAddress) {
		this.firmAddress = firmAddress;
	}

	public String getFirmStreet() {
		return firmStreet;
	}

	public void setFirmStreet(String firmStreet) {
		this.firmStreet = firmStreet;
	}

	public String getFirmArea() {
		return firmArea;
	}

	public void setFirmArea(String firmArea) {
		this.firmArea = firmArea;
	}

	public String getFirmCity() {
		return firmCity;
	}

	public void setFirmCity(String firmCity) {
		this.firmCity = firmCity;
	}

	public String getFirmPincode() {
		return firmPincode;
	}

	public void setFirmPincode(String firmPincode) {
		this.firmPincode = firmPincode;
	}

	public String getFirmState() {
		return firmState;
	}

	public void setFirmState(String firmState) {
		this.firmState = firmState;
	}

	public String getFirmBankOne() {
		return firmBankOne;
	}

	public void setFirmBankOne(String firmBankOne) {
		this.firmBankOne = firmBankOne;
	}

	public String getFirmAccountOne() {
		return firmAccountOne;
	}

	public void setFirmAccountOne(String firmAccountOne) {
		this.firmAccountOne = firmAccountOne;
	}

	public String getFirmBankTwo() {
		return firmBankTwo;
	}

	public void setFirmBankTwo(String firmBankTwo) {
		this.firmBankTwo = firmBankTwo;
	}

	public String getFirmAccountTwo() {
		return firmAccountTwo;
	}

	public void setFirmAccountTwo(String firmAccountTwo) {
		this.firmAccountTwo = firmAccountTwo;
	}

	public String getFirmBankThree() {
		return firmBankThree;
	}

	public void setFirmBankThree(String firmBankThree) {
		this.firmBankThree = firmBankThree;
	}

	public String getFirmAccountThree() {
		return firmAccountThree;
	}

	public void setFirmAccountThree(String firmAccountThree) {
		this.firmAccountThree = firmAccountThree;
	}

	public String getFirmAddedBy() {
		return firmAddedBy;
	}

	public void setFirmAddedBy(String firmAddedBy) {
		this.firmAddedBy = firmAddedBy;
	}

	public Date getFirmAddedDate() {
		return firmAddedDate;
	}

	public void setFirmAddedDate(Date firmAddedDate) {
		this.firmAddedDate = firmAddedDate;
	}

	public String getFirmUpdatedBy() {
		return firmUpdatedBy;
	}

	public void setFirmUpdatedBy(String firmUpdatedBy) {
		this.firmUpdatedBy = firmUpdatedBy;
	}

	public Date getFirmUpdatedDate() {
		return firmUpdatedDate;
	}

	public void setFirmUpdatedDate(Date firmUpdatedDate) {
		this.firmUpdatedDate = firmUpdatedDate;
	}

	public String getFirmApprovedBy() {
		return firmApprovedBy;
	}

	public void setFirmApprovedBy(String firmApprovedBy) {
		this.firmApprovedBy = firmApprovedBy;
	}

	public Date getFirmApprovedDate() {
		return firmApprovedDate;
	}

	public void setFirmApprovedDate(Date firmApprovedDate) {
		this.firmApprovedDate = firmApprovedDate;
	}

	public Date getFirmExpiryDate() {
		return firmExpiryDate;
	}

	public void setFirmExpiryDate(Date firmExpiryDate) {
		this.firmExpiryDate = firmExpiryDate;
	}

	public Date getFirmDeletedDate() {
		return firmDeletedDate;
	}

	public void setFirmDeletedDate(Date firmDeletedDate) {
		this.firmDeletedDate = firmDeletedDate;
	}

	public String getFirmDBName() {
		return firmDBName;
	}

	public void setFirmDBName(String firmDBName) {
		this.firmDBName = firmDBName;
	}

	public String getFirmDBUserName() {
		return firmDBUserName;
	}

	public void setFirmDBUserName(String firmDBUserName) {
		this.firmDBUserName = firmDBUserName;
	}

	public String getFirmDBPassword() {
		return firmDBPassword;
	}

	public void setFirmDBPassword(String firmDBPassword) {
		this.firmDBPassword = firmDBPassword;
	}

	public int getFirmRowState() {
		return firmRowState;
	}

	public void setFirmRowState(int firmRowState) {
		this.firmRowState = firmRowState;
	}

	public Firm getFirmObject() {
		return firmObject;
	}

	public void setFirmObject(Firm firmObject) {
		this.firmObject = firmObject;
	}

	public Industry getIndustryObject() {
		return industryObject;
	}

	public void setIndustryObject(Industry industryObject) {
		this.industryObject = industryObject;
	}	
}
