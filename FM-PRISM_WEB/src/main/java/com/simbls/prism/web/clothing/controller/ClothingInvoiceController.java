package com.simbls.prism.web.clothing.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simbls.prism.web.clothing.service.ClothingInvoiceService;
import com.simbls.prism.web.common.DateTimeGeneratorService;
import com.simbls.prism.web.industry.model.Invoice;


@Controller
@RequestMapping(value = "/clothing")
public class ClothingInvoiceController {
	private static final Logger logger = Logger.getLogger(ClothingInvoiceController.class);
	@Autowired private ClothingInvoiceService clothingInvoiceService;
	@Autowired private DateTimeGeneratorService dateTimeGeneratorService; 
	public static String compactDate;
	
	/**
	 * Register Invoice
	 * @param invoiceData
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/invoiceRegister", method=RequestMethod.POST)
	public String invoiceRegister(Invoice invoiceData, ModelMap map) throws Exception{
		logger.info("------------------------->>>" + invoiceData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Invoice Registration ::: WEB ::: CeramicsInvoiceController");
		compactDate = dateTimeGeneratorService.compactDate();
		invoiceData.setInvoiceEnteredDate(compactDate);
		clothingInvoiceService.invoiceRegister(invoiceData);
		logger.info("Successfully Registered Invoice ::: WEB ::: CeramicsInvoiceController");
		logger.info("------------------------->>>" + invoiceData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = invoiceData.getIndustryCategory() + "/firmTabPages/ownerInvoiceTab :: InvoiceTabAddInvoiceFragment";
		return returnChunk;
	}
	
	/**
	 * Update Invoice
	 * @param invoiceData
	 * @param map
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/invoiceUpdate", method=RequestMethod.POST)
	public String invoiceUpdate(Invoice invoiceData, ModelMap map) throws Exception{
		logger.info("------------------------->>>" + invoiceData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating Invoice Update ::: WEB ::: CeramicsInvoiceController");
		compactDate = dateTimeGeneratorService.compactDate();
		invoiceData.setInvoiceUpdatedDate(compactDate);
		clothingInvoiceService.invoiceUpdate(invoiceData);
		logger.info("Successfully Registered Invoice ::: WEB ::: CeramicsInvoiceController");
		logger.info("------------------------->>>" + invoiceData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = invoiceData.getIndustryCategory() + "/firmUpdateForms/updateInvoiceForm :: UpdateInvoiceSuccessModalChunk";
		return returnChunk;
	}
	
	/**
	 * Retrieve Invoice List from Firm DB
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveInvoiceList", method=RequestMethod.GET)
	public String retrieveInvoiceList(Invoice invoiceData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + invoiceData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of all the Registered Invoices ::: WEB ::: Invoice Controller");
		List<Invoice> registeredInvoiceList = clothingInvoiceService.retrieveInvoiceList(invoiceData);
		modelMap.addAttribute("registeredInvoiceList", registeredInvoiceList);
		logger.info("Successfully retrieved Invoice List from DB - " + invoiceData.getFirmDBName() + " ::: WEB ::: Invoice Controller");
		logger.info("------------------------->>>" + invoiceData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = invoiceData.getIndustryCategory() + "/firmTabPages/ownerInvoiceTab :: InvoiceTabInvoiceListFragment";
		return returnChunk;
	}
	
	/**
	 * Retrieve Invoice Details Along with Products Purchased Against a Specific to Invoice
	 * By Invoice ID from Firm DB
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveInvoiceDetails", method=RequestMethod.GET)
	public String retrieveInvoiceDetails(Invoice invoiceData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + invoiceData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of all the Products Purchased against InvoiceID - " + invoiceData.getInvoiceID() + " ::: WEB ::: Invoice Controller");
		List<Invoice> registeredInvoiceDetails = clothingInvoiceService.retrieveInvoiceDetails(invoiceData);
		modelMap.addAttribute("registeredInvoiceDetails", registeredInvoiceDetails);
		logger.info("Successfully retrieved Invoice List from DB - " + invoiceData.getFirmDBName() + " ::: WEB ::: Invoice Controller");
		logger.info("------------------------->>>" + invoiceData.getIndustryCategory() + "<<<-------------------------");
		String returnChunk = invoiceData.getIndustryCategory() + "/firmDetails/invoiceDetails :: ViewInvoiceDetailsTableChunk";
		return returnChunk;
	}
	
	/**
	 * Retrieve Invoice List from Firm DB for a particular Dealer
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	@RequestMapping(value="/retrieveDealerSpecificInvoiceList", method=RequestMethod.GET)
	public @ResponseBody List<Invoice> retrieveDealerSpecificInvoiceList(Invoice invoiceData, ModelMap modelMap) throws Exception {
		logger.info("------------------------->>>" + invoiceData.getIndustryCategory() + "<<<-------------------------");
		logger.info("Initiating retrival of all the Registered Invoices ::: WEB ::: Invoice Controller");
		List<Invoice> retrieveDealerSpecificInvoiceList = clothingInvoiceService.retrieveInvoiceList(invoiceData);
		modelMap.addAttribute("retrieveDealerSpecificInvoiceList", retrieveDealerSpecificInvoiceList);
		logger.info("Successfully retrieved Invoice List from DB - " + invoiceData.getFirmDBName() + " ::: WEB ::: Invoice Controller");
		logger.info("------------------------->>>" + invoiceData.getIndustryCategory() + "<<<-------------------------");
		return retrieveDealerSpecificInvoiceList;
	}
	
}
