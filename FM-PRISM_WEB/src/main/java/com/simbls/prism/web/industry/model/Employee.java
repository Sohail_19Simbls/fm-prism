package com.simbls.prism.web.industry.model;

public class Employee {
	private long employeeID;
	private String employeeName;
	private String employeeContactOne;
	private String employeeContactTwo;
	private String employeeHomeAddress;
	private String employeeAadharNumber;
	private String employeeJoiningDate;
	private String employeeSalary;
	private String employeeIncentive;
	private String employeeEnteredBy;
	private String employeeEnteredDate;
	private String employeeUpdatedBy;
	private String employeeUpdatedDate;
	private String employeeRowstate;

	private Employee employeeObject;

	public long getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(long employeeID) {
		this.employeeID = employeeID;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeContactOne() {
		return employeeContactOne;
	}

	public void setEmployeeContactOne(String employeeContactOne) {
		this.employeeContactOne = employeeContactOne;
	}

	public String getEmployeeContactTwo() {
		return employeeContactTwo;
	}

	public void setEmployeeContactTwo(String employeeContactTwo) {
		this.employeeContactTwo = employeeContactTwo;
	}

	public String getEmployeeHomeAddress() {
		return employeeHomeAddress;
	}

	public void setEmployeeHomeAddress(String employeeHomeAddress) {
		this.employeeHomeAddress = employeeHomeAddress;
	}

	public String getEmployeeAadharNumber() {
		return employeeAadharNumber;
	}

	public void setEmployeeAadharNumber(String employeeAadharNumber) {
		this.employeeAadharNumber = employeeAadharNumber;
	}

	public String getEmployeeJoiningDate() {
		return employeeJoiningDate;
	}

	public void setEmployeeJoiningDate(String employeeJoiningDate) {
		this.employeeJoiningDate = employeeJoiningDate;
	}

	public String getEmployeeSalary() {
		return employeeSalary;
	}

	public void setEmployeeSalary(String employeeSalary) {
		this.employeeSalary = employeeSalary;
	}

	public String getEmployeeIncentive() {
		return employeeIncentive;
	}

	public void setEmployeeIncentive(String employeeIncentive) {
		this.employeeIncentive = employeeIncentive;
	}
	
	public String getEmployeeEnteredBy() {
		return employeeEnteredBy;
	}

	public void setEmployeeEnteredBy(String employeeEnteredBy) {
		this.employeeEnteredBy = employeeEnteredBy;
	}

	public String getEmployeeEnteredDate() {
		return employeeEnteredDate;
	}

	public void setEmployeeEnteredDate(String employeeEnteredDate) {
		this.employeeEnteredDate = employeeEnteredDate;
	}

	public String getEmployeeUpdatedBy() {
		return employeeUpdatedBy;
	}

	public void setEmployeeUpdatedBy(String employeeUpdatedBy) {
		this.employeeUpdatedBy = employeeUpdatedBy;
	}

	public String getEmployeeUpdatedDate() {
		return employeeUpdatedDate;
	}

	public void setEmployeeUpdatedDate(String employeeUpdatedDate) {
		this.employeeUpdatedDate = employeeUpdatedDate;
	}

	public String getEmployeeRowstate() {
		return employeeRowstate;
	}

	public void setEmployeeRowstate(String employeeRowstate) {
		this.employeeRowstate = employeeRowstate;
	}

	public Employee getEmployeeObject() {
		return employeeObject;
	}

	public void setEmployeeObject(Employee employeeObject) {
		this.employeeObject = employeeObject;
	}
	
	
}
