package com.simbls.prism.web.util;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component("centralRestUtil")
public class CentralRestUtil {

	private static final Logger logger = Logger.getLogger(CentralRestUtil.class);
	
	@Value("${rest.client.connectionTimeoutMillis}")
	private int restClientConnectionTimeoutMillis;

	@Value("${rest.client.readTimeoutMillis}")
	private int restClientReadTimeoutMillis;

	@Value("${rest.client.maxConnectionsPerHost}")
	private int restClientMaxConnectionsPerHost;

	@Value("${rest.client.maxTotalConnections}")
	private int restClientMaxTotalConnections;

	@Value("${rest.fmprism.url}")
	private String url;

	private RestTemplate restTemplate;
	
	/**
	 * Firm Operations
	 */
	public static final String FIRM_REGISTER					=	"/firm/firmRegister";
	
	/**
	 * Login Validation - Validation of Credentials
	 */
	public static final String VALIDATE_CREDENTIALS				=	"/user/validateCredentials";
	public static final String RESET_USER_PASSWORD 				=	"/user/resetPasswordFirstLogin";
	
	/**
	 * Product Category Operations
	 */
	public static final String RETRIEVE_PRODUCT_CATEGORY 		=	"/productCategory/retrieveProductCategories";
	
	/**
	 * Product Sub Category Operations
	 */
	public static final String RETRIEVE_PRODUCT_SUB_CATEGORY 	=	"/productSubCategory/retrieveProductSubCategories";
	
	
	
	

	private HttpClient getHttpClient() {
		final PoolingClientConnectionManager connectionManager = new PoolingClientConnectionManager();
		connectionManager.setDefaultMaxPerRoute(restClientMaxConnectionsPerHost);
		connectionManager.setMaxTotal(restClientMaxTotalConnections);
		final DefaultHttpClient httpClient = new DefaultHttpClient(connectionManager);
		return httpClient;
	}

	private ClientHttpRequestFactory getClientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(getHttpClient());
		factory.setConnectTimeout(restClientConnectionTimeoutMillis);
		factory.setReadTimeout(restClientReadTimeoutMillis);

		return factory;

	}

	public RestTemplate getRestTemplate() {
		if (this.restTemplate == null) {
			restTemplate = new RestTemplate(getClientHttpRequestFactory());
			/* decorateRestTemplate(); */
		}
		return restTemplate;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		logger.info("Retrieving URL " + url + " ::: WEB ::: RESTUtil");
		return url;
	}
}
