package com.simbls.prism.web.central.model;

public class ExpenseSubCategory {
	private int expenseTypeID;
	private String expenseType;
	
	public int getExpenseTypeID() {
		return expenseTypeID;
	}
	public void setExpenseTypeID(int expenseTypeID) {
		this.expenseTypeID = expenseTypeID;
	}
	public String getExpenseType() {
		return expenseType;
	}
	public void setExpenseType(String expenseType) {
		this.expenseType = expenseType;
	}
	
}
