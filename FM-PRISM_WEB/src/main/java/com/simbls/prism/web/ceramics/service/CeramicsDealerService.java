package com.simbls.prism.web.ceramics.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simbls.prism.web.industry.model.Dealer;

@Component
public interface CeramicsDealerService {
	/**
	 * Register Dealer details 
	 * @param dealer
	 * @param dbCredentials
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public Dealer dealerRegister(Dealer dealerData) throws Exception;
	
	/**
	 * Update Dealer details 
	 * @param dealer
	 * @param dbCredentials
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public Dealer dealerUpdate(Dealer dealerData) throws Exception;
	
	/**
	 * Retrieve all registered Dealers
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public List<Dealer> retrieveDealerList(Dealer dealerData) throws Exception;
	
	
	/**
	 * Retrieve all registered Dealers
	 * @return
	 * @throws Exception
	 * @author Sohail Razvi
	 */
	public Dealer retrieveDealerDetails(Dealer dealerData) throws Exception;
	

}