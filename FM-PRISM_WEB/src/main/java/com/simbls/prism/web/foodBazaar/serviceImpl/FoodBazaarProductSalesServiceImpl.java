package com.simbls.prism.web.foodBazaar.serviceImpl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.foodBazaar.service.FoodBazaarProductSalesService;
import com.simbls.prism.web.industry.model.ProductSales;
import com.simbls.prism.web.industry.model.SalesBasket;
import com.simbls.prism.web.util.FoodBazaarRestUtil;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class FoodBazaarProductSalesServiceImpl implements FoodBazaarProductSalesService {
	private static final Logger logger = Logger.getLogger(FoodBazaarProductSalesServiceImpl.class);
	@Autowired private FoodBazaarRestUtil foodBazaarRestUtil;
	@Inject RestClient restClient;
	
	/**
	 * 
	 */
	public List<ProductSales> productSalesRegister(List<ProductSales> productSalesData) throws Exception {
		logger.info("Continuing Product Sales Details Registration ::: WEB ::: Product Sales Service Impl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.PRODUCT_SALES_REGISTER;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productSalesData));
		ProductSales[] productSalesObject = mapper.readValue(httpResponse.getBody().toString(), ProductSales[].class);	
		List<ProductSales> productSalesList = Arrays.asList(productSalesObject);
		logger.info("Successfully received control from REST ::: WEB ::: Product Sales Service Impl");
		return productSalesList;
	}
	
	/**
	 * 
	 */
	public ProductSales productSalesDetailsUpdate(ProductSales productSalesData) throws Exception {
		logger.info("Continuing Product Sales Details Update ::: WEB ::: Product Sales Service Impl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.PRODUCT_SALES_UPDATE;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productSalesData));
		ProductSales productSalesObject = mapper.readValue(httpResponse.getBody().toString(), ProductSales.class);	
		logger.info("Successfully received control from REST ::: WEB ::: Product Sales Service Impl");
		return productSalesObject;
	}
	
	/**
	 * 
	 */
	public SalesBasket productSalesBasket(SalesBasket salesBasketData) throws Exception {
		logger.info("Continuing Product Sales being collected in Sale Basket ::: WEB ::: Product Sales Service Impl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.PRODUCT_SALES_BASKET;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(salesBasketData));
		SalesBasket obtainedSalesObject = mapper.readValue(httpResponse.getBody().toString(), SalesBasket.class);	
		logger.info("Successfully received control from REST ::: WEB ::: Product Sales Service Impl");
		return obtainedSalesObject;
	}
	
	
	/**
	 * 
	 */
	public List<SalesBasket> generateSalesBasketBill(List<SalesBasket> salesBasketData) throws Exception {
		logger.info("Continuing Product Sales being collected in Sale Basket ::: WEB ::: Product Sales Service Impl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.GENERATE_SALES_BASKET_BILL;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(salesBasketData));
		SalesBasket[] obtainedSalesObject = mapper.readValue(httpResponse.getBody().toString(), SalesBasket[].class);
		List<SalesBasket> obtainedSalesArray = Arrays.asList(obtainedSalesObject);
		logger.info("Successfully received control from REST ::: WEB ::: Product Sales Service Impl");
		return obtainedSalesArray;
	}
	
	/**
	 * 
	 */
	public SalesBasket retrieveTodaysSalesDetails(SalesBasket salesBasketData) throws Exception {
		logger.info("Continuing retrieval of todays sales ::: WEB ::: Product Sales Service Impl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.RETRIEVE_TODAYS_SALES_DETAILS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(salesBasketData));
		SalesBasket obtainedTodaysSalesDetails = mapper.readValue(httpResponse.getBody().toString(), SalesBasket.class);
		logger.info("Successfully received control from REST ::: WEB ::: Product Sales Service Impl");
		return obtainedTodaysSalesDetails;
	}
	
	/**
	 * 
	 */
	public SalesBasket retrieveCustomisedSalesDetails(SalesBasket salesBasketData) throws Exception {
		logger.info("Continuing retrieval of Customised sales Details ::: WEB ::: Product Sales Service Impl");
		String url = foodBazaarRestUtil.getUrl() + FoodBazaarRestUtil.RETRIEVE_CUSTOMISED_SALES_DETAILS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(salesBasketData));
		SalesBasket obtainedCustomisedSalesDetails = mapper.readValue(httpResponse.getBody().toString(), SalesBasket.class);
		logger.info("Successfully received control from REST ::: WEB ::: Product Sales Service Impl");
		return obtainedCustomisedSalesDetails;
	}



}
