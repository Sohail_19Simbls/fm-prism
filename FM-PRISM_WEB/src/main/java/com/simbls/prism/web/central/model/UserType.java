package com.simbls.prism.web.central.model;

public class UserType {
	private int userTypeID;
	private String userType;
	
	private UserType userTypeObject;

	public int getUserTypeID() {
		return userTypeID;
	}

	public void setUserTypeID(int userTypeID) {
		this.userTypeID = userTypeID;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public UserType getUserTypeObject() {
		return userTypeObject;
	}

	public void setUserTypeObject(UserType userTypeObject) {
		this.userTypeObject = userTypeObject;
	}
	
}
