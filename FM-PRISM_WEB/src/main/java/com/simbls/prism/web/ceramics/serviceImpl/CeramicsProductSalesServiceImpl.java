package com.simbls.prism.web.ceramics.serviceImpl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.ceramics.service.CeramicsProductSalesService;
import com.simbls.prism.web.industry.model.ProductSales;
import com.simbls.prism.web.industry.model.SalesBasket;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.CeramicsRestUtil;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class CeramicsProductSalesServiceImpl implements CeramicsProductSalesService {
	private static final Logger logger = Logger.getLogger(CeramicsProductSalesServiceImpl.class);
	@Autowired private CeramicsRestUtil ceramicsRestUtil;
	@Inject RestClient restClient;
	
	/**
	 * 
	 */
	public ProductSales productSalesRegister(ProductSales productSalesData) throws Exception {
		logger.info("Continuing Product Sales Details Registration ::: WEB ::: CeramicsProductSalesServiceImpl");
		String url = ceramicsRestUtil.getUrl() + CeramicsRestUtil.PRODUCT_SALES_REGISTER;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productSalesData));
		ProductSales productSalesObject = mapper.readValue(httpResponse.getBody().toString(), ProductSales.class);	
		logger.info("Successfully received control from REST ::: WEB ::: CeramicsProductSalesServiceImpl");
		return productSalesObject;
	}
	
	/**
	 * 
	 */
	public SalesBasket productSalesBasket(SalesBasket salesBasketData) throws Exception {
		logger.info("Continuing Product Sales being collected in Sale Basket ::: WEB ::: CeramicsProductSalesServiceImpl");
		String url = ceramicsRestUtil.getUrl() + CeramicsRestUtil.PRODUCT_SALES_BASKET;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(salesBasketData));
		SalesBasket obtainedSalesObject = mapper.readValue(httpResponse.getBody().toString(), SalesBasket.class);	
		logger.info("Successfully received control from REST ::: WEB ::: CeramicsProductSalesServiceImpl");
		return obtainedSalesObject;
	}
	
	
	/**
	 * 
	 */
	public List<SalesBasket> generateSalesBasketBill(List<SalesBasket> salesBasketData) throws Exception {
		logger.info("Continuing Product Sales being collected in Sale Basket ::: WEB ::: CeramicsProductSalesServiceImpl");
		String url = ceramicsRestUtil.getUrl() + CeramicsRestUtil.GENERATE_SALES_BASKET_BILL;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(salesBasketData));
		SalesBasket[] obtainedSalesObject = mapper.readValue(httpResponse.getBody().toString(), SalesBasket[].class);
		List<SalesBasket> obtainedSalesArray = Arrays.asList(obtainedSalesObject);
		logger.info("Successfully received control from REST ::: WEB ::: CeramicsProductSalesServiceImpl");
		return obtainedSalesArray;
	}



}
