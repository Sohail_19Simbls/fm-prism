package com.simbls.prism.web.common;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class DateTimeGeneratorService {
	private static final Logger logger = Logger.getLogger(DateTimeGeneratorService.class);
	/**
	 * Obtain Current Date in dd-mm-YYYY format.
	 * @return
	 */
	public String compactDate(){
		logger.info("Initiating creation of Current Date ::: WEB ::: DateTimeGenerator");
		int day = GregorianCalendar.getInstance().get(Calendar.DAY_OF_MONTH);
		int month = GregorianCalendar.getInstance().get(Calendar.MONTH);
		int year = GregorianCalendar.getInstance().get(Calendar.YEAR);
		/* -------------------------------- */
		String formattedDay = null;
		if(day < 10){
			formattedDay = "0" + day;
		} else {
			formattedDay = String.valueOf(day);
		}
		/* -------------------------------- */
		String formattedMonth = null;
		if((month+1) < 10){
			formattedMonth = "0" + (month + 1);
		} else {
			formattedDay = String.valueOf(month + 1);
		}
		/* -------------------------------- */
		String currentDate = formattedDay + "-" + formattedMonth + "-" + year;
		logger.info("Returning the Current Compat Date -" + currentDate + " ::: WEB ::: DateTimeGenerator");
		return currentDate;
	}
	
	public String compactTime(){
		logger.info("Initiating creation of Current Time with AM_PM extension::: WEB ::: DateTimeGenerator");
		int hour = GregorianCalendar.getInstance().get(Calendar.HOUR);
		int min = GregorianCalendar.getInstance().get(Calendar.MINUTE);
		int sec = GregorianCalendar.getInstance().get(Calendar.SECOND);
		int am_pm = GregorianCalendar.getInstance().get(Calendar.AM_PM);
		
		//	Ensure that 0 hours is represented as 12
		String formattedHours = null;
		if(hour == 0){
			formattedHours = "12";
		} else{
			formattedHours = String.valueOf(hour);
		}
		
		//	Ensure that 0 is appended for all minutes between 0 to 9
		String formattedMinutes = null;
		if(min < 10){
			formattedMinutes = "0" + min;
		} else {
			formattedMinutes = String.valueOf(min);
		}
		
		//	Ensure that 0 is appended for all seconds between 0 to 9
		String formattedSeconds = null;
		if(sec < 10){
			formattedSeconds = "0" + sec;
		} else {
			formattedSeconds = String.valueOf(sec);
		}
		//	Ensure an AM_PM extension
		String AM_PM = null;
		if(am_pm == 0){
			AM_PM = "AM";
		} else if(am_pm == 1){
			AM_PM = "PM";
		}
		String currentTime = formattedHours + ":" + formattedMinutes + ":" + formattedSeconds + " " + AM_PM;
		logger.info("Returning the Current Compat Date -" + currentTime + " ::: WEB ::: DateTimeGenerator");
		return currentTime;
	}
	
	

	
	
	

}
