package com.simbls.prism.web.clothing.serviceImpl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.clothing.service.ClothingProductSalesService;
import com.simbls.prism.web.industry.model.ProductSales;
import com.simbls.prism.web.industry.model.SalesBasket;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.ClothingRestUtil;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class ClothingProductSalesServiceImpl implements ClothingProductSalesService {
	private static final Logger logger = Logger.getLogger(ClothingProductSalesServiceImpl.class);
	@Autowired private ClothingRestUtil clothingRestUtil;
	@Inject RestClient restClient;
	
	/**
	 * 
	 */
	public ProductSales productSalesRegister(ProductSales productSalesData) throws Exception {
		logger.info("Continuing Product Sales Details Registration ::: WEB ::: Product Sales Service Impl");
		String url = clothingRestUtil.getUrl() + ClothingRestUtil.PRODUCT_SALES_REGISTER;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productSalesData));
		ProductSales productSalesObject = mapper.readValue(httpResponse.getBody().toString(), ProductSales.class);	
		logger.info("Successfully received control from REST ::: WEB ::: Product Sales Service Impl");
		return productSalesObject;
	}
	
	/**
	 * 
	 */
	public SalesBasket productSalesBasket(SalesBasket salesBasketData) throws Exception {
		logger.info("Continuing Product Sales being collected in Sale Basket ::: WEB ::: Product Sales Service Impl");
		String url = clothingRestUtil.getUrl() + ClothingRestUtil.PRODUCT_SALES_BASKET;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(salesBasketData));
		SalesBasket obtainedSalesObject = mapper.readValue(httpResponse.getBody().toString(), SalesBasket.class);	
		logger.info("Successfully received control from REST ::: WEB ::: Product Sales Service Impl");
		return obtainedSalesObject;
	}
	
	
	/**
	 * 
	 */
	public List<SalesBasket> generateSalesBasketBill(List<SalesBasket> salesBasketData) throws Exception {
		logger.info("Continuing Product Sales being collected in Sale Basket ::: WEB ::: Product Sales Service Impl");
		String url = clothingRestUtil.getUrl() + ClothingRestUtil.GENERATE_SALES_BASKET_BILL;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(salesBasketData));
		SalesBasket[] obtainedSalesObject = mapper.readValue(httpResponse.getBody().toString(), SalesBasket[].class);
		List<SalesBasket> obtainedSalesArray = Arrays.asList(obtainedSalesObject);
		logger.info("Successfully received control from REST ::: WEB ::: Product Sales Service Impl");
		return obtainedSalesArray;
	}



}
