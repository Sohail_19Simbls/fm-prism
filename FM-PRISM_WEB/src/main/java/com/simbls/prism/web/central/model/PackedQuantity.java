package com.simbls.prism.web.central.model;

public class PackedQuantity {

	private long packedQuantityID;
	private String packedQuantity;
	
	private PackedQuantity packedQuantityObject;

	public long getPackedQuantityID() {
		return packedQuantityID;
	}

	public void setPackedQuantityID(long packedQuantityID) {
		this.packedQuantityID = packedQuantityID;
	}

	public String getPackedQuantity() {
		return packedQuantity;
	}

	public void setPackedQuantity(String packedQuantity) {
		this.packedQuantity = packedQuantity;
	}

	public PackedQuantity getPackedQuantityObject() {
		return packedQuantityObject;
	}

	public void setPackedQuantityObject(PackedQuantity packedQuantityObject) {
		this.packedQuantityObject = packedQuantityObject;
	}
	
}
