package com.simbls.prism.web.clothing.serviceImpl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.clothing.service.ClothingInvoiceService;
import com.simbls.prism.web.industry.model.Invoice;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.ClothingRestUtil;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class ClothingInvoiceServiceImpl implements ClothingInvoiceService {
	private static final Logger logger = Logger.getLogger(ClothingInvoiceServiceImpl.class);
	@Autowired private ClothingRestUtil clothingRestUtil;
	@Inject RestClient restClient;
	
	
	/**
	 * 
	 */
	public void invoiceRegister(Invoice invoiceData) throws Exception {
		logger.info("Continuing Invoice Registration ::: WEB ::: Invoice Service Impl");
		String url = clothingRestUtil.getUrl() + ClothingRestUtil.INVOICE_REGISTER;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(invoiceData));
		mapper.readValue(httpResponse.getBody().toString(), Invoice.class);	
		logger.info("Successfully received control from REST ::: WEB ::: Invoice Servie Impl");
	}
	
	/**
	 * 
	 */
	public void invoiceUpdate(Invoice invoiceData) throws Exception {
		logger.info("Continuing Invoice Updation ::: WEB ::: Invoice Service Impl");
		String url = clothingRestUtil.getUrl() + ClothingRestUtil.INVOICE_UPDATE;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(invoiceData));
		mapper.readValue(httpResponse.getBody().toString(), Invoice.class);	
		logger.info("Successfully received control from REST ::: WEB ::: Invoice Servie Impl");
	}

	/**
	 * 
	 */
	public List<Invoice> retrieveInvoiceList(Invoice invoiceData) throws Exception {
		logger.info("Continuing retrival of all the invoices ::: WEB ::: Invoice Service Impl ");
		String url = clothingRestUtil.getUrl() + ClothingRestUtil.RETRIEVE_INVOICE_LIST;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(invoiceData));
		Invoice[] invoices = mapper.readValue(httpResponse.getBody().toString(), Invoice[].class);
		List<Invoice> invoiceList = Arrays.asList(invoices);
		logger.info("Obtained Invoice list of Size -" + invoiceList.size() + " from REST ::: WEB ::: CeramicsInvoiceServiceImpl");
		return invoiceList;
	}
	
	/**
	 * 
	 */
	public List<Invoice> retrieveInvoiceDetails(Invoice invoiceData) throws Exception {
		logger.info("Continuing retrival of all the Products Purchased against invoiceID - " + invoiceData.getInvoiceID() + " ::: WEB ::: Invoice Service Impl ");
		String url = clothingRestUtil.getUrl() + ClothingRestUtil.RETRIEVE_INVOICE_DETAILS;
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(invoiceData));
		Invoice[] invoiceDetailsArray = mapper.readValue(httpResponse.getBody().toString(), Invoice[].class);
		List<Invoice> invoicePurchaseDetails = Arrays.asList(invoiceDetailsArray);
		logger.info("Obtained Invoice list of Size -" + invoicePurchaseDetails.size() + " from REST ::: WEB ::: CeramicsInvoiceServiceImpl");
		return invoicePurchaseDetails;
	}
	
}
