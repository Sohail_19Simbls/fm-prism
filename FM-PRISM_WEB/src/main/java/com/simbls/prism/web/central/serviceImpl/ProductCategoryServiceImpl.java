package com.simbls.prism.web.central.serviceImpl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbls.prism.web.central.model.ProductCategory;
import com.simbls.prism.web.central.service.ProductCategoryService;
import com.simbls.prism.web.util.RestClient;
import com.simbls.prism.web.util.CentralRestUtil;
import com.simbls.prism.web.util.UtilHelper;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService{
	
	private static final Logger logger = Logger.getLogger(ProductCategoryServiceImpl.class);
	@Autowired private CentralRestUtil centralRestUtil;
	@Inject	RestClient restClient;	
	
	/**
	 * Auto Retrieve Product Category List from Central DB 
	 */
	public List<ProductCategory> retrieveProductCategories(ProductCategory productCategoryData) throws Exception {
		logger.info("Continuing retrieval of Product Categories from Central DB ::: WEB ::: CeramicsProductServiceImpl");
		String url = centralRestUtil.getUrl() + CentralRestUtil.RETRIEVE_PRODUCT_CATEGORY;	
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> httpResponse = restClient.postDataJson(url,UtilHelper.getCommonHeader(), mapper.writeValueAsString(productCategoryData));
		ProductCategory[] productCategoryObject = mapper.readValue(httpResponse.getBody().toString(), ProductCategory[].class);
		List<ProductCategory> retrievedProductCategoryList = Arrays.asList(productCategoryObject);
		logger.info("Successfully obtained Product Category List from REST ::: WEB ::: CeramicsProductServiceImpl");
		return retrievedProductCategoryList;
	}

}
