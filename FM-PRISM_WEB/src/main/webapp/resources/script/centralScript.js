/*******************************************/
/*Following Function always loads the webpage from top on Refresh*/
/*******************************************/
$(document).ready(function () {
	window.scrollTo(0,0);
});


$(document).ready(function() {
	$(function() {
      $('[data-toggle="datepicker"]').datepicker({
        autoHide: true,
        zIndex: 2048,
        format: 'dd-mm-yyyy',
      });
    });
	
	/*
	 * Master Login AJAX Call
	 * Redirected to UserController.java
	 */
	$("#loginClick").click(function() {
		var loginData = $("#loginData").serialize();
		$.ajax({
			url				:			'validateCredentials',
			type			:			'POST',
			data			:			loginData,
			success			:			function(response) {
				$(window).scrollTop(0);
				$("#mainContentHolder").hide().html(response).fadeIn(500);
				$("#titleImage").attr("src", "https://s3.amazonaws.com/fmprism.root/customers/" + userObject.firmObject.firmAbbrivation + ".png");
			},
			error			:			function(xhr, textStatus, errorThrown){
				console.log("STATUS: " + textStatus + "\nERROE THROWN: " + errorThrown);
			}
		});
	});
	
	/*
	 * Master Logout AJAX Call
	 * Redirected to UserController.java
	 */
	
	$('#logout').click(function() {
		$.ajax({
			url 			:			'logout',
			type			:			'GET',
			success			:			function(response) {
				$('#ownerHome').hide().html(response).fadeIn(100);
			},
			error		:		function(xhr, textStatus, errorThrown){
				console.log("STATUS: " + textStatus + "\nERROE THROWN: " + errorThrown);
			}
		});
	});
	
});


function getCurrentDateTime(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1;
	var yyyy = today.getFullYear();
	var hr = (today.getHours() + 24) % 12 || 12;
	var min = today.getMinutes();
	var sec = today.getSeconds();

	if(dd<10) {
	    dd = '0'+dd
	} 

	if(mm<10) {
	    mm = '0'+mm
	} 

	return(dd + '/' + mm + '/' + yyyy + ' ' + hr + ':' + min + ':' + sec);
}



/**
 * Super Admin Ajax Calls. All these are redirected to Home controller.
 */

/**
 * The following Ajax calls are related to all the Admin calls related to Super Admin UserPage
 * Navigate to Super Admin User Tab
 */



$("#superAdminUserTab").click(function(){
	$.ajax({
		type		:		"GET",
		url			:		"superAdminUserTab",
		success		:		function(response){
			$("#superAdminDashBoardMainContainer").hide().html(response).fadeIn(100);
		},
		error		:		function(xhr, textStatus, errorThrown){
			console.log("STATUS: " + textStatus + "\nERROE THROWN: " + errorThrown);
		}
	});
});

/**
 * Navigate to Add User Sub Tab
 */

$("#addUserSubTab").click(function(){
	$.ajax({
		type		:		"GET",
		url			:		"addUserSubTab",
		success		:		function(response){
			$("#superAdminDashBoardMainContainer").hide().html(response).fadeIn(100);
		},
		error		:		function(xhr, textStatus, errorThrown){
			console.log("STATUS: " + textStatus + "\nERROE THROWN: " + errorThrown);
		}
	});
});
